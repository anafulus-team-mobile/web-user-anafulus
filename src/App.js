import React, { Component }             from 'react';
import Beranda                          from './Pages/Beranda';
import LoginSuccess                     from './Pages/LoginSuccess';
import AccountUser                      from './Pages/AccountUser';

import UserDashboard                    from './Pages/User-Dashboard';
import ChangeDataUser                   from './Component/ChangeUserData';
import ChangeAddress                    from './Component/ChangeUserAddress';
import ChangeAddData                    from './Component/ChangeAddDataUser';
import ChangePIN                        from './Component/ChangePIN'; 
import Notification                     from './Pages/Notification';

import AccountBankNull                  from './Component/AccountBank';
import AccountBankFill                  from './Component/AccountBankFill';
import DetailAccountBank                from './Component/DetailAccountBank';
import ChangeAccountBank                from './Component/ChangeBank';

import PinjamanMurabahah                from './Pages/PinjamanMurabahah';
import ApplyMurabahah                   from './Pages/Murabahah';
import PinjamanReguler                  from './Pages/PinjamanReguler';
import ApplyReguler                     from './Pages/Reguler';
import PinjamanBadanUsaha               from './Pages/PinjamanBadanUsaha';
import ApplyBadanUsaha                  from './Pages/BadanUsaha';
import ViaAgent                         from './Component/ViaAgent';
import AgentList                        from './Component/AgentList';
import ViaAgentReg                      from './Component/ViaAgentReg';
import AgentListReg                     from './Component/AgentListReg';
import ConfirmBorrow                    from './Pages/ConfirmBorrow';
import Approval                         from './Pages/Approval';

import Tentang                          from './Pages/Tentang';
import HubungiKami                      from './Pages/ContactUs';
import FAQ                              from './Pages/Faq';

import KebijakanPerusahaan              from './Pages/KebijakanPerusahaan';
import KebijakanPinjaman                from './Pages/KebijakanPinjaman';
import KebijakanPrivasi                 from './Pages/KebijakanPrivasi';
import TermCon                          from './Pages/TermCondition';
import RegulasiOJK                      from './Pages/RegulasiOJK';

import Masuk                            from './Component/Login';
import Daftar                           from './Component/Register';
import FormDaftar                       from './Component/FormSignup';
import FormOTP                          from './Component/FormOTP';
import GantiNomorPonsel                 from './Component/ChangePhoneNum';
import ResetPIN                         from './Component/ForgotPIN';
import VerificationPIN                  from './Component/VerificationPIN';

import AmountWithdraw                   from './Component/AmountWithdraw';
import ConfirmWithdraw                  from './Component/ConfirmWithdraw';
import ConfirmPinWithdraw               from './Pages/ConfirmPinWithdraw';
import WithdrawSuccess                  from './Pages/WithdrawSuccess.jsx';

import PaymentMethod                    from './Pages/PaymentMethod';
import PaymentDirect                    from './Component/PaymentDirect';

import HistoryEmpty                     from './Component/HistoryEmpty';
import HistoryPending                   from './Component/HistoryPending';
import DetailPending                    from './Component/DetailPending';
import HistoryApprove                   from './Component/HistoryApprove';
import DetailApprove                    from './Component/DetailApprove';
import HistoryDecline                   from './Component/HistoryDecline';
import FormDecline                      from './Component/FormDecline';
import DetailDecline                    from './Component/DetailDecline';
import HistoryDone                      from './Component/HistoryDone';
import DetailDone                       from './Component/DetailDone';
import HistoryOngoing                   from './Component/HistoryOngoing';
import DetailOngoing                    from './Component/DetailOngoing';
import HistoryCanceled                  from './Component/HistoryCanceled';
import DetailCanceled                   from './Component/DetailCanceled';
import HistoryOverdue                   from './Component/HistoryOverdue';
import DetailOverdue                    from './Component/DetailOverdue';

import ModalSucess                      from './Component/ModalSucessReg';

import { BrowserRouter, Route, 
         Switch }                       from 'react-router-dom';

class App extends Component {
  render(){
    return(
        <BrowserRouter>
          <Switch>
            <Route path="/"                       component={Beranda} exact />
            <Route path="/Login"                  component={Masuk}/>
            <Route path="/LoginSuccess"           component={LoginSuccess}/>
            <Route path="/AccountUser"            component={AccountUser}/>
            <Route path="/UserRegister"           component={Daftar}/>
            <Route path="/FormRegister"           component={FormDaftar}/>
            <Route path="/Form-OTP"               component={FormOTP}/>
            <Route path="/ChangeNumPhone"         component={GantiNomorPonsel}/>
            <Route path="/Reset"                  component={ResetPIN}/>
            <Route path="/Verify"                 component={VerificationPIN}/>

            <Route path="/Dashboard"              component={UserDashboard}/>
            <Route path="/ChangeUserData"         component={ChangeDataUser}/>
            <Route path="/ChangeAddress"          component={ChangeAddress}/>
            <Route path="/ChangeAddData"          component={ChangeAddData}/>
            <Route path="/AccountBank"            component={AccountBankNull}/>
            <Route path="/BankList"               component={AccountBankFill}/>
            <Route path="/DetailAccount"          component={DetailAccountBank}/>
            <Route path="/Edit-Bank"              component={ChangeAccountBank}/>
            <Route path="/PINChanger"             component={ChangePIN}/>
            <Route path="/Notif"                  component={Notification}/>

            <Route path="/Loan-Murabahah"         component={PinjamanMurabahah}/>
            <Route path="/Apply-Murabahah"        component={ApplyMurabahah}/>
            <Route path="/Loan-Reguler"           component={PinjamanReguler}/>
            <Route path="/Apply-Reguler"          component={ApplyReguler}/>
            <Route path="/Loan-Badan-Usaha"       component={PinjamanBadanUsaha}/>
            <Route path="/Form-Badan-Usaha"       component={ApplyBadanUsaha}/>
            <Route path="/Via-Agent"              component={ViaAgent}/>
            <Route path="/Via-Agent-Reg"          component={ViaAgentReg}/>
            <Route path="/Agen-List"              component={AgentList}/>
            <Route path="/Agen-List-Reg"          component={AgentListReg}/>
            <Route path="/Confirmation-PIN"       component={ConfirmBorrow}/>
            <Route path="/Approval"               component={Approval}/>

            <Route path="/Amount"                 component={AmountWithdraw}/>
            <Route path="/Confirm-Withdraw"       component={ConfirmWithdraw}/>
            <Route path="/ConfirmationWithdrawPIN"component={ConfirmPinWithdraw}/>
            <Route path="/Withdraw-Success"       component={WithdrawSuccess}/>
            
            <Route path="/History-Empty"          component={HistoryEmpty}/>
            <Route path="/History-Pending"        component={HistoryPending}/>
            <Route path="/History-Approve"        component={HistoryApprove}/>
            <Route path="/History-Done"           component={HistoryDone}/>
            <Route path="/History-Ongoing"        component={HistoryOngoing}/>
            <Route path="/History-Canceled"       component={HistoryCanceled}/>
            <Route path="/History-Decline"        component={HistoryDecline}/>
            <Route path="/History-Overdue"        component={HistoryOverdue}/>
            <Route path="/Form-Reason-Decline"    component={FormDecline}/>

            <Route path="/Detail-History-Pending" component={DetailPending}/>
            <Route path="/Detail-History-Approve" component={DetailApprove}/>
            <Route path="/Detail-History-Done"    component={DetailDone}/>
            <Route path="/Detail-History-Ongoing" component={DetailOngoing}/>
            <Route path="/Detail-History-Canceled"component={DetailCanceled}/>
            <Route path="/Detail-History-Decline" component={DetailDecline}/>
            <Route path="/Detail-History-Overdue" component={DetailOverdue}/>

            <Route path="/About"                  component={Tentang} />
            <Route path="/ContactUs"              component={HubungiKami}/>
            <Route path="/Faq"                    component={FAQ}/>
            <Route path="/Kebijakan-Perusahaan"   component={KebijakanPerusahaan}/>
            <Route path="/Kebijakan-Pinjaman"     component={KebijakanPinjaman}/>
            <Route path="/Kebijakan-Privasi"      component={KebijakanPrivasi}/>
            <Route path="/TermCon"                component={TermCon}/>
            <Route path="/RegOJK"                 component={RegulasiOJK}/>

            <Route path="/Payment"                component={PaymentMethod}/> 
            <Route path="/Direct-Payment"         component={PaymentDirect}/>

            <Route path="/Sucess"                 component={ModalSucess}/>
          </Switch>
        </BrowserRouter>
    );
  }
};

export default App;
