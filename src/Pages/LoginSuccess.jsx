import React                          from 'react';

import HeaderAccount                  from '../Component/HeaderAccount';
import Carousel                       from '../Component/Carousel';
import ContentLandingTop              from '../Component/ContentLandingTop';
import ContentLandingBottom           from '../Component/ContentLandingBottom';
import Footer                         from '../Component/Footer';

const LoginSuccess = () => {
    return(
      <div>
        <HeaderAccount/>
          <Carousel/>
            <ContentLandingTop/>
            <ContentLandingBottom/>
          <Footer/>
      </div>
    );
  };

export default LoginSuccess;
