import React, { Fragment }                              from 'react';
import HeaderAccount                                    from '../Component/HeaderAccount';
import ContentLandingBottom                             from '../Component/ContentLandingBottom';
import Footer                                           from '../Component/Footer';

import IcoCelebration2                                  from '../Icon/icon_penarikan_dana.png';

import { Link }                                         from 'react-router-dom';

import { Container, Col, Row, Button }                  from 'reactstrap';

import '../Css/Login.css';  
  

class PinjamanMurabahah extends React.Component {
    
    render(){
      return(
        <Fragment>
            <HeaderAccount/>
                    <Container>
                      <Row>
                       <Col>
                            <center>
                            <img src={IcoCelebration2} alt="congratulationIcon"/>
                            <h5>PENARIKAN DANA BERHASIL!</h5>
                            <p>Dana akan sampan ke rekening anda paling lambast 7 hari kerja.</p>
                            <Link to="/Dashboard"><Button id="btn-access">Kembali ke Dashboard</Button></Link>
                            </center>
                       </Col>
                      </Row>
                    </Container>
                <ContentLandingBottom/>
            <Footer/>
        </Fragment>
      );
      
    }
    
  };
  
  export default PinjamanMurabahah;

  
      