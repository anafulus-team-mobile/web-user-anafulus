import React, { Fragment }                              from 'react';
import HeaderAccount                                    from '../Component/HeaderAccount';
import ContentLandingBottom                             from '../Component/ContentLandingBottom';
import Footer                                           from '../Component/Footer';

import { Link }                                         from 'react-router-dom';

import IcoCelebration                                   from '../Icon/celebration-anafulus.png';

import { Container, Col, Row, Button }                  from 'reactstrap';

import '../Css/Login.css';  
  

class Approval extends React.Component {
    render(){
      return(
        <Fragment>
            <HeaderAccount/>
              <center>
                <Container>
                  <Row>
                    <Col>
                      <img src={IcoCelebration} alt="congratulationIcon"/>
                      <h5>PENGAJUAN PINJAMAN BERHASIL!</h5>
                      <p>Anda sudah melengkapi data untuk pengajuan pinjaman. 
                      <br/>Proses pinjaman selanjutnya dapat dilihat pada  pada Beranda atau Riwayat.</p>
                    </Col>
                  </Row>
                      <Col><Link to="/ContactUs"><Button className="btn-green1">Hubungi Kami</Button></Link></Col> 
                      <br/>                
                      <Col><Link to="/"><Button className="btn-green1">Kembali ke Beranda</Button></Link></Col>
                </Container>
                </center>
                <ContentLandingBottom/>
            <Footer/>
        </Fragment>
      ); 
    }
  };
  
  export default Approval;

  
      