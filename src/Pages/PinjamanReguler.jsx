import React, { Fragment }                          from 'react';
import Header                                       from '../Component/Header';
import SimulationReguler                            from '../Component/SimulationReguler'
import ContentLandingTop                            from '../Component/ContentLandingTop';
import ContentLandingBottom                         from '../Component/ContentLandingBottom';
import Footer                                       from '../Component/Footer';

import BigBanner                                    from '../Images/image-2.jpg';


import { Container, Row, Col }                      from 'reactstrap';
import '../Css/PinjamanMurabahah.css';

class PinjamanReguler extends React.Component {
    render(){
    return(
        <Fragment>
            <Header/>
                <Container fluid>
                    <Row>
                        <img src={BigBanner} className="img-responsive" alt="banner-loan-muarabahah" width="100%" height="730px"/>
                        <Col xs="5" className="banner-text">
                                <p className="text-banner1">Penuhi berbagai kebutuhan anda</p>
                                <p className="text-banner2">Lakukan peminjaman di Anafulus sebagai 
                                solusi berbagai kebutuhan anda.</p>
                        </Col>
                        <Col xs="2"></Col>
                        <SimulationReguler/>
                    </Row>
                </Container>
            <ContentLandingTop/>
            <ContentLandingBottom/>
        <Footer/>
      </Fragment>
    );
  };
}

export default PinjamanReguler;
