import React, { Fragment }                              from 'react';
import Header                                           from '../Component/Header';
import ContentLandingBottom                             from '../Component/ContentLandingBottom';
import Footer                                           from '../Component/Footer';

import { Link }                                         from 'react-router-dom';

import PhotoTeam                                        from '../Icon/manager-avatar@2x.png';
import LogoAnafulus                                     from '../Images/logo-tagline.png';

import { Container, Row, Col, 
        Button, Label}                    from 'reactstrap';
import '../Css/Style.css';

class Tentang extends React.Component {
    render() {
        return (
            <Fragment>
                <Header/>
                    <Container className="body-content">  
                        <Row>
                            <Col>
                                <h3 id="title-page">Tentang Anafulus</h3><hr/>
                            </Col>
                        </Row>
                        <img src={LogoAnafulus} width="400" height="300" class="mx-auto d-block" alt="logo-anafulus" />
                            <Col>
                                <h5>Tentang Perusahaan</h5>
                                <br/>
                                <p>
                                Berdiri pada tahun 2019, PT Anafulus adalah perusahaan teknologi finansial yang menciptakan
                                ekonomi modern dengan misi untuk membantu mempertemukan pelaku usaha yang
                                mengalami kesulitan modal usaha dengan para investor. Saat ini, Anafulus berkembang
                                menjadi fintech peer to peer lending yang memudahkan pinjaman basis online. Marketplace
                                lending pendanaan mikro dengan sistem Reguler, Murabahah (Syariah), dan Badan Usaha dengan target pasar Perorangan
                                dan Usaha Kecil Menengah dengan cakupan wilayah seluruh Indonesia.
                                Anafulus memberikan layanan finansial yang terpercaya, cepat, dan netral. Dengan misi 
                                memberdayakan ekonomi kreatif dengan kegiatan utama, yaitu sebagai penyedia layanan pinjam
                                meminjam uang secara modern basis teknologi infomasi. Oleh sebab itu, PT Anafulus dapat membantu pengusaha UKM
                                agar dapat menjalankan usaha dengan menyediakan permodalan yang dibutuhkan.
                                Anafulus memiliki tenaga kerja ahli dalam bidang financial dan teknologi yang bertekad
                                untuk terciptanya peningkatan ekonomi yang lebih baik lagi. Serta menjalin kerjasama
                                dengan mitra-mitra di Indonesia. Perusahaan kami senantiasa berusaha untuk menjamin kerahasiaan setiap data nasabah dan memperluas jangkauan hingga
                                dapat dengan mudah di akses ke seluruh Indonesia.
                                </p>
                            </Col>
                            <Col>
                                <h5>Visi & Misi</h5>
                                <br/>
                                <p>
                                <u>Visi:</u> <br/>
                                1. Menjadi perusahaan fintech yang terkemuka dan terpercaya.
                                <br />
                                <br />
                                <u>Misi:</u> <br/>
                                1. Memanfaatkan perkembangan teknologi informasi untuk berperan serta dalam meningkatkan ekonomi kreatif dan memberikan
                                pelayanan hingga ke seluruh Indonesia.
                                <br/>
                                2. Menyediakan layanan finansial platform yang dapat menjadi jembatan antara pelaku usaha dan investor permodalan. 
                                </p>
                            </Col>
                            <Col>
                            <h5>Nilai Perusahaan</h5>
                            <br/>
                            Integritas : Sikap yang konsisten dan teguh dalam menjunjung tinggi nilai nilai
                            luhur dan keyakinan.
                            <br />
                            Independensi : Sikap dan penilaian yang dimiliki atas dasar pertimbangan
                            dan pemikiran yang tidak dipengaruhi oleh kepentingan di pihak
                            manapun.
                            <br />
                            Pelayanan Prima : Suatu perilaku yang dilakukan dengan tujuan memberi
                            kepuasan kepada konsumen dan pemangku kepentingan.
                            <br />
                            Inovasi : Suatu tindakan yang dilakukan dalam rangka menciptakan produk
                            baru atau proses kerja atau hal baru lainnya yang akan memberi nilai lebih
                            bagi perusahaan.
                            <br />
                            Profesionalisme: Suatu sikap, perilaku dan pemikiran yang mencerminkan
                            pengetahuan dan kemampuan dalam menjalankan suatu profes
                            </Col>
                            <Col>
                                <h5>Tim Kami</h5>
                            </Col>
                        <Row>
                            <Col>
                                <center>
                                <img src={PhotoTeam} alt="icon-team1"/>
                            <Col>
                                <Label className="sh-name">Nama</Label>
                                <br />
                                <Label className="sh-position">Jabatan</Label>
                                </Col>
                                </center> 
                            </Col>
                            <Col>
                                <center>
                                <img src={PhotoTeam} alt="icon-team2"/>
                            <Col>
                                <Label className="sh-name">Nama</Label>
                                <br />
                                <Label className="sh-position">Jabatan</Label>
                                </Col>
                                </center> 
                            </Col>
                            <Col>
                                <center>
                                <img src={PhotoTeam} alt="icon-team3"/>
                            <Col>
                                <Label className="sh-name">Nama</Label>
                                <br />
                                <Label className="sh-position">Jabatan</Label>
                            </Col>
                                </center> 
                            </Col>
                        </Row>
                                <br/>
                                <br/>
                            <Col>
                                 <center><Link to="/ContactUs"><Button className="btn-green2">Hubungi Kami</Button></Link></center>
                            </Col>
                    </Container>
                    <ContentLandingBottom/> 
                <Footer/>           
            </Fragment>
        );
    }
}
export default Tentang;