// Component, Page imported //
import React, { Fragment }                    from 'react';
import HeaderAccount                          from '../Component/HeaderAccount';
import ContentLandingBottom                   from '../Component/ContentLandingBottom';
import Footer                                 from '../Component/Footer';

import { Link }                               from 'react-router-dom';

// Img, Icon,logo imported //

// Styling imported //
import { Container, Row, Col, Button,
         Label, Form, FormGroup}      from 'reactstrap';
import '../Css/Reuse.css';
import '../Css/UserDashboard.css';


class PinjamanReguler extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
          modal: false
        };

    
        this.toggle = this.toggle.bind(this);
      }
    
      toggle() {
        this.setState(prevState => ({
          modal: !prevState.modal
        }));
      }

    render() {
        return (
            <Fragment>
                <HeaderAccount/>
                    <Container className="container">
                        <h3>Pinjaman Reguler</h3><hr/>
                            <Row>
                                <Col sm="7" xs="12" className="col-dashboard-user">
                                    <Container className="container-wrap2"> 
                                        <Col xs={{offset:1}}>
                                            <Container>
                                                <Row>
                                                    <Col sm="5"><Label className="title-dashboard">Data Diri</Label></Col>
                                                    <Col sm="2"></Col>
                                                    <Col sm="4"><Link to="/ChangeUserData"><Button className="btn-yellow1">Ubah Data Diri</Button></Link></Col>
                                                </Row>
                                            </Container>
                                    <Container>
                                        <Row>
                                            <Col xs={{size:5 }}>
                                                <Label className="title-data">Nama</Label>
                                                <br/>
                                                <Col><p className="data-account">Jhon Doe</p></Col>
                                                <br/>
                                                <Label className="title-data">Nomor Ponsel</Label>
                                                <br/>
                                                <Col><p className="data-account">08110000033</p></Col>
                                                <br/>
                                                <Label className="title-data">Email</Label>
                                                <br/>
                                                <Col><p className="data-account">jhon@mail.com</p></Col>
                                                <br/>
                                                <Label className="title-data">Jenis Kelamin</Label>
                                                <br/>
                                                <Col><p className="data-account">Laki-laki</p></Col>
                                                <br/>
                                                <Label className="title-data">Tanggal Lahir</Label>
                                                <br/>
                                                <Col><p className="data-account">1 Januari 1990</p></Col>
                                                <br/>
                                                <Label className="title-data">Pendidikan</Label>
                                                <br/>
                                                <Col><p className="data-account">SMA</p></Col>                                                            
                                                </Col>
                                            
                                            <div style={{border:'solid 1px #bdbdbd'}}/>           
                                                <Col xs={{size:5, offset:1}}>
                                                <Label>Nomor KTP</Label>
                                                <br/>
                                                <Col><p className="data-account">123456789101</p></Col>
                                                <br/>
                                                <Label>Nomor Kartu Keluarga</Label>
                                                <br/>
                                                <Col><p className="data-account">123456789101</p></Col>
                                                <br/>
                                                <Label>Nomor NPWP</Label>
                                                <br/>
                                                <Col><p className="data-account">12346789101</p></Col>
                                                <br/>
                                                <Label>Gaji Terakhir / Pendapatan</Label>
                                                <br/>
                                                <Col><p className="data-account">Rp 5.000.000</p></Col>
                                                <br/>
                                                <Label>Pekerjaan</Label>
                                                <br/>
                                                <Col><p className="data-account">Wirausaha, Pemilik rumah makan</p></Col>
                                                </Col>
                                        </Row>
                                    </Container>
                                        </Col>
                                </Container>
                                <Container className="container-wrap2">
                                     <Col xs={{offset:1}}>
                                        <Container>
                                            <Row>
                                                <Col sm="5"><Label className="title-dashboard">Alamat</Label></Col>
                                                <Col sm="2"></Col>
                                                <Col sm="4"><Link to="/ChangeAddress"><Button className="btn-yellow1">Ubah Alamat</Button></Link></Col>
                                            </Row>
                                        </Container>
                                        <Container>
                                            <Row>
                                                <Col xs={{size:5}}>
                                                    <Label>Alamat: </Label>
                                                        <br/>
                                                        <p className="data-account">
                                                        Jl. Aster Nomor 23 Kelurahan Mawar Kecamatan Anggrek 
                                                        Kota Jakarta Timur Provinsi DKI Jakarta
                                                        78880                   
                                                        </p>                                                      
                                                </Col>
                                                <div style={{border:'solid 1px #bdbdbd'}}></div>        
                                                <Col xs={{size:5, offset:1}}>
                                                    <Label>Alamat: </Label>
                                                    <br/>
                                                    <p className="data-account">Jl. Abcde Nomor 20 Kelurahan Sukma Jaya Kecamatan Pangkalan Jati 
                                                    Kota Surabaya Provinsi Jawa Timur
                                                    78888                  
                                                    </p>
                                                </Col>
                                            </Row>
                                        </Container>
                                    </Col>
                                </Container>      
                                <Container className="container-wrap2">
                                    <Col xs={{offset:1}}>
                                        <Container>
                                            <Row>
                                                <Col sm="5"><Label className="title-dashboard">Data Persyaratan Pinjaman</Label></Col>
                                                <Col sm="2"></Col>
                                                <Col sm="4"><Link to="/ChangeAddData"><Button className="btn-yellow1">Ubah Data Persyaratan</Button></Link></Col>
                                                </Row>
                                        </Container>    
                                        <Container>
                                            <Row>
                                                <Col xs={{size:5}}>
                                                    <Label>Foto Pribadi dengan KTP</Label>
                                                    <br/>
                                                    <Col><p className="data-account">Sudah terunggah</p></Col>
                                                    <br/>
                                                    <Label>Foto KTP</Label>
                                                    <br/>
                                                    <Col><p className="data-account">Sudah terunggah</p></Col>
                                                    <br/>
                                                    <Label>Foto Kartu Keluarga</Label>
                                                    <br/>
                                                    <Col><p className="data-account">Sudah terunggah</p></Col>
                                                    </Col>
                                                    <div style={{border:'solid 1px #bdbdbd'}}>
                                                    </div> 
                                                    <Col xs={{size:5, offset:1}}>
                                                    <Label>Foto Usaha & Slip Gaji</Label>
                                                    <br/>
                                                    <Col><p className="data-account">Sudah terunggah</p></Col>
                                                    <br/>
                                                    <Label>Foto Rumah</Label>
                                                    <br/>
                                                    <Col><p className="data-account">Sudah terunggah</p></Col>
                                                </Col>
                                            </Row>  
                                                <Label className="title-dashboard">Data Keluarga / Kerabat  (Tidak serumah)</Label>
                                            <Row>
                                                    <Col xs={{size:5}}>
                                                        <Label>Nama Keluarga / Kerabat</Label>
                                                        <br/>
                                                        <Col><p className="data-account">Jhony</p></Col>
                                                        <br/>
                                                        <Label>Nomor Ponsel</Label>
                                                        <br/>
                                                        <Col><p className="data-account">08110000044</p></Col>
                                                    </Col>
                                                <div style={{border:'solid 1px #bdbdbd'}}></div> 
                                                    <Col xs={{size:5, offset:1}}>
                                                        <Label>Hubungan Kekeluargaan</Label>
                                                        <br/>
                                                        <Col><p className="data-account">Adik</p></Col>
                                                    </Col>
                                            </Row>
                                        </Container>
                                        </Col>
                                    </Container>
                                </Col>
                                    <Col sm={{size:'4', offset:'1'}} xs="12" >
                                        <Col className="apply-pinjaman-murabahah">
                                            <Form>
                                                <h5>Detail Pinjaman</h5>
                                                    <FormGroup>
                                                        <Label for="">Nominal Pinjaman</Label>
                                                        <p name="nominalPinjaman">Rp 5.000.000</p>
                                                        <FormGroup>
                                                            <Label for="tenorPinjman">Tenor Pinjaman</Label>
                                                            <p name="tenorPinjaman">16 Minggu</p>
                                                        </FormGroup>

                                                        <FormGroup>
                                                            <Label for="jenisCicilan">Jenis cicilan</Label>
                                                            <p name="jenisCicilan">Reguler</p>
                                                        </FormGroup>
                                                    </FormGroup>  
                                                    <FormGroup>
                                                        <Label id="Title-Cicilan">Cicilan:</Label> 
                                                        <p id="Total-Cicilan">Rp 250.000 / Minggu</p>
                                                    </FormGroup>
                                                <center><Link to="/Via-Agent-Reg"><Button className="btn-green1">Lanjutkan</Button></Link></center>                          
                                            </Form>
                                        </Col> 
                                    </Col>
                            </Row>
                    </Container>
                <ContentLandingBottom/>
            <Footer/>
            </Fragment>
        );
    }
}

export default PinjamanReguler;