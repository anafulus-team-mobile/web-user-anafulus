import React, { Fragment }                              from 'react';
import Header                                           from '../Component/Header';
import ContentLandingBottom                             from '../Component/ContentLandingBottom';
import Footer                                           from '../Component/Footer';

import { Link }                                         from 'react-router-dom';
import { Card, Button, CardHeader, CardBody,
         CardText, Container, Col, Row, 
         Input}                                         from 'reactstrap';

import '../Css/Login.css';  
  

class ConfirmBorrow extends React.Component {
    
    render(){
      return(
        <Fragment>
            <Header/>
                    <Container className="card-login">
                        <Col sm="12" md={{ size: 6, offset: 3 }}>
                            <Card>
                                <CardHeader>
                                    <h5>Masukkan PIN</h5>
                                </CardHeader>
                                <CardBody>
                                <Row>
                                    <Col>
                                        <CardText>Silahkan masukkan 6 digit PIN anda untuk verifikasi bahwa anda ingin melakukan pengajuan pinjaman murabahah</CardText>
                                        <Input type="number" id="input-box"/>
                                    </Col>
                                </Row>
                                <br/>
                                    <Col className="btn-card">
                                        <Link to="/Approval">
                                            <Button className="btn-green2">Konfirmasi</Button>
                                        </Link>
                                    </Col>
                                        <center><a href="/Reset">Lupa PIN?</a></center>
                                </CardBody> 
                            </Card>
                        </Col>
                    </Container>
                <ContentLandingBottom/>
            <Footer/>
        </Fragment>
      );
      
    }
    
  };
  
  export default ConfirmBorrow;

  
      