import React, { Fragment }                      from 'react';
import Header                                   from '../Component/Header';
import ContentLandingBottom                     from '../Component/ContentLandingBottom';
import Footer                                   from '../Component/Footer';

import Iframe                                   from 'react-iframe';

import { Container, Row, Col}                   from 'reactstrap';
import '../Css/Style.css'

class ContactUs extends React.Component {
    render() {
        return (
            <Fragment>
                    <Header/>
                        <Container className="body-content">  
                            <Row>
                                <Col>
                                    <h3 id="title-page">Hubungi Kami</h3><hr/>
                                </Col>
                            </Row>
                            <Row>
                                <Col sm="6">
                                    <h5>Jam Operasional</h5>
                                    <p>Senin - Jumat jam 08.00 - 17.00 WIB</p>

                                    <h5>Email</h5>
                                    <p>Anda dapat mengirimkan email ke : info@anafulus.co.id </p>
                                    
                                    <h5>Dukungan Layanan Pelanggan</h5>
                                    <p>Untuk respon lebih cepat Anda bisa menghubungi 
                                        Layanan Pelanggan kami di (021) 22903175-77. 
                                    <br/>
                                        Atau
                                    <br/>                                            
                                        Whatsapp di nomor (+62) 8123456789</p>
                                </Col>
                                <Col sm="6">
                                        <h5>Alamat</h5>
                                        <p>Gedung Kospin Jasa Lantai 6
                                        Jl. Jendral Gatot Subroto No.1 RT .01
                                        RW.01 Menteng Dalam, Tebet, 
                                        Jakarta Selatan DKI Jakarta 12870.</p>
                                    <Iframe url="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3966.1655864425716!2d106.83911131422028!3d-6.24189599548179!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69f6a8d64de5ff%3A0xeb3afaf4aee3d78e!2sKospin+Jasa!5e0!3m2!1sid!2sid!4v1559275037008!5m2!1sid!2sid" className="iframe-map embed-responsive" allowfullscreen/>
                                </Col>
                            </Row>
                        </Container>
                      <ContentLandingBottom/> 
                    <Footer/>           
                </Fragment>
        );
    }
};

export default ContactUs;