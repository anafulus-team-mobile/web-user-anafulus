// Component, Page imported //
import React, { Fragment }                    from 'react';
import HeaderAccount                          from '../Component/HeaderAccount';
import ContentLandingBottom                   from '../Component/ContentLandingBottom';
import Footer                                 from '../Component/Footer';

import { Link }                                 from 'react-router-dom';

// Styling imported //
import { Container, Row, Col, Button,
         Label, Form, FormGroup, Input }      from 'reactstrap';
import '../Css/Reuse.css';
import '../Css/UserDashboard.css';


class PinjamanBadanUsaha extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
          modal: false
        };

    
        this.toggle = this.toggle.bind(this);
      }
    
      toggle() {
        this.setState(prevState => ({
          modal: !prevState.modal
        }));
      }

    render() {
        return (
            <Fragment>
                <HeaderAccount/>
                    <Container className="container">
                        <h3>Formulir Peminjaman Badan Usaha</h3><hr/>
                            <Row>
                                <Col sm="7" xs="12" className="col-dashboard-user mx-auto">
                                    <Container className="container-wrap2"> 
                                        <Col xs={{size:12}}>
                                            <Container>
                                                <Form>
                                                    <Label>*Harus diisi</Label>
                                                    <FormGroup>
                                                        <Label for="namaPerusahaan">Nama Perusahaan*</Label>
                                                        <Input type="text" id="input-box"/>
                                                    </FormGroup>
                                                    <FormGroup>
                                                        <Label for="namaPIC">Nama PIC*</Label>
                                                        <Input type="text" id="input-box"/>
                                                    </FormGroup>
                                                    <FormGroup>
                                                        <Label for="nomorPonsel">Nomor Ponsel*</Label>
                                                        <Input type="num" id="input-box"/>
                                                    </FormGroup>
                                                    <FormGroup>
                                                        <Label>Email*</Label>
                                                        <Input type="text" id="input-box"/>
                                                    </FormGroup>
                                                    <br/>
                                                    <center><Link to="/Confirmation-PIN"><Button className="btn-green2">Lanjutkan</Button></Link></center>
                                                </Form>
                                            </Container>
                                        </Col>
                                    </Container>
                                </Col>
                            </Row>
                        </Container>
                <ContentLandingBottom/>
            <Footer/>
            </Fragment>
        );
    }
}

export default PinjamanBadanUsaha;