import React, { Fragment }            from 'react';
import Header                         from '../Component/Header';
import Carousel                       from '../Component/Carousel';
import ContentLandingTop              from '../Component/ContentLandingTop';
import ContentLandingBottom           from '../Component/ContentLandingBottom';
import Footer                         from '../Component/Footer';
import Sticky                         from 'react-sticky-el'

import '../Css/Header.css';


const Beranda = () => {
    return(
      <Fragment>
        <Sticky>
            <Header/>
        </Sticky>
        <Carousel/>
        <ContentLandingTop/>
        <ContentLandingBottom/>
        <Footer/>
      </Fragment>
    );
  };

export default Beranda;
