// Component, Page imported //
import React, { Fragment }                    from 'react';
import HeaderAccount                          from '../Component/HeaderAccount';
import SideBarProfile                         from '../Component/SidebarProfile';
import ContentLandingBottom                   from '../Component/ContentLandingBottom';
import Footer                                 from '../Component/Footer';

import { Link }                               from 'react-router-dom';

// Img, Icon,logo imported //
import EmptyInfo                              from '../Icon/money.png';
import IcoMurabahah                           from '../Icon/mosque-1.png';
import IcoReguler                             from '../Icon/funding.png';
import IcoBadanUsaha                          from '../Icon/store-1.png';

// import IcoProcess1                            from '../Icon/file.png';
import IcoProcess1                            from '../Icon/checked-1.png';
// import IcoProcess2                            from '../Icon/clipboard-with-a-list-1.png';
import IcoProcess2                            from '../Icon/minus-1.png';
import IcoProcess3                            from '../Icon/verification-of-delivery-list-clipboard-symbol.png';
import IcoProcess4                            from '../Icon/thumb-up-sign.png';
import IcoProcess5                            from '../Icon/money-1.png';

// Styling imported //
import { Container, Row, Col, Button, 
         Label, Modal, ModalHeader, 
         ModalBody}                          from 'reactstrap';
import '../Css/UserDashboard.css';


class UserDashboard extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
          modal: false
        };
    
        this.toggle = this.toggle.bind(this);
      }
    
      toggle() {
        this.setState(prevState => ({
          modal: !prevState.modal
        }));
      }

    render() {
        return (
            <Fragment>
                <HeaderAccount/>
                    <div>
                        <Container>
                            <Row>
                                {/* Component sidebar profile*/}
                                <Col sm="4">
                                    <SideBarProfile/>
                                </Col>
                                {/* end */}
                                    <Col sm="8" className="col-dashboard-user">
                                        <h3>Dashboard</h3><hr/>
                                            <Label className="title-dashboard">Pilihan Pinjaman</Label>
                                    <Container className="container-wrap1">
                                        <Fragment>
                                            <Row>
                                                <Col className="d-flex p-2">
                                                    <Link to="/Loan-Murabahah">
                                                        <Button className="btn-choose-installment" id="btn-murabahah">
                                                            <Row>
                                                                <Col xs="1"></Col>
                                                                    <Col xs="2"> <img src={IcoMurabahah} alt="ico-pinjaman-murabahah" className="ico-btn"/> </Col>
                                                                        <Col xs="1"></Col>
                                                                            <Col> <h6>Pinjaman Murabahah</h6> </Col>
                                                            </Row>
                                                        <h6 className="desc-btn-choose-installment">Ingin melakukan pinjaman syariah ? Pilih pinjaman Murabahah.</h6>
                                                        </Button>  
                                                    </Link>
                                                </Col>

                                                <Col className="d-flex p-2">
                                                    <Button className="btn-choose-installment" id="btn-reguler" 
                                                            onClick={this.toggle}>{this.props.buttonLabel}
                                                            <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
                                                                <ModalHeader toggle={this.toggle}>Pemberitahuan</ModalHeader>            
                                                                    <ModalBody>
                                                                        Mohon maaf, anda tidak dapat melakukkan 
                                                                        pinjaman karena saat ini anda sedang
                                                                        dalam proses peminjaman. 
                                                                    </ModalBody>
                                                            </Modal>
                                                        <Row>
                                                            <Col xs="1"></Col>
                                                                <Col xs="3"> <img src={IcoReguler} alt="ico-pinjaman-reguler" className="ico-btn"/> </Col>
                                                                    <Col><h6>Pinjaman Reguler</h6></Col>
                                                        </Row>
                                                    <h6 className="desc-btn-choose-installment">Butuh pinjaman ? Lakukan pengajuan Pinjaman Reguler. </h6>   
                                                    </Button>
                                                            </Col>

                                                <Col className="d-flex p-2">
                                                    <Link to="/Loan-Badan-Usaha">
                                                        <Button className="btn-choose-installment" id="btn-badan-usaha">
                                                            <Row>
                                                                <Col xs="1"></Col>
                                                                    <Col xs="3"> <img src={IcoBadanUsaha} alt="ico-pinjaman-badan-usaha" className="ico-btn"/> </Col>
                                                                        <Col> <h6>Pinjaman Badan Usaha</h6> </Col>
                                                            </Row>
                                                        <h6 className="desc-btn-choose-installment">Peminjaman untuk kebutuhan usaha anda.</h6>
                                                        </Button>
                                                    </Link>
                                                </Col>  
                                            </Row>
                                        </Fragment>
                                    </Container>
                                                
                                        <Label className="title-dashboard">Proses Pinjaman</Label>
                                            <div>
                                            <Container className="container-wrap1">
                                                <Row className="process-installment-step">
                                                    <Col sm="1"></Col>
                                                    <Col sm="2">
                                                        <img src={IcoProcess1} alt="file" className="ico-proses"/>  
                                                        <br/>      
                                                        <Label>Upload<br/>Berkas</Label>
                                                    </Col>
                                                    <Col sm="2">
                                                        <img src={IcoProcess2} alt="valid-data"/>
                                                        <br/>
                                                        <Label>Validasi<br/>Berkas</Label>
                                                    </Col>
                                                    <Col sm="2">
                                                        <img src={IcoProcess3} alt="verify"/>
                                                        <br/>
                                                        <Label>Verifikasi Berkas</Label>
                                                    </Col>
                                                    <Col sm="2">
                                                        <img src={IcoProcess4} alt="apply"/>
                                                        <br/>
                                                        <Label>Proses Disetujui</Label>
                                                    </Col>
                                                    <Col sm="2">
                                                        <img src={IcoProcess5} alt="finish"/>
                                                        <br/>
                                                        <Label>Dana<br/>Dicairkan </Label>
                                                    </Col>
                                                </Row>
                                                <Row style={{color:'#757575'}}>
                                                    <Col sm={{size:2, offset:1}}> 
                                                        <Label>Keterangan:</Label>
                                                    </Col> 
                                                    <Col sm="8" style={{textAlign:'justify'}}>
                                                        <p>Mohon maaf berkas anda tidak lengkap sehingga tidak dapat di validasi. 
                                                        Untuk  melakukan pinjaman silahkan lengkapi data diri anda terlebih dulu.
                                                        </p>
                                                    </Col>

                                                    <Col sm="5" className="mx-auto">    
                                                        <Link to="/AccountUser"><Button className="btn-green1 btn-block">Lengkapi Data Diri</Button></Link>{' '}
                                                        <Link to="/Amount"><Button className="btn-yellow1 btn-block">Tarik Dana</Button></Link>
                                                    </Col> 
                                                </Row>
                                            </Container>    
                                            </div>
                                            <Label className="title-dashboard">Informasi Cicilan</Label>
                                            <div>
                                            <Container className="container-wrap1">
                                                <Row>
                                                    <Col>
                                                        <img src={EmptyInfo} alt="empty-info" className="mx-auto"/>
                                                        <br/>
                                                    <Label>Anda belum pernah melakukan pinjaman di Anafulus.</Label> 
                                                    </Col>
                                                </Row>    
                                            </Container>
                                            </div>
                                            <div>
                                            <Container className="container-wrap1">
                                                <Row>
                                                    <Col xs="4">
                                                        <Label name="id-pinjaman">Id Pinjaman</Label>
                                                        <br/>
                                                        <Label name="id-pinjaman" className="user-amount">12XXX</Label>
                                                    </Col>
                                                    <Col xs="4">
                                                        <Label name="jum-cicilan">Jumlah Cicilan</Label>
                                                        <br/>
                                                        <Label name="jum-cicilan" className="user-amount">Rp 250.000</Label>
                                                        <br/>
                                                    </Col>
                                                    <Col xs="4">
                                                        <Label name="tempo-pinjaman">Tangal Jatuh Tempo</Label>
                                                        <br/>
                                                        <Label name="tempo-pinjaman" className="user-amount" style={{color:'#f1545a'}}>1 April 2019</Label>
                                                    </Col> 
                                                </Row>    
                                                <Row>                                                 
                                                    <Col sm={{size:2, offset:1}}>
                                                        <Label>Keterangan: </Label>
                                                    </Col>
                                                    <Col sm="8">
                                                        <Label style={{color:'#f1545a', textAlign:'justify'}}>
                                                        Anda sudah melewati jatuh tempo pembayaran. Silahkan  lakukan 
                                                        pembayaran dan denda keterlambatan anda.
                                                        </Label>
                                                    </Col>
                                                    <Col sm="5" className="mx-auto">
                                                        <Link to="/History-Overdue">
                                                            <Button className="btn-green1 btn-block">Rincian Pinjaman</Button>{' '}
                                                        </Link>
                                                        <Link to="/Payment">
                                                            <Button className="btn-yellow1 btn-block">Pembayaran</Button>
                                                        </Link>
                                                    </Col>
                                                </Row>              
                                            </Container>
                                            </div>
                                        </Col>
                                    </Row>
                                </Container>
                        </div>
                <ContentLandingBottom/>
            <Footer/>
            </Fragment>
        );
    }
}

export default UserDashboard;