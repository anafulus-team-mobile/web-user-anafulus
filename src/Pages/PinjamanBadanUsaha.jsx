import React, { Fragment }                      from 'react';
import Header                                   from '../Component/Header';
import ContentLandingTop                        from '../Component/ContentLandingTop';
import ContentLandingBottom                     from '../Component/ContentLandingBottom';
import Footer                                   from '../Component/Footer';

import { Link }                                 from 'react-router-dom';

import BigBanner                                from '../Images/image-4.jpg';

import { Container, Row, Col, Button }          from 'reactstrap';
import '../Css/PinjamanMurabahah.css';

const PinjamanBadanUsaha = () => {
    return(
      <Fragment>
        <Header/>
          <div>
            <Container fluid>
              <Row>
              <img src={BigBanner} alt="banner-loan-corporate" className="img-responsive" width="100%" height="700px"/>
                <Col sm="12" className="banner-text2">
                  <p className="text-banner1">Penuhi berbagai kebutuhan anda 
                  dengan keberkahan</p>
                  <p className="text-banner2">Lakukan peminjaman Murabahah di Anafulus
                  untuk berbagai keperluan anda. </p>
                </Col>
                <Col><center><Link to="/Form-Badan-Usaha"><Button id="btn-ajukan-badan-usaha"> Ajukan Pinjaman Badan Usaha</Button></Link></center></Col>
              </Row>
            </Container>
          </div>
          <ContentLandingTop/>
          <ContentLandingBottom/>
        <Footer/>
      </Fragment>
    );
  };

export default PinjamanBadanUsaha;
