// Component, Page imported //
import React, { Component, Fragment }           from 'react';
import HeaderAccount                            from '../Component/HeaderAccount';
import ContentLandingBottom                     from '../Component/ContentLandingBottom';
import Footer                                   from '../Component/Footer';

import { Link }                                 from 'react-router-dom';
// Img, Icon, Logo Imported //
import LogoBCA                                  from '../Logo/Bank/image-1.png'
import LogoCIMB                                 from '../Logo/Bank/image-10.png'
import LogoPermata                              from '../Logo/Bank/image-9.png'
import LogoKospin                               from '../Logo/Bank/image-12.png'
import LogoAlfa                                 from '../Logo/Bank/image-13.png'
import LogoIndomart                             from '../Logo/Bank/image-14.png'
import LogoPos                                  from '../Logo/Bank/image-11.png'

// Styling imported //
import { Container, Row, Col, Label, Card, CardBody,CardText, CardFooter, Button, UncontrolledCollapse}           from 'reactstrap';
import '../Css/UserDashboard.css';

class Payment extends Component{
    render() {
        return (
            <Fragment>
                <HeaderAccount/>
                    <div>
                        <Container>
                            <Row>   
                                <Col xs="1"></Col>
                                    <Col md="9" className="col-dashboard-user">
                                        <h3>Metode Pembayaran</h3>
                                            <hr/>
                                        <Col md="12" className="container-wrap1"> 
                                        <center><Label style={{fontWeight:'bold', fontSize:'18px'}}>Pilih Metode Pembayaran</Label></center>
                                        <Container>
                                            <hr style={{border: 'dashed 0.5px #bdbdbd'}}/>
                                            <Row>
                                                <Col>
                                                    <Card className="payment-choose">
                                                        <Button id="toggler" style={{backgroundColor:'#fff', border:'0'}}>
                                                            <CardBody>
                                                               <img src={LogoBCA} alt="logo-bca"/>
                                                                <img src={LogoCIMB} alt="logo-bca"/>
                                                            </CardBody>
                                                        </Button>
                                                    </Card>
                                                    <CardText>Bayar Melalui Virtual Account</CardText>
                                                </Col>
                                                
                                                <Col className="card-payment">
                                                    <Card className="payment-choose">
                                                        <Button style={{backgroundColor:'#fff', border:'0'}}>
                                                            <CardBody>
                                                                <img src={LogoAlfa} alt="logo-alfa"/>
                                                                <img src={LogoIndomart} alt="logo-indomart"/>
                                                            </CardBody>
                                                        </Button>
                                                    </Card>
                                                    <CardText>Bayar Melalui Merchant</CardText>
                                                </Col>
                                                <Col>
                                                    <Card className="payment-choose">
                                                        <Button style={{backgroundColor:'#fff', border:'0'}}>
                                                            <CardBody>
                                                                <img src={LogoKospin} alt="logo-kospin"/>
                                                                <img src={LogoPos} alt="logo-pos"/>
                                                            </CardBody>
                                                        </Button>
                                                    </Card>
                                                    <CardText>Bayar Melalui Partner</CardText>
                                                </Col>
                                            </Row>
                                            <hr style={{border: 'dashed 0.5px #bdbdbd'}}/>
                                            <UncontrolledCollapse toggler="#toggler">
                                            <Row>
                                                <Col>
                                                    <Card>
                                                        <CardBody>
                                                            <h5>Informasi Tagihan Pembayaran</h5>
                                                            <hr/>
                                                            <Row>
                                                                <Col>
                                                                    <Label>Jumlah Pembayaran</Label>
                                                                    <p>Rp 250.000</p>
                                                                </Col>
                                                                <Col>
                                                                    <Label>Nomor Kontrak</Label>
                                                                    <p>111xxxxxx</p>
                                                                </Col>
                                                                <Col>
                                                                    <Label>Nama Perusahaan</Label>
                                                                    <br/>
                                                                    <p>Anafulus</p>
                                                                </Col>
                                                            </Row>
                                                            <hr/>
                                                                    <h5>Cara Melakukan Pembayaran</h5>
                                                        </CardBody>
                                                        <CardFooter>
                                                            <Row>
                                                                <Col>
                                                                Virtual Account BCA
                                                                </Col>
                                                                <Col>
                                                                Virtual Account CIMB
                                                                </Col>
                                                            </Row>
                                                        </CardFooter>
                                                    </Card>
                                                </Col>
                                            </Row>
                                            <Button className="btn-yellow2">Proses Pembayaran</Button>
                                            <br/>
                                            <Link to="/ContactUs"><Button className="btn-green2">Hubungi Kami</Button></Link>
                                            </UncontrolledCollapse>
                                        </Container>
                                    </Col>   
                                </Col>
                            </Row>
                        </Container>    
                    </div>
                <ContentLandingBottom/>
            <Footer/>
        </Fragment>
        );
    }
}

export default Payment;