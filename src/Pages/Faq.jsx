import React, { Fragment }                      from 'react';
import Header                                   from '../Component/Header';
import ContentLandingBottom                     from '../Component/ContentLandingBottom';
import Footer                                   from '../Component/Footer';

// Styling imported
import classnames                               from 'classnames';

import { Container, Row, Col, TabContent, 
         TabPane, Nav, NavItem, NavLink, 
         Card,CardTitle, CardText}              from 'reactstrap';

class Faq extends React.Component {
  constructor(props) {
    super(props);
    this.toggle = this.toggle.bind(this);
    this.state = {
      activeTab: '0'
    };
  }

  toggle(tab) {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab
      });
    }
  }
  
render() {
  return (
    <Fragment>
      <Header/>
        <Container className="body-content">  
          <Row>
            <Col>
              <h3 id="title-page">Bantuan / FAQ</h3><hr/>
            </Col>
          </Row>
        <div>
          <Nav tabs style={{color:'#000', fontFamily:'Roboto'}}>
            <NavItem>
              <NavLink className={classnames({ active: this.state.activeTab === '0' })}
                       onClick={() => { this.toggle('0'); }}> Pinjaman Perorangan
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink className={classnames({ active: this.state.activeTab === '1' })}
                       onClick={() => { this.toggle('1'); }}> Kategori 1
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink className={classnames({ active: this.state.deactiveTab === '2' })}
                       onClick={() => { this.toggle('2'); }}> Kategori 2
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink className={classnames({ active: this.state.deactiveTab === '3' })}
                       onClick={() => { this.toggle('3'); }}> Kategori 3
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink className={classnames({ active: this.state.deactiveTab === '4' })}
                       onClick={() => { this.toggle('4'); }}> Kategori 4
              </NavLink>
             </NavItem>
            <NavItem>
              <NavLink className={classnames({ active: this.state.deactiveTab === '5' })}
                       onClick={() => { this.toggle('5'); }}> Kategori 5
              </NavLink>
            </NavItem>
          </Nav>
        <br/>
        
        <TabContent activeTab={this.state.activeTab}>
          <TabPane tabId="0">
            <Row>
              <Col sm="6">
                <Card body style={{border:'none'}}>
                  <CardTitle className="text-green">1. Question 1 ?</CardTitle>
                    <CardText>Answer : Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut 
                              labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut 
                              aliquip ex ea commodo consequat. 
                    </CardText>
                </Card>
              </Col>
              <Col sm="6">
                <Card body style={{border:'none'}}>
                  <CardTitle className="text-green">3. Question 3 ?</CardTitle>
                    <CardText>Answer : Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut 
                              abore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut 
                              aliquip ex ea commodo consequat. 
                    </CardText>
                </Card>
              </Col>
              <Col sm="6">
                <Card body style={{border:'none'}}>
                  <CardTitle className="text-green">2. Question 2 ?</CardTitle>
                    <CardText>Answer : Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut 
                              labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut 
                              aliquip ex ea commodo consequat. 
                    </CardText>
                </Card>
              </Col>
              <Col sm="6">
                <Card body style={{border:'none'}}>
                  <CardTitle className="text-green">4. Question 4 ?</CardTitle>
                    <CardText>Answer : Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut 
                              labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut 
                              aliquip ex ea commodo consequat. 
                    </CardText>
                </Card>
              </Col>
            </Row>
          </TabPane>
        <TabPane tabId="1">
            <Row>
              <Col sm="12">
                <h4>Lorem ipsum dolor sit amet</h4>
              </Col>
            </Row>
        </TabPane>
      </TabContent>
      </div>  
        </Container>
        <ContentLandingBottom/> 
      <Footer/>           
    </Fragment>
        );
    }
}
export default Faq;