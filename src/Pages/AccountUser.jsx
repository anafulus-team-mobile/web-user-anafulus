// Component, Page imported //
import React, { Fragment }                    from 'react';
import HeaderAccount                          from '../Component/HeaderAccount';
import ContentLandingBottom                   from '../Component/ContentLandingBottom';
import Footer                                 from '../Component/Footer';

import { Link }                               from 'react-router-dom';

// Img, Icon,logo imported //
import PhotoUser                              from '../Icon/user-1.png';

// Styling imported //
import { Container, Row, Col, Button, 
         UncontrolledCollapse, Label }        from 'reactstrap';
import '../Css/Reuse.css';
import '../Css/UserDashboard.css';


class AccountUser extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
          modal: false
        };

    
        this.toggle = this.toggle.bind(this);
      }
    
      toggle() {
        this.setState(prevState => ({
          modal: !prevState.modal
        }));
      }

    render() {
        return (
            <Fragment>
                <HeaderAccount/>
                    <Container className="container">
                        <Row>
                            <Col md="3" className="col-profile-user">
                                <div className="profile-user-top">
                                    <img src={PhotoUser} alt="user-profile"/>
                                        <h5>Jhon Doe</h5>  
                                        <Label>Penilaian Anda:</Label>
                                        <br/>
                                        <Label className="score-level">Baik</Label>
                                        <hr/>
                                        <Label className="font-weight-bold">Saldo</Label> 
                                        <br/>
                                        <Label className="user-amount">Rp 2.000.000</Label>
                                        <br/>
                                    <Link to="/Amount"><Button className="btn-green1">Tarik Dana</Button></Link>
                                </div>
                                <hr/>      
                        <Row className="dashboard-choose">
                            <Col>
                                <Link to="/Dashboard" className="dashboard-menu">Dashboard</Link>
                            </Col>
                        </Row>
                        <Row className="dashboard-choose">
                            <Col> 
                                <Link className="dashboard-menu" id="toggler">Akun Saya</Link>
                            </Col>

                        </Row>
                        <UncontrolledCollapse toggler="#toggler">
                                <Col className="dashboard-choose">
                                    <Col><Link to="/ChangeUserData"className="dashboard-menu">Ubah Data Diri</Link></Col>
                                </Col>
                                <Col className="dashboard-choose">
                                    <Col><Link to="/ChangeAddress" className="dashboard-menu">Ubah Alamat</Link></Col>
                                </Col>
                                <Col className="dashboard-choose">
                                    <Col><Link to="/ChangeAddData" className="dashboard-menu">Ubah Data Persyaratan</Link></Col>
                                </Col>
                                <Col className="dashboard-choose">
                                    <Col><Link to="/AccountBank" className="dashboard-menu">Rekening Bank</Link></Col>
                                </Col>
                                <Col className="dashboard-choose">
                                    <Col><Link to="/Reset" className="dashboard-menu">Ubah PIN</Link></Col>
                                </Col>
                            </UncontrolledCollapse>
                        <Row className="dashboard-choose">
                            <Col>
                                <Link className="dashboard-menu">Riwayat</Link>
                            </Col>
                        </Row>

                        <Row className="dashboard-choose">
                            <Col>
                                <Link to="/Notif" className="dashboard-menu">Notifikasi</Link>
                            </Col>
                        </Row>
                        <Row className="dashboard-choose">
                            <Col>
                                <Link to="/" style={{color:'#f1545a'}}>Keluar</Link>
                            </Col>
                        </Row>          
                            </Col>
                            
                            <Col md="1"></Col>
                                <Col md="8" className="col-dashboard-user">
                                    <h3>Akun Saya</h3><hr/>
                                        <Container className="container-wrap2"> 
                                                <Col xs={{offset:1}}>
                                                    <Container>
                                                        <Row>
                                                            <Col sm="5"><Label className="title-dashboard">Data Diri</Label></Col>
                                                            <Col sm="2"></Col>
                                                            <Col sm="4"><Link to="/ChangeUserData"><Button className="btn-yellow1">Ubah Data Diri</Button></Link></Col>
                                                        </Row>
                                                    </Container>
                                                    <Container>
                                                        <Row >
                                                            <Col xs={{size:5 }}>
                                                                <Label className="title-data">Nama</Label>
                                                                <br/>
                                                                <Col><p className="data-account">Jhon Doe</p></Col>
                                                                <br/>
                                                                <Label className="title-data">Nomor Ponsel</Label>
                                                                <br/>
                                                                <Col><p className="data-account">08110000033</p></Col>
                                                                <br/>
                                                                <Label className="title-data">Email</Label>
                                                                <br/>
                                                                <Col><p className="data-account">jhon@mail.com</p></Col>
                                                                <br/>
                                                                <Label className="title-data">Jenis Kelamin</Label>
                                                                <br/>
                                                                <Col><p className="data-account">Laki-laki</p></Col>
                                                                <br/>
                                                                <Label className="title-data">Tanggal Lahir</Label>
                                                                <br/>
                                                                <Col><p className="data-account">1 Januari 1990</p></Col>
                                                                <br/>
                                                                <Label className="title-data">Pendidikan</Label>
                                                                <br/>
                                                                <Col><p className="data-account">SMA</p></Col>                                                            
                                                            </Col>
                                                            <div style={{border:'solid 1px #bdbdbd'}}>
                                                            </div>        
                                                            <Col xs={{size:5, offset:1}}>
                                                                <Label>Nomor KTP</Label>
                                                                <br/>
                                                                <Col><p className="data-account">123456789101</p></Col>
                                                                <br/>
                                                                <Label>Nomor Kartu Keluarga</Label>
                                                                <br/>
                                                                <Col><p className="data-account">123456789101</p></Col>
                                                                <br/>
                                                                <Label>Nomor NPWP</Label>
                                                                <br/>
                                                                <Col><p className="data-account">12346789101</p></Col>
                                                                <br/>
                                                                <Label>Gaji Terakhir / Pendapatan</Label>
                                                                <br/>
                                                                <Col><p className="data-account">Rp 5.000.000</p></Col>
                                                                <br/>
                                                                <Label>Pekerjaan</Label>
                                                                <br/>
                                                                <Col><p className="data-account">Wirausaha, Pemilik rumah makan</p></Col>
                                                            </Col>
                                                        </Row>
                                                    </Container>
                                                </Col>
                                        </Container>
                                            
                                        <Container className="container-wrap2">
                                            <Col xs={{offset:1}}>
                                                <Container>
                                                    <Row>
                                                        <Col sm="5"><Label className="title-dashboard">Alamat</Label></Col>
                                                        <Col sm="2"></Col>
                                                        <Col sm="4"><Link to="/ChangeAddress"><Button className="btn-yellow1">Ubah Alamat</Button></Link></Col>
                                                    </Row>
                                                </Container>
                                                <Container>
                                                    <Row >
                                                        <Col xs={{size:5}}>
                                                            <Label>Alamat: </Label>
                                                            <br/>
                                                            <p className="data-account">
                                                            Jl. Aster Nomor 23 Kelurahan Mawar Kecamatan Anggrek 
                                                            Kota Jakarta Timur Provinsi DKI Jakarta
                                                            78880                   
                                                            </p>                                                      
                                                        </Col>
                                                        <div style={{border:'solid 1px #bdbdbd'}}>
                                                        </div>        
                                                        <Col xs={{size:5, offset:1}}>
                                                            <Label>Alamat: </Label>
                                                            <br/>
                                                            <p className="data-account">Jl. Abcde Nomor 20 Kelurahan Sukma Jaya Kecamatan Pangkalan Jati 
                                                            Kota Surabaya Provinsi Jawa Timur
                                                            78888                  
                                                            </p>
                                                        </Col>
                                                    </Row>
                                                </Container>
                                            </Col>
                                        </Container>          
                                            
                                        <Container className="container-wrap2">
                                            <Col xs={{offset:1}}>
                                                <Container>
                                                    <Row>
                                                        <Col sm="5"><Label className="title-dashboard">Data Persyaratan Pinjaman</Label></Col>
                                                        <Col sm="2"></Col>
                                                        <Col sm="4"><Link to="/ChangeAddData"><Button className="btn-yellow1">Ubah Data Persyaratan</Button></Link></Col>
                                                    </Row>
                                                </Container>    
                                                <Container>
                                                    <Row>
                                                        <Col xs={{size:5}}>
                                                            <Label>Foto Pribadi dengan KTP</Label>
                                                            <br/>
                                                            <Col><p className="data-account">Sudah terunggah</p></Col>
                                                            <br/>
                                                            <Label>Foto KTP</Label>
                                                            <br/>
                                                            <Col><p className="data-account">Sudah terunggah</p></Col>
                                                            <br/>
                                                            <Label>Foto Kartu Keluarga</Label>
                                                            <br/>
                                                            <Col><p className="data-account">Sudah terunggah</p></Col>
                                                        </Col>
                                                        <div style={{border:'solid 1px #bdbdbd'}}>
                                                        </div> 
                                                        <Col xs={{size:5, offset:1}}>
                                                            <Label>Foto Usaha & Slip Gaji</Label>
                                                            <br/>
                                                            <Col><p className="data-account">Sudah terunggah</p></Col>
                                                            <br/>
                                                            <Label>Foto Rumah</Label>
                                                            <br/>
                                                            <Col><p className="data-account">Sudah terunggah</p></Col>
                                                        </Col>
                                                    </Row>  
                                                <Label className="title-dashboard">Data Keluarga / Kerabat  (Tidak serumah)</Label>
                                                    <Row>
                                                        <Col xs={{size:5}}>
                                                            <Label>Nama Keluarga / Kerabat</Label>
                                                            <br/>
                                                            <Col><p className="data-account">Jhony</p></Col>
                                                            <br/>
                                                            <Label>Nomor Ponsel</Label>
                                                            <br/>
                                                            <Col><p className="data-account">08110000044</p></Col>
                                                        </Col>
                                                        <div style={{border:'solid 1px #bdbdbd'}}>
                                                        </div> 
                                                        <Col xs={{size:5, offset:1}}>
                                                            <Label>Hubungan Kekeluargaan</Label>
                                                            <br/>
                                                            <Col><p className="data-account">Adik</p></Col>
                                                        </Col>
                                                    </Row>
                                                </Container>
                                            </Col>
                                        </Container>
                                    </Col>
                                </Row>
                            </Container>
                <ContentLandingBottom/>
            <Footer/>
            </Fragment>
        );
    }
}

export default AccountUser;