import React, { Fragment }                              from 'react';
import HeaderAccount                                    from '../Component/HeaderAccount';
import ContentLandingBottom                             from '../Component/ContentLandingBottom';
import Footer                                           from '../Component/Footer';
import ReactCodeInput                                   from 'react-code-input';

import { Link }                                         from 'react-router-dom';

import { Card, Button, CardHeader, CardBody,
         CardText, Container, Col, Row }                from 'reactstrap';

import '../Css/Login.css'; 
  

class ConfirmPinWithdraw extends React.Component {
    
    render(){
      return(
        <Fragment>
            <HeaderAccount/>
                <div>
                    <Container>
                        <center>
                            <Col sm="10" md="5" className="mx-auto">
                                <Card>
                                    <CardHeader>
                                        <h5>Masukkan PIN</h5>
                                    </CardHeader>
                                    <CardBody>
                                    <Row>
                                        <Col>
                                            <CardText>Silahkan masukkan 6 digit PIN anda untuk verifikasi bahwa
                                            anda ingin melakukan penarikan dana.</CardText>
                                            <ReactCodeInput type="number" fields={6} />
                                        </Col>
                                    </Row>
                                        <Col>
                                            <Link to="/Withdraw-Success"><Button className="btn-green1 btn-block">Konfirmasi</Button></Link>
                                        </Col>
                                            <center><a href="/Reset">Lupa PIN?</a></center>
                                    </CardBody> 
                                </Card>
                            </Col>
                        </center>
                    </Container>
                </div>
                <ContentLandingBottom/>
            <Footer/>
        </Fragment>
      );
      
    }
    
  };
  
export default ConfirmPinWithdraw;

  
      