// Component, Page imported //
import React, { Component, Fragment }           from 'react';
import HeaderAccount                            from '../Component/HeaderAccount';
import SideBarProfile                           from '../Component/SidebarProfile';
import ContentLandingBottom                     from '../Component/ContentLandingBottom';
import Footer                                   from '../Component/Footer';


// Img, Icon, Logo Imported //

// Styling imported //
import { Container, Row, Col, 
        Card, CardText, CardBody}               from 'reactstrap';
import '../Css/UserDashboard.css';

class Notification extends Component{
    render() {
        return (
            <Fragment>
                <HeaderAccount/>
                    <div>
                    <Container>
                        <Row>
                            {/* Component sidebar profile*/}
                            <Col sm="4">
                                <SideBarProfile/>
                            </Col>
                                {/* end */}
                                <Col sm="8" className="col-dashboard-user">
                                    <h3>Notifikasi</h3>
                                        <hr/>
                                    <Col md="12" className="container-wrap2">
                                        <Card>
                                            <CardBody style={{border:' solid 1px'}}>
                                                <Row>
                                                    <Col>
                                                        <div className="float-left">02-04-2019 12:11</div>
                                                        <div className="float-right" style={{color:'#e6ba21'}}>Belum dilihat</div>
                                                    </Col> 
                                                </Row>
                                                     <CardText>Proses pinjaman anda diterima dan sudah pada tahap verifikasi data.</CardText>
                                            </CardBody>
                                        </Card>
                                       <br />
                                        <Card>
                                            <CardBody style={{border:' solid 1px'}}>
                                                <Row>
                                                    <Col>
                                                        <div className="float-left">02-04-2019 12:11</div>
                                                        <div className="float-right" style={{color:'#229f89'}}>Sudah dilihat</div>
                                                    </Col> 
                                                </Row>
                                                <CardText>Proses pinjaman anda diterima dan sudah pada tahap verifikasi data.</CardText>
                                            </CardBody>
                                        </Card>
                                    </Col>    
                            </Col>
                        </Row>
                    </Container>    
                    </div>
                <ContentLandingBottom/>
            <Footer/>
        </Fragment>
        );
    }
}

export default Notification;