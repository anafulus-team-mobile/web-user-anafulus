(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/Dashboard/UserDashboard.css":
/*!********************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js??ref--6-oneOf-3-1!./node_modules/postcss-loader/src??postcss!./src/Dashboard/UserDashboard.css ***!
  \********************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../node_modules/css-loader/dist/runtime/api.js */ "./node_modules/css-loader/dist/runtime/api.js")(false);
// Module
exports.push([module.i, ".col-profile-user{\n    padding:16px 16px 16px 16px;\n    width: 288px;\n    height: 900px;\n    box-shadow: 0 3px 6px 0 rgba(0, 0, 0, 0.16);\n    border: solid 1px #bdbdbd;\n    background-color: #ffffff;\n    margin-bottom: 20px;\n}\n\n.score-level{\n    margin: 0 auto;\n    width: 100px;\n    border-radius: 15px;\n    background-color: #46cbb3;\n}\n\n.user-amount{\n    font-size: 20px;\n  font-weight: bold;\n  font-style: normal;\n  font-stretch: normal;\n  line-height: 1.2;\n  letter-spacing: normal;\n  color: #229f89;\n}\n\n.container{\n    margin-top: 36px;\n}\n\n.choose-installment  {\n    text-align: center;\n    margin-right: 75px;\n    padding: 25px 25px 25px 25px;\n    box-shadow: 0 3px 6px 0 rgba(0, 0, 0, 0.16);\n    background-color:#fafafa;\n}\n\nButton.btn-choose-installment{\n    height: 160px;\n    border-radius: 18px;\n    border:0;\n}\n\n#btn-murabahah{\n    background-image: linear-gradient(to bottom, #95f2e2, #009b80);\n\n}\n\n#btn-reguler{\n    background-image: linear-gradient(to bottom, #37d1b6, #aed581);\n\n}\n\n#btn-badan-usaha{\n    background-image: linear-gradient(to bottom, #fdd835, #aed581);\n\n}\n\n.desc-btn-choose-installment{\n    font-size: 10pt;\n}\n\n.process-installment  {\n    padding: 20px 20px 20px 20px;\n    box-shadow: 0 3px 6px 0 rgba(0, 0, 0, 0.16);\n    text-align: center;\n    background-color:#fafafa;\n}\n\n.process-installment-step{\n    margin-bottom: 48px;\n\n}\n\n.info-installment   {\n    text-align: center;\n}\n\n.title-dashboard{\n    font-weight: bolder;\n    font-size: 20px;\n    margin-top:20px;\n    margin-bottom: 15px;\n}\n\n.profile-user-top   {\n    text-align: center;\n}\n\nButton.btn-withdraw{\n    width: 240px;\n    height: 48px;\n    box-shadow: 0 3px 6px 0 rgba(0, 0, 0, 0.16);\n    background-color: #229f89;\n    border:0;\n    border-radius:0;\n}\nButton.btn-data-complete{\n    width: 250px;\n    height: 40px;\n    background-color: #229f89;\n    border:0;\n    border-radius:0;\n}\n\nButton.btn-data-withdraw{\n    width: 250px;\n    height: 40px;\n    background-color: #e6ba21;\n    border:0;\n    border-radius:0;\n}\n\n.dashboard-menu{\n    font-family: Roboto;\n    font-size: 18px;\n    font-weight: normal;\n    font-style: normal;\n    font-stretch: normal;\n    line-height: 1.17;\n    letter-spacing: normal;\n    color: #212121;\n    \n}\n\n.dashboard-choose{\n    padding: 10px 10px 10px 10px;\n    \n}\n\n.dashboard-menu:hover{\n    color: #212121;\n}\n\n.dashboard-choose:hover{\n    background-color: #f1f1f1;\n    color: #212121;\n    \n}\n", ""]);



/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/css/ContactUs.css":
/*!**********************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js??ref--6-oneOf-3-1!./node_modules/postcss-loader/src??postcss!./src/css/ContactUs.css ***!
  \**********************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../node_modules/css-loader/dist/runtime/api.js */ "./node_modules/css-loader/dist/runtime/api.js")(false);
// Module
exports.push([module.i, ".body-content       {\n        margin-top: 24px;\n        font-family: Roboto;\n}\n\n#title-page{\n    font-size: 25pt;\n    color: #229f89;\n}\n\nIframe{\n    border: 0px;\n    width: 420px;\n    height: 320px; \n}", ""]);



/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/css/ContentLandingBottom.css":
/*!*********************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js??ref--6-oneOf-3-1!./node_modules/postcss-loader/src??postcss!./src/css/ContentLandingBottom.css ***!
  \*********************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../node_modules/css-loader/dist/runtime/api.js */ "./node_modules/css-loader/dist/runtime/api.js")(false);
// Module
exports.push([module.i, ".wrap-images    {\n    position: relative;\n    padding-top: 56px;\n    padding-bottom: 56px;\n}\n\n.banner-anafulus  {\n    width: 100%;\n    padding-top: 56px;\n}\n\n.disclaimer         {\n    text-align: justify;\n    font-family: Roboto;\n    font-size: 16px;\n    font-weight: normal;\n    font-style: normal;\n    font-stretch: normal;\n    line-height: 1.5;\n    letter-spacing: normal;\n    color: #757575;\n}", ""]);



/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/css/ContentLandingTop.css":
/*!******************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js??ref--6-oneOf-3-1!./node_modules/postcss-loader/src??postcss!./src/css/ContentLandingTop.css ***!
  \******************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../node_modules/css-loader/dist/runtime/api.js */ "./node_modules/css-loader/dist/runtime/api.js")(false);
// Module
exports.push([module.i, "h4          {\n    text-align: center;\n    padding-top: 64px;\n    padding-bottom: 32px;\n    font-family: Roboto;\n    font-size: 32px;\n    font-weight: bold;\n    font-style: normal;\n    font-stretch: normal;\n    line-height: 1.19;\n    letter-spacing: normal;\n}\n\n.title-about      {\n    font-family: Roboto;\n    font-size: 32px;\n    font-weight: bold;\n    font-style: normal;\n    font-stretch: normal;\n    line-height: 0.75;\n    letter-spacing: normal;\n    text-align: left;\n    color: #212121;\n    margin-top: 16px;\n}\n\n.content-about  {\n    text-align: justify;\n    font-family: Roboto;\n    font-size: 20px;\n    font-weight: normal;\n    font-style: normal;\n    font-stretch: normal;\n    line-height: 1.2;\n    letter-spacing: normal;\n    color: #212121;\n    margin-top:23px;\n}\n\n#btn-more   {\n    font-family: Roboto;\n    color: #ffffff;\n    padding: 4px 40px 4px 40px;\n    border-radius: 2px;\n    margin-top:20px;\n}\n\nspan        {\n    color: #229f89;\n}\n\n.section    {\n    font-size: 16px;\n    text-align: center;\n    padding-bottom: 64px;\n}\n\n.desc-section{\n    font-family: Roboto;\n    font-size: 16px;\n    font-weight: normal;\n    font-style: normal;\n    font-stretch: normal;\n    line-height: 1.5;\n    letter-spacing: normal;\n    text-align: center;\n    color: #212121;\n}\n\n.img-resposive  {\n    padding-top: 40px;\n}\n", ""]);



/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/css/Faq.css":
/*!****************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js??ref--6-oneOf-3-1!./node_modules/postcss-loader/src??postcss!./src/css/Faq.css ***!
  \****************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../node_modules/css-loader/dist/runtime/api.js */ "./node_modules/css-loader/dist/runtime/api.js")(false);
// Module
exports.push([module.i, ".Tabs-Faq{\n    \n\n}", ""]);



/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/css/Footer.css":
/*!*******************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js??ref--6-oneOf-3-1!./node_modules/postcss-loader/src??postcss!./src/css/Footer.css ***!
  \*******************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../node_modules/css-loader/dist/runtime/api.js */ "./node_modules/css-loader/dist/runtime/api.js")(false);
// Imports
exports.push([module.i, "@import url(https://fonts.googleapis.com/css?family=Fredoka+One&display=swap);", ""]);

// Module
exports.push([module.i, "/* Font Fredoka One */\n\n/* Container footer style */\n.footer         {\n    font-family: Roboto;\n    font-size: 20px;\n    background-color: #757575;\n    color: #ffffff;\n    text-decoration: none;\n}\n\n/* Logo anafulus footer style */\n.logo-anafulus   {\n    font-family: Fredoka One;\n    font-size: 48px;\n    font-weight: normal;\n    font-style: normal;\n    font-stretch: normal;\n    line-height: 1.21;\n    letter-spacing: normal;\n    text-align: left;\n    color: #ffffff;\n    padding-top: 19px;\n}\n\n/* Text & Title footer style */\nh5              {\n    font-family: Roboto;\n    font-size: 20px;\n    padding-top: 24px;\n}\n\n/* Informations list footer style */\nul              {\n    padding-left: 0;\n}\n\nli{\n    list-style-type: none;\n    padding-left: 0;\n    line-height: 43px;\n}\n\nul li a         {\n    color: #ffffff;\n}\n\n/* Information list hover style */\nul li a:hover   {\n    color: #ffffff;\n}\n\n/* Icon sosial media footer style */\n.ico-social     {\n    padding-right: 16px;\n    padding-bottom: 80px;\n}\n\n/* Logo OJK footer style */\n.logo-ojk       {\n    padding-top: 16px;\n    padding-bottom: 20px;\n}\n\n/* Text copyright footer style */\n.copy           {\n    font-family: Roboto;\n    text-align: center;\n    background-color: #616161;\n    color:#ffffff;   \n}", ""]);



/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/css/Header.css":
/*!*******************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js??ref--6-oneOf-3-1!./node_modules/postcss-loader/src??postcss!./src/css/Header.css ***!
  \*******************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../node_modules/css-loader/dist/runtime/api.js */ "./node_modules/css-loader/dist/runtime/api.js")(false);
// Imports
exports.push([module.i, "@import url(https://fonts.googleapis.com/css?family=Fredoka+One&display=swap);", ""]);

// Module
exports.push([module.i, ".nav{\n    font-family: Roboto;\n    background-color:#229f89;\n}\n\n.down-list{\n    color:black;\n}\n\n.down-list:hover{\n    color:black;\n}\n\n#logo           {\n    font-family: Fredoka One;\n    font-size: 48px;\n    font-weight: normal;\n    font-style: normal;\n    font-stretch: normal;\n    line-height: 1.21;\n    letter-spacing: normal;\n    text-align: left;\n    color: #ffffff;\n    padding-left: 64px;\n}\n\n.ml-auto    {\n    font-family: Roboto;\n    font-size: 20px;\n    font-weight: bold;\n    font-style: normal;\n    font-stretch: normal;\n    line-height: 1.2;\n    letter-spacing: normal;\n    opacity: 100%;\n    color:#ffffff;\n    margin-right: 64px;\n}\n\n.menu:hover{\n    opacity: 0.5;\n}\n\n#collapse{\n    background-color:none;\n    border-collapse: collapse;\n    border:3px solid #444;          /* added line */\n    border-radius:4px\n}\n\n#login          {\n    font-family: Roboto;\n    width: 130px;\n    height: 40px;\n    font-size: 16px;\n    border-radius: 5px;\n    text-decoration: none;\n    padding-top:8px;\n    margin-top: 10px;\n    margin-right: 24px;\n}\n\n#login:hover    {\n    color: #229f89;\n}\n\n#signup         {\n    width: 130px;\n    height: 40px;\n    border-radius: 5px;\n    font-family: Roboto;\n    padding-top:8px;\n    font-size: 16px;\n    margin-top: 10px;\n}\n\n#signup:hover   {\n    color: #229f89;\n}\n\n.btn-account{\n    margin-top: 10px;\n    margin-left: 100px;\n}\n\n#account-login{\n    text-decoration: none;\n    font-family: Roboto;\n    font-size: 20px;\n    font-weight: bold;\n    font-style: normal;\n    font-stretch: normal;\n    line-height: 1.2;\n    letter-spacing: normal;\n    text-align: center;\n    color: #ffffff\n}\n\n#account-login:hover{\n    opacity: 0.5;\n}", ""]);



/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/css/Login.css":
/*!******************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js??ref--6-oneOf-3-1!./node_modules/postcss-loader/src??postcss!./src/css/Login.css ***!
  \******************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../node_modules/css-loader/dist/runtime/api.js */ "./node_modules/css-loader/dist/runtime/api.js")(false);
// Module
exports.push([module.i, ".card-login{\n    text-align: center;\n    margin-top: 40px;\n\n}\n\n.card-header{\n    height: 110px;\n    background-image: linear-gradient(to bottom, #74d5c3, #12917a 29%, #2a8070 59%, #216c5e);\n    color: #ffffff;\n}\n\n.btn-card        {\n    width: 320px;\n    height: 40;\n    font-family: Roboto;\n    border-radius: 0;\n    background-color: none;\n    margin-top: 15px;\n    margin-bottom: 15px;\n}\n\n#btn-access{\n    background-color: #229f89;\n    text-decoration: none;\n    color: #ffffff;\n    border: 0px;\n    outline-color: none;\n    border-radius: 0px;\n}\n\n#btn-login-google{\n    color:#ed1c24;\n    border-radius: 1px;\n    box-shadow: 0 3px 6px 0 rgba(0, 0, 0, 0.16);\n    border: solid 1px #ed1c24;\n    text-align: left;\n}\n\n#btn-login-facebook{\n    color:#3a559f;\n    border-radius: 1px;\n    box-shadow: 0 3px 6px 0 rgba(0, 0, 0, 0.16);\n    border: solid 1px #3a559f;\n    text-align: left;\n}\n\n#num-phone{\n    float: left;\n}\n\n#label-num-phone{\n    background-color:#e6d492;\n    width: 150px;\n    margin: 0 auto;\n}\n\n#change-num{\n    font-family: Roboto;\n    font-size: 14px;\n    font-weight: normal;\n    font-style: normal;\n    font-stretch: normal;\n    line-height: 1.5;\n    letter-spacing: normal;\n    color: #229f89;\n    float: right;\n    margin-top: 5px;\n}\n\n\n#input-box{\n    border-radius: 0;\n    border-top: 0;\n    border-left: 0;\n    border-right: 0;\n    text-align: center;\n}\n\ninput[type=\"number\"]::-webkit-outer-spin-button, \ninput[type=\"number\"]::-webkit-inner-spin-button{\n    -webkit-appearance: none;\n    margin: 0;\n} \n\ninput[type=\"number\"]{\n    -moz-appearance: textfield;\n}", ""]);



/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/css/PinjamanBadanUsaha.css":
/*!*******************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js??ref--6-oneOf-3-1!./node_modules/postcss-loader/src??postcss!./src/css/PinjamanBadanUsaha.css ***!
  \*******************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../node_modules/css-loader/dist/runtime/api.js */ "./node_modules/css-loader/dist/runtime/api.js")(false);
// Module
exports.push([module.i, "#btn-ajukan-badan-usaha{\n    width: fit-content;\n    font-family: Roboto;\n    font-size: 24px;\n    font-weight: bold;\n    font-style: normal;\n    font-stretch: normal;\n    line-height: 1;\n    letter-spacing: normal;\n    color: #ffffff;\n    box-shadow: 0 3px 6px 0 rgba(0, 0, 0, 0.16);\n    border-radius: 0;\n    border-color: #e6ba21;\n    background-color: #e6ba21;\n    padding: 20px 20px 20px 20px;\n    display: block;\n    margin: 0 auto;\n}", ""]);



/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/css/PinjamanMurabahah.css":
/*!******************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js??ref--6-oneOf-3-1!./node_modules/postcss-loader/src??postcss!./src/css/PinjamanMurabahah.css ***!
  \******************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../node_modules/css-loader/dist/runtime/api.js */ "./node_modules/css-loader/dist/runtime/api.js")(false);
// Module
exports.push([module.i, ".banner-text{\n  margin-top:-450px;\n  height: fit-content;\n  font-family: Helvetica;\n  font-weight: bold;\n  font-style: normal;\n  font-stretch: normal;\n  line-height: 1.2;\n  letter-spacing: normal;\n  text-align: left;\n  color: #ffffff;\n  opacity: 0.62;\n  background-color: #212121;\n  padding: 20px 20px 20px 20px;\n}\n\n.text-banner{\n  \n}\n\n.text-banner1{\n  font-family: Helvetica;\n  font-size: 40px;\n  font-weight: bold;\n  font-style: normal;\n  font-stretch: normal;\n  line-height: 1.2;\n  letter-spacing: normal;\n  text-align: left;\n  color: #ffffff;\n}\n\n.text-banner2{\n  font-size: 32px;\n  font-weight: normal;\n  font-style: normal;\n  font-stretch: normal;\n  line-height: 1.25;\n  letter-spacing: normal;\n  text-align: left;\n  color: #ffffff;\n\n}\n\n.form-apply-pinjaman-murabahah{\n  margin-top: -650px;\n  width: 376px;\n  height: 630px;\n  font-family: Roboto;\n  letter-spacing: normal;\n  color: #212121;\n  box-shadow: 0 3px 6px 0 rgba(0, 0, 0, 0.16);\n  border: solid 1px #bdbdbd;\n  background-color: #ffffff;\n  float: right;\n  \n}\n\n#btn-hitung{\n  width: 320px;\n  height: 40px;\n  border-radius: 0;\n  object-fit: contain;\n  box-shadow: 0 3px 6px 0 rgba(0, 0, 0, 0.16);\n  background-color: #229f89;\n  border-color: #229f89;\n  display: block;\n  margin: 0 auto;\n}\n\n#btn-ajukan{\n  width: 320px;\n  height: 40px;\n  border-radius: 0;\n  object-fit: contain;\n  box-shadow: 0 3px 6px 0 rgba(0, 0, 0, 0.16);\n  background-color: #e6ba21;\n  border-color: #e6ba21;\n  display: block;\n  margin: 0 auto;\n}\n\n#Title-Cicilan{\n  font-family: Roboto;\n  font-size: 20px;\n  font-weight: bold;\n  font-style: normal;\n  font-stretch: normal;\n  line-height: 1.2;\n  letter-spacing: normal;\n  text-align: left;\n}\n\n#Cicilan {\n  font-family: Roboto;\n  font-size: 24px;\n  font-weight: bold;\n  font-style: normal;\n  font-stretch: normal;\n  line-height: 1;\n  letter-spacing: normal;\n  text-align: left;\n  color: #229f89;\n}\n\n#note-nominal-pinjaman{\n  font-family: Roboto;\n  font-size: 16px;\n  font-weight: normal;\n  font-style: normal;\n  font-stretch: normal;\n  line-height: 1.5;\n  letter-spacing: normal;\n  text-align: left;\n  color: #229f89;\n}", ""]);



/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/css/PinjamanReguler.css":
/*!****************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js??ref--6-oneOf-3-1!./node_modules/postcss-loader/src??postcss!./src/css/PinjamanReguler.css ***!
  \****************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../node_modules/css-loader/dist/runtime/api.js */ "./node_modules/css-loader/dist/runtime/api.js")(false);
// Module
exports.push([module.i, "  .form-apply-pinjaman-murabahah{\n    font-family: Roboto;\n    letter-spacing: normal;\n    color: #212121;\n    box-shadow: 0 3px 6px 0 rgba(0, 0, 0, 0.16);\n    border: solid 1px #bdbdbd;\n    background-color: #ffffff;\n    float: right;\n  }\n  \n  #btn-hitung{\n    width: 320px;\n    height: 40px;\n    border-radius: 0;\n    object-fit: contain;\n    box-shadow: 0 3px 6px 0 rgba(0, 0, 0, 0.16);\n    background-color: #229f89;\n    border-color: #229f89;\n    display: block;\n    margin: 0 auto;\n  }\n  \n  #btn-ajukan{\n    width: 320px;\n    height: 40px;\n    border-radius: 0;\n    object-fit: contain;\n    box-shadow: 0 3px 6px 0 rgba(0, 0, 0, 0.16);\n    background-color: #e6ba21;\n    border-color: #e6ba21;\n    display: block;\n    margin: 0 auto;\n  }\n  \n  #Title-Cicilan{\n    font-family: Roboto;\n    font-size: 20px;\n    font-weight: bold;\n    font-style: normal;\n    font-stretch: normal;\n    line-height: 1.2;\n    letter-spacing: normal;\n    text-align: left;\n  }\n  \n  #Cicilan {\n    font-family: Roboto;\n    font-size: 24px;\n    font-weight: bold;\n    font-style: normal;\n    font-stretch: normal;\n    line-height: 1;\n    letter-spacing: normal;\n    text-align: left;\n    color: #229f89;\n  }\n  \n  #note-nominal-pinjaman{\n    font-family: Roboto;\n    font-size: 16px;\n    font-weight: normal;\n    font-style: normal;\n    font-stretch: normal;\n    line-height: 1.5;\n    letter-spacing: normal;\n    text-align: left;\n    color: #229f89;\n  }", ""]);



/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/css/Register.css":
/*!*********************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js??ref--6-oneOf-3-1!./node_modules/postcss-loader/src??postcss!./src/css/Register.css ***!
  \*********************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../node_modules/css-loader/dist/runtime/api.js */ "./node_modules/css-loader/dist/runtime/api.js")(false);
// Module
exports.push([module.i, ".card-register{\n    text-align: center;\n    margin-top: 40px;\n}\n\n.card-register-form{\n    margin-top: 40px;\n}\n\n.card-header{\n    height: 110px;\n    background-image: linear-gradient(to bottom, #74d5c3, #12917a 29%, #2a8070 59%, #216c5e);\n    color: #ffffff;\n}\n\n.card-header-form{\n    text-align: center;\n}\n\n.btn-card        {\n    width: 320px;\n    height: 40;\n    font-family: Roboto;\n    background-color: none;\n    margin-top: 15px;\n    margin-bottom: 15px;\n}\n\n#btn-access{\n    background-color: #229f89;\n    text-decoration: none;\n    color: #ffffff;\n    border: 0px;\n    outline-color: none;\n    border-radius: 0px;\n}\n#btn-cancel{\n    background-color: #ed1c24;\n    text-decoration: none;\n    color: #ffffff;\n    border: 0px;\n    outline-color: none;\n    border-radius: 0px;\n}\n\n#btn-login-google{\n    color:#ed1c24;\n    border-radius: 1px;\n    box-shadow: 0 3px 6px 0 rgba(0, 0, 0, 0.16);\n    border: solid 1px #ed1c24;\n    text-align: left;\n}\n\n#btn-login-facebook{\n    color:#3a559f;\n    border-radius: 1px;\n    box-shadow: 0 3px 6px 0 rgba(0, 0, 0, 0.16);\n    border: solid 1px #3a559f;\n    text-align: left;\n}\n\n#num-phone{\n    float: left;\n}\n\n#input-box{\n    border-radius: 0;\n    border-top: 0;\n    border-left: 0;\n    border-right: 0;\n    text-align: center;\n}\n\n.input-title{\n    float: left;\n    margin-top:8px;\n}\n\n.input-guide{\n    float:left;\n    font-size:10pt;\n    color:#aca2a2;\n}\n\ninput[type=\"number\"]::-webkit-outer-spin-button, \ninput[type=\"number\"]::-webkit-inner-spin-button{\n    -webkit-appearance: none;\n    margin: 0;\n} \n\ninput[type=\"number\"]{\n    -moz-appearance: textfield;\n}\n\n.checkbox{\n    margin-top:16px;\n}\n\n.p-form-signup{\n    font-size: 9pt;\n    color: #229f89;\n    float: left;\n}", ""]);



/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/css/index.css":
/*!******************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js??ref--6-oneOf-3-1!./node_modules/postcss-loader/src??postcss!./src/css/index.css ***!
  \******************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../node_modules/css-loader/dist/runtime/api.js */ "./node_modules/css-loader/dist/runtime/api.js")(false);
// Module
exports.push([module.i, "body {\n  margin: 0;\n  font-family: -apple-system, BlinkMacSystemFont, \"Segoe UI\", \"Roboto\", \"Oxygen\",\n    \"Ubuntu\", \"Cantarell\", \"Fira Sans\", \"Droid Sans\", \"Helvetica Neue\",\n    sans-serif;\n  -webkit-font-smoothing: antialiased;\n  -moz-osx-font-smoothing: grayscale;\n}\n\ncode {\n  font-family: source-code-pro, Menlo, Monaco, Consolas, \"Courier New\",\n    monospace;\n}\n", ""]);



/***/ }),

/***/ "./src/App.js":
/*!********************!*\
  !*** ./src/App.js ***!
  \********************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/esm/react-router-dom.js");
/* harmony import */ var _Beranda__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Beranda */ "./src/Beranda.jsx");
/* harmony import */ var _Component_Login__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./Component/Login */ "./src/Component/Login.jsx");
/* harmony import */ var _UserAccount__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./UserAccount */ "./src/UserAccount.jsx");
/* harmony import */ var _Component_Register__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./Component/Register */ "./src/Component/Register.jsx");
/* harmony import */ var _Component_FormSignup__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./Component/FormSignup */ "./src/Component/FormSignup.jsx");
/* harmony import */ var _FormOTP__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./FormOTP */ "./src/FormOTP.jsx");
/* harmony import */ var _Component_ChangePhoneNum__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./Component/ChangePhoneNum */ "./src/Component/ChangePhoneNum.jsx");
/* harmony import */ var _Component_ForgotPIN__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./Component/ForgotPIN */ "./src/Component/ForgotPIN.jsx");
/* harmony import */ var _Dashboard_User_Dashboard__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./Dashboard/User-Dashboard */ "./src/Dashboard/User-Dashboard.jsx");
/* harmony import */ var _PinjamanMurabahah__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./PinjamanMurabahah */ "./src/PinjamanMurabahah.jsx");
/* harmony import */ var _PinjamanReguler__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./PinjamanReguler */ "./src/PinjamanReguler.jsx");
/* harmony import */ var _PinjamanBadanUsaha__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./PinjamanBadanUsaha */ "./src/PinjamanBadanUsaha.jsx");
/* harmony import */ var _Component_Tentang__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./Component/Tentang */ "./src/Component/Tentang.jsx");
/* harmony import */ var _Component_ContactUs__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./Component/ContactUs */ "./src/Component/ContactUs.jsx");
/* harmony import */ var _Faq__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./Faq */ "./src/Faq.jsx");
/* harmony import */ var _Component_KebijakanPerusahaan__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./Component/KebijakanPerusahaan */ "./src/Component/KebijakanPerusahaan.jsx");
/* harmony import */ var _Component_KebijakanPinjaman__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./Component/KebijakanPinjaman */ "./src/Component/KebijakanPinjaman.jsx");
/* harmony import */ var _Component_KebijakanPrivasi__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./Component/KebijakanPrivasi */ "./src/Component/KebijakanPrivasi.jsx");
/* harmony import */ var _Component_TermCondition__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./Component/TermCondition */ "./src/Component/TermCondition.jsx");
/* harmony import */ var _Component_RegulasiOJK__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./Component/RegulasiOJK */ "./src/Component/RegulasiOJK.jsx");
/* harmony import */ var _Component_ModalSucessReg__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ./Component/ModalSucessReg */ "./src/Component/ModalSucessReg.jsx");
var _jsxFileName = "/Users/mac/Desktop/anafulus-web/src/App.js";
























class App extends react__WEBPACK_IMPORTED_MODULE_0__["Component"] {
  render() {
    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["BrowserRouter"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 35
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Switch"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 36
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Route"], {
      path: "/",
      component: _Beranda__WEBPACK_IMPORTED_MODULE_2__["default"],
      exact: true,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 37
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Route"], {
      path: "/Login",
      component: _Component_Login__WEBPACK_IMPORTED_MODULE_3__["default"],
      __source: {
        fileName: _jsxFileName,
        lineNumber: 38
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Route"], {
      path: "/UserAccount",
      component: _UserAccount__WEBPACK_IMPORTED_MODULE_4__["default"],
      __source: {
        fileName: _jsxFileName,
        lineNumber: 39
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Route"], {
      path: "/UserRegister",
      component: _Component_Register__WEBPACK_IMPORTED_MODULE_5__["default"],
      __source: {
        fileName: _jsxFileName,
        lineNumber: 40
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Route"], {
      path: "/FormRegister",
      component: _Component_FormSignup__WEBPACK_IMPORTED_MODULE_6__["default"],
      __source: {
        fileName: _jsxFileName,
        lineNumber: 41
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Route"], {
      path: "/Form-OTP",
      component: _FormOTP__WEBPACK_IMPORTED_MODULE_7__["default"],
      __source: {
        fileName: _jsxFileName,
        lineNumber: 42
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Route"], {
      path: "/ChangeNumPhone",
      component: _Component_ChangePhoneNum__WEBPACK_IMPORTED_MODULE_8__["default"],
      __source: {
        fileName: _jsxFileName,
        lineNumber: 43
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Route"], {
      path: "/Reset-PIN",
      component: _Component_ForgotPIN__WEBPACK_IMPORTED_MODULE_9__["default"],
      __source: {
        fileName: _jsxFileName,
        lineNumber: 44
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Route"], {
      path: "/Dashboard",
      component: _Dashboard_User_Dashboard__WEBPACK_IMPORTED_MODULE_10__["default"],
      __source: {
        fileName: _jsxFileName,
        lineNumber: 46
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Route"], {
      path: "/Loan-Murabahah",
      component: _PinjamanMurabahah__WEBPACK_IMPORTED_MODULE_11__["default"],
      __source: {
        fileName: _jsxFileName,
        lineNumber: 48
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Route"], {
      path: "/Loan-Reguler",
      component: _PinjamanReguler__WEBPACK_IMPORTED_MODULE_12__["default"],
      __source: {
        fileName: _jsxFileName,
        lineNumber: 49
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Route"], {
      path: "/Loan-Badan-Usaha",
      component: _PinjamanBadanUsaha__WEBPACK_IMPORTED_MODULE_13__["default"],
      __source: {
        fileName: _jsxFileName,
        lineNumber: 50
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Route"], {
      path: "/About",
      component: _Component_Tentang__WEBPACK_IMPORTED_MODULE_14__["default"],
      __source: {
        fileName: _jsxFileName,
        lineNumber: 52
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Route"], {
      path: "/ContactUs",
      component: _Component_ContactUs__WEBPACK_IMPORTED_MODULE_15__["default"],
      __source: {
        fileName: _jsxFileName,
        lineNumber: 53
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Route"], {
      path: "/FAQ",
      component: _Faq__WEBPACK_IMPORTED_MODULE_16__["default"],
      __source: {
        fileName: _jsxFileName,
        lineNumber: 54
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Route"], {
      path: "/Kebijakan-Perusahaan",
      component: _Component_KebijakanPerusahaan__WEBPACK_IMPORTED_MODULE_17__["default"],
      __source: {
        fileName: _jsxFileName,
        lineNumber: 55
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Route"], {
      path: "/Kebijakan-Pinjaman",
      component: _Component_KebijakanPinjaman__WEBPACK_IMPORTED_MODULE_18__["default"],
      __source: {
        fileName: _jsxFileName,
        lineNumber: 56
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Route"], {
      path: "/Kebijakan-Privasi",
      component: _Component_KebijakanPrivasi__WEBPACK_IMPORTED_MODULE_19__["default"],
      __source: {
        fileName: _jsxFileName,
        lineNumber: 57
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Route"], {
      path: "/Term-Con",
      component: _Component_TermCondition__WEBPACK_IMPORTED_MODULE_20__["default"],
      __source: {
        fileName: _jsxFileName,
        lineNumber: 58
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Route"], {
      path: "/Regulation-OJK",
      component: _Component_RegulasiOJK__WEBPACK_IMPORTED_MODULE_21__["default"],
      __source: {
        fileName: _jsxFileName,
        lineNumber: 59
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Route"], {
      path: "/Sucess",
      component: _Component_ModalSucessReg__WEBPACK_IMPORTED_MODULE_22__["default"],
      __source: {
        fileName: _jsxFileName,
        lineNumber: 61
      },
      __self: this
    })));
  }

}

/* harmony default export */ __webpack_exports__["default"] = (App);

/***/ }),

/***/ "./src/Beranda.jsx":
/*!*************************!*\
  !*** ./src/Beranda.jsx ***!
  \*************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _Component_Header__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Component/Header */ "./src/Component/Header.jsx");
/* harmony import */ var _Component_Carousel__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Component/Carousel */ "./src/Component/Carousel.jsx");
/* harmony import */ var _Component_ContentLandingTop__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./Component/ContentLandingTop */ "./src/Component/ContentLandingTop.jsx");
/* harmony import */ var _Component_ContentLandingBottom__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./Component/ContentLandingBottom */ "./src/Component/ContentLandingBottom.jsx");
/* harmony import */ var _Component_Footer__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./Component/Footer */ "./src/Component/Footer.jsx");
var _jsxFileName = "/Users/mac/Desktop/anafulus-web/src/Beranda.jsx";







const Beranda = () => {
  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0__["Fragment"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 10
    },
    __self: undefined
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Component_Header__WEBPACK_IMPORTED_MODULE_1__["default"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 11
    },
    __self: undefined
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Component_Carousel__WEBPACK_IMPORTED_MODULE_2__["default"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 12
    },
    __self: undefined
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Component_ContentLandingTop__WEBPACK_IMPORTED_MODULE_3__["default"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 13
    },
    __self: undefined
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Component_ContentLandingBottom__WEBPACK_IMPORTED_MODULE_4__["default"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 14
    },
    __self: undefined
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Component_Footer__WEBPACK_IMPORTED_MODULE_5__["default"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 15
    },
    __self: undefined
  }));
};

/* harmony default export */ __webpack_exports__["default"] = (Beranda);

/***/ }),

/***/ "./src/Component/Carousel.jsx":
/*!************************************!*\
  !*** ./src/Component/Carousel.jsx ***!
  \************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var reactstrap__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! reactstrap */ "./node_modules/reactstrap/es/index.js");
/* harmony import */ var _Images_promo_anafulus_jpg__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../Images/promo-anafulus.jpg */ "./src/Images/promo-anafulus.jpg");
/* harmony import */ var _Images_promo_anafulus_jpg__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_Images_promo_anafulus_jpg__WEBPACK_IMPORTED_MODULE_2__);
var _jsxFileName = "/Users/mac/Desktop/anafulus-web/src/Component/Carousel.jsx";



const items = [{
  src: _Images_promo_anafulus_jpg__WEBPACK_IMPORTED_MODULE_2___default.a,
  altText: 'Image1'
}, {
  src: _Images_promo_anafulus_jpg__WEBPACK_IMPORTED_MODULE_2___default.a,
  altText: 'Image2'
}, {
  src: _Images_promo_anafulus_jpg__WEBPACK_IMPORTED_MODULE_2___default.a,
  altText: 'Image3'
}];

const Carousel = () => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["UncontrolledCarousel"], {
  items: items,
  __source: {
    fileName: _jsxFileName,
    lineNumber: 22
  },
  __self: undefined
});

/* harmony default export */ __webpack_exports__["default"] = (Carousel);

/***/ }),

/***/ "./src/Component/ChangePhoneNum.jsx":
/*!******************************************!*\
  !*** ./src/Component/ChangePhoneNum.jsx ***!
  \******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/esm/react-router-dom.js");
/* harmony import */ var _Header__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Header */ "./src/Component/Header.jsx");
/* harmony import */ var _ContentLandingBottom__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./ContentLandingBottom */ "./src/Component/ContentLandingBottom.jsx");
/* harmony import */ var _Footer__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./Footer */ "./src/Component/Footer.jsx");
/* harmony import */ var reactstrap__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! reactstrap */ "./node_modules/reactstrap/es/index.js");
/* harmony import */ var _css_Login_css__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../css/Login.css */ "./src/css/Login.css");
/* harmony import */ var _css_Login_css__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_css_Login_css__WEBPACK_IMPORTED_MODULE_6__);
var _jsxFileName = "/Users/mac/Desktop/anafulus-web/src/Component/ChangePhoneNum.jsx";








class ChangePhoneNum extends react__WEBPACK_IMPORTED_MODULE_0__["Component"] {
  render() {
    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0__["Fragment"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 14
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Header__WEBPACK_IMPORTED_MODULE_2__["default"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 15
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["Container"], {
      className: "card-login",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 16
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["Col"], {
      sm: "12",
      md: {
        size: 6,
        offset: 3
      },
      __source: {
        fileName: _jsxFileName,
        lineNumber: 17
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["Card"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 18
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["CardHeader"], {
      className: "card-header",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 19
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h5", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 20
      },
      __self: this
    }, "Ganti Nomor Ponsel")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["CardBody"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 22
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["Col"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 23
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["CardTitle"], {
      id: "old-num-phone",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 24
      },
      __self: this
    }, "Nomor Ponsel Lama"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["Input"], {
      type: "number",
      min: "10",
      max: "12",
      id: "input-box",
      value: "08123456789",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 25
      },
      __self: this
    })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["Col"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 27
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["CardTitle"], {
      id: "new-num-phone",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 28
      },
      __self: this
    }, "Nomor Ponsel Baru"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["Input"], {
      type: "number",
      min: "10",
      max: "12",
      id: "input-box",
      value: "08123987654",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 29
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 30
      },
      __self: this
    }, "*Kami akan mengirimkan kode verifikasi melalui SMS")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["Col"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 32
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["CardTitle"], {
      id: "num-idcard",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 33
      },
      __self: this
    }, "Nomor KTP"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["Input"], {
      type: "number",
      min: "10",
      max: "12",
      id: "input-box",
      value: "323294294200000",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 34
      },
      __self: this
    })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["Col"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 36
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["CardTitle"], {
      id: "num-kk",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 37
      },
      __self: this
    }, "Nomor Kartu Keluarga"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["Input"], {
      type: "number",
      min: "10",
      max: "12",
      id: "input-box",
      value: "322042042002022",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 38
      },
      __self: this
    })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 40
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 41
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["Col"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 42
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["Button"], {
      className: "btn-card",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 43
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Link"], {
      to: "/Login",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 45
      },
      __self: this
    }, "Ganti Nomor Ponsel"))))))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_ContentLandingBottom__WEBPACK_IMPORTED_MODULE_3__["default"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 52
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Footer__WEBPACK_IMPORTED_MODULE_4__["default"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 53
      },
      __self: this
    }));
  }

}

/* harmony default export */ __webpack_exports__["default"] = (ChangePhoneNum);

/***/ }),

/***/ "./src/Component/ContactUs.jsx":
/*!*************************************!*\
  !*** ./src/Component/ContactUs.jsx ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var reactstrap__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! reactstrap */ "./node_modules/reactstrap/es/index.js");
/* harmony import */ var _Header__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Header */ "./src/Component/Header.jsx");
/* harmony import */ var _ContentLandingBottom__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./ContentLandingBottom */ "./src/Component/ContentLandingBottom.jsx");
/* harmony import */ var react_iframe__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! react-iframe */ "./node_modules/react-iframe/dist/es/iframe.js");
/* harmony import */ var _Footer__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./Footer */ "./src/Component/Footer.jsx");
/* harmony import */ var _css_ContactUs_css__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../css/ContactUs.css */ "./src/css/ContactUs.css");
/* harmony import */ var _css_ContactUs_css__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_css_ContactUs_css__WEBPACK_IMPORTED_MODULE_6__);
var _jsxFileName = "/Users/mac/Desktop/anafulus-web/src/Component/ContactUs.jsx";








class ContactUs extends react__WEBPACK_IMPORTED_MODULE_0___default.a.Component {
  render() {
    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0__["Fragment"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 12
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Header__WEBPACK_IMPORTED_MODULE_2__["default"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 13
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Container"], {
      className: "body-content",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 14
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Row"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 15
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Col"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 16
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h3", {
      id: "title-page",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 17
      },
      __self: this
    }, "Hubungi Kami"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("hr", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 17
      },
      __self: this
    }))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      class: "row",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 21
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      class: "col align-items-start",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 22
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Col"], {
      md: "10",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 23
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h5", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 24
      },
      __self: this
    }, "Jam Operasional"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 25
      },
      __self: this
    }, "Senin - Jumat jam 08.00 - 17.00 WIB"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h5", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 27
      },
      __self: this
    }, "Email"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 28
      },
      __self: this
    }, "Anda dapat mengirimkan email ke : infoanafulus@anafulus.co.id "), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h5", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 30
      },
      __self: this
    }, "Dukungan Layanan Pelanggan"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 31
      },
      __self: this
    }, "Untuk respon lebih cepat Anda bisa menghubungi Layanan Pelanggan kami di 021 22903175-77. Atau Whatsapp di nomor 08123456789"))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      class: "col align-items-center",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 37
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Col"], {
      md: "10",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 38
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h5", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 39
      },
      __self: this
    }, "Alamat"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 40
      },
      __self: this
    }, "Gedung Kospin Jasa Lantai 6 Jl. Jendral Gatot Subroto No.1 RT .01 RW.01 Menteng Dalam, Tebet, Jakarta Selatan DKI Jakarta 12870."), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 44
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_iframe__WEBPACK_IMPORTED_MODULE_4__["default"], {
      url: "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3966.1655864425716!2d106.83911131422028!3d-6.24189599548179!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69f6a8d64de5ff%3A0xeb3afaf4aee3d78e!2sKospin+Jasa!5e0!3m2!1sid!2sid!4v1559275037008!5m2!1sid!2sid",
      allowfullscreen: true,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 46
      },
      __self: this
    }))))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_ContentLandingBottom__WEBPACK_IMPORTED_MODULE_3__["default"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 52
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Footer__WEBPACK_IMPORTED_MODULE_5__["default"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 53
      },
      __self: this
    }));
  }

}

/* harmony default export */ __webpack_exports__["default"] = (ContactUs);

/***/ }),

/***/ "./src/Component/ContentLandingBottom.jsx":
/*!************************************************!*\
  !*** ./src/Component/ContentLandingBottom.jsx ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _Images_anafulus_jpg__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../Images/anafulus.jpg */ "./src/Images/anafulus.jpg");
/* harmony import */ var _Images_anafulus_jpg__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_Images_anafulus_jpg__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var reactstrap__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! reactstrap */ "./node_modules/reactstrap/es/index.js");
/* harmony import */ var _css_ContentLandingBottom_css__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../css/ContentLandingBottom.css */ "./src/css/ContentLandingBottom.css");
/* harmony import */ var _css_ContentLandingBottom_css__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_css_ContentLandingBottom_css__WEBPACK_IMPORTED_MODULE_3__);
var _jsxFileName = "/Users/mac/Desktop/anafulus-web/src/Component/ContentLandingBottom.jsx";





class ContentLandingBottom extends react__WEBPACK_IMPORTED_MODULE_0___default.a.Component {
  render() {
    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 9
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_2__["Container"], {
      fluid: true,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 10
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_2__["Row"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 11
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("Col-full", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 12
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      className: "banner-anafulus",
      src: _Images_anafulus_jpg__WEBPACK_IMPORTED_MODULE_1___default.a,
      alt: "banner",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 12
      },
      __self: this
    })))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_2__["Container"], {
      className: "disclaimer",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 15
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h5", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 16
      },
      __self: this
    }, "Disclaimer Resiko"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 17
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 18
      },
      __self: this
    }, "1. Layanan Pinjam Meminjam Berbasis Teknologi Informasi merupakan kesepakatan perdata antara Pemberi Pinjaman dengan Penerima Pinjaman,   sehingga segala risiko yang timbul dari kesepakatan tersebut ditanggung sepenuhnya oleh masing-masing pihak.", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 20
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 20
      },
      __self: this
    }), "2. Risiko kredit atau gagal bayar ditanggung sepenuhnya oleh Pemberi  Pinjaman. Tidak ada lembaga atau otoritas negara yang bertanggung jawab  atas risiko gagal bayar ini.", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 22
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 22
      },
      __self: this
    }), "3. Penyelenggara dengan persetujuan dari masing-masing Pengguna (Pemberi  Pinjaman dan/atau Penerima Pinjaman) mengakses, memperoleh, menyimpan, mengelola, dan/atau menggunakan data pribadi Pengguna (\"Pemanfaatan Data\") pada atau di dalam benda, perangkat elektronik (termasuk smartphone atau telepon seluler), perangkat keras (hardware)  maupun lunak (software), dokumen elektronik, aplikasi atau sistem  elektronik milik Pengguna atau yang dikuasai Pengguna, dengan memberitahukan tujuan, batasan, dan mekanisme Pemanfaatan Data tersebut kepada Pengguna yang bersangkutan sebelum memperoleh persetujuan yang dimaksud.", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 27
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 27
      },
      __self: this
    }), "4. Pemberi Pinjaman yang belum memiliki pengetahuan dan pengalaman  pinjam meminjam, disarankan untuk tidak menggunakan layanan ini.", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 29
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 29
      },
      __self: this
    }), "5. Penerima Pinjaman harus mempertimbangkan tingkat bunga pinjaman dan biaya lainnya sesuai dengan kemampuan dalam melunasi pinjaman.", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 31
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 31
      },
      __self: this
    }), "6. Setiap kecurangan tercatat secara digital di dunia maya dan dapat diketahui  masyarakat luas di media sosial.", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 33
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 33
      },
      __self: this
    }), "7. Pengguna harus membaca dan memahami informasi ini sebelum membuat  keputusan menjadi Pemberi Pinjaman atau Penerima Pinjaman.", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 35
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 35
      },
      __self: this
    }), "8. Pemerintah yaitu dalam hal ini Otoritas Jasa Keuangan, tidak bertanggung  jawab atas setiap pelanggaran atau ketidakpatuhan oleh Pengguna, baik Pemberi Pinjaman maupun Penerima Pinjaman (baik karena kesengajaan  atau kelalaian Pengguna) terhadap ketentuan peraturan perundang-undangan maupun kesepakatan atau perikatan a antara Penyelenggara dengan Pemberi Pinjaman dan/atau Penerima Pinjaman.", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 39
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 39
      },
      __self: this
    }), "9. Setiap transaksi dan kegiatan pinjam meminjam atau pelaksanaan  kesepakatan mengenai pinjam meminjam antara atau yang melibatkan Penyelenggara, Pemberi Pinjaman, dan/atau Penerima Pinjaman wajib dilakukan melalui escrow account dan virtual account sebagaimana yang diwajibkan berdasarkan Peraturan Otoritas Jasa Keuangan Nomor 77/POJK. 01/2016 tentang Layanan Pinjam Meminjam Uang Berbasis Teknologi Informasi dan pelanggaran atau ketidakpatuhan terhadap ketentuan tersebut merupakan bukti telah terjadinya pelanggaran hukum oleh Penyelenggara sehingga Penyelenggara wajib menanggung ganti rugi yang diderita oleh masing-masing Pengguna sebagai akibat langsung dari pelanggaran hukum tersebut di atas tanpa mengurangi hak Pengguna yang menderita kerugian menurut Kitab Undang-Undang Hukum Perdata.", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 46
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 47
      },
      __self: this
    }), "PT Jasa Komunitas Digital (Anafulus) merupakan badan hukum yang didirikan berdasarkan Hukum Republik Indonesia. Berdiri sebagai perusahaan yang telah diatur oleh dan dalam pengawasan Otoritas Jasa Keuangan (OJK) di Indonesia, Perusahaan menyediakan layanan interfacing sebagai penghubung pihak yang memberikan pinjaman dan pihak yang membutuhkan pinjaman meliputi pendanaan dari individu, organisasi, maupun badan hukum kepada individu atau badan hukum tertentu. Perusahaan tidak menyediakan segala bentuk saran atau rekomendasi pendanaan terkait pilihan-pilihan dalam situs ini.", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 49
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 50
      },
      __self: this
    }), "Isi dan materi yang tersedia pada situs Anafulus.id dimaksudkan untuk memberikan informasi dan tidak dianggap sebagai sebuah penawaran, permohonan, undangan, saran, maupun rekomendasi untuk menginvestasikan sekuritas, produk pasar modal, atau jasa keuangan lainya. Perusahaan dalam memberikan jasanya hanya terbatas pada fungsi administratif.", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 52
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 53
      },
      __self: this
    }), "Pendanaan dan pinjaman yang ditempatkan di rekening Anafulus adalah tidak dan tidak akan dianggap sebagai simpanan yang diselenggarakan oleh Perusahaan seperti diatur dalam Peraturan Perundang-Undangan tentang Perbankan di Indonesia. Perusahaan atau setiap Direktur, Pegawai, Karyawan, Wakil, Afiliasi, atau Agen-Agennya tidak memiliki tanggung jawab terkait dengan setiap gangguan atau masalah yang terjadi atau yang dianggap terjadi yang disebabkan oleh minimnya persiapan atau publikasi dari materi yang tercantum pada situs Perusahaan.")));
  }

}

/* harmony default export */ __webpack_exports__["default"] = (ContentLandingBottom);

/***/ }),

/***/ "./src/Component/ContentLandingTop.jsx":
/*!*********************************************!*\
  !*** ./src/Component/ContentLandingTop.jsx ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _Icon_mosque_png__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../Icon/mosque.png */ "./src/Icon/mosque.png");
/* harmony import */ var _Icon_mosque_png__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_Icon_mosque_png__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _Icon_group_2_png__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../Icon/group-2.png */ "./src/Icon/group-2.png");
/* harmony import */ var _Icon_group_2_png__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_Icon_group_2_png__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _Icon_loan_png__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../Icon/loan.png */ "./src/Icon/loan.png");
/* harmony import */ var _Icon_loan_png__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_Icon_loan_png__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _Images_gatsu_jpg__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../Images/gatsu.jpg */ "./src/Images/gatsu.jpg");
/* harmony import */ var _Images_gatsu_jpg__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_Images_gatsu_jpg__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var reactstrap__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! reactstrap */ "./node_modules/reactstrap/es/index.js");
/* harmony import */ var _css_ContentLandingTop_css__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../css/ContentLandingTop.css */ "./src/css/ContentLandingTop.css");
/* harmony import */ var _css_ContentLandingTop_css__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_css_ContentLandingTop_css__WEBPACK_IMPORTED_MODULE_6__);
var _jsxFileName = "/Users/mac/Desktop/anafulus-web/src/Component/ContentLandingTop.jsx";








class ContentLandingTop extends react__WEBPACK_IMPORTED_MODULE_0___default.a.Component {
  render() {
    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 12
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["Container"], {
      className: "section",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 13
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h4", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 14
      },
      __self: this
    }, "Mengapa Pinjam di ", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 14
      },
      __self: this
    }, "Anafulus")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["Row"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 15
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["Col"], {
      sm: "4",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 16
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      className: "img-resposive",
      href: "/",
      src: _Icon_mosque_png__WEBPACK_IMPORTED_MODULE_1___default.a,
      alt: "murabahah",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 17
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h4", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 18
      },
      __self: this
    }, "Pinjaman Murabahah"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
      className: "desc-section",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 19
      },
      __self: this
    }, "Menyediakan peminjaman berbasis syariah untuk pergi umrah atau naik haji ataupun usaha anda.")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["Col"], {
      sm: "4",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 23
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      className: "img-resposive",
      href: "/",
      src: _Icon_group_2_png__WEBPACK_IMPORTED_MODULE_2___default.a,
      alt: "tanggung-renteng",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 24
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h4", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 25
      },
      __self: this
    }, "Pinjaman Tanggung Renteng"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
      className: "desc-section",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 26
      },
      __self: this
    }, "Menyediakan peminjaman tanggung renteng atau kelompok. Sehingga memungkinkan anda meminjam.")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["Col"], {
      sm: "4",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 30
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      className: "img-resposive",
      href: "/",
      src: _Icon_loan_png__WEBPACK_IMPORTED_MODULE_3___default.a,
      alt: "mudah-cepat",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 31
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h4", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 32
      },
      __self: this
    }, "Pinjaman Mudah dan Cepat"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
      className: "desc-section",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 33
      },
      __self: this
    }, "Pengajuan peminjaman mudah dan cepat.")))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["Container"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 37
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["Row"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 38
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["Col"], {
      sm: "2",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 39
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      className: "img-responsive",
      href: "/",
      src: _Images_gatsu_jpg__WEBPACK_IMPORTED_MODULE_4___default.a,
      alt: "gedung-kospin",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 40
      },
      __self: this
    })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["Col"], {
      sm: "12",
      md: {
        size: 8,
        offset: 2
      },
      __source: {
        fileName: _jsxFileName,
        lineNumber: 42
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
      className: "title-about",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 43
      },
      __self: this
    }, " Tentang Kami "), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
      className: "content-about",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 44
      },
      __self: this
    }, " Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 47
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 48
      },
      __self: this
    }), "Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["Button"], {
      href: "/About",
      color: "warning",
      id: "btn-more",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 53
      },
      __self: this
    }, "Selengkapnya")))));
  }

}

;
/* harmony default export */ __webpack_exports__["default"] = (ContentLandingTop);

/***/ }),

/***/ "./src/Component/Footer.jsx":
/*!**********************************!*\
  !*** ./src/Component/Footer.jsx ***!
  \**********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/esm/react-router-dom.js");
/* harmony import */ var _css_Footer_css__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../css/Footer.css */ "./src/css/Footer.css");
/* harmony import */ var _css_Footer_css__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_css_Footer_css__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _Icon_logo_ojk_png__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../Icon/logo-ojk.png */ "./src/Icon/logo-ojk.png");
/* harmony import */ var _Icon_logo_ojk_png__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_Icon_logo_ojk_png__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _Icon_facebook_png__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../Icon/facebook.png */ "./src/Icon/facebook.png");
/* harmony import */ var _Icon_facebook_png__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_Icon_facebook_png__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _Icon_instagram_png__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../Icon/instagram.png */ "./src/Icon/instagram.png");
/* harmony import */ var _Icon_instagram_png__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_Icon_instagram_png__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var reactstrap__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! reactstrap */ "./node_modules/reactstrap/es/index.js");
var _jsxFileName = "/Users/mac/Desktop/anafulus-web/src/Component/Footer.jsx";
// Import Lib, Component React & Icon  
 // Import React


 // Import footer css

 // Import logo ojk from folder Images

 // Import logo ojk from folder Icon

 // Import logo ojk from folder Icon 

 // Import component from reactstrap

class Footer extends react__WEBPACK_IMPORTED_MODULE_0___default.a.Component {
  render() {
    //Render view/UI
    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 13
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_6__["Container"], {
      fluid: true,
      className: "footer",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 14
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_6__["Container"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 15
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_6__["Row"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 16
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_6__["Col"], {
      sm: "4",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 17
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h5", {
      className: "logo-anafulus",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 18
      },
      __self: this
    }, "anafulus"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 19
      },
      __self: this
    }, "Gedung Kospin Jasa Lantai 6 Jl. Jendral Gatot Subroto No.1 RT .01 RW.01 Menteng Dalam, Tebet, Jakarta Selatan DKI Jakarta 12870 ", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 22
      },
      __self: this
    }), "Telp : 021 22903175-77 ", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 23
      },
      __self: this
    }), "Info@anafulus.co.id")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_6__["Col"], {
      sm: "1",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 27
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_6__["Col"], {
      sm: "3",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 28
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h5", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 29
      },
      __self: this
    }, "Informasi"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 30
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("ul", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 31
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 32
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Link"], {
      to: "/Kebijakan-Perusahaan",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 32
      },
      __self: this
    }, "Kebijakan Perusahaan")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 33
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
      href: "/Kebijakan-Pinjaman",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 33
      },
      __self: this
    }, "Kebijakan Pinjaman")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 34
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
      href: "/Kebijakan-Privasi",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 34
      },
      __self: this
    }, "Kebijakan Privasi")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 35
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
      href: "/Term-Con",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 35
      },
      __self: this
    }, "Syarat & Ketentuan")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 36
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
      href: "/Regulation-OJK",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 36
      },
      __self: this
    }, "Regulasi OJK"))))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_6__["Col"], {
      sm: "1",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 40
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_6__["Col"], {
      sm: "3",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 41
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h5", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 42
      },
      __self: this
    }, "Terdaftar dan Diawasi Oleh:"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
      href: "https://ojk.go.id/",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 43
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      className: "logo-ojk",
      src: _Icon_logo_ojk_png__WEBPACK_IMPORTED_MODULE_3___default.a,
      alt: "ojk",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 44
      },
      __self: this
    })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_6__["Row"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 46
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_6__["Col"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 47
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h5", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 48
      },
      __self: this
    }, "Ikuti Kami"))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
      href: "https://www.instagram.com/?hl=id",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 51
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      className: "ico-social",
      src: _Icon_facebook_png__WEBPACK_IMPORTED_MODULE_4___default.a,
      alt: "ico-fb",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 51
      },
      __self: this
    })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
      href: "https://id-id.facebook.com/",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 52
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      className: "ico-social",
      src: _Icon_instagram_png__WEBPACK_IMPORTED_MODULE_5___default.a,
      alt: "ico-ig",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 52
      },
      __self: this
    }))))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_6__["Row"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 57
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_6__["Col"], {
      className: "copy",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 58
      },
      __self: this
    }, "Copryright 2019 PT. Jasa Komunitas Digital. All Right Reserved."))));
  }

}

/* harmony default export */ __webpack_exports__["default"] = (Footer); // Open access Footer.jsx for other file

/***/ }),

/***/ "./src/Component/ForgotPIN.jsx":
/*!*************************************!*\
  !*** ./src/Component/ForgotPIN.jsx ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/esm/react-router-dom.js");
/* harmony import */ var _Header__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Header */ "./src/Component/Header.jsx");
/* harmony import */ var _ContentLandingBottom__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./ContentLandingBottom */ "./src/Component/ContentLandingBottom.jsx");
/* harmony import */ var _Footer__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./Footer */ "./src/Component/Footer.jsx");
/* harmony import */ var reactstrap__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! reactstrap */ "./node_modules/reactstrap/es/index.js");
/* harmony import */ var _css_Login_css__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../css/Login.css */ "./src/css/Login.css");
/* harmony import */ var _css_Login_css__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_css_Login_css__WEBPACK_IMPORTED_MODULE_6__);
var _jsxFileName = "/Users/mac/Desktop/anafulus-web/src/Component/ForgotPIN.jsx";





 // import { DateTimePicker } from 'react-widgets';



class ForgotPIN extends react__WEBPACK_IMPORTED_MODULE_0__["Component"] {
  render() {
    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0__["Fragment"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 15
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Header__WEBPACK_IMPORTED_MODULE_2__["default"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 16
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["Container"], {
      className: "card-login",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 17
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["Col"], {
      sm: "12",
      md: {
        size: 6,
        offset: 3
      },
      __source: {
        fileName: _jsxFileName,
        lineNumber: 18
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["Card"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 19
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["CardHeader"], {
      className: "card-header",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 20
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h5", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 21
      },
      __self: this
    }, "Reset PIN")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["CardBody"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 23
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 24
      },
      __self: this
    }, "Silahkan masukkan nomor ponsel Anda. Kami akan mengirimkan kode verifikasi melalui SMS untuk Reset PIN Anda."), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["Col"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 27
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["CardTitle"], {
      id: "num-phone",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 28
      },
      __self: this
    }, "Nomor Ponsel"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["Input"], {
      type: "number",
      min: "10",
      max: "12",
      id: "input-box",
      value: "08123987654",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 29
      },
      __self: this
    })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["Col"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 31
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["CardTitle"], {
      id: "num-idcard",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 32
      },
      __self: this
    }, "Tanggal Lahir"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["Input"], {
      type: "number",
      min: "10",
      max: "12",
      id: "input-box",
      DateTimePicker: true,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 33
      },
      __self: this
    })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 35
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 36
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["Col"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 37
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["Button"], {
      className: "btn-card",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 38
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Link"], {
      to: "/Login",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 40
      },
      __self: this
    }, "Ganti Nomor Ponsel"))))))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_ContentLandingBottom__WEBPACK_IMPORTED_MODULE_3__["default"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 47
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Footer__WEBPACK_IMPORTED_MODULE_4__["default"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 48
      },
      __self: this
    }));
  }

}

/* harmony default export */ __webpack_exports__["default"] = (ForgotPIN);

/***/ }),

/***/ "./src/Component/FormSignup.jsx":
/*!**************************************!*\
  !*** ./src/Component/FormSignup.jsx ***!
  \**************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/esm/react-router-dom.js");
/* harmony import */ var _Header__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Header */ "./src/Component/Header.jsx");
/* harmony import */ var _ContentLandingBottom__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./ContentLandingBottom */ "./src/Component/ContentLandingBottom.jsx");
/* harmony import */ var _Footer__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./Footer */ "./src/Component/Footer.jsx");
/* harmony import */ var reactstrap__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! reactstrap */ "./node_modules/reactstrap/es/index.js");
/* harmony import */ var _css_Register_css__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../css/Register.css */ "./src/css/Register.css");
/* harmony import */ var _css_Register_css__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_css_Register_css__WEBPACK_IMPORTED_MODULE_6__);
var _jsxFileName = "/Users/mac/Desktop/anafulus-web/src/Component/FormSignup.jsx";








class FormSignup extends react__WEBPACK_IMPORTED_MODULE_0__["Component"] {
  constructor(props) {
    super(props);
    this.state = {
      modal: false
    };
    this.toggle = this.toggle.bind(this);
  }

  toggle() {
    this.setState(prevState => ({
      modal: !prevState.modal
    }));
  }

  render() {
    const externalCloseBtn = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
      className: "close",
      style: {
        position: 'absolute',
        top: '15px',
        right: '15px'
      },
      onClick: this.toggle,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 29
      },
      __self: this
    }, "\xD7");
    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0__["Fragment"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 31
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Header__WEBPACK_IMPORTED_MODULE_2__["default"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 32
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["Container"], {
      className: "card-register",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 33
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["Col"], {
      sm: "12",
      md: {
        size: 6,
        offset: 3
      },
      __source: {
        fileName: _jsxFileName,
        lineNumber: 34
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["Card"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 35
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["CardHeader"], {
      className: "card-header-form",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 36
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h5", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 37
      },
      __self: this
    }, "Daftar"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 38
      },
      __self: this
    }, "Silahkan isi data di bawah ini")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["CardBody"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 40
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("form", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 41
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
      className: "p-form-signup",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 42
      },
      __self: this
    }, "*Harus Diisi"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 43
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 44
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["Col"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 45
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["CardTitle"], {
      className: "input-title",
      id: "user-email",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 46
      },
      __self: this
    }, "Email"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["Input"], {
      type: "text",
      id: "input-box",
      placeholder: "jhondoe@gmail.com",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 47
      },
      __self: this
    })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["Col"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 49
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["CardTitle"], {
      className: "input-title",
      id: "user-name",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 50
      },
      __self: this
    }, "Nama Lengkap sesuai E-KTP*"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["Input"], {
      type: "text",
      id: "input-box",
      placeholder: "John Doe",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 51
      },
      __self: this
    })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["Col"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 53
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["CardTitle"], {
      className: "input-title",
      id: "num-phone",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 54
      },
      __self: this
    }, "Nomor Ponsel*"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["Input"], {
      type: "number",
      id: "input-box",
      placeholder: "08123456789",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 55
      },
      __self: this
    })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["Col"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 57
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["CardTitle"], {
      className: "input-title",
      id: "user-pin",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 58
      },
      __self: this
    }, "PIN*"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["Input"], {
      type: "number",
      id: "input-box",
      placeholder: "654321",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 59
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
      className: "input-guide",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 60
      },
      __self: this
    }, "Masukkan PIN anda dengan 6 digit angka")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 62
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 63
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["Col"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 64
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["CardTitle"], {
      className: "input-title",
      id: "user-pin-confirm",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 65
      },
      __self: this
    }, "Konfirmasi PIN*"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["Input"], {
      type: "number",
      id: "input-box",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 66
      },
      __self: this
    })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["Col"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 68
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 70
      },
      __self: this
    }, "Saya telah membaca dan setuju dengan ", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Link"], {
      to: "/",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 70
      },
      __self: this
    }, "Kebijakan Privasi"), ", ", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Link"], {
      to: "",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 70
      },
      __self: this
    }, "Regulasi OJK "), "dan ", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Link"], {
      to: "",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 71
      },
      __self: this
    }, "Syarat & Ketentuan"))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 74
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 75
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["Col"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 76
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["Button"], {
      className: "btn-card",
      id: "btn-access",
      onClick: this.toggle,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 77
      },
      __self: this
    }, this.props.buttonLabel, "Daftar"))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["Modal"], {
      isOpen: this.state.modal,
      toggle: this.toggle,
      className: this.props.className,
      external: externalCloseBtn,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 85
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["ModalHeader"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 86
      },
      __self: this
    }, "Registrasi Berhasil"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["ModalBody"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 87
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 88
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 89
      },
      __self: this
    }, "Selamat anda telah berhasil mendaftarkan akun anda. Anda dapat mengajukan pinjaman untuk berbagai kebutuhan anda dengan melengkapi semua data.")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["ModalFooter"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 93
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["Button"], {
      id: "btn-access",
      href: "/Login",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 94
      },
      __self: this
    }, "Masuk sekarang"), ' ', react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["Button"], {
      id: "btn-cancel",
      href: "/",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 95
      },
      __self: this
    }, "Tidak"))))))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_ContentLandingBottom__WEBPACK_IMPORTED_MODULE_3__["default"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 102
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Footer__WEBPACK_IMPORTED_MODULE_4__["default"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 103
      },
      __self: this
    }));
  }

}

/* harmony default export */ __webpack_exports__["default"] = (FormSignup);

/***/ }),

/***/ "./src/Component/Header.jsx":
/*!**********************************!*\
  !*** ./src/Component/Header.jsx ***!
  \**********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _css_Header_css__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../css/Header.css */ "./src/css/Header.css");
/* harmony import */ var _css_Header_css__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_css_Header_css__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/esm/react-router-dom.js");
/* harmony import */ var reactstrap__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! reactstrap */ "./node_modules/reactstrap/es/index.js");
var _jsxFileName = "/Users/mac/Desktop/anafulus-web/src/Component/Header.jsx";



 // Class Header

class Header extends react__WEBPACK_IMPORTED_MODULE_0___default.a.Component {
  constructor(props) {
    super(props); // Responsive navbar with toggle

    this.toggle = this.toggle.bind(this);
    this.state = {
      isOpen: false
    };
  }

  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  } // Call view


  render() {
    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 36
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_3__["Navbar"], {
      className: "nav",
      expand: "sm",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 37
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_3__["NavbarBrand"], {
      id: "logo",
      href: "/",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 38
      },
      __self: this
    }, "anafulus"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_3__["NavbarToggler"], {
      onClick: this.toggle,
      id: "collapse",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 39
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_3__["Collapse"], {
      isOpen: this.state.isOpen,
      navbar: true,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 40
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_3__["Nav"], {
      className: "ml-auto",
      navbar: true,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 41
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_3__["NavItem"], {
      className: "menu",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 42
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_3__["NavLink"], {
      href: "/",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 43
      },
      __self: this
    }, "Beranda")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_3__["NavItem"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 47
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_3__["UncontrolledDropdown"], {
      nav: true,
      inNavbar: true,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 48
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_3__["DropdownToggle"], {
      nav: true,
      className: "menu",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 49
      },
      __self: this
    }, "Pinjaman"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_3__["DropdownMenu"], {
      right: true,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 52
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_3__["DropdownItem"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 53
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_2__["Link"], {
      to: "/Loan-Murabahah",
      className: "down-list",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 54
      },
      __self: this
    }, "Murabahah")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_3__["DropdownItem"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 56
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_2__["Link"], {
      to: "/Loan-Reguler",
      className: "down-list",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 57
      },
      __self: this
    }, "Reguler")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_3__["DropdownItem"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 59
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_3__["DropdownItem"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 60
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_2__["Link"], {
      to: "/Loan-Badan-Usaha",
      className: "down-list",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 61
      },
      __self: this
    }, "Badan Usaha"))))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_3__["NavItem"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 66
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_3__["NavLink"], {
      href: "/",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 67
      },
      __self: this
    }, "Pendanaan")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_3__["NavItem"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 69
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_3__["UncontrolledDropdown"], {
      nav: true,
      inNavbar: true,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 70
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_3__["DropdownToggle"], {
      nav: true,
      className: "menu",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 71
      },
      __self: this
    }, "Tentang Anafulus"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_3__["DropdownMenu"], {
      right: true,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 74
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_3__["DropdownItem"], {
      className: "down-list",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 75
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_2__["Link"], {
      to: "/About",
      className: "down-list",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 76
      },
      __self: this
    }, "Tentang Anafulus")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_3__["DropdownItem"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 78
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_2__["Link"], {
      to: "/ContactUs",
      className: "down-list",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 79
      },
      __self: this
    }, "Hubungi Kami")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_3__["DropdownItem"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 81
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_3__["DropdownItem"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 82
      },
      __self: this
    }, "Bantuan (FAQ)")))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_3__["Col"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 88
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_3__["Col"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 89
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_3__["Button"], {
      id: "login",
      title: "login",
      color: "light",
      size: "sm",
      href: "/Login",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 90
      },
      __self: this
    }, "Masuk"), ' ', react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_3__["Button"], {
      id: "signup",
      title: "signup",
      color: "light",
      size: "sm",
      href: "/UserRegister",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 96
      },
      __self: this
    }, "Daftar"), ' '))));
  }

}

/* harmony default export */ __webpack_exports__["default"] = (Header);

/***/ }),

/***/ "./src/Component/HeaderAccount.jsx":
/*!*****************************************!*\
  !*** ./src/Component/HeaderAccount.jsx ***!
  \*****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _css_Header_css__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../css/Header.css */ "./src/css/Header.css");
/* harmony import */ var _css_Header_css__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_css_Header_css__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _Icon_user_4_png__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../Icon/user-4.png */ "./src/Icon/user-4.png");
/* harmony import */ var _Icon_user_4_png__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_Icon_user_4_png__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/esm/react-router-dom.js");
/* harmony import */ var reactstrap__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! reactstrap */ "./node_modules/reactstrap/es/index.js");
var _jsxFileName = "/Users/mac/Desktop/anafulus-web/src/Component/HeaderAccount.jsx";




 // Class Header

class Header extends react__WEBPACK_IMPORTED_MODULE_0___default.a.Component {
  constructor(props) {
    super(props); // Responsive navbar with toggle

    this.toggle = this.toggle.bind(this);
    this.state = {
      isOpen: false
    };
  }

  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  } // Call view


  render() {
    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 37
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__["Navbar"], {
      className: "nav",
      expand: "sm",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 38
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__["NavbarBrand"], {
      id: "logo",
      href: "/",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 39
      },
      __self: this
    }, "anafulus"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__["NavbarToggler"], {
      onClick: this.toggle,
      id: "collapse",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 40
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__["Collapse"], {
      isOpen: this.state.isOpen,
      navbar: true,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 41
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__["Nav"], {
      className: "ml-auto",
      navbar: true,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 42
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__["NavItem"], {
      className: "menu",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 43
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__["NavLink"], {
      href: "/",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 44
      },
      __self: this
    }, "Beranda")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__["NavItem"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 48
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__["UncontrolledDropdown"], {
      nav: true,
      inNavbar: true,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 49
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__["DropdownToggle"], {
      nav: true,
      className: "menu",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 50
      },
      __self: this
    }, "Pinjaman"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__["DropdownMenu"], {
      right: true,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 53
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__["DropdownItem"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 54
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_3__["Link"], {
      to: "/Loan-Murabahah",
      className: "down-list",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 55
      },
      __self: this
    }, "Murabahah")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__["DropdownItem"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 57
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_3__["Link"], {
      to: "/Loan-Reguler",
      className: "down-list",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 58
      },
      __self: this
    }, "Reguler")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__["DropdownItem"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 60
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__["DropdownItem"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 61
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_3__["Link"], {
      to: "/Loan-Badan-Usaha",
      className: "down-list",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 62
      },
      __self: this
    }, "Badan Usaha"))))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__["NavItem"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 67
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__["NavLink"], {
      href: "/",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 68
      },
      __self: this
    }, "Pendanaan")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__["NavItem"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 70
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__["UncontrolledDropdown"], {
      nav: true,
      inNavbar: true,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 71
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__["DropdownToggle"], {
      nav: true,
      className: "menu",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 72
      },
      __self: this
    }, "Tentang Anafulus"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__["DropdownMenu"], {
      right: true,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 75
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__["DropdownItem"], {
      className: "down-list",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 76
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_3__["Link"], {
      to: "/About",
      className: "down-list",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 77
      },
      __self: this
    }, "Tentang Anafulus")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__["DropdownItem"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 79
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_3__["Link"], {
      to: "/ContactUs",
      className: "down-list",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 80
      },
      __self: this
    }, "Hubungi Kami")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__["DropdownItem"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 82
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__["DropdownItem"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 83
      },
      __self: this
    }, "Bantuan (FAQ)")))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "btn-account",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 90
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: _Icon_user_4_png__WEBPACK_IMPORTED_MODULE_2___default.a,
      alt: "icon-user",
      hspace: "10px",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 91
      },
      __self: this
    }), ' ', react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_3__["Link"], {
      to: "/Dashboard",
      id: "account-login",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 92
      },
      __self: this
    }, "Halo, John Doe"))))));
  }

}

/* harmony default export */ __webpack_exports__["default"] = (Header);

/***/ }),

/***/ "./src/Component/KebijakanPerusahaan.jsx":
/*!***********************************************!*\
  !*** ./src/Component/KebijakanPerusahaan.jsx ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var reactstrap__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! reactstrap */ "./node_modules/reactstrap/es/index.js");
/* harmony import */ var _Header__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Header */ "./src/Component/Header.jsx");
/* harmony import */ var _ContentLandingBottom__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./ContentLandingBottom */ "./src/Component/ContentLandingBottom.jsx");
/* harmony import */ var _Footer__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./Footer */ "./src/Component/Footer.jsx");
/* harmony import */ var _css_ContactUs_css__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../css/ContactUs.css */ "./src/css/ContactUs.css");
/* harmony import */ var _css_ContactUs_css__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_css_ContactUs_css__WEBPACK_IMPORTED_MODULE_5__);
var _jsxFileName = "/Users/mac/Desktop/anafulus-web/src/Component/KebijakanPerusahaan.jsx";







class KebijakanPerusahaan extends react__WEBPACK_IMPORTED_MODULE_0___default.a.Component {
  render() {
    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0__["Fragment"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 11
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Header__WEBPACK_IMPORTED_MODULE_2__["default"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 12
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Container"], {
      className: "body-content",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 13
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Row"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 14
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Col"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 15
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h3", {
      id: "title-page",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 16
      },
      __self: this
    }, "Kebijakan Perusahaan"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("hr", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 16
      },
      __self: this
    }))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Row"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 19
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Col"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 20
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h5", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 21
      },
      __self: this
    }, "Title"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 22
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 23
      },
      __self: this
    }, "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 24
      },
      __self: this
    }), "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit,sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?")))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_ContentLandingBottom__WEBPACK_IMPORTED_MODULE_3__["default"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 29
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Footer__WEBPACK_IMPORTED_MODULE_4__["default"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 30
      },
      __self: this
    }));
  }

}

/* harmony default export */ __webpack_exports__["default"] = (KebijakanPerusahaan);

/***/ }),

/***/ "./src/Component/KebijakanPinjaman.jsx":
/*!*********************************************!*\
  !*** ./src/Component/KebijakanPinjaman.jsx ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var reactstrap__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! reactstrap */ "./node_modules/reactstrap/es/index.js");
/* harmony import */ var _Header__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Header */ "./src/Component/Header.jsx");
/* harmony import */ var _ContentLandingBottom__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./ContentLandingBottom */ "./src/Component/ContentLandingBottom.jsx");
/* harmony import */ var _Footer__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./Footer */ "./src/Component/Footer.jsx");
/* harmony import */ var _css_ContactUs_css__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../css/ContactUs.css */ "./src/css/ContactUs.css");
/* harmony import */ var _css_ContactUs_css__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_css_ContactUs_css__WEBPACK_IMPORTED_MODULE_5__);
var _jsxFileName = "/Users/mac/Desktop/anafulus-web/src/Component/KebijakanPinjaman.jsx";







class KebijakanPinjaman extends react__WEBPACK_IMPORTED_MODULE_0___default.a.Component {
  render() {
    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0__["Fragment"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 11
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Header__WEBPACK_IMPORTED_MODULE_2__["default"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 12
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Container"], {
      className: "body-content",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 13
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Row"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 14
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Col"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 15
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h3", {
      id: "title-page",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 16
      },
      __self: this
    }, "Kebijakan Pinjaman"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("hr", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 16
      },
      __self: this
    }))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Row"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 19
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Col"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 20
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h5", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 21
      },
      __self: this
    }, "Title"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 22
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 23
      },
      __self: this
    }, "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 24
      },
      __self: this
    }), "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit,sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?")))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_ContentLandingBottom__WEBPACK_IMPORTED_MODULE_3__["default"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 29
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Footer__WEBPACK_IMPORTED_MODULE_4__["default"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 30
      },
      __self: this
    }));
  }

}

/* harmony default export */ __webpack_exports__["default"] = (KebijakanPinjaman);

/***/ }),

/***/ "./src/Component/KebijakanPrivasi.jsx":
/*!********************************************!*\
  !*** ./src/Component/KebijakanPrivasi.jsx ***!
  \********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var reactstrap__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! reactstrap */ "./node_modules/reactstrap/es/index.js");
/* harmony import */ var _Header__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Header */ "./src/Component/Header.jsx");
/* harmony import */ var _ContentLandingBottom__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./ContentLandingBottom */ "./src/Component/ContentLandingBottom.jsx");
/* harmony import */ var _Footer__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./Footer */ "./src/Component/Footer.jsx");
/* harmony import */ var _css_ContactUs_css__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../css/ContactUs.css */ "./src/css/ContactUs.css");
/* harmony import */ var _css_ContactUs_css__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_css_ContactUs_css__WEBPACK_IMPORTED_MODULE_5__);
var _jsxFileName = "/Users/mac/Desktop/anafulus-web/src/Component/KebijakanPrivasi.jsx";







class KebijakanPrivasi extends react__WEBPACK_IMPORTED_MODULE_0___default.a.Component {
  render() {
    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0__["Fragment"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 11
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Header__WEBPACK_IMPORTED_MODULE_2__["default"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 12
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Container"], {
      className: "body-content",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 13
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Row"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 14
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Col"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 15
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h3", {
      id: "title-page",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 16
      },
      __self: this
    }, "Kebijakan Privasi"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("hr", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 16
      },
      __self: this
    }))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Row"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 19
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Col"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 20
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h5", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 21
      },
      __self: this
    }, "Title"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 22
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 23
      },
      __self: this
    }, "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 24
      },
      __self: this
    }), "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit,sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?")))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_ContentLandingBottom__WEBPACK_IMPORTED_MODULE_3__["default"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 29
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Footer__WEBPACK_IMPORTED_MODULE_4__["default"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 30
      },
      __self: this
    }));
  }

}

/* harmony default export */ __webpack_exports__["default"] = (KebijakanPrivasi);

/***/ }),

/***/ "./src/Component/Login.jsx":
/*!*********************************!*\
  !*** ./src/Component/Login.jsx ***!
  \*********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/esm/react-router-dom.js");
/* harmony import */ var _Header__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Header */ "./src/Component/Header.jsx");
/* harmony import */ var _ContentLandingBottom__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./ContentLandingBottom */ "./src/Component/ContentLandingBottom.jsx");
/* harmony import */ var _Footer__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./Footer */ "./src/Component/Footer.jsx");
/* harmony import */ var reactstrap__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! reactstrap */ "./node_modules/reactstrap/es/index.js");
/* harmony import */ var _css_Login_css__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../css/Login.css */ "./src/css/Login.css");
/* harmony import */ var _css_Login_css__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_css_Login_css__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _Icon_icon_google_png__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../Icon/icon-google.png */ "./src/Icon/icon-google.png");
/* harmony import */ var _Icon_icon_google_png__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(_Icon_icon_google_png__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _Icon_facebook_login_png__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../Icon/facebook-login.png */ "./src/Icon/facebook-login.png");
/* harmony import */ var _Icon_facebook_login_png__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(_Icon_facebook_login_png__WEBPACK_IMPORTED_MODULE_8__);
var _jsxFileName = "/Users/mac/Desktop/anafulus-web/src/Component/Login.jsx";










class Login extends react__WEBPACK_IMPORTED_MODULE_0__["Component"] {
  render() {
    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0__["Fragment"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 15
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Header__WEBPACK_IMPORTED_MODULE_2__["default"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 16
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["Container"], {
      className: "card-login",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 17
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["Col"], {
      sm: "12",
      md: {
        size: 6,
        offset: 3
      },
      __source: {
        fileName: _jsxFileName,
        lineNumber: 18
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["Card"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 19
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["CardHeader"], {
      className: "card-header",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 20
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h5", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 21
      },
      __self: this
    }, "Masuk"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 22
      },
      __self: this
    }, "Silahkan masukkan nomor ponsel anda")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["CardBody"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 24
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["Col"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 25
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["CardTitle"], {
      id: "num-phone",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 26
      },
      __self: this
    }, "Nomor Ponsel"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["Input"], {
      type: "number",
      min: "10",
      max: "12",
      id: "input-box",
      placeholder: "Contoh: 08123456789",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 27
      },
      __self: this
    })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["Col"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 29
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("u", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 30
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Link"], {
      to: "/ChangeNumPhone",
      id: "change-num",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 30
      },
      __self: this
    }, "Ganti nomor ponsel ?"))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 32
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 33
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["Col"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 34
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["Button"], {
      className: "btn-card",
      id: "btn-access",
      href: "/Form-OTP",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 35
      },
      __self: this
    }, "Masuk")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["Col"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 42
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 43
      },
      __self: this
    }, "Atau masuk dengan")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["Col"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 45
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["Button"], {
      className: "btn-card",
      id: "btn-login-facebook",
      color: "light",
      href: "/UserAccount",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 46
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: _Icon_facebook_login_png__WEBPACK_IMPORTED_MODULE_8___default.a,
      hspace: "16",
      alt: "icon-facebook-login",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 51
      },
      __self: this
    }), "Masuk dengan Facebook")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["Col"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 55
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["Button"], {
      className: " btn-card",
      id: "btn-login-google",
      color: "light",
      href: "/UserAccount",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 56
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: _Icon_icon_google_png__WEBPACK_IMPORTED_MODULE_7___default.a,
      hspace: "16",
      alt: "icon-google-login",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 61
      },
      __self: this
    }), "Masuk dengan Google")))))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_ContentLandingBottom__WEBPACK_IMPORTED_MODULE_3__["default"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 69
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Footer__WEBPACK_IMPORTED_MODULE_4__["default"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 70
      },
      __self: this
    }));
  }

}

/* harmony default export */ __webpack_exports__["default"] = (Login);

/***/ }),

/***/ "./src/Component/ModalSucessReg.jsx":
/*!******************************************!*\
  !*** ./src/Component/ModalSucessReg.jsx ***!
  \******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var reactstrap__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! reactstrap */ "./node_modules/reactstrap/es/index.js");
/* harmony import */ var _Header__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Header */ "./src/Component/Header.jsx");
/* harmony import */ var _ContentLandingBottom__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./ContentLandingBottom */ "./src/Component/ContentLandingBottom.jsx");
/* harmony import */ var _Footer__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./Footer */ "./src/Component/Footer.jsx");
var _jsxFileName = "/Users/mac/Desktop/anafulus-web/src/Component/ModalSucessReg.jsx";






class ModalSucessReg extends react__WEBPACK_IMPORTED_MODULE_0___default.a.Component {
  constructor(props) {
    super(props);
    this.state = {
      modal: false
    };
    this.toggle = this.toggle.bind(this);
  }

  toggle() {
    this.setState(prevState => ({
      modal: !prevState.modal
    }));
  }

  render() {
    const externalCloseBtn = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
      className: "close",
      style: {
        position: 'absolute',
        top: '15px',
        right: '15px'
      },
      onClick: this.toggle,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 25
      },
      __self: this
    }, "\xD7");
    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0__["Fragment"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 27
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 28
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Header__WEBPACK_IMPORTED_MODULE_2__["default"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 29
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Modal"], {
      isOpen: this.state.modal,
      toggle: this.toggle,
      className: this.props.className,
      external: externalCloseBtn,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 30
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["ModalHeader"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 31
      },
      __self: this
    }, "Registrasi Berhasil"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["ModalBody"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 32
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("b", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 33
      },
      __self: this
    }, "Selamat anda telah berhasil mendaftarkan akun anda. Anda dapat mengajukan pinjaman untuk berbagai kebutuhan anda dengan melengkapi semua data.")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["ModalFooter"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 37
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Button"], {
      color: "primary",
      onClick: this.toggle,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 38
      },
      __self: this
    }, "Masuk sekarang"), ' ', react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Button"], {
      color: "secondary",
      onClick: this.toggle,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 39
      },
      __self: this
    }, "Tidak")))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_ContentLandingBottom__WEBPACK_IMPORTED_MODULE_3__["default"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 43
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Footer__WEBPACK_IMPORTED_MODULE_4__["default"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 44
      },
      __self: this
    }));
  }

}

/* harmony default export */ __webpack_exports__["default"] = (ModalSucessReg);

/***/ }),

/***/ "./src/Component/Register.jsx":
/*!************************************!*\
  !*** ./src/Component/Register.jsx ***!
  \************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _Header__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Header */ "./src/Component/Header.jsx");
/* harmony import */ var _ContentLandingBottom__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./ContentLandingBottom */ "./src/Component/ContentLandingBottom.jsx");
/* harmony import */ var _Footer__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./Footer */ "./src/Component/Footer.jsx");
/* harmony import */ var reactstrap__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! reactstrap */ "./node_modules/reactstrap/es/index.js");
/* harmony import */ var _css_Register_css__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../css/Register.css */ "./src/css/Register.css");
/* harmony import */ var _css_Register_css__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_css_Register_css__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _Icon_icon_google_png__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../Icon/icon-google.png */ "./src/Icon/icon-google.png");
/* harmony import */ var _Icon_icon_google_png__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_Icon_icon_google_png__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _Icon_facebook_login_png__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../Icon/facebook-login.png */ "./src/Icon/facebook-login.png");
/* harmony import */ var _Icon_facebook_login_png__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(_Icon_facebook_login_png__WEBPACK_IMPORTED_MODULE_7__);
var _jsxFileName = "/Users/mac/Desktop/anafulus-web/src/Component/Register.jsx";









class Register extends react__WEBPACK_IMPORTED_MODULE_0__["Component"] {
  render() {
    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 15
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Header__WEBPACK_IMPORTED_MODULE_1__["default"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 16
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__["Container"], {
      className: "card-register",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 17
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__["Col"], {
      sm: "12",
      md: {
        size: 6,
        offset: 3
      },
      __source: {
        fileName: _jsxFileName,
        lineNumber: 18
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__["Card"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 19
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__["CardHeader"], {
      className: "card-header",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 20
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h5", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 21
      },
      __self: this
    }, "Daftar"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 22
      },
      __self: this
    }, "Silahkan daftarkan email dan kata sandi anda")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__["CardBody"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 24
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__["Col"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 25
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__["CardTitle"], {
      id: "num-phone",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 26
      },
      __self: this
    }, "Nomor Ponsel"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__["Input"], {
      type: "number",
      min: "10",
      max: "12",
      id: "input-box",
      placeholder: "Contoh: 08123456789",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 27
      },
      __self: this
    })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 29
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 30
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__["Col"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 31
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__["Button"], {
      className: "btn-card",
      id: "btn-access",
      href: "/FormRegister",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 32
      },
      __self: this
    }, "Daftar")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__["Col"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 39
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 40
      },
      __self: this
    }, "Atau masuk dengan")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__["Col"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 42
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__["Button"], {
      className: "btn-card",
      id: "btn-login-facebook",
      color: "light",
      href: "/FormRegister",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 43
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: _Icon_facebook_login_png__WEBPACK_IMPORTED_MODULE_7___default.a,
      hspace: "16",
      alt: "icon-facebook-login",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 48
      },
      __self: this
    }), "Masuk dengan Facebook")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__["Col"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 52
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__["Button"], {
      className: " btn-card",
      id: "btn-login-google",
      color: "light",
      href: "/FormRegister",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 53
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: _Icon_icon_google_png__WEBPACK_IMPORTED_MODULE_6___default.a,
      hspace: "16",
      alt: "icon-google-login",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 58
      },
      __self: this
    }), "Masuk dengan Google")))))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_ContentLandingBottom__WEBPACK_IMPORTED_MODULE_2__["default"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 66
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Footer__WEBPACK_IMPORTED_MODULE_3__["default"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 67
      },
      __self: this
    }));
  }

}

/* harmony default export */ __webpack_exports__["default"] = (Register);

/***/ }),

/***/ "./src/Component/RegulasiOJK.jsx":
/*!***************************************!*\
  !*** ./src/Component/RegulasiOJK.jsx ***!
  \***************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var reactstrap__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! reactstrap */ "./node_modules/reactstrap/es/index.js");
/* harmony import */ var _Header__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Header */ "./src/Component/Header.jsx");
/* harmony import */ var _ContentLandingBottom__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./ContentLandingBottom */ "./src/Component/ContentLandingBottom.jsx");
/* harmony import */ var _Footer__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./Footer */ "./src/Component/Footer.jsx");
/* harmony import */ var _css_ContactUs_css__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../css/ContactUs.css */ "./src/css/ContactUs.css");
/* harmony import */ var _css_ContactUs_css__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_css_ContactUs_css__WEBPACK_IMPORTED_MODULE_5__);
var _jsxFileName = "/Users/mac/Desktop/anafulus-web/src/Component/RegulasiOJK.jsx";







class RegulasiOJK extends react__WEBPACK_IMPORTED_MODULE_0___default.a.Component {
  render() {
    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0__["Fragment"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 11
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Header__WEBPACK_IMPORTED_MODULE_2__["default"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 12
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Container"], {
      className: "body-content",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 13
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Row"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 14
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Col"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 15
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h3", {
      id: "title-page",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 16
      },
      __self: this
    }, "Regulasi OJK"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("hr", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 16
      },
      __self: this
    }))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Row"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 19
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Col"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 20
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h5", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 21
      },
      __self: this
    }, "Title"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 22
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 23
      },
      __self: this
    }, "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 24
      },
      __self: this
    }), "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit,sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?")))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_ContentLandingBottom__WEBPACK_IMPORTED_MODULE_3__["default"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 29
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Footer__WEBPACK_IMPORTED_MODULE_4__["default"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 30
      },
      __self: this
    }));
  }

}

/* harmony default export */ __webpack_exports__["default"] = (RegulasiOJK);

/***/ }),

/***/ "./src/Component/Tentang.jsx":
/*!***********************************!*\
  !*** ./src/Component/Tentang.jsx ***!
  \***********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var reactstrap__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! reactstrap */ "./node_modules/reactstrap/es/index.js");
/* harmony import */ var _Icon_manager_avatar_2x_png__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../Icon/manager-avatar@2x.png */ "./src/Icon/manager-avatar@2x.png");
/* harmony import */ var _Icon_manager_avatar_2x_png__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_Icon_manager_avatar_2x_png__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _Images_logo_tagline_png__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../Images/logo-tagline.png */ "./src/Images/logo-tagline.png");
/* harmony import */ var _Images_logo_tagline_png__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_Images_logo_tagline_png__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _Header__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./Header */ "./src/Component/Header.jsx");
/* harmony import */ var _ContentLandingBottom__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./ContentLandingBottom */ "./src/Component/ContentLandingBottom.jsx");
/* harmony import */ var _Footer__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./Footer */ "./src/Component/Footer.jsx");
/* harmony import */ var _css_ContactUs_css__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../css/ContactUs.css */ "./src/css/ContactUs.css");
/* harmony import */ var _css_ContactUs_css__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(_css_ContactUs_css__WEBPACK_IMPORTED_MODULE_7__);
var _jsxFileName = "/Users/mac/Desktop/anafulus-web/src/Component/Tentang.jsx";









class Tentang extends react__WEBPACK_IMPORTED_MODULE_0___default.a.Component {
  render() {
    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0__["Fragment"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 13
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Header__WEBPACK_IMPORTED_MODULE_4__["default"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 14
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Container"], {
      className: "body-content",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 15
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Row"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 16
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Col"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 17
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h3", {
      id: "title-page",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 18
      },
      __self: this
    }, "Tentang Anafulus"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("hr", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 18
      },
      __self: this
    }))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: _Images_logo_tagline_png__WEBPACK_IMPORTED_MODULE_3___default.a,
      width: "400",
      height: "300",
      class: "mx-auto d-block",
      alt: "logo-anafulus",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 21
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Row"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 22
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Col"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 23
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h5", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 24
      },
      __self: this
    }, "Title"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 25
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 26
      },
      __self: this
    }, "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 27
      },
      __self: this
    }), "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit,sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?"))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Row"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 31
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Col"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 32
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h5", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 33
      },
      __self: this
    }, "Title"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 34
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 35
      },
      __self: this
    }, "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 36
      },
      __self: this
    }), "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit,sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?"))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Row"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 40
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Col"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 41
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h5", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 42
      },
      __self: this
    }, "Tim Kami"))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Row"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 45
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Col"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 46
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 48
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: _Icon_manager_avatar_2x_png__WEBPACK_IMPORTED_MODULE_2___default.a,
      alt: "icon-team",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 49
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 50
      },
      __self: this
    }, "Nama", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 50
      },
      __self: this
    }), "Jabatan"))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Col"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 54
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 56
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: _Icon_manager_avatar_2x_png__WEBPACK_IMPORTED_MODULE_2___default.a,
      alt: "icon-team",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 57
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 58
      },
      __self: this
    }, "Nama", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 58
      },
      __self: this
    }), "Jabatan"))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Col"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 62
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 64
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: _Icon_manager_avatar_2x_png__WEBPACK_IMPORTED_MODULE_2___default.a,
      alt: "icon-team",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 65
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 66
      },
      __self: this
    }, "Nama", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 66
      },
      __self: this
    }), "Jabatan"))))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_ContentLandingBottom__WEBPACK_IMPORTED_MODULE_5__["default"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 72
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Footer__WEBPACK_IMPORTED_MODULE_6__["default"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 73
      },
      __self: this
    }));
  }

}

/* harmony default export */ __webpack_exports__["default"] = (Tentang);

/***/ }),

/***/ "./src/Component/TermCondition.jsx":
/*!*****************************************!*\
  !*** ./src/Component/TermCondition.jsx ***!
  \*****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var reactstrap__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! reactstrap */ "./node_modules/reactstrap/es/index.js");
/* harmony import */ var _Header__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Header */ "./src/Component/Header.jsx");
/* harmony import */ var _ContentLandingBottom__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./ContentLandingBottom */ "./src/Component/ContentLandingBottom.jsx");
/* harmony import */ var _Footer__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./Footer */ "./src/Component/Footer.jsx");
/* harmony import */ var _css_ContactUs_css__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../css/ContactUs.css */ "./src/css/ContactUs.css");
/* harmony import */ var _css_ContactUs_css__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_css_ContactUs_css__WEBPACK_IMPORTED_MODULE_5__);
var _jsxFileName = "/Users/mac/Desktop/anafulus-web/src/Component/TermCondition.jsx";







class TermCon extends react__WEBPACK_IMPORTED_MODULE_0___default.a.Component {
  render() {
    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0__["Fragment"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 11
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Header__WEBPACK_IMPORTED_MODULE_2__["default"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 12
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Container"], {
      className: "body-content",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 13
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Row"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 14
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Col"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 15
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h3", {
      id: "title-page",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 16
      },
      __self: this
    }, "Syarat & Ketentuan"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("hr", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 16
      },
      __self: this
    }))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Row"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 19
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Col"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 20
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h5", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 21
      },
      __self: this
    }, "Title"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 22
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 23
      },
      __self: this
    }, "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 24
      },
      __self: this
    }), "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit,sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?")))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_ContentLandingBottom__WEBPACK_IMPORTED_MODULE_3__["default"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 29
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Footer__WEBPACK_IMPORTED_MODULE_4__["default"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 30
      },
      __self: this
    }));
  }

}

/* harmony default export */ __webpack_exports__["default"] = (TermCon);

/***/ }),

/***/ "./src/Dashboard/User-Dashboard.jsx":
/*!******************************************!*\
  !*** ./src/Dashboard/User-Dashboard.jsx ***!
  \******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _Icon_user_1_png__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../Icon/user-1.png */ "./src/Icon/user-1.png");
/* harmony import */ var _Icon_user_1_png__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_Icon_user_1_png__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _Icon_money_png__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../Icon/money.png */ "./src/Icon/money.png");
/* harmony import */ var _Icon_money_png__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_Icon_money_png__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _Component_HeaderAccount__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../Component/HeaderAccount */ "./src/Component/HeaderAccount.jsx");
/* harmony import */ var _Component_ContentLandingBottom__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../Component/ContentLandingBottom */ "./src/Component/ContentLandingBottom.jsx");
/* harmony import */ var _Component_Footer__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../Component/Footer */ "./src/Component/Footer.jsx");
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/esm/react-router-dom.js");
/* harmony import */ var reactstrap__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! reactstrap */ "./node_modules/reactstrap/es/index.js");
/* harmony import */ var _UserDashboard_css__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./UserDashboard.css */ "./src/Dashboard/UserDashboard.css");
/* harmony import */ var _UserDashboard_css__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(_UserDashboard_css__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var _Icon_mosque_1_png__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../Icon/mosque-1.png */ "./src/Icon/mosque-1.png");
/* harmony import */ var _Icon_mosque_1_png__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(_Icon_mosque_1_png__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var _Icon_funding_png__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../Icon/funding.png */ "./src/Icon/funding.png");
/* harmony import */ var _Icon_funding_png__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(_Icon_funding_png__WEBPACK_IMPORTED_MODULE_10__);
/* harmony import */ var _Icon_store_1_png__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../Icon/store-1.png */ "./src/Icon/store-1.png");
/* harmony import */ var _Icon_store_1_png__WEBPACK_IMPORTED_MODULE_11___default = /*#__PURE__*/__webpack_require__.n(_Icon_store_1_png__WEBPACK_IMPORTED_MODULE_11__);
/* harmony import */ var _Icon_checked_1_png__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../Icon/checked-1.png */ "./src/Icon/checked-1.png");
/* harmony import */ var _Icon_checked_1_png__WEBPACK_IMPORTED_MODULE_12___default = /*#__PURE__*/__webpack_require__.n(_Icon_checked_1_png__WEBPACK_IMPORTED_MODULE_12__);
/* harmony import */ var _Icon_minus_1_png__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../Icon/minus-1.png */ "./src/Icon/minus-1.png");
/* harmony import */ var _Icon_minus_1_png__WEBPACK_IMPORTED_MODULE_13___default = /*#__PURE__*/__webpack_require__.n(_Icon_minus_1_png__WEBPACK_IMPORTED_MODULE_13__);
/* harmony import */ var _Icon_verification_of_delivery_list_clipboard_symbol_png__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ../Icon/verification-of-delivery-list-clipboard-symbol.png */ "./src/Icon/verification-of-delivery-list-clipboard-symbol.png");
/* harmony import */ var _Icon_verification_of_delivery_list_clipboard_symbol_png__WEBPACK_IMPORTED_MODULE_14___default = /*#__PURE__*/__webpack_require__.n(_Icon_verification_of_delivery_list_clipboard_symbol_png__WEBPACK_IMPORTED_MODULE_14__);
/* harmony import */ var _Icon_thumb_up_sign_png__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ../Icon/thumb-up-sign.png */ "./src/Icon/thumb-up-sign.png");
/* harmony import */ var _Icon_thumb_up_sign_png__WEBPACK_IMPORTED_MODULE_15___default = /*#__PURE__*/__webpack_require__.n(_Icon_thumb_up_sign_png__WEBPACK_IMPORTED_MODULE_15__);
/* harmony import */ var _Icon_money_1_png__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ../Icon/money-1.png */ "./src/Icon/money-1.png");
/* harmony import */ var _Icon_money_1_png__WEBPACK_IMPORTED_MODULE_16___default = /*#__PURE__*/__webpack_require__.n(_Icon_money_1_png__WEBPACK_IMPORTED_MODULE_16__);
var _jsxFileName = "/Users/mac/Desktop/anafulus-web/src/Dashboard/User-Dashboard.jsx";











 // import IcoProcess1                              from '../Icon/file.png';

 // import IcoProcess2                              from '../Icon/clipboard-with-a-list-1.png';






class UserDashboard extends react__WEBPACK_IMPORTED_MODULE_0__["Component"] {
  render() {
    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0__["Fragment"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 28
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Component_HeaderAccount__WEBPACK_IMPORTED_MODULE_3__["default"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 29
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_7__["Container"], {
      className: "container",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 30
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_7__["Row"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 31
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_7__["Col"], {
      md: "3",
      className: "col-profile-user",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 32
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "profile-user-top",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 33
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: _Icon_user_1_png__WEBPACK_IMPORTED_MODULE_1___default.a,
      alt: "user-profile",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 34
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h5", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 35
      },
      __self: this
    }, "Jhon Doe"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 36
      },
      __self: this
    }, "Penilaian Anda:"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
      className: "score-level",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 37
      },
      __self: this
    }, "Baik"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("hr", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 38
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 39
      },
      __self: this
    }, "Saldo"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
      className: "user-amount",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 40
      },
      __self: this
    }, "Rp 2.000.000"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_7__["Button"], {
      className: "btn-withdraw",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 41
      },
      __self: this
    }, "Tarik Dana")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("hr", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 43
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_7__["Row"], {
      className: "dashboard-choose",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 45
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_7__["Col"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 46
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_6__["Link"], {
      className: "dashboard-menu",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 47
      },
      __self: this
    }, "Dashboard"))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_7__["Row"], {
      className: "dashboard-choose",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 50
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_7__["Col"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 51
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_6__["Link"], {
      className: "dashboard-menu",
      id: "toggler",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 52
      },
      __self: this
    }, "Akun Saya"))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_7__["UncontrolledCollapse"], {
      toggler: "#toggler",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 55
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_7__["Col"], {
      className: "dashboard-choose",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 56
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_6__["Link"], {
      className: "dashboard-menu",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 56
      },
      __self: this
    }, "Ubah Data Diri")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_7__["Col"], {
      className: "dashboard-choose",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 57
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_6__["Link"], {
      className: "dashboard-menu",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 57
      },
      __self: this
    }, "Ubah Alamat")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_7__["Col"], {
      className: "dashboard-choose",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 58
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_6__["Link"], {
      className: "dashboard-menu",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 58
      },
      __self: this
    }, "Ubah Data Persyaratan")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_7__["Col"], {
      className: "dashboard-choose",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 59
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_6__["Link"], {
      className: "dashboard-menu",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 59
      },
      __self: this
    }, "Rekening Bank")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_7__["Col"], {
      className: "dashboard-choose",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 60
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_6__["Link"], {
      className: "dashboard-menu",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 60
      },
      __self: this
    }, "Ubah PIN"))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_7__["Row"], {
      className: "dashboard-choose",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 62
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_7__["Col"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 63
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_6__["Link"], {
      className: "dashboard-menu",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 64
      },
      __self: this
    }, "Riwayat"))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_7__["Row"], {
      className: "dashboard-choose",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 67
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_7__["Col"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 68
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_6__["Link"], {
      className: "dashboard-menu",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 69
      },
      __self: this
    }, "Notifikasi"))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_7__["Row"], {
      className: "dashboard-choose",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 72
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_7__["Col"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 73
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_6__["Link"], {
      to: "/",
      style: {
        color: '#f1545a'
      },
      __source: {
        fileName: _jsxFileName,
        lineNumber: 74
      },
      __self: this
    }, "Keluar")))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_7__["Col"], {
      md: "1",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 79
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_7__["Col"], {
      md: "8",
      className: "col-dashboard-user",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 80
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h3", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 81
      },
      __self: this
    }, "Dashboard"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("hr", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 81
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
      className: "title-dashboard",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 82
      },
      __self: this
    }, "Pilihan Pinjaman"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_7__["Container"], {
      className: "choose-installment",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 83
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0__["Fragment"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 84
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_7__["Row"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 85
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_7__["Col"], {
      className: "d-flex p-2",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 86
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_7__["Button"], {
      className: "btn-choose-installment",
      id: "btn-murabahah",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 87
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: _Icon_mosque_1_png__WEBPACK_IMPORTED_MODULE_9___default.a,
      alt: "ico-pinjaman-murabahah",
      className: "ico-btn",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 88
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h6", {
      "title-btn-install": true,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 89
      },
      __self: this
    }, "Pinjaman Murabahah"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h6", {
      className: "desc-btn-choose-installment",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 90
      },
      __self: this
    }, "Ingin melakukan pinjaman syariah ? Pilih pinjaman Murabahah."))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_7__["Col"], {
      className: "d-flex p-2",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 95
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_7__["Button"], {
      className: "btn-choose-installment",
      id: "btn-reguler",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 96
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: _Icon_funding_png__WEBPACK_IMPORTED_MODULE_10___default.a,
      alt: "ico-pinjaman-reguler",
      className: "ico-btn",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 97
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h6", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 98
      },
      __self: this
    }, "Pinjaman Reguler"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h6", {
      className: "desc-btn-choose-installment",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 99
      },
      __self: this
    }, "Butuh pinjaman ? Lakukan pengajuan Pinjaman Reguler. "))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_7__["Col"], {
      className: "d-flex p-2",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 102
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_7__["Button"], {
      className: "btn-choose-installment",
      id: "btn-badan-usaha",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 103
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: _Icon_store_1_png__WEBPACK_IMPORTED_MODULE_11___default.a,
      alt: "ico-pinjaman-badan-usaha",
      className: "ico-btn",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 104
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h6", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 105
      },
      __self: this
    }, "Pinjaman Badan Usaha"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h6", {
      className: "desc-btn-choose-installment",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 106
      },
      __self: this
    }, "Peminjaman untuk kebutuhan usaha anda.")))))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
      className: "title-dashboard",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 113
      },
      __self: this
    }, "Proses Pinjaman"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_7__["Container"], {
      className: "process-installment",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 114
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_7__["Row"], {
      className: "process-installment-step",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 115
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_7__["Col"], {
      sm: "1",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 116
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_7__["Col"], {
      sm: "2",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 117
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: _Icon_checked_1_png__WEBPACK_IMPORTED_MODULE_12___default.a,
      alt: "file",
      className: "ico-proses",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 118
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 120
      },
      __self: this
    }, "Upload", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 120
      },
      __self: this
    }), "Berkas")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_7__["Col"], {
      sm: "2",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 123
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: _Icon_minus_1_png__WEBPACK_IMPORTED_MODULE_13___default.a,
      alt: "valid-data",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 124
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 127
      },
      __self: this
    }, "Validasi", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 127
      },
      __self: this
    }), "Berkas")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_7__["Col"], {
      sm: "2",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 130
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: _Icon_verification_of_delivery_list_clipboard_symbol_png__WEBPACK_IMPORTED_MODULE_14___default.a,
      alt: "verify",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 131
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 134
      },
      __self: this
    }, "Verifikasi Berkas")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_7__["Col"], {
      sm: "2",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 137
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: _Icon_thumb_up_sign_png__WEBPACK_IMPORTED_MODULE_15___default.a,
      alt: "apply",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 138
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 141
      },
      __self: this
    }, "Proses Disetujui")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_7__["Col"], {
      sm: "2",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 144
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: _Icon_money_1_png__WEBPACK_IMPORTED_MODULE_16___default.a,
      alt: "finish",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 145
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 148
      },
      __self: this
    }, "Dana", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 148
      },
      __self: this
    }), "Dicairkan ")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_7__["Col"], {
      sm: "1",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 150
      },
      __self: this
    })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 152
      },
      __self: this
    }, "Keterangan: Mohon maaf berkas anda tidak lengkap sehingga tidak dapat di validasi. Untuk  melakukan pinjaman silahkan lengkapi data diri anda terlebih dulu."), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_7__["Row"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 155
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_7__["Col"], {
      sm: "1",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 156
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_7__["Col"], {
      sm: "5",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 157
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_7__["Button"], {
      className: "btn-data-complete",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 158
      },
      __self: this
    }, "Lengkapi Data Diri")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_7__["Col"], {
      sm: "5",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 161
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_7__["Button"], {
      className: "btn-data-withdraw",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 162
      },
      __self: this
    }, "Tarik Dana")))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
      className: "title-dashboard",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 168
      },
      __self: this
    }, "Informasi Cicilan"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_7__["Container"], {
      className: "info-installment",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 169
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_7__["Row"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 170
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: _Icon_money_png__WEBPACK_IMPORTED_MODULE_2___default.a,
      alt: "empty-info",
      className: "mx-auto",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 171
      },
      __self: this
    })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 173
      },
      __self: this
    }, "Anda belum pernah melakukan pinjaman di Anafulus."))))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Component_ContentLandingBottom__WEBPACK_IMPORTED_MODULE_4__["default"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 178
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Component_Footer__WEBPACK_IMPORTED_MODULE_5__["default"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 179
      },
      __self: this
    }));
  }

}

/* harmony default export */ __webpack_exports__["default"] = (UserDashboard);

/***/ }),

/***/ "./src/Dashboard/UserDashboard.css":
/*!*****************************************!*\
  !*** ./src/Dashboard/UserDashboard.css ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../node_modules/css-loader/dist/cjs.js??ref--6-oneOf-3-1!../../node_modules/postcss-loader/src??postcss!./UserDashboard.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/Dashboard/UserDashboard.css");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(true) {
	module.hot.accept(/*! !../../node_modules/css-loader/dist/cjs.js??ref--6-oneOf-3-1!../../node_modules/postcss-loader/src??postcss!./UserDashboard.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/Dashboard/UserDashboard.css", function() {
		var newContent = __webpack_require__(/*! !../../node_modules/css-loader/dist/cjs.js??ref--6-oneOf-3-1!../../node_modules/postcss-loader/src??postcss!./UserDashboard.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/Dashboard/UserDashboard.css");

		if(typeof newContent === 'string') newContent = [[module.i, newContent, '']];

		var locals = (function(a, b) {
			var key, idx = 0;

			for(key in a) {
				if(!b || a[key] !== b[key]) return false;
				idx++;
			}

			for(key in b) idx--;

			return idx === 0;
		}(content.locals, newContent.locals));

		if(!locals) throw new Error('Aborting CSS HMR due to changed css-modules locals.');

		update(newContent);
	});

	module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ "./src/Faq.jsx":
/*!*********************!*\
  !*** ./src/Faq.jsx ***!
  \*********************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var reactstrap__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! reactstrap */ "./node_modules/reactstrap/es/index.js");
/* harmony import */ var _Component_Header__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Component/Header */ "./src/Component/Header.jsx");
/* harmony import */ var _Component_ContentLandingBottom__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./Component//ContentLandingBottom */ "./src/Component/ContentLandingBottom.jsx");
/* harmony import */ var _Component_Footer__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./Component/Footer */ "./src/Component/Footer.jsx");
/* harmony import */ var _css_Faq_css__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./css/Faq.css */ "./src/css/Faq.css");
/* harmony import */ var _css_Faq_css__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_css_Faq_css__WEBPACK_IMPORTED_MODULE_5__);
var _jsxFileName = "/Users/mac/Desktop/anafulus-web/src/Faq.jsx";
 // import { Link } from 'react-router-dom';








class Faq extends react__WEBPACK_IMPORTED_MODULE_0___default.a.Component {
  constructor(props) {
    super(props);
    this.toggle = this.toggle.bind(this);
    this.state = {
      collapse: false
    };
  }

  toggle() {
    this.setState(state => ({
      collapse: !state.collapse
    }));
  }

  render() {
    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0__["Fragment"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 23
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Component_Header__WEBPACK_IMPORTED_MODULE_2__["default"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 24
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Container"], {
      className: "body-content",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 25
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Row"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 26
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Col"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 27
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h3", {
      id: "title-page",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 28
      },
      __self: this
    }, "Bantuan / FAQ"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("hr", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 28
      },
      __self: this
    }))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 31
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Button"], {
      color: "primary",
      onClick: this.toggle,
      style: {
        marginBottom: '1rem'
      },
      __source: {
        fileName: _jsxFileName,
        lineNumber: 32
      },
      __self: this
    }, "Toggle"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Collapse"], {
      isOpen: this.state.collapse,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 33
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Card"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 34
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["CardBody"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 35
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      class: "container",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 36
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      class: "row",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 37
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      class: "col align-self-start",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 38
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Col"], {
      sm: "4",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 39
      },
      __self: this
    }, "1. Question 1 ?", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 41
      },
      __self: this
    }), "Answer : Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Col"], {
      sm: "4",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 45
      },
      __self: this
    }, "2. Question 2 ?", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 47
      },
      __self: this
    }), "Answer : Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Col"], {
      sm: "4",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 51
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 52
      },
      __self: this
    }, "3. Question 3 ? Answer : Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."))))))))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 64
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Button"], {
      color: "primary",
      onClick: this.toggle,
      style: {
        marginBottom: '1rem'
      },
      __source: {
        fileName: _jsxFileName,
        lineNumber: 65
      },
      __self: this
    }, "Toggle"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Collapse"], {
      isOpen: this.state.collapse,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 66
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Card"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 67
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["CardBody"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 68
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      class: "container",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 69
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      class: "row",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 70
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      class: "col align-self-start",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 71
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 72
      },
      __self: this
    }, "1. Question 1 ?", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 74
      },
      __self: this
    }), "Answer : Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 79
      },
      __self: this
    }, "2. Question 2 ?", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 81
      },
      __self: this
    }), "Answer : Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      class: "col align-self-center",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 86
      },
      __self: this
    }, "One of three columns"))))))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 96
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Button"], {
      color: "primary",
      onClick: this.toggle,
      style: {
        marginBottom: '1rem'
      },
      __source: {
        fileName: _jsxFileName,
        lineNumber: 97
      },
      __self: this
    }, "Toggle"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Collapse"], {
      isOpen: this.state.collapse,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 98
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Card"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 99
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["CardBody"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 100
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      class: "container",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 101
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      class: "row",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 102
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      class: "col align-self-start",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 103
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 104
      },
      __self: this
    }, "1. Question 1 ?", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 106
      },
      __self: this
    }), "Answer : Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 111
      },
      __self: this
    }, "2. Question 2 ?", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 113
      },
      __self: this
    }), "Answer : Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      class: "col align-self-center",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 118
      },
      __self: this
    }, "One of three columns")))))))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Component_ContentLandingBottom__WEBPACK_IMPORTED_MODULE_3__["default"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 128
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Component_Footer__WEBPACK_IMPORTED_MODULE_4__["default"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 129
      },
      __self: this
    }));
  }

}

/* harmony default export */ __webpack_exports__["default"] = (Faq);

/***/ }),

/***/ "./src/FormOTP.jsx":
/*!*************************!*\
  !*** ./src/FormOTP.jsx ***!
  \*************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/esm/react-router-dom.js");
/* harmony import */ var _Component_Header__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Component/Header */ "./src/Component/Header.jsx");
/* harmony import */ var _Component_ContentLandingBottom__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./Component/ContentLandingBottom */ "./src/Component/ContentLandingBottom.jsx");
/* harmony import */ var _Component_Footer__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./Component/Footer */ "./src/Component/Footer.jsx");
/* harmony import */ var reactstrap__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! reactstrap */ "./node_modules/reactstrap/es/index.js");
/* harmony import */ var _css_Login_css__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./css/Login.css */ "./src/css/Login.css");
/* harmony import */ var _css_Login_css__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_css_Login_css__WEBPACK_IMPORTED_MODULE_6__);
var _jsxFileName = "/Users/mac/Desktop/anafulus-web/src/FormOTP.jsx";








class FormOTP extends react__WEBPACK_IMPORTED_MODULE_0__["Component"] {
  render() {
    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0__["Fragment"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 14
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Component_Header__WEBPACK_IMPORTED_MODULE_2__["default"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 15
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["Container"], {
      className: "card-register",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 16
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["Col"], {
      sm: "12",
      md: {
        size: 6,
        offset: 3
      },
      __source: {
        fileName: _jsxFileName,
        lineNumber: 17
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["Card"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 18
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["CardHeader"], {
      className: "card-header",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 19
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h5", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 20
      },
      __self: this
    }, "Verifikasi Akun")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["CardBody"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 22
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 23
      },
      __self: this
    }, "Silahkan verifikasi akun kamu dengan memasukkan kode yang telah dikirimkan melalui SMS ke nomor:"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["Col"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 25
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["CardTitle"], {
      id: "label-num-phone",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 26
      },
      __self: this
    }, "08123456789"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["Input"], {
      type: "number",
      min: "10",
      max: "12",
      id: "input-box",
      placeholder: "121314",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 27
      },
      __self: this
    })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 29
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 30
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["Col"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 31
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["Button"], {
      className: "btn-card",
      id: "btn-access",
      href: "/UserAccount",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 32
      },
      __self: this
    }, "Verifikasi")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["Col"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 39
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 40
      },
      __self: this
    }, "OTP kamu akan berakhir dalam ", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("b", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 40
      },
      __self: this
    }, "waktu 9 menit 43 detik"))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Link"], {
      to: "",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 42
      },
      __self: this
    }, "Kirim ulang kode verifikasi"))))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Component_ContentLandingBottom__WEBPACK_IMPORTED_MODULE_3__["default"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 47
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Component_Footer__WEBPACK_IMPORTED_MODULE_4__["default"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 48
      },
      __self: this
    }));
  }

}

/* harmony default export */ __webpack_exports__["default"] = (FormOTP);

/***/ }),

/***/ "./src/Icon/checked-1.png":
/*!********************************!*\
  !*** ./src/Icon/checked-1.png ***!
  \********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFAAAABOCAYAAAC3zZFGAAAAAXNSR0IArs4c6QAAEttJREFUeAHdXAl0k9eVvpK1epPkVd4XbMBmDYttoKQMNGQhU4ZkKDQJkGQ4PZkkEAIJmU7ambTTc3qgoQFCkhbSzpRJyKQEAuW0hLKEHQcKCUviBe+7jRdZlmzL2ua7SuUjbEn/b1tie+eY/9f/tvs+7nt3efc9Cd3GtGrVqkiZTFbgdDoT8adXKBTp+J0OkhIdDkec1WrVktNJMrnMIJVIW/Beb3M4qvG9SiqVNqJco81mK3z77beNt2sYklvd8csvv5wFsB5Vq1RP2O32cfFxcRatNlKmiYxUabUaeUR4OIWHhxE/IyLCqam2lrp7evHXQ+Zu/OFpMputJpO512gy29o7DEqJVHLNarXtwlj+/NZbb5XdyjEFHcDFixeHJCUlzQZnLZJIJI+HhYUqx+fmqHLHjg5PSU4icJLf8TbW1PjNd4BDm2+0UkVVtamsqrant7fXYrfZPnFKJPvq6upO79692+63gRFmBg3AN954Q9bR0bEiVK3+96SkBOXkiePjRmdlyQHgkEgWAnBgY0CQqmsbLCXlFS3NrW32vr6+n2m12g9Aj21g2UD8DjiAIFQK4Jaq1aqfzp5RoLp/9sx0IS7zN5ChAujZFpYK+tvla1WXrn7da7FYfrZ169aPke/0LDPS94ACuGbNmscgCH45YXyu6tGHHkjF+0jpo5EA6O68z2qlU19cqCmvrDFa+vpe2bJlyyF33kifAQEQwM0BWFtSU5JjHlu4IBECYaR09dcPBIDuxrpMJjp2urCh6UZrGyT56s2bNx935w33OSIAWUAkJiZuCwkJWfnkkn+WjR2TPVw6fNYLJIDuTipqaung0RNWqEq/bWhoWDMSQTNsAFmHUyqVf1IqFXkrn16mjouNcdMX0GcwAGQC2zoMtP+zI92W3t7CLrP5H7dv3949HMKHBSCmLCu8x5IT9AnLnlyiQhpO3646vQ47GWxW6rRbqamvl2r6eqjF2uv63edwUlFpCakkUoqTKylSrqAMdTilKUNJK1OQDr/DQmTD79tioQN/PdYLMCugk86HDlk/1MaGDCA47z6sdyfzpk1RLXjoARl0u6H26Spfb+mhT9rr6ZK5g8wA0eJwkN2LgKwtKhnUPveokoaQGuBlh2toiT6NxodqBpUT84El9Ylz523FZeXGvj7rfEjqi2LqucsMafSvvvrq46i46/GFjyogad1tiH7Wg7tOGlvpbFcblVvMoup5A9BbxQRVKM3WxtHcKD1lgkuHmkrLKyFgzllIIln+5ptv/lFsfdEAvvTSSxPBeYXPLn9CDWkrtn2Yr07qAYftbq+j3W315BBd89uCYgH0bHZ+XDKtTBhFGnDoUGZIY3ML7T90tAf29UxI6K882/T1HuIrw/P7unXrYuRy+cXFi74fkZ2V6Znl990O8Ha31dHW5nL6wtThZYL6re7KNLa2CRcaUKLcbKQj7U3UjnV1YriWQkQuMxGwwXUajby6rmHJ1KlTf3/+/HlBwSIIIIx/tUqpPDdrRn5y/vSpojmW17gNDSV0qLPFtcYNGKPon8MBkBtn4VRkMtD5rnYaG6ahKAgcMSlKqyEIFGWH0bhg+vS8/y4sLPRrAvq35NGjSqH4NDMjPXvunNmCZZlAtpPOGNvo5erL9FV3J3+6ranM3ElrSy7Qp5gJYm24/CmTpNAwRstlsr1CxPvlwNdee21DVJRuyYqnlirF2LPw1dEfQehvmiuo1znU1c47qcPlQM/WbFhKLhpaXTNhQriOoDp4Znt9z0xLCYHCnTR12nTNmTNnDnsthI8+AQR4MzF1331u5QowoTD783r3m5YK2tPeMGRB4Ys4/h4IAN3tF4EbqyD9Z2liBddFFj6j0lPlX5dcn5aXn3/83LlzXv1qPqclLIyPlv7gMZVarXb37/PJnPceBMUhQ7PPMndKRmFHC22o/ob6QLNQAgPRw3O/qwxVKj/0VdYrgK+//vpzyUmJCalweAolXld2QyH+C8ALqudSiBCR+UzvKUjonU2VotbEhPhYio2NTvjx+vXPeOtiEIDr16+PgB70qwUPzZd7qzDw2xkoxh+11g78fMf/3tNURfuwXotJs/OnycGvm9j+H1h+EIAQFr+YNmWySqsZVHZgXaq1dNOWpjKyifq/HFT9tn7gNfv96hIqgs4olHh/ZkxWZmioSvWLgWVvAhDWRqrT6Vg5b85sQQudCXgP0pbt2Ls1WTGGrXXFxGMRSgVTJyudEnoWRkWaZ9mbAIRXZccDc+co4KbyLDPonc2zPTDL7gQ9bxBxQ/xQbjLSbxvKXCanv6oKuZzy75ukDJFKt3mW6wfw+eef18vlsgJYG4Lc1w2uO9jZ5NnOXf3+eVsjtdr6BMcwIXesDM7jOS+++GKiu3A/gND1Fs3Mny4RozCzY6DZanG3cdc/O619tLOxUnAcUuiGk8blOOALXegu3A8gpu+ycTljI9wZvp4uPx6m772WDt2oo8pek+CwRqWlRILZlrkLugBk1QXg5sbERLu/+3ye7GoNqKXhs6PbkHG8XdgQ0MHZAEMwF5aay4PrAhC7+Qtyxo4RVs0xKHaG3qvplKFF1NAyUpOdPT09D3NhF4ChoeqnJozL0QnVZr1PrCdZqK07Mb+ux0xF3V2CpGVlpGl5yeOCUkQSKGw2+5xRGen822/aC0fBvZ7+0FQuOMSUxARy2O33M3Yyg8GQC7O3E+I5zF9NdlBeMhv8FbmlebPik2lGbCKZoH7srSqlll5B57Eo+oqMHdQFT3ZEiG9LFliRVqMxtba357LOl54QHy9oTvDWo9nh1zkrisBAFMoHcFsL5pECO3Oc5iSk0Q8/3x8Qg5I9S619FopQ+waQ+4yNibICwHQpduf1Wp1GcBuL92156/F2p39KG02bPcBjesZpo+m+GH1ASGPz7oYIHVcTHhbB2EnBjglwHAhuqvKmt7d924BQLaIR9iEz5702MZ/CZTdzB2I0qBNcE6jEwlIoIUwvkrGTQSnMCQsLc0ljf5U4YuB2pkdSRtHPp3ynf9q6aWG7/P3SK1SOtStQqaZXeM9arVJLYZGMkcI8yQoP8ys/XHRxuEWgU6Y6gqZFxvpdsJnzFgC8n0yaMQg8jk79oKKIfldyOaCkGWHaCSXEP2LP2Zklszsc8WKiRnkNDGSaqdXTz7OnkwwhviYIqB9dO04NXqZOHqatL877EOBtvFIYSLJcbbWJADBUpSTsm+ml2APVieHAQAoQ5qp1GZNc4DHFHCC0ccwMyh4Q3/JdXcIggcHlmfN2YNpuuXaBfwY89TgFlRLigCoIkWjX2gcuDDgR/hqUwpoM9Yiq4h2wZMSz/Fd2HiWrvl1OcrH9uDZz8iCBweAdqC2n94q+hFYgPFB/dPjKE3avoibo4MRSuMPUJeyFcOtcrloj/Iel+R/qiwc5MeOVanondzY9kZBNvx47izQIYfNMboHxxqXTZAvQvrNn++53tcTnbq+7iOvoBbBrl8LD2oxzF/0Zvl60Hhzjq8xQvu/FrtgnMJuYo9yJOVGDOMAfpeaSCtq+Z+JyvOZtL/4qqOBxnzEiwkD4vArOp7RIneSo7xLBgbEy/25+z8GKee8DB71T8zUdaKkaxIkD6zPn7QJ4GyAwgjVtPfuMEAFgNw792O2OWqnNaq8Rw4GpmF7BSO8BxA8brt/EiZ79uAXG5iAJDM++3O+piIAVSt04jwIBXC212my1xq4uQSNXr1D7jgMR6s1PPjspfo+dsYM3agaByHZpsAWGN9JSEKwplPioGWZGOfRoSZPB0CloZmjgnVD+3XgXanyo+Q4IlbeqLoMTS/uns4vzar+hYAuMgbRy4FGsXDjm22gyQbNzNLE3pqrD0MmGpN/9EB3szzCpjHhHLhiJI6h21pdSdU8XVJlwquwx0mmEYART2nobhxyKfaxCeL3HwUcbNuCaZJjHl5qaW1S8ULMU9JU4qHsyAhUPI2AyWMkKwXLkNm9YjY7Q+jUteeyMVVtbuxJ+hItSPmsL4K7W1AnvtC3S9W+HBgvD297uioRMQRoaW25AhZFe27hxY5fLEsGm0v9eufq1oDadDishXYSEEqTgDi3Akf5ijkuUXK8w4MzdTh6GC0A89wFAUfbcrAjhrc87FB9Bsmbr4gTLcIGy6moJjtF+yu8uABHS3wh1pgZrIX/zm+bA/XSzjeC3+F2VOQ9nTITSjbZ2XgPL3333XVdsi5sDCacXd4ELBff0kqAPLrwH18K5MYmUAekvlK5XVJpwoul9d7l+AIHq3i+vXBX2JKLm0pgUih1g6LsbvBufkVDRnhEhPHhsxWWVmL3fTl/+3Q8gDiGXWCx9FyurqgVBDINKM18Tz/XviXQ/pi4fZhRKdU3NmKjWM+7py+X7AeQfCFdY9/GefZ3QsPmnz8T64g9ikmmCWjiK1Wcjd0hGRmgEPZeU7VcHZlLZMjp8/JQBz1WepN8EIE4qXuvu7j7wxfm/Ca6FchxBXaUfReFBMu88iQzWO49hTWoO9lpugsFrd1e+KTb19FoObNq0qdqzwKCaEon0x4ePnbDhkgbPcl7fk6ETvqDPupmNvZa88z6yzfVMajblhAnPItxJQ19cuszT998GjmSQRnL27FlzQUFBOK4PmTQmO0vQqmbFmmOMr8F2DUYK5EEbT/oe06fRcn2G5yef76cKzxtxN80myInPBhYaxIFcAFLmV5e+vGxvbxe31/rD6BSaB/3Qa2MDe7zNv5nz8rWxkLqjRFHSaeyikorKXj764a3CIA7kQjjmaZk2bZqxuOT6d/KmT1HB7e+tbv83Dn3NwyYQxxkHOvwt0ByYp4uln2SMd5147x+AjxeARrsPHOzE2veKr5PsPpGJioraDkfryZ0fftTJ3gehJMOC/GL8KFoek0o45ixU/JbnM0U8bX+aPkEUeDzmfZ8dwW6H+TRuPtrpi2CvHMiFjx8/7sRauL+ry7zI0NmpzRkz+uaAFC8tMieyMZ4Ma+UCDlgHIpYmEBzI/7ksMJ7WZ4o6qclDO3r6XE9tQ+N13ML0IExdn1EFPgHkRviw8YwZM/a23Ghdji288LTUZFGsxbdqFEREUQ0iDVpswtKc+/KVRgog63n/mTWJ5uA+BbHp4pVrtqtFJa3wUs3csWOHX5XOL4DcIY55dufn5x+oqq55NkEfr4yJFueN4WtJ5mqYaCc1Iq6G700YThougGyePRCbRP+RMYH0CkFlop+08uoaOnnuvBnXRRVs27ZN0EkqCCC3DE5sxx0CnxeXlj2VmZnOd/31d+jvhaf0REzpuTify9GPpb1d/BhSGg6A82KS6HUIirna+P7wETGdNsAb9efDn1uw/v0DVJZrYuqIApAbgmSuy8vLK/ryqyuLY6OjJfFxAEVEYrOPtwOmQEqzK4w3p8x2G3WIDFYSCyA7Qx8Bx61Oy6FH4VkJH+KNHdcrq+gvR45DpXUuxgU8x0QMzVVE1Jrm2djatWsn4S6Bo1Pvm6R9+MHvQcPxKcg9qw165yDG/2urhbAxkAXBPLyp5I07fV17wrtnvAE0OkJHy/TpNDFMO6gPMR/Yxj1z/qK9qLTsBrzMc9ipIqaeu8yQAeSKL7zwQrRGE/lZtC5q4tPLlio4Umm4qcdupxsQNG3QIZsRBVtn7aFWhNgawaUcvVBc8u3VT7HwloQjYoA3vXnflrceeffMXzC4EE0AjP506GhfZ5fxgtnc88hw7mIdFoBMGN/clp2d9TY48Nl/WfGU8m67fKzd0Ak97zCY3/HOLzdseAVD8jYBhP4PRq7x4pjYk1g3frd08SLl2NHZgh0OtUAwbm+rgKQ9fOJMD3bWnsDO2r6h0uRZXrQQ8azk+Y4rQa5CuHxQV18/trj4elRiYoJaTMCmZxv+3k2dgbt7phW2/V9PnGkvraw8CfPsYbimCv31LSZv2FPYW+OrV68eHxkR8T96fVw6bnaLjosVJ6m9teX+FggOZODOnr/Y1tphqEYg1Qr2e7rbH+kzoAC6icF1Ud/Dtv2OlJRkzSMPztONBMiRAMiXLELCGlpa21ohMP4V6skRN42BegYFQCaOb/PFMbIlCH/4NS71Uk0YlxuWmzNGnpigF3Sfew5uKACyA4C3HbHGWcuqasxmc3cftmvXwpbdhTaHJSQ8afH2HjQA3Z2xtE5OTp4Jab0Q9vRiKNYxY0dn2cfl5rhuguO7CPwlIQDhJaaahkbCDb2dFTU1CglJWnAR9x5oxPtxP+qZkdyP6o8ud17QAXR35H7yrRcIaPq+QiFfjFOiBbgdyBEdHaXQabUSjSaC8CRcCc+H+Uin07quQYZLiRCPRwgpc/2ZzN0EP5PTYOzCFq1VEhIiPYsdxY9xRd/BgXsW7n6D9bzlAHoOhLkzNTU1GbuAqTqdbgLOjGTbHPYMh82eDGD0uD07BmEAhGWgFaeCmmQh0jrEJVc57c5SXE93DdxcU19fXxtsLvOkeeD7/wMnw7vu4cxBAAAAAABJRU5ErkJggg=="

/***/ }),

/***/ "./src/Icon/facebook-login.png":
/*!*************************************!*\
  !*** ./src/Icon/facebook-login.png ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAAAXNSR0IArs4c6QAAAcFJREFUSA3tUztLA0EQnrm9KL6C2igW8QFCwEYIJhLEwlIQhDOtjYiFfyIgWNpYiFgEtBIhsbE2bYpYCEFiI6IkaiNiJOa149zhJlukuISky8Hdzs3ufN/ONzMYjsQIuvgYXcR2oLtOYLadAVEJEN8J6BEBv0ACEBKyvQYIowq3XYJcTdJRkcyL+8T2hwIDiBrhiO8OANsn4I6oINFJIftynMlEyw3w5lbLGSDBZ4WqiQZ41FjcnPaihzzmDwogMFmi+tMyAQf/yhI9KYSlLd+yCXTABZiCQfYizKg9e22dgIggBxUFwtcNMugKX7pP+fS1A21K9iU1UXR4txkQfHMTZu1QbsR8eiRfn35J8CYQ0kBkslMgop+PDSgaVxJx4IOUNcsOksKsQjJaVQAFo3jtheFbKcs8BcYkA17yfMypfZcEVE7Fd19VkL5mrvYL/G+/ELLOxjkJoe93oAYanDQnCLAuj73jKgMe/7GQFVt3oEgWU/GdJNtOHYIb57NSVPxCGCw/rHK5h5xz/x93BAgLXMgbJ4bwORA4nU+n95xWFf1VS6BxyG3k0YGV3VmJFKq29gg0MZqbPYma66J5uy7RH3DGgryJHslmAAAAAElFTkSuQmCC"

/***/ }),

/***/ "./src/Icon/facebook.png":
/*!*******************************!*\
  !*** ./src/Icon/facebook.png ***!
  \*******************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAAAXNSR0IArs4c6QAAAaBJREFUWAljYACC////swBxARBfAeLfQEwrADIbZAfILhaQ3TDLN9LKRjzmguxkYQQSBUB39INdQ3+iEOSAK0B7telvN9jGKyAH/AYyIfFBf1f8ATngPxXs/QM04xQQ3wNiEBsdmAEFtNAFwXyQAygEq4H65bEaDhUEyvfjsoPSoJ8CtCOPkZGR7FBkwudyAnJXgfJFlFgOMp8SB0wFWg5KwBQBShxwjCKboZopSQMf0R0ATGj6QDFshZoKuloYnxIHYEt4AkCDHWGGE0NTEgXEmE9QzZByQDbQO0JI+DEW7x1FkkdWuwqLWrAQKWngKzDbvcdlEEgcKA8qhjHUABOnMi59NI8CoOWsQMtVB8wBQItlgZh3IB0AqgUZcTmAlDRgBAxO5MJnFzDOvyEbDJQXAfJtkMWAbF80PgqXkvaAAtABD5FNAzrAHsg/gCxGiE3zRDjqgNEQICYEsLViCemjlvwfUC64SS3TyDDnBsgBc8jQSC0tcynpnGL0BUAFERATC8CdUyZoFRoM9BKokwpqatMyTYDMBvVFC4E4GGQ3AGKgDdyqCHbdAAAAAElFTkSuQmCC"

/***/ }),

/***/ "./src/Icon/funding.png":
/*!******************************!*\
  !*** ./src/Icon/funding.png ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAC8AAAAvCAYAAABzJ5OsAAAAAXNSR0IArs4c6QAABeBJREFUaAXVmWuIVVUUgB3HV2lqvrIEFbVMI53AqCgKf5QlIZFRmr0kKSTpBWL9iSiKrKQpqD+R2GDag6QsCVJULLJQbKKSih6jyRhaNpOPfDRz+77rPac9d8Z7zz3HRl3wcfZjrb3XWWeftfe5t0uXjJLL5cbDbVCdcajONcfhi2ETbIEzoCoJx8vLqrQD4eREbN+BMbADdkIOksghlJbCkqqqqiNJDDrS6dZRY8K2QehF9l0p/5PQTrXe8DwchtchlWSJvGv8KlgCB+F6aIIk0g+l12A/kb8uiUFHOlHkOuor2cakLSisZflM5zqD+g8lDYJObA5Q3QVnBs0npuhLWsnM6FfDRJhUiV2xburIhwMR9aQvamjWk0r3sOGUKBPxPvA2fJzF4eMS+RQO+ILXQo8UtrFJfq0SAQcx9ZnyTmZxee5jmTbrpDviVK4vwCgbAvHGvBkNWoP2ziqaijua201tOSzQ+a0U9sDLEG4046g/DB/BCuhMGchkj4K+LS6a2Aw1D2Z3wflWeALaLBnqHrb2ww1Fxv97lTkHwGqoh2K/htGmz/N8YdvlaDpMYzrtRrIOEgu2vjvnwhAwFbo+G+DHClKqNhthAbgDr4JQjvrMZEqbyFO/D/bC3NCiVBnd7jAfPGH+Bs3gGH/Az/AeTIJ2wepoXPTGwe/wKQyPdCgbeWWey0bJO891EDwEh+FdGBAZHeuKju/NRfAl6KzOL4QZMA3uh/ehEXbBXPDJlhX0boU/4UM4D3pAO+dX0fgsfAZ/wWKI77bULOjVwFbYDjOh3XmFtm4wAV6FJqiF00uNG/Wh55g/wQ5YCo+BEkf+aDWX+47CtMiw3BVdndoMOu4aLyno9II6+BvuLqkcdKJrtN8o2HHJS+z8W1QvgdMCm7JF9F1i+2BWqEzdpXQjGOk2S4S6mcQl9gm0e0rhOMVl9IfD7aDEzufXfLFyqTrGOmHUv7Ic6lLvCs+A0ifss0zbVPD9mFzcV66OTbzm2+TQcoZF/aOpnwPrSYFucjql0+6MEmUVl5ZHYNsi2UyhEUyDqSXLwWwws7rM6oPZ3RsuBx2/otD+JFc/97y5Wm70V4p+w26H8ZBasjgfbXAHgtmvpHxvoR6NPSfoX0ZZ51vBG+oFqSXLsnEX1IFRwezzKXsu8am8WGgfUWizfUuhzZ3X7OSNpJYszjcwaxNMZjnkMwpL4gj4JCQ65B2wrYCnRGUYjIGKjh4ahpLaeZzZxkCbwN9vJoSDJig/iM5+WJNA99gqRE35Bdy97gEjkkjQvQAaYRm0SYnUh8KFEGUdX1gzj1v+QXgOEn1JodcXrgXT7zpQ4jzv9v41uPN9A3cm8h4ldO8At/wVcHYpO/pnwQ6oh5GldKM+9cBNdA/shjWgxM7nNykapoC73yE7IcoY0Vjtruh4mnwaPPt/D7eAUe8P/cDDXg3UgRF3fPeHsoKetl+AwVkEAyHepIycknfe0SiPhg9gO0S5uuRE6PUED1CrwZ2zATYU6jrbDI63EMw+ZQU9b94Do09qZmRA+djOq4TCWfAtrI6MklzRN9KXgic/H/VKeAWip5FojRd8uAs7n9TjUB3NT7mN8600xJEvGHqwegQ0Npt0qjBnb1gOfoy4Z8RCXef1Of8ZuJceH+VgGo7EWny2UTZfe4jKtJkEYyYtDkXxfNgALcwfHvxqbIODRngRBb/GT5Wf3nL42gA36XxfCr4QY+FUEFfDKjbJjeEGUuluq60YCeksyeF4tvl4YtdAPUzpLK+L56k02qG9v+msB68nROJlU+nsRFxb829L5sdY6eQF/SyRv4wx3gSviYWbHulSg8zZLYvzQ/D4avBsnkgKDvvBUgc3U88yf/xXZKLJi5R2UzdtPYATbip+2iURv8D8PfMl2AlrIZVkWfM9mdGPilnQv4LZjbanStPdSpjOO5P0xlH/T1I77xBE3COzUfdGkoiOzwZ//TXic3B8G9eTX1zz8BR8DmNPfo+LPMRpz/4jippTVf8FHqIjWceYP7MAAAAASUVORK5CYII="

/***/ }),

/***/ "./src/Icon/group-2.png":
/*!******************************!*\
  !*** ./src/Icon/group-2.png ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "static/media/group-2.6f8415d7.png";

/***/ }),

/***/ "./src/Icon/icon-google.png":
/*!**********************************!*\
  !*** ./src/Icon/icon-google.png ***!
  \**********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAAAXNSR0IArs4c6QAAA4NJREFUSA2dVmlIVFEUPue9l6OSMzkpEeZIJFEkbS5FRYVNStICUWlGPyL6kf3IFPoRJkYRCNLyq4KokHBIKQPBykb/VLQMAxFOphVEaaRtOi0ub7md+/BNb944bhcO95zvfPecu573EMZpP9yZLkET9yLiBgZsKVGdCMCo7wOE19R7VVBvO73+j9HCED+y/cxdkyaAcpaCFJFXiGSEITJZHkVkFbNbfJ/CPGREJBhw5xTTHC+Rz24lT2D3M4aFs9qet5h5YbMLbsopo+A3pxGcx3ys2uCZOTjXRQMY2LSKb8cVkohVjXJU6vtIFJLYUYx3DBlW29e/OBR/tWfIhOuqHux7XnaqpCI/tJlWAtk+ilAjD8bcS37y5Bf3BzdmJoEgbGWIJTSdCw7vi7oxxumQnuD34fRatcu530JigKzSvs53FqtAs/gmbeLgA5gvCfBW8c8RR1pdANroDiE76fD6zkw6UhSiICHsIZ8oZfaCrbATMEbl187HZx5lzJRgifYw1xghpgUh9kBAUtpc56Jty87zIysRNKcxZrxeZUJQIkKGmYSJQ1rc9q5muGxG/+sa06ppE93/keiaANo7/g5mWyh9WABBCzZdcy5PwGuLuYU9PrNjqjoFVnmw75aBSewhOCzYdM0efgbtJClGhGEQxBuqayfAh+sGZu4FZLfogfjNmK4zyKY+14J/QNkLx2mTqrmjl9mgVFnOOlT7y4B/bRZUVU3qge2uZ6LcPeyjw18RloDBUUGRoZ5A9RVzwN6R1dCuOVBFXLEo62lFGHkcQ+keqowITjVLA61Rf7ZX76fVXlQWFsuAoeJH8Wgn4NQb35rT0VbCZ86DU30ksTSG1xrLbQf1BOnNW+ZJmsqLXYKFxk0/kWpkQWx+V3BPv75L7uc7aeU7YpSkI67PVZkxw6nWYf2SyJY1HI37qCfg3sVNeYUMwUNqCLOM4uX6K4mNJNHwCVo8pHwph5m/+RnrjVaOuxqP2e5wK7Ql3zzvA8nF6QOE5ZOMlYRfaV7O40hCjaEMvxIeUeGdAfGDi+nTjaWNZbE3DEIoAQe+1b1/lly8oJPUzSTmjwp3j9v+xgf+aEwtaSnJCSsyY80UljS5XQrgGfqb2EdRJ3rZdDfAo2nKia5tbT3WWYyZwCBl3M1LlWdAETK2gb5eS5FBEvn42+ilS9ZBE2iVUWygw+82xlj7fxOTIvd5EwyrAAAAAElFTkSuQmCC"

/***/ }),

/***/ "./src/Icon/instagram.png":
/*!********************************!*\
  !*** ./src/Icon/instagram.png ***!
  \********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAAAXNSR0IArs4c6QAAArZJREFUWAnFl7trFEEcx7O5XEwIRIKkUQQfKFFby9jFRlHQ+hA7CwOe/4Ah9moh2BwiCgEbGwM+sIuWPkATRSOk0BgLsZCI5uX5+W5mjt29cWcO3LsffHZmfjO/x87OzO52dSH1er0EVZiFNShK5FsxFKuk2Db4dFERc/wqZiniUiWPa3E27b9UlcAscQ8VGPsrvj+aGEOZOHN6BEU+86v4Lyso5QDchaSsqaMoeYfjblDg09Bj6t8pG9KdmZL/2XweRdEfHJ6De3CS9k/KOWhIT6PWWmWR4TOwAAqyE0ZhL1g5aCpTlHV4zG3rcewz+s2iMRdhldcMOw5RyolpoB+FGbBy0Y5DocehNZES7QJlFyI1Bo0zjauY7KB+Cg6ADpR5mKbvA31K7hJMgOov4S0chhFIixIIkFuyYlwvXIEVh80Gujuw1YydcIxpUoXsgnms+qEMj5o8NCteoRoEze7T5u60JiSBirmjy2nT3NZtY3MkdxSdvjWwjKNh6IUvMAAhop0xwprQ7OkUTO6OlL3vHHiBk99YHIXQ4AogvydUQZ5tFu6rL4FPxiy9d92+str9RvE525Fs+xKw/ZrSVmXDGDjPDOvMBrDtbLnLKLSPWxVrszvP0LcIVzDWIlwHPY5tECKrDNoDSyC77eAU3wxsweoMC/EX5aTTg1t5HZtFuo7BP4PHpr59Sv8SDIFm6yb4RIeVTkzxxjc45CCSjwegk1BJjMM3yMoyikko684oa9kBrrZvDcSzZC4PKStMrT4o+qiPQfJl9IS+H/T1o78BZ8EvrqxydHoc52Ew6xldH1RAp1+wtDIDyZjaHXrNLoD9INHrtpXTkuG8r5VqXOvQxbcNC09LCeiQ6ZSsK4H3nYqu2EpA33qdkpoODP0Z39dibLPEP6fxnRNYSVyAdv2eK1ZJwf8CV0xl8fQcSL0AAAAASUVORK5CYII="

/***/ }),

/***/ "./src/Icon/loan.png":
/*!***************************!*\
  !*** ./src/Icon/loan.png ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAHgAAAB4CAYAAAA5ZDbSAAAAAXNSR0IArs4c6QAAFcJJREFUeAHtXWlwHMd1frP3Lnaxi5u4CYIEL1EiqZCSLMe2pEiWdVh2LFmJYqms/FBUlUolKVU5/pGU9SdVqSS/kh+pUqKKnajsKLFLtyXLskzRpHVSp0XwBk9ci2ux9zWT7w0Icnc5e8xuz2ABblcB2J2Zfv36fd2vX7/3eiCR4HLvE0+2OxyWJ4ik7wsmLYRc8PRhIXREE5Esth9LkuMH+555+oRI2haRxBq06k8CDYDrDxOhHDUAFirO+iPWALj+MBHKUQNgoeKsP2INgOsPE6EcNQAWKs76I9YAuP4wEcpRA2Ch4qw/Yg2A6w8ToRw1ABYqzvoj1gC4/jARylEDYKHirD9iNtEsjYfHQ+t9PT9SbPYDWrQf+epXvtfc5LkZ96xa942+dvT4JqObKEo/k8lceOGt/X+v+YAij2aioUnNezVclGqoW1XVYDD0E0mi+1FZ+OCqhKGJiYlKHjPkGZvN9unWrRuvM4R4EaINFV1EMGvlsukAWywWRZIkZa0IUF8/FNP7bTrAiiLL+oSydp5WFClrdm9MBxjwZhXzB7LZci3SnpIucsOwy6YDDPWcMqw39U84aTaLKwCwEkMnTV+LzBasVnvYPYS0rht5zXSAgW0YGvqqBFiWlaCRYGrRNh1gSbLMg5GrEmAM7HEtEIy8ZjrA6OQMVNVVaUlbrZYFI8HUom06wFBTU5jAV+UMhg9gUQsEI6+ZDjBG8Tl06KqcwQ6H7ayRYGrRNh3gublJACxltJhZ+9eyn5jdR+nfT1r/28hGP5vcTfvP3EahREBtBvtg6blbH/pGb1Nzk5HtFqO9UsGGjETKA++/uF+WFFV7NdnDyg19v4ne0HfA0K2TTSLpwWLCEHE9q0YF0Qosq+VyJDRjA8DLX6+KvxPJCGVJ+eLy/gF4k92SVSAZQ+0RhOwku5ESTmcclJXzQ7+jC9N0W88GI5utO9qH5qd4iF8ShKxYKJ3liOnlgW8E04auwTw0Exk3ZeT80O/nAPhq80efT0Ty8GOZxNLGr1LGAgwFFEl7KSU78jp3ODRNSfnqsbNk7Ao/WMhPNGCAIymv4RtGQwGOpz0UUztxSTOpQMczGfp0Dtvhq6RMpeM0n86PM8iKlaIY/HFoOCNLvu4U3NJ8vIWiKW01tH9yjHa3NJM1x/gS3LwmOSnLnlJzy9tz0xoNShSHip6Lt5PHfk7jvphLhgHMvqqZWCeFU35NTg9MjdGjLcfIbxgHms2SO2qcMLVazGCZ+jg0pHULg99L05F11Nt8zjBTyzDxprN2mo6uUzuh1bvxeJSOnX2JrneZ6383ViFe2dNTqQAdj7TgRr4dwk+ykTUZ6aYMrGm71RibxLA1OBjrUgHOKsXGkEQvx7aQbOgu8EqBm3mFu/ZafDOlsN5qFZZNEJOANZ1RxRCAs7KFLiz2g/muknx/mOyls5klD1fJB1fpzZmMh95N9MPBUVzMs1iDWVYsMyOKIVRDiRY6g3Unjg6WKiHZRa/GRko9sqrv7U8M0WTWB59VcWcG+wlOL2yg+XibIX0VDnAGXqtziwN0PjQIhot3jHuTwcg+ACGMpdfeLA5icP8qvpHiSjlHoUTnQusB8tBFz5ZYnIUCzJbzbKydPp++ruzsXeqGhBHupRei25BqKbZjK0mNu/J6fBOdTrcgLlpexKzpRoPXqjaL6Eh5+dZ1SIodG6PBHRiR5WfvMtkkDI13EgN0KNG3fGnV/z2Waqe34hsorDgr7ItE4+E+OgzZRVK+CutU9pgwgFk1n14Ypg8n9mI2llNLucxJNIF16qfRa2gua/YmJpcPMZ/jcEE+B410Mt1acu0tbI0t6k8RWj0xN0IpbDFFFSEAy9jMT4R76M2xO9Tggl7m2Mr8JNVN/xO5ltKgtVoLq9eXolvo7cQgJas4W5fMuujAmVtUDSjKqq4Z4OV19+Wj36LFJG/oqwFIoijU2S9jm+gVCGi1Lse/xVLzcmwrzcrsnq1ODmE4Rl47/nWaiPTAR1ANjfzpURPAzMAUXG0/O/wQzSfa8ylX8S0oe+n56Hb6INELNV9756pgoaoq7Kw5gnX3/yI76FSm9u1OKNlKLxx5ADuR2vfH1q//pfVJvb3iGZbOOugMTPvnR/+ImCFRZU520xQs6x7bInVYY2QxNuGhZrZ5IJ7JtNDT4b30TnIA9MQMzCQsa16PW1zz1OwMISiTRVaMfnZ1A8xrw2KymT6CMfWLE/dSMlvamaGfJd46+Wg646VOa4TaLTGkQcBVUEXn9Letr0YGWRkn0230TGQXvZXYgMpimUzLTjo+u1X1hDHQDmtK94CX/uOkvaIlj4Hl+OUkjKkPxm/E7N0Asdek4ctKc6t9ih7yfkx7XefJI6XrBmS2O1IYdp8nu1TD8LfJ9WX7UusDfb4ztKfvIPX4LpDXESarpbLM47IAc95QONUMv3InRtMWOja77aKlLHa0FhNAn3WB7vd+Rl92j1EbZrPFnGaLsaNmYEQUh+pjfhZW/2i6tL+9KCHdNzhJL0UjbaM00n6YOpumyQfVbbeUHvhXAKyOzqxTDfOFkgHVM8WOizOhDRdziMyXsE9K0J2eo/g5Thvts1iPKlI6ukVYSYULGZ/qgnwOxmAQtoL5RSGXLU4D/jHq959WgW52LlCTI6KpwqW/PbRZhvqV0rKdkmk3xbC4h5N+NY95Jt5Biwm/TseFMV22wOm30zlBd3tG6RbMZofJh+V54L+X7MM+dysdTAxSuop9rmjJWHF+gMFt88yQH2u0z7EIoKPkxADgma0aZjf/9C9UgDkJLJVxUQKb7QzAFm0wiOpcjzVEd7hP0De9n1MbrGwzSgTy+EV0hF6JY4lKd5jRZBVtKGQDqC5bgpzWBNmsDLBMtolwn/k6twr2l6uMZ/30v9EdcG966RHfhzRoN/RgAHFUiD1sbyB4sOTAWOak3v5KmJgO+LLxQ5cPFRRLt6g37vP4icHIeTM+THHMrD8PvE29tnDefVFf5qDN/jN8vbrmRtHmaizCAWZLbyBwioYCJ3XJg20AjiGfnN9cUT0oIKyJ/dQajtFfBQ6STbDhxWvuC5FttA9RoSVwK1N0g/4TNBiAjYA9q57ChuzYwkYEGlx6qpV9VjjAVktG3avt7D5UtvHcBxJY/3ntrxRgthGw0tAhGD6jqQ7a4dRKTc1tQd/nM0glejs5iJAfC7wycLmFLu8kXdP1MVJh47oa5KMs5xbXA2Bd1co+LBxgPlTF1p3eEcwd5Hr6ikSLCFIcTnUJB/gEPFTsNi2VbqPFqwWGjQMGjt7+2zAxWHaii7GuKNHcatBLI2NxThbtLiUKw03ItFd7WfUAk8VDku/3SHGuF4aFLAUo495DinX154qteoDtFiu1BXaTrfeviezdNYHMClKWvBTzfYfs7r1YMsRlVtTEWA2Vha/BbJDwwarCI6PleOTnuZ7e4rM7aVtLD1makWzQ/Rhlx/8V6Zpzesmoq58ieSje9E1Kur9Eg2DFjzNDwZQ+Z4oCW4L7orf/WfRd73pfSSeFA8y5WXwc47Mpfa9FziAPiZMH9BQnZu/Otm66pqWLJIuFLIFbSUmNkxx8liir74U2CjkB7O2U9NxBZPHRAIznXc2dNJmIUiRb+Ssmg7EOOhLcjqMo+rZJE+Fe3YOiElkJB5hjmMdmt6s/lTBQ7TM2yUK72nro0U274HddWmkkm5+s7X+IGbxI8tzL0LfRisgrODeUdu0BuHdj3b2cmXJP5waaScXpnfkJOFUqs/BPYR/PP/VShANsRsdcVhvd2NFHj2/eS/1N+YaQ5OgmS8eDpGTDpIR+DZBL70fhwaW0AyFQz32UtffnsR+A+n+4dxuxpngbIIcy+Wd88x6u0y/WDX+8+++wZ7Owo3pp/eTZUPnG3ux+dbma6Gt9I/T4lr3U59U+mirZW/DmkU6o6wlE5ifBorb3ADt2ytiHKeH9NmVcuzS74sFg2u5rg5bAdiydAMj6VK8mUYMuWuBHcCLY4LFH8RPhEKIife/dazNYN63sReLT5jEkr6vhQsSC53BKgY84Gp25UUl/LRh0u7He3juwhW7rHoYjobRBxu8AUSIfUebCP+Oc5hE0kZ8BoVrM1i6KNv8ZpZ03YEyX3lDw858sBun1mdP03vwk9sj59Crpg+hnLAiZ+hwhakW4MICwoc+5qILLESWesPAqKraR9tFLkmL/K1tzfCqfD27Px1vVE4KcdT+JNM4UEgFWYnZ7bQ76g55huqd/M21v6cT2pTQYLEj1tU3enSS5RkiJwy+uFKpXC2UBcNq5pyy4Kj382tncQR1ON/W4fPTmzFmaxfpsfoE5iHBgN1J3+prPIOA/RQGOBSO7ww33aEGSopS3BnNimw2jwu9aVH96fefU9BwGmi3jU/MjdHp++OJLVcxR4wNNfvrGwFa8dmmYutxedKDydvFmW5Ks0EA8IHgKFhYJAzZfBIVPXPG9x9FE98H46nU20c+DY3QsYtYrIThlJ01DLcdpU9sR6mqapBb3rOoSLSWSPIALe8MVeVS47Reo0zuFKMlpOt86QB9N7sFJhl7DVfd1Levo20PX0I2d/cSzOPdlaoW8mvWdeWBebsbeu83hpp9Pj8HKHtccPyJ56vaep90972HWnsOMnas46a4kwLkMsjOcU0M4RaTbd55+N72T3r/wBaj0iknkkiv5mefojR399CfD19G1reugkmx1ZfYxfw4YXdu8reSz2smNz/tmz2OV11ITJbta9qYV2aQ7uz+gXevex1o7i7VV22AsRkg3OvwuiQ5k9N3U/xuExsbplyfvhiEm7kQcC+8mgPvoyPW0LdChWq/FmF/p67wXH3D76Fvdm1Sj6+AcnCwCQXbZYnTb0GuqSnbbY1WlDesGmIW6rLo3t4/CigvTS0fvx+kGuAoFlG2BTvrupt20HX9tFx0YAsgaRoINvh6nlx7oHqGEnKX3F3hbVntpds7T1za+iOSJsZpe0FLeHC3BK6uLPv9Zum/Ls+R36vf/FpIe9Abo4Y07aUdr16oAd5l/Nvz6YVkzyFu9tZ9N4q3PXZueo/UtJyGHyjxoy7wU/q0JYCbGZnlP8wW6a+R5ctsqcw0WMsHf/Q4X3Q0Hxi3rhiraBmnRWMlrDPLmpha6s3M9dTmrj0+7IMM7N72oGrR8eoG1ZS2lZoC5cQa5r/ks3brhNayZhfvN8uyxL3lvey89OLSjLizl8hwXf+JLrb10U6AHhpf+1c+Gfyn1xYF9alJ7pUdTinOydEcIwEyKja9h7NGuW3cIgOuz9Po9fvrO8E5ogNUff2WP211dQzSE/bueySfBncq5XJtxLMVpE+cOFQYwg8zHJ3Z0fky8Z9P2LPBT+cUDUO/o3UhbYTGvldLl8NDt7YPEwYrKigKP1CRt7/xUPSpaWZ3KnhIKMK8X7dhC7ej6CB6W8qqaR3g/Rjr7l9da+QIcIRs8AYQzys9jltU2gMvux1rX3EI5CgWYibOq7vefUdfkcrOYIzT39G2mDkSI1lpxoW+3dwwSa6jSRSF2Ca9Xc6krTywoTfPyXeEAM+lW9xx8pifUU3CXm7ryE/uW1/Kr/TkjpB+OkFJzmGfvehwSaPeIzetelrYhAFuxP+5HpIMd4qUKR4ja1+DsXe4zz+Jb2gbIzsGOIqXVPaNqvFX3ttk2TxCGwwT2tNpqh918dwDgtV6u93dSE4ITWoV3Gx2eKdVu0bov4lrxoVUjdQdMfT7G4YVlrVXYzzzoFePe1KJfL9da7S66pvlynlcuX274mllGzgoM0tx6ej4bBjCvOx0IRvMbYrTK768bNN4dyRkMcgK2XpEoj4z9pqJvz67Vl1LXWA57/NrZorytZC0n2nLO5ccwgLkRjls2IRihZU3f3DmYy4fwz0pqijJTPyQ5/C6a19qy4XB05hR5Fp8ia/qs8PZzCXImyJVFUfOmODJnZNHvT9PBDecG+Zxh1WG+9NaApcrrYD1zYMGIoshpgPoecqN/Qkr0M+QgaQ8w1bJVwuSMv0629DFKuO+klPvL8Lu6hLPVjK3SCJaj3OyPpXyqsJpQIbzBHIKGAsyqx4v3RvCZ4VyAt/iNifMq6RnKIuldnnsF2ZQ8M0pHYpZARoIaAG7KjJM9c0I92SDbenJEVPtH/Ps62oIoUy7AnH7DSXIFOVS1N1ZAwVCAuS1+I4wdxynjObLeCoBFFjWDMnGSshP/hlzo/VDJ2pa7VpsqyJxxqSySM/Yq1PVJnE16hDKOHQh8X8pH1Kqq69og9sO5hcOAnN5qdDF0DWbmnTjCwW97yS1bBPqdFSVDSvh9yoz9DSkLv9IFbi5PDDQ7/O1pJDEs/BM54vuKrN25tSr/vDvQlfcwnyM20npebgwzWHl1+YsRfyVJsUgWmLEXTzfz/yHd2Nz2FbRV02Kn/u9DGE8yZmz23D9UdeCsWH8t8hx5Q/9CMTmE4yy3IysT78Oq0dQNWJ1MYpTtem4XkV4ZWcs4QAXVYWBZ0lAGNlBIen5+PpDNWk7juvaxhMIKRb4r2TjJC68vgVvmeEoREmUvc8J/zPswJZvuAshQsTWC7PHYB4eHh4012Qt6ZbiKLmiP0kjtxbWa2lWwf5UX3jAUXOYbB2HJE/kvckZfw0TTd4y0sN/8XZbtO7WuG3mtJkFXw5gkWXtRr+p2FRwZURbfAbj/CInFq2FBVx1eWVSQ428B5HxbQhch9eEs993UUrWgq+VSkrLst6t6aVDiJyhznsHl/a05hY0vT/hHZEsdrqnBVMqEfxhcwKHpAMPcasNPVe0qWfyb9MmnsMc9X9AN479aYAs1hZ+mWv57KQzD/L2S8WxXJ+ha+IKKboWtUtUMzi78Gur5YC3N11TXlj6qer6qJWK1SgPV1q22XlUzqdrGluop/A5e3QAr2Rgpsy9ANWv5lWvjSE9tZ+wNbHAW9FS59KyiSNphpUtPiP9gOsBQU85qDpEp8WOkJMYggZU9l2uVg1iLR6tCAgB7qqpYQyXTAUYKtB0g65/BHDjg0N9KF1jS9uTvquICfa80zbIq+lqVTAcY2Dqq8Rfw23MIbsmVLzJZs+eqYgP9XvsAo5Pw4OuewKSosxeOvhUv8LrK1Tk9oLnKpVgK753pM1iGE7YaFS285ytCsBrdVRujpgNcG7uN2nolIDwefP1jj9nXN/X0S06r5p7vg2PHO5vdbt2GdCaIV/2FO2tehycXahvTvEjIFjfFp/V7tdLZTMvtf/r4d7VAggP2qMemfPLSU09Vp/+1iOKa/sWwCKHly/c+8WS7w2F5AqS/v3ytnv4GT+sHxgz+JYvtx5Lk+MG+Z54+IbK92oazSE4atAyRQANgQ8RaP0QbANcPFoZw0gDYELHWD9EGwPWDhSGcNAA2RKz1Q7QBcP1gYQgnDYANEWv9EG0AXD9YGMJJA2BDxFo/RBsA1w8WhnDSANgQsdYP0QbA9YOFIZz8PzOE+Zvoe4FpAAAAAElFTkSuQmCC"

/***/ }),

/***/ "./src/Icon/logo-ojk.png":
/*!*******************************!*\
  !*** ./src/Icon/logo-ojk.png ***!
  \*******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "static/media/logo-ojk.c8f6419b.png";

/***/ }),

/***/ "./src/Icon/manager-avatar@2x.png":
/*!****************************************!*\
  !*** ./src/Icon/manager-avatar@2x.png ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "static/media/manager-avatar@2x.505f5f97.png";

/***/ }),

/***/ "./src/Icon/minus-1.png":
/*!******************************!*\
  !*** ./src/Icon/minus-1.png ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFAAAABOCAYAAAC3zZFGAAAAAXNSR0IArs4c6QAAFONJREFUeAHdXHlwVVWa/+59e9aXBLInJCEECGFNZLNRdNwQl1ZhcLDVHsc/pt23Vqtr/rBrpqarpdwdq0enHKvHccpi7HbpHhdQoRUC2FEgKJAAWcmevJe373d+3328rO/e+14S1HCqXu7NPeee5Xe/823nO0egHzDdf//9GXq9fq0kSYX45RsNhjL8P08QhMJwJJIXDAat3D29XmcXBbGPJOlsKBJpw/NWURS7kdUdCoUOvPTSS44fahjC993www8/XAmwrrOYzdvD4fCSvNxcv9Waoc/MyDBbrZmG9LQ0SktLJb6mpqbQUF8/ORwO8ni95Pbgh6vL7Q66XG6fw+UODdnsJkEUjgWDobcwlj8/99xzp77PMZ13ALdu3aorKiraAMq6CZR1C0Ax1VQvNlcvqkorKS4iUJLieCORCA329lEoGFAuI0nU2z9AZ1rbXKdaO7w+n88fDIX+F22929nZ+eXOnTvDii/PQMZ5A/Cpp57S22y2O1Msll8VFRWYViyrya2qrDQwVSWaEgFwYl1AkNo6uvwnT5/p6x0YDAcCgV9brdY30Z/QxLIz8f+MA4iOigDuVrPZ/E8b1q8zX7phXbkalakNYioAjq0PrIIajn7b0nD0mB/p1y+++OLbyJfGlpnu/YwC+NBDD91sNBr+dVlNtXnzNVfNMxqN0+rfdAGMNR4IBumLg1+1n25pd/gDgcdeeOGFj2N5073OCIAAbqPJaHy+pKR47s03bi6EQJhuv+T3ZwrAWGecLhd99uWBrp7+gUFI8geef/75PbG8qV6nBSALiMLCwpd1Ot3dt23bol+0cMFU+xH3vZkGMNbImfYO+vDTvUHU/+9dXV0PTUfQTBlA1uFMBsP7Jot59d0/v92SO3dOrH+JX8GjZJbEV0hckvg3+kzCs6He3qgUxnNJ4O7iB8ktCTpc8eNn8vPEm+WSgzY7vffRbg+EzgG32339q6++6kmuhmjpKQGIKVsGqvusuKgw/47tfwuVzjyVtqNgeYZJtPeSMNhOomOAyDlAgtdJQtBPUsBHkeajJOGewZKMFgqnZlHQmkf+wkXkK1xMIWuBDOhUOuDz++mDTz7zDdpsZ8LhyFXQIc8mW0/SAILyVhoMhr+suagWguJKPfStxNtk6gp4SRzqIqHrBIl9rSQM9wEwBwl+EAADFfSREIbGEYH6xtfWJryD52hHYhD1RpIMZgqnZOCXRZFUK4WycslXupx8xUtkgJOhSJbUe+sPhU40n3ZA2FwFSd2Q+IDQrWQKP/744zeDb7y15afXm5bWVCf2KoOGnzDcC8BaSABo4kA7CQMdJOIZMbUxWPFSGM87WwGgL14ungFUvYEiaVby51ZQIG8+fuUUxL0PFEoClPQEP3DT6RYImHq/IIq379ixY6dCg5MeJwzggw8+uAxqSf1dd2xPKS0pnlRR3AdMQX43ibZuEju+I92ZBlDeSRLd9rjFJz3UBHD8GwxmMLeMvOWryF29EVO9gMJp2aBY0/iCCv91w+p57+Pd3lAovB4S+rBCsXGPwYW106OPPjoHpljD1ptuyFhQWaH9AlMdqEpw20jXcpiMn79Bum/3kNjfBt6mRE1xquV6HACbP0QCSYDQ0TmHyNR5nNJOfAECFCmUMYci5rSEqDEdNnhWZqahrfPsttra2tcPHTqkKVg0AYTxbzGZjPt/sm5tEfiesuE6doDhIIntjWTYv5MMf31f5nMQpcnxC64vSQBHuwCWgfaM3c1k6fyWSKejQE4J3Drain22NRMKgWS02Yevu2j16v88cOCA6tfTjzYa/w4C45355eVVl2/coAk2qyGCC1R37HPSNR2ICglPgtM1fvNTeirzJfRF53ORABAz6vEhwX+dtTcASLAfnUG13tUrl+mG7Paq9rNn30HBa9UKq4LyxBO//M2cnJxtd/7sVrOmPcsU5ugn3eFPSPcdpmvvaUhWt1rb2nlTpsDRqlmi68BKDBBYOgisiCVDntIszdVSxbwS3Zm2zuLauovS9+3bt1uprCKAjz322Drody//4u6fp2ratKFAFLzmg6THlJXVFEzjaacZAJD7IKAe0ecmY3+LTH3h9BwKA0g1SmT1bH5ZqeG7plN14Id7MJU74o1HkaeZTKY3t2/bkmqxWOK9N/qMpy0UYF3TQTJ++h9QiqHXKaklo2/9IHcC1KGMA+9Q2tFPyDjQFtU1VXpiNpnomssuMaemWN5UKhaXAp988sl/LCst2bLh4rXqzAK1Cq4h0h3ZRfoGCAsPe9YhOWcqzRAFju+OBPBATKCwUFZ+VPEeX2DcfyyZu3v7LSuWLz/75b59k1SbSQBCWU7Hp/nwju23ppjNJmU9kQcH/qI/8jF43l5M27OYKrBlZzKdFwCBHViOzmMjMeCBBbOUJB1kqYrCXZA3V9d4oumSVatW/Q6qDcyi0TRpCoNf/POqFSuMmZkZk/JGX8MdpqkIxViWtmxZ/Ein7bg+j/wjkR7KfQo0hfQjH5LIZqRKSktNFRZWVqSAov5lYrFxIMHaKIXH4+4rL79UXUSB8lhJ1kNdEXthnrENO8sSK916ew9lQOgZhjo1x7CubqVJEuguGBXzxg51HIDQ+X531d9shPwwjXs+9gVZuWXzrK0xapr9AHreuP5M4x/+8PreVkr77nPSO/qiirtCfQa9XlizcrkJ0vmlsUVGgLrnnnt4XXYtrA0wBJXEKgHI3wBpRlANZnsSoG6l//VPUHG0pfLS6kWwaHUb77vvvsLYuEcAhK5308XrV0taCrMw3APHwLckcIMJ2qixxn6UVyYIaBIpzfVk6j2l2kURgmZlzZIIULwxVnAsgNtrFi/OjmXEvXJjcEfJXhX2282kyhK3wfP/kNUM5ofmlq/J2NOsOo25NxWlxZmYqbfFeiYDyKoLwK2eMycn9jz+FYoo+/OELjg5L7BkgG5o7GsjER5ytZQFZwP4YPUTTzyRyeVkALGav3nxwqqA2otyYeh67AxN2J+nVeGPKF/23sB+t7Qf0exVeVmJ5PV6N8mY8B9I3e3Ll9bkar3JzlD2JF+oyQiPTQqmslaaX1qSbTJFp7GISAJjJBy+bH5FubrVIfO/lqgbXquFWZqvgzDRD2JdCTwxujoYfyClRYXsubuUsRPtdjvzvj6dTlQGkIUFeAMvAPEaxoWaBAQxifBnGuxdqo4GrEgSLLVhxo55YFl+Xp66n52pj5ceefVsVplsyX5qjNPvIhOzKg13XO6cHDa/ykSssuVnZ2di0UAlgaR53VZeelQpdiFkifBimzsaZYeD2ngy0tIyGTsR5FiQnWm1qhXmCS8O98trtqrlLoBMEeadwd6Nmaa6FELpqSmZelEsFLFgtNiSlqruPMAUJucgANTUdGY/hJi6Oh4rCxKVZLGYYZDoF3JkQSXIccQiif8OVrl8WADX4Avx343zlP1vZcuJ5sKxYUmHKRC/eQmUH7YNkSSbjPiIExM+rOCyw632DQltMC/h55tuYh7P7n8t36bsqRfF+XrEhOSlpabEH0GsN9xRdlnNhADhgCCsSdAq6KHz63AP65GfxUtoNxQIUETBUcsmmGDrId3B92WvCrmmD6AcRRHWrifFbAJnAw9EoHdWmiYFYnT8dRUGEm/sis9MqUSlNUQ1lxEhioB4cYefxf2lqHqK4fmgSHYBhTZsI8mMOmYiMbtiiuerSoLxge8XyZEpDzcqRWc6K7EOJtPqD+LUOAewHlLY5nA4c3NystWnMeJOlHhVMoMlv5fo7Emio58Sla8gSstSrzcC9oEG4tED8ythsIv0h7Cg5XUl1Q3Fwrw2wjxablWxFHl9fgnYDQFAsdfpdmP9XMWThUo5pEyRVym3MzmH1QMswFPDn4jajqoKEWYZeqcTQkQheksCgA6sCp76Sg5imtzYFJ7wWHVQSlTsMq6V96vAd9qvRxxIl9PpXKzeFCo1pcmrVxr1qlcTy2VhhCUB+Rd7FucqADh9Z6tKeFucl6b7iGMQTSmIgFWfkG6Ph7CbqoOFSLvT4Yw3Q0a7wmTN0lKfWJjY6Iuz705C3EyItQSVDUA8Ko/Hh4kRbhURrd7ucLrUbWF8jUgmvF0JxtnNPthGexzBGENZBXI07OjTyXdge14I39Nw8ws9QzabOoAc1I2oJolVjQs8RTBGX3GNHEqsNlSn08UA9vBEb7UPO9QjgTCFI1nwgaVmRlfx1WqezXk8TuilvqLqqCBRGQsoMAwh0sM88Ov+gYFMBFur8EHwwBSAh2hPWeFVqXg2Z3HIWwREwqHBahoHQ4VdoilYyWwQea+tQEJje0en8thZiPDXmVsGXpinXG6W54TliP/8qADhMSuknr5+BHAIR59++mmnLKuxf+ytw0ePwQWhnqTCKpkXqpeavbmB3HLylNdqDuDk6RYbtkT8NxeMKTvvNjZ+G7tXrCCSXQwqLKUIpvOFljjCn7dJ+EqXaQ6t6UyLiG20f+SCMmgI6e8OhsMdPQjzV02sYOZVkFSEPRgXWArOKQGAFdgWAR1QJfUPDrFdeeqVV17p4WIjVIezB94+cvSYTeXdc3xwHoXhy5N3DKkWnh2ZLDnZ6vDPW0ZBbNJRixPkETWfbnFh+r4eG90IgJAsf/jmSKO6OsON8eYVBCVGcorOGd2xqmbrFWYqnLruhT8hX4H2zDpxugUiIzp9ecQjAGIT8knsXGxoaW1T9yZCOklWhMauueWCUGmY9znrrqfAnFJQ3wgccamhs6c3GAwE9sWm7zgA+R8g+/jb77xrh4at7CBk8Y4vFoYrKly2gqRZLFCYDYVyS8m1ZCN2NMFUVVFdMNWlXXu+sAOYB8aiO86XfvDgwb66urpFZpO5vLSkyDK24Lh7NrTh3uLtpyKiPDla9bysF7Nun8RWr3F91PiHvdlBmKeOtVvIW3GR7IFRe+XIse9cLe2df3z22Wd/P7bcJJqFefKrXZ/tIRzSoEyFXAPcPpGKVRSuqCWJzTwN8h/b6I/hPpwxl3zov2P5JoqYlGmF+4rNh9LBb46E4Hh5cmLfx1EgZ+7fv9+9bt26dI/Hs3RR1QLlmpnc2XcGzwUvuAscaJ7MRsKJPYn3/3miQMloJveyK2kY1BdmL5PK1OVu7a3/yoGzaZ6BnPhoYjcnUSAXAC/c8fXhRmloSF2r4bISmz9LL6cQOjMr/IWYKc6Vm8i5/JroBkQehEoadjgJirMPat6OeMUmUSAX4r0Q4IWO4yea1q++qNaiU3Mu8tQFL5SwtiFhO76AGEJ517mabyJeT+I9m2EK5GUJF5ZTXQxewYLoMkW8ds89A2i084OP7NBOfqm0kz0uBfL72dnZrzqczi/e+K//sas6argwnJDMB8M1kGarNlOErZUfkfOVjwoIQfVy1m4mx6rryF9QBb6n7tvkMb/70W4nzun6Eicf/Z6HGS/FpUAuuGfPHgm88D04Dn86POzIQgQrluVUEjpJWJuVsqFg8z4SnI3APHFa0QIzQIGsqrC09S5YI/M8f36lrD2ojETO+vTLem9HV3fz0NDQ1TB1FQ0MRQC5Ft5svH79+j/09Q/cgfD+VGz1V6RYuVVmxrCXI0ULZWVbPn2DYwrlBfno8qRcLtE/UwQw6tiEwn9Oz2NVxXbxbVGBwR9aIzXAIms8fnIA55itf+2115xqxTVrq6+v96xZs+YDWCh3FRTkmbD+qVZfNA98UYJRHoHTQcork7fCChxvohHxNKniKQLIFoWUmkHOtbeQfcPt5/Q8KBQa0pbbP93WTn+p/8oDe3ftyy+/DIaunjQB5NdBiUO1dXWfHz/ZfBtCgQ2aRztxR3lxmr03fGYBM2zwINlUYpUn0a1hSQI4cujEkktk4DyVazB9S6JKcgLgdcEb9eddn/vB/y6DynJMHbpobkIAclFI5s7Vq1cf/+bw0S1zc3LEvNy52vXHLBZrLoCcK4MoZUPpBqgSlkhl6wVUqRgJpQUgQGFhFUZ9vOvSs2gDTuu4lLxVa8lbthJ+S4Q9ckRFAqm5pZX+b/eeMMDbigN4PkvgFbkISCW59MgjjywHP9xdt3JF1qarr4CGo84Wx9XOgPBeE2zu02G3k9iDYwHYFOS4awgd+WAKDmLiqc58E2oEtTVDLeJoWlbcMTWxbstLjyxF5e37shs+T/YkszNU9uclQG2xfkXQp32HGsLHm071w82ykZ0qsbxErkkDyJXee++9ORkZGR/lZGct+/vb/8445aOfuDI++smGk4ywyUU+xQib/kQPAGWrBtNdaj6Mo58AqmjAVLTIi968bssrZ76SGgpmQepPMQEwev/jTwPDTsdXWOa9dipnsU4JQO4vn9xWWVn5EmJr7vqHO39mmtLhY1wRO36Y4jikjEM++Afq43MO2Clk7+4CYbIWwYePgdqZCtmERPwKS9loIBBXlFwasg9Dz9vll8KRf/vNb3/7GN6OCu/kqtEKodGuDYdTbMcK1eu3br3JBNtZ+4UkSjCAWmeoJlHdSNEzkLS79u7z4mCe7VhZe3ckYwo3CQsRpbrhfGiEcHkTy6JVJ0825xQWFiDkWl3LV6pr4nO2BrxuNyhRITpr4gsa/w/Atv9k776hpjOte2GeXfvMM88c0HhFM3vKUzhezQ888EBNemrqG/kF+WXXbboyJ3duApI6XkXnns0UBTJw+w81DA7Y7G0wze6EXZuQiqLStZGsGQUwViuOi7oCq/avwSmbee3VV2RNFcjpAsiHLAI4W8/AwGAgEPwF1JPdsT7O1PW8AMidwz4y3ka2DUA+i6NDzEuXVKdWL15oKCzIh0GQWLPJAshTnpcdweOCp1rbcTAlDIpQ6BHYsm+hS1MSElpAJzYSrVpU8llaFxcXr4e+eCNCYrcCvDmLqirDS6oXp/NJcNi8rPh2IgDCS0ztXd2EE3qHz7S3GxGm0gfQ3gGY7+F81H3TOR9VsWNjMs47gGPakm/51AsENN2A45K3wlW+FqcDRRBebMyyWoXMzHTClXAkPFkz+ZdBAz19ZLPbCEe/kwOn8PLP5fYQvESS3eHEzAwKUKX2+/2Bt3FoxocQDG0T2zyf/3/vAI4dDFNnaWlpMSitNNtqrYGOt0CSwuWhYLgEVJQPz3gO9mIIYAMD2BXUo9eJnaCwVlBXEyLjj4GaccDa2Y7zTWVj+zzx/v8BzgY4o+fka2sAAAAASUVORK5CYII="

/***/ }),

/***/ "./src/Icon/money-1.png":
/*!******************************!*\
  !*** ./src/Icon/money-1.png ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFAAAABOCAYAAAC3zZFGAAAAAXNSR0IArs4c6QAAFDZJREFUeAHdnAl4VcUVx8kjC0kISxRk50WJKAjKatDKIlrLh9CKRam0kn6lKIKIrKJWUkxrEYyAQNRqVQp8CLVViywuiFVAsCgYgppaTWQRDRZCgAAJpL//5d3Xl7fd+7YEO993MvfOcubMf86cOTNzX+Lq1WG4++67G8XHx2dVV1e3glogSktRXFyc8exKq8f7AdK/FpFmPDscDuO9qqrq/SeeeOIIeXUS4mq71XvvvbcDINxIu0OgrtCHkAGMCcqZM2cOUOZrwBFI9ZKSkgxASa8BMmVaA2434p3U/TtFX3v88cc/V53aCjEHcPjw4fVbt259DR0aQkcHEx+l0+t4Xrd///4tq1atOh1JZ138s+AxCJ4/Im4IrRage/fufS9S/layxQzAnJyc+EOHDo1CgHHQJ9BaNGg9063USqhI8qdMmdIczb1BYDJQneE1r0mTJkuRpyoSvoHqRh1ABHUA3AiEv4dGlyJ8fqyED9QpM901iHfyfjuA5i1YsOBFnqvN/GjEUQVw4sSJwxD0QcB7IzExMffRRx8tj4aQkfKYPn164xMnTjwAH5mSnPnz56+PlKdZPyoAAlx/GP4eKsb2TM/Ly9tjNnAuxcjpZIAfQSYngzxj3rx5GyOVLyIAZcBbtWq1EGFGIdgtjOzqSAWqjfp4Aj/GHi+nrT+xkE2MZKEJG0D5cPXr138JITIBcDDuQ2FtdD5abUyYMOEyrdQM/GcVFRXDnn766ePh8HaEU0lTgca3Ulf1r/i+gac+s6DsOnnyZHcGv0FycvI/0MrWSg81hKyBaF43wHuDhpY0bdp0CivdmVAbtVMe85DsdDofo+yldsqHWwYA4yorKy+iftrp06cHAuz2UHiFBKDLdjxHA2Oxd3IJYhbQ8u6s5BtpIC1mjXgwxibWw3+sJGkEi8tfPbKCPsYHzfXIvOeee7rSyB8ZsCE0sMkjKyaPtCPzYAwwdmoRFNOVnfauTEhIGHLq1KllDF4f+rjDTsdsATh58uTzGZ3XYDi2NsCT4JgJ9+xgai3FNXrfTofCLTNt2jQ53IM43CijvVcxVT3s7JosAWTaJoshzJ9i2mrVjVWIu+uuu1JTUlKa0UALAOxLbClftIXBsziGsrxA/Df6fj0LZEWwNiwFZNr+GQZfAl5uMEbh5MmPbNu2rY6yLmTkM5hGHWivI+BpDyvDnmDFV3tf6iRZlVM+7srR/Pz8Q1Zl6essTFZH5HqBsrcEKx8UQGxBDpVbHT9+vF8wJqHkaVtFhzMA6WI0u6NAo/7FkI65ziNde9VSnouILyW/PrHfAHhXUv42yjTxW8ArMS0trZSpOo8t5l6vLJ9XwM5mNrwjDDBbOT4FXAkBAWQErqTMSHyl3jiZWp0iDoB3A+DpoEGa1pa4FSQZTkL/gtYC3C7SishvzDRaTFoK5DcA3ggyRlM+YBmviscZtI9IW+aV7vOqPo8dO3YwZ5EfYg/XYQ/92uCAAMJxPo39wo7K+7QeIAFwdMT0M7KTiL8DpNeJt5K+HTD2EH+H/Tkku4P96QmAAX1M8tPh05s6SdTdgKxa5AIG2pkAtYbnQEzHCjvbN/WdHctI+D8O4z7+mPsFELXNpnBJINT9MbKTRme/oVwFAh2G+h87dqwUJ7aC/fSJUBxymQF4TYGXbKUD8B4+ePCgXw0x5WrevHkadWbyfkP79u2zae8FyPKMEMd6M3jsBcgRPK8w+ZmxD4DYiDSm7e8w6leZhaIVA1oBnfgP5AS4+EWLFn0XCm/5ovhqv6K+bHImcTJatby4uPhdK42iX3nUGUb5y4jnMni/xIauh8eSxx57rMRCjqnUe5OpvAalqnH/4rMXBrwHYLbMBlOLNn2zWYx2knoQoeMYoBt8S5xNwe/8KUA9w5vbtt13331N2Zn8gkGYQLo0rxoT8AI0yQo8cdXZJObhJh7fAwyZkD7wymFKX4cm+uCgOmZgESmm/CuUv99MM+MaFRnhdmSMQqhcs0A0Y0ZvL+B9jjCnaCMggHRqOO12gdzyUcdB3UTJw/NmottYVcfOnTv3W6XZCdjWL6g7FD6TIa3yBs/CwkK30x6ID2bit9S9ncFt71nGLaASKbAAyvVWU88KUXiWrdI0yMK2+LgfLm04TQflzvgNgL/t8OHD71D2hN8CQRJnz55dRvbr0KdBivlkCROwmY0Wz/bMdAPILqAFMnfat2/fk54Fov2MEFvhKQDTmKZZ3vy5T2lDWnPILZt3mbp655ZvIW33Hj9+fCtTBreQ2KSfkJhvx56YlcOJ0Z5CBmo/dc9gU35k8tCuBDvXoUGDBneQ1gWgzwTTQrNeODFaJO02NBwZArpK3ryFDXItpM5QM88NIBkCcI2ZEasYm3WMtrbBvwowBzIN4/v37x+fkZHRHzszH9B+DTUlfx3louLAe/eFE5cyeMth/wAZSgDGNojwWks9YWUEA0C5LiRewh7wMzMjljEa8A/4V9Fm2yNHjnTu2bNnEqBp56P73Pqk5wGmVuFTsZCjWbNm2g8/iSZl05aO5gLaW+/2WZF1x32xfFHlGX4gV36DYaRPI2ol4ANuwWTolCOFFbcvftwuTp8P8F4FgDr3kywxAQ++9dB6aZzMiCjkAFbr2CsPouIKQwNJ+AkU8+lrSsqKVsrzDigBLRjosi0lyCB/qz3TqiFkWytMvrUVM8hrkNOYxvGMRiIr33V8QXB7bQmgdgBuPSBdizDdxo0bdx7Teg/aWIRgHYkvJW97rOTRgsV2rif8MzEVb+Mf7gulLWR7AxmfFXYO/KlOVN7KS8ymjD/hEPwt0sGvmlOjlF6Ykf2ApxMZrb49mOJp/upFI61NmzYtaOtO6El2N/1C5ckM0unRDmGnKeyE0a5QmURaPjU1VVNYoCWjjX0WL158lHctYgd57wmITSJtI1B99sFaRN6GlqFNITnUHjwlvzMeBhoN29shDwYRPcqQs/q/BVA/h1GP7OzsBshSBHhfEPdAJifpllssyhgBfg5WdB1XXUqC3CBpcil7+0I05iDvbpvqukRfQpoo3FAq7JDXUScASmoEkK9Xn4625Y45E1mKeP6CrHjSryK2PNIXHwXAuxHwZlNvMfQsSc8QL8Ixn8X+tbtRKIp/4F0q7OTGOOmI9qd1EbStK0eYdGxRl61bt67s1auXDhvk4sjI+xy3+ROSHUx/+jAL8LuSfxiSr5YIZUCj6agTv+0O9sFf8V5vzJgxCY0bN+5NO7pW2BDOyRP1BOAAwwbyUOtTWB0hfEunt0PnQVds3LjxNLIUKh1qRprlFGbqyuWZQ9nLAES7i/F07k5Wde1o7ud9L3Q9zzqmMwIeRzPSZDpyaU/7cct2ztb831/qyxVz6jinBQ3WCYANGzaspP03EaQBcSbHaTpEEIByqh0Iadmxo0ePahXtRtFD9GMWLtkqNGo7rsk23o2pTL40eiQ2txWxlv5k6AIedSfTFLdGihRSoJ4uvlpIyBYY2m9Cqh2lwmhPJRrwFjLUg9riumQCQBHs90K29qfUv4Kyurn7D3vcv3tegOluhU6uJU8rfCqA6iDWDMbgkG85SGYFz5jD4W+FnYF8o0aNZC/qJDD9jtPwMUh3w11cAHxMx9Rpy0D9Kleh6oKCgtPeFQBNZ4tGMh32yfcub/cdWQ3s4mF+AA1sQcV/260cjXJyO8rLy3vAazmkL+vr0ddLMPCN6eg25NLBZyOlW4Rd5Etb0zmUGMRBwRrzSI42Etmz6oAilfwK+O624GU7Wz+9YPAOyF04wPJfqwBqKwV4VzP9nkPi9pAul5ogSwfsohMgPyJPl09tSAs6xUpKSjZwFLaDspdTZxZbNAcn3UV00ME06w3fMeSlkPcM9yKyrVEJgCf374ChgXAUgLYCGpLCLqIXoKcjVNDOmQxp7DT0Dacwu9H2ClwIrYq6axV4O8lbC6/RPMsXdHJmuHPq1Km7EVA2K6grg7ZVcLumW7MFlO1K/Wc56ZY/mQhlkqabu42syo/wHM3QEv5nNRCutgDUuSGVxiHQLdRpDdlaveiUFot/YzdexLHV3YIuuTtQfyeg5tJhTS05zlmkZ+HX7UcLtXswFhIG6xLeU+HRnDQtOBekp6d3Qx7TTh5BrlfISoEyyDcvwXWCXAi9RNuNADpVB7riEWmApzA7q4G8aEm3DJQbgaA5kDrxKRXcwvCeTrqTtAbQNsgw7qTF0XF9D6Pt2YWkKzQnfQdaMZ2V8q0ZM2akk/8x6QOgETxfQ9nzeTa+i+F5IuVPELclTWEAZaRd5gIiebSjkVdhFPD4o8PakbzfDH2CZstsyL5GFOCpKVxs2EBeNPqWgXK3U0l3qivp/FxG9ZRZiTx1fiok32oycbnySI9De2TfxvN6M7HS/kn9SYD3rsps2bKljB3IJvLUSU1rJ+QZLlc9j9CGZ1GN4FXGAJUCHV2k975QBu0vrFExvJeWDGKB7EsxTIfZ4UHD+iWlin6As/qBZx2mUwbvJqAFrutDs4iD/Gt5EUCyeZOxg5vMTHYgVZ06dVrLx94FDEqSmR6tGDPhwAxcDr8F9OFq4q8j5Q0OZxcRtONDmPeEoZCptmBs2DyEcE8di/JmtmyZ6YPtos1/4WIY9s0s4DrOklmISeCzjN3Y3+vp+C0MYGfiSDYPcWDQjZV+u0MXxrx8zMczpuGNSQdMpgheyaJhNVBm8ajFrkNQ2VBdp56IhDFbTmnxTn0uYrgIAPgyHdM03hwJ49qqy/FUD2bND2nPuBmzapf+aTHqQx/VcYGnw9qWULhhmDBTZQNARuQVEmSTpoTLsbbqMfXjOVEeQXv6yMjWFhTgPMWTl6DDBzMtnrNIW+6YWUHKhl3N0rsBIHedX6OW306aNOlyvobfaRY8F2MArMIVeZsBb05H7Gz1fLpB3QbUvZgMJ9SZs4CmPoUCJLDL6U5WMTbb2NUYALrKvoxxvYnncxpAyTpnzpw1RKKwgmtDcAcgTmP2XYM2tSS2y2sYOK0wC3vW+hsjIwD/74OMP50sgY4BRjL21HDY7XRcGOHDGvZP5d0aqM86UM8v+fa4Hw7uO3aY1VUZzM0F+Iz6gl9bt5ACgMkF0a5JyqId2CYAOYJnYMmHdvtSqMCcvqrgBlAvMH8QVV7OaUk380hI6edS0IdI+F/ZAJCNvLYWEW/5qavdlK4R5MKtJH+fdxnvd50gUWchyjrEM89zChs/AYXhZv7LxhjPQufSs+5NkOck4Olg4cJwCCCkedp7T0RhVujkmuegAUzuFDbeF1A1NFAcUOWHMKof4Lkvk5MdlGvdZFZjw+bRtCikwGmMfmQtjcsEjE1lZWV/8bwCCMRMiw7HcNMYNO3YaoQaGqgcfXPMCD2Hqs6sUdLihe2Zfuehi5YdHJaa2zaLWrWbzd3xZ8i3hFalOM2wo+l2JOCu5WHK5aNQpd7lfTRQBQAjDwB3s6Dko96fe1fy966bMO5efwr45a59rb9iUUljdiRxct2QmWJ79VTDaFATNLAnsQa4HFt63EogMNBP0IZQp5O/sn4B1NRlb5wLGC/BIMuOjYB5HGpeSR25+KJY7XfjOBQYwMo5lEHWXYftAHgXAob2/F9B77rcmYD1dQBLO3+lwMOuvbRPWb8AqhQnvk9zxfhDkF+J9/9j79MTb07sT9vRoQkIWMV3LjOff/75iDbs3vzNd1ZDcHD0oZ1RUKhujBaLXdRfynReZ/L0F9Nffbn2Mm18yVZP095vCAigAEP7RgLgBhjpAPIuvxxciQh1Ho/9pIB8riab4Tcg0CVQBpk6TgpZS+VeMVirGayj1A8GoGaBZNLvQnQKo3PPfPqje5l3+R1c0AWSPudTpxELzRB85BpHb6S7Q0AAVUJTF2GHYBO34GRPctfy88AGv4gfvjyAkCdLS0uDuQXdKZML35nYzff8sLJMch3m1jjQ9VdJV6SEatobTX4lU3aOv3LeaTjMUwF9INP3SquZFBRAMUbYg2jiIEZuM3SKkVeyj+1xLRzrlekd9DEPaeZJcyId6o/GPsWhwCy+j/5LrJx2XJQy2jgFGBLJltONsqQi3zTiLDu/5ZOa2wqsfFmAtwE/UdeEnwDAH6moDxWDBkCPg9pQfjgFu9AZnT1251l3Gvuh93nfCukWLmoBAOSiZUK3wrs974XInheoAeQaQJnbWNmrkfUHzL5tgcp6ptsGUJVYmW9CmFXcW+iO4SRJtmwYwum2TFq4k2d9bqbPLf4MXUaa7It+8hDQzpAfbtBNXQK8tf/VFad5Z+PDj0FOYMoqfTjHe+7DAp+CXgkhAai6OjOksTcB8BhAFJMUFETJDh2nzkfES7FDRaojX45vAofBZwDv7eigOcV5rdWgGXIR2peADP1C/a1MyACqa/qqnumwkkdp1VBG7LDSv2+BhbEJMr8KcGWAODKcrWtYAAoonU6wwf6DHtGs790/H2Nh7Axwq6ElaF0O/Qg6k8j3G8IG0OTGki8jvRhBRiHIajP9XI7RPF0KLUbG0ZHKHDGAAgqBnEQzIX2B8BArWAHP51xAzisA7iFklBOtf2dSHKmQUQHQFIJVWt8p50HlGOXfYFN2m3l1GTNLuiKTBrgRtu5e/eu7aMkTVQBNobAv1zHSedCnCJxTV0BqQPEUBJwTAGcwM940ZYxWHBMAJZxrM34rj7kAeYgOrBbRCf0GLiyDLb4WIY7B60F7N1JuMHQ+9CBTdTlxTNqMGYAIbASt1vw27Sp1ytWxdIBcg2auxg98PdLv9fhXBQ1x7PXNiwAbCpVCr0Gr+f+om2K1TYS/EWIOoNmQGXOIoG2VTkj0G+W+xPq89yueS3jWOZ3xzPtXONlfQPGcCMvRFrUnvx15ns9NSduoAcE3XcvevYT3Wgu1DqBnz6Sd7dq1a0PnDYDIc8d6dpGqGMAqFrCKAXOPnvknGXtirWUSIFD4LwVZrgPWynB9AAAAAElFTkSuQmCC"

/***/ }),

/***/ "./src/Icon/money.png":
/*!****************************!*\
  !*** ./src/Icon/money.png ***!
  \****************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIYAAACGCAYAAAAYefKRAAAAAXNSR0IArs4c6QAAGn5JREFUeAHtXQd8FNXzHwhpJBDAhI4JLXREUZCOVKmi+Kf+VMACCCIdKSoqCCgI/58g/oAAIk1EimL5gTQR6UVAEQgY6T2EJLQQ7jffxdvs292727vcJXtx5/OZ3Htv55WdnX1v3sy8DZEFFgcsDlgcsDhgccDigMUBiwMWBywOWBywOGBxwOKAxQGLAxYHLA5YHLA4YHHA4oDFAYsDFgcsDggcyCXkclamLN9Ob8YAE9xWOo/hJON2xkOMtxgtyGIO5Ob+XmZMY7SZEC/zmGowWpCFHAjmvjYzmlEglGPCDFIrC/nyj+6qPN89pmjlAzB7OvQf/cR8fPNYOuYx3mU0uyCoxzfTx7zxuHl/Vz6L8Z1vZSytx4GubevRjDE9KW8oVhjfwPAPF9G/F/4gND5gQRIFBCr6tN2j5CunacHwR+nOzetKWghzPkbTKaN5lKP0s/QgHu84xrzqcQcF5qFFH/anp5vXooDcmFB8BwEB2vYhFHmUgsHdFyxWnho99wGtm9VHORjwP4zREgwlVzxMR3G9OMZ2evXr1IilhR/0ozKliuhdztay8EIl9Po3w3ZaMy5/mzHa8h38h7G45k644P1BXWhwjzYUHBSod9kqc4MD/iIYmK8nMWL50LxhJYsWosWTB1CDmhXduHWL1BkH/EUw1vBNtFLfSO5cuahjy9q0hIVCb61X01t54xzwB8EYw7ejEYr84aE0cUhX6tulhe7dXk1KoZ0H4unuXdiS3IOQ4CBqVreae5WMUPPuxF/AHwTjdTUza1SKoXVxoymyIHZ6IqSxIHyzYQ/1GPUJJae6r+xDP1nx8WCxUS/kbDYbnT0KV4kG7mhKTFBgdsGoyjyKVPKpT5dmNPPtl5RFcvrkucvUtMc4ij95Xi5zJxEclIdWTh9CrRo4dmXgAV+5lkJ30mCCIEq5oRW+1MRzFJAnSO7axna3E3u/ox0rJ8hlfydg1BAMG2qC7Mqb3cDVlxnziZI5V7fPoYIR4coiunfvHr0w8hNa+PXPQrm7mXVzR1OzOs6XkJFTltDEOavdbdoR/Yt8Ya6ji9lZbvYZ46aaOeevJAmCsWLdTuo29GO6fQfOVM8BS5MzoTh9/grV7foWneJfLwGWkAVeasvrzZh9xsAycoFRNi9WLV+K5r7fRxKEvmPj6NCxU86YsogvvsFoX8dh/9inrrA2bhQ1r1tdXSznB09YQDOWrJWXD/mC54lErlqO8arnTfi2ptkFA3efwBiNhBvwG9P2ZNylqvMu599Uln0zczi1bfyIskhOHz95gZ58+X3WWSCbXgFMa9MYxzMmeaVFHzVi9qUEt92H8RtGI2O9zXTYUmDd1mqFRIICUSyqALVuqFU0oVi+O2M5TZz9NaWz/qID2ANPYtysc02vCI1AEI4xXtMjsMo848BQroa3Te22tufxoOYwCjsYzqthGRfY69jyhgTZkvfMt9kOL5Vx71cTbDUqRss0Svq/01iKajNaYBIONOVxrGfEPtH+4CAsaxjrMBqBTkxkryv99uvWwpa69zNb4s4424iX2tvYGytcV9CjXyxFIYwWmJAD8K42YqzPGOHm+AowvebBhwQH2thVrylX0B7ktGPt1M1B+AO5Pyif3ubjFG4Qzjgj944Z6WPG4YxYrtwFuHnLM0KAjfTnbvveosdLgR1SPKMn9+mtcWRrO/DOQk+AQuholsC1E4ylGT0FWG2PMzrqw4zl4EsM4z8aFvPdw4CmfkDJXDY5k5yJ4frYFanb9of8DR73g2ae3nh8WQKduRe4aDGTLGeEMptZgB2lcmYbycb6SyzB8A33YbPI75ums6TVK5Zg+IbPWKLkbW39V5+g8o0ryj192X8hpVzEipUBP/30E4WGhmYUeCmVlpZGDRs25LgU7LbvQ4mHSlGL0W3tWTq4eh/tXrRdznPCZsSaqKxgpY1xQHjhwh4Io0LRD0g1L5+4pBGKFi1aULVqglHWWC8GqWrWrEk7duyQqS8cOU+hEaEUWuB+gL39VybgHZTsnFIUWkkfcSD1Sgot6/u5pvVBg7B79h10795daPzurTRa+EIc3UvH5ksfLMHQ54tXSzm2h078Ek8Le8TRzWtQ+jOgQoUK9PDDD2cU+CDVqVMnKlu2rNDy9fNJFNdxBp05eFoot2eEKc9emA2/2BE8xFiSUXGEKxtG4n6X+7nKMVU1bFXl+8hbKIxuXE1VkdzP7t69W/PQdAm5MDU1lX799VdKTEykyMhIql69umG9ZNOmTdSxY0cpqEndfnhUPkq5JOo8apqszsMy+ArjKUZ/2OPrjVFvHTBkw1i8eLGNH7JLPH78uO2VV16xBQUFCf2Hh4fbhgwZYvvzzz9dtoF+3nvvPaG+M55n54wBzWcVY3NGv4Xo6Ohx+/fvj1PeQMGCBY9wPkhZpkznzZuXZs2aRW3atFEW66aPHj1KDRo0oDt37LFGWrKYmBhas2YNlShRQntRVfLZZ5/RsGHDCLsVZ5CdgvEtD6y1s8H5w7Vx48ZRv379hKEWKVJE90Hm5nO0VatWpZUrV1KhQoWEOnqZpKQkio2N1W1LTV+yZElpmUEfrgDC1rlzZ0pISHBI6roVh1UzdQHaVitNCyymufmQsB5qaLkgT5482Y5GHkS+fPmoZcuWBH1i8+bNhoQC9ztlyhR9odB5nU+fPi3R6/FJXQZh27dvn7SFrV+/PmEGU4NOF2oSn+Q3c6sNlS1XblWNjS5t+PMB+qaVOc/MoKQziXKVJ554glasWCHnzZRQzxjTp08n9ZbR1XiTk5PpwQcfFMhy5c5Frd/pQLFNK9GepTtpy/T1ZLsHteE+QCE9dkytB9uvOv6dNm0avfPOOwJBds0Y0cpRlHwkmp58u71DobiRmCoIBeo+8oh+nKayXX9OnzoFfVyElm+2owrNK0sz6qPdalPNrmIgGXYt3oLsEgzZXIwbiSoXRbn4HKoj2DD5v5pLXbp00ZTlpIIbN0R7B+6tSMViMp/ArwfKRAm3rDR7Cxc8yGSXYAhDzeXEAHvnxm3JOKSsUKVKFcN7f2U9f0pDL1HD/q/2CEUnfhaXjcBA7P69A/oLunfa9korKwYtpbQb4latV69e8puT2U4uXrwoafOXLl0iHD/EOv3QQw9R0aJFM9u0XN/V1lAmVCRKly5NYWFhklHLXvzr8t2UzBbL4tVK0F+7EujU7gT7JemXt8lC3mjm9m0E14vgeP4W6bydO88NFrE3+kjnWvTEYIREiLBhyn9p3zLxaAj27Nu3b6fgYNmwKFYykIuPj6eZM2fSxo0bpS0bBEINUPwaNWpEffr0oUqVKrkliGrlMyAgQHKSPfXUU9S7d2/D1soFCxbQ66+/rh6aw/znn39ObdtmeE0dEvKF69evE5Ti7777jn7//XfppVDSm1Iw0tPSCXrFgVV7lWOVHg52Io0bNxbKjWYuXLhAbP0jtjhqGOGsjWeffZbeeustKlWqlDMy+ZpaMOQLnID9YtKkSYQ2XcHNmzfpySefpAMHDrgilYR41SrYC50D9JC5c+dKuxA9PcZe2xQ6hn0wOJx8ck8CLeg+SyMUoOnbt6/HQrF+/XqqUaMGLVq0yC2hQL/Lly+nxx57THq7kM8MXL16ldi8TWPGjKH0dOdxt4jP+PbbbyXLp7M+O3ToQMuWLXNGIl3DrmXAgAE0YsQIciYUIIbzKjtgKHcabu84okQBSr+TTusmfEs752+lm0mIcxGhXbt2hP22EYOSWJMkpj3//PNCsIqaxlUeDxGzVfHixSUdxBn95MmTXT70Xbt2Efs+qHbt2k7vCTRdu3aVdJ7Lly8T9AEsfQUKFJCcaJgB33jjDf6ikPNHiZli8ODBtGTJEmdDl6+ZYimRR+Mg0bx5c+lN90TrxkzhaNouFBNJZRuWp0otqlKBUoV4qSJKPJ1IR9b9RvGbj9IVDqrRg4ULFzr1c6iXkqjYIpLSeOs6fGsiYPqHLuNrgG8GM4Ua8hbMS+GF89NFDt5RgqkFAybvkSNHEgJZnNk5lDekTEOnwPJx65b4QPKEBFKb9zpQuYYVlOSa9Imtx2jNqBWUxoEtSoDiC/M2/BN6oBaMlm+2lUL7lvZeQJfjLwpVIOzsHZV2IMIFL2agaEKBVi8fRasUp66ze0hhfVtmbBB6NJWOYR8ZlgvsPmDexfTniVCgLUyzaqEIzh9C/dYOcSkUqF+mXnl6bdNwCouUVz0US9P5qFGj3NJVgsND6LkFL1GhmPshflJD/AdbWcRK+BKw+9ATiu5ze0lWVL2+TSUYEABo4UeOHKG9e/dK66jeoI2UYUuK3YcSMFP0WTOQ8gQbN99gTC+t6EfB4eL2GG5ubPPcATgHn/24u6YKnGW+BGxJlYDlAzOFMzDOIWetuH9tOlcJ79GjRyf29JWGQQmhZ1CoMmOfUA4Ddgq1fQLLhztCYW8vT3AgtRrbgVYN/cJeJLUNZXj27NlymaPEia3HOYIrw8QdUbwAJZ29JpPD0zlhwgTD9g25ooEElFW1AIcXzidEhZ/a+5e6pXvZpWNIA7l27doP/PBaqkfljTycbFi77QBFs+cXfexZj34Rs3nh8Dm5LgJjDh06JOftiTJlykjhd/a8H/7+ZaqlxFsMhJk7ISFBaA67j8xC2YaxQhNnzpyh8+dFbR4EOcDzuyxHCgYCZtXLCLakmYVYxaEhe1t79oiOLZTPmTPHUJidvQ2T/eIAyvgcKRhwiKkBdorMAgxxasCWWA3QlTZs2EA9e/YkbF093VWp2/VhHgdMoGh8yIilPcmufIZxZgBjT8YyjM7NaEzgDQAD3QUwGZ5POKSwlY2KEmMS0J56tkAZV/MCaBvR6wsdFS5cmD766CMJPex4K3tL63tYN9PVMGNAKFYzvs+IhThLhIL78QjwIM6dO0effvqp5EOA8UYNcJ2rARbNzELSuYydhL0tCEBOBAgGZoqm/nhzmMa7deumGTq2v2qAmTuzEL8JpwJEwLnQnAgQDCwffgt8pkMzdiw16kBa+D4yC/E/iYIBhxowJwJ0DOgUMiAwtxSjHf7cFk/nfztrz0q/CB4JCQkRyryRgTEGRiMl5C8WQVXaZMwAcGwd3XBYJtGLPsJFOKYQuGIH1Ptz+3Eq/XhZe5Fbv3/tTtDwAZ5RbwG8nzDfnzx5UvLDsOUWwR9jGeHomciYpQBtSghfqvNSQ6r7ckZk/6z2/6bkCxnrOCyUcCD5Cpo0aSKdeVC2//Kq/pS/2H1F9ciPv9Oa0RnHBuBo09uFwNpXr149ZTMUGBpIAzayh1GrQwp0mgxzaHrzyXQ7WXTG4YwIzo9mBiDYiDNZvXq17rlSbjuJ8f7NZ6YjN+tiKXEIK4d8IQgFCJs29a06Mnr0aM14Vg1dRvfuYkdlHOBNVLvb026m0adtp1H67YyPiLhskYViTsfpGqFo3bp1pr5pgeisZs2aSVFhOJmGICUzgUPBgFDA7awELB/Dhw9XFnk9XadOHVLHXVxiV/Xil+fTLdUb66xzbGsRjqf2vaReTqGZradqIs/12sLygZki6cw14TKCZ8aPH++RfQK7qri4OEKwL4xjngQKC4PxUUazlBSrWkI6Eq9cPux9w9WMA7G+hq1bt+oGteLLLwUfLERnD5yWh+BoKbETwLPo6BRYkUrFOFAnlmDRvG+8ykXYkmL3AUVTrVvZ20TM5NNPP23PGv6FHoHg4q+++spwHSbMlqVEIxiORoypc/78+Zq3WY/+xx9/lE5zw8HEjjLi4/qED4TgkBDC1FyF52FaRXgcPI6uwJVgoL670dbO+kQgL2I23QUIxcCBA6VINL26keUKU/UOj1DiX5dp35eCDmdewYBQwI2dP39+vXuSyzAtDh06VHoQcqEqgTOn8+bNo4iICNUVbRaxjAhJc2RdRA0jggE6BNW++OKLUpAN8u4Clg8Y1TyZKdAXBGriRO3mAi74xoNacFBQOSloZs/SHbRp6jrl8LJFMBzqGBgZdAosH5gpXAkF6MF4vJ3OAGc5wFx1RJFeHTByxowZ9MADYtSTHq2rMnyLArspBBW767vAi4GPm3kqFOgXgqEGHE5+YUlvjiaLdRhJpa6TVXldwcCWFNMllgLoFGplUG9weODffPON3iVNGQJTvvgiI+hFQ6AowNJz8OBBKRK6cuXKLqOhFVU1ScRo4sMhW7ZskXYszj40AsMVBAFbUgT/xsTEaNozUoAPnrz22muaWa9iiyrUdtwzFMhRZWYEjY4B49XYsWPdHit2E3/88YdQD8pdTO0ydPbQGc1xOugd/AkhKYReqOQig/MTOM1lB6NLiZ1e/Yt4CuwOYF7HkgXfB8zc3rJofvnllxqdJLJsFL2wOOMelGMyy1ICy6cAnlo04dhSQnSt0kJ84/rJP9B+hVKVkpIiCZK7BiIjs5dyHK7SMJ8b+eSRq3b0rkPQEJuhhAD+F55dZr2gLDJlWncp8WSk6mhsdbRTrefqaprF1+dyMuAbFzt37hRuEV/lRcS4I1B+COVvGsEy7aiet8u9JhjQ2pVwNeGyMkuXjovnKXARy0lOBlg01dBqbHt1kZA//7vol+KLaQJBFmW8JhjqI/gHVu6jXfx9atgkYEFdPUx7thJBszkZoGQrIYy/pxkemU9ZJKfT+V+Pb5y6luALUoG4Rqsu+iqrUT6xLYXXEN9kgr/BKOBsBL5gpwZsDfXsEHx0gKZOnaomd5jHx8fQPnY/CPa1g57yie213nE8e52s+oVdR33vAYEBut3j880OlpEGXGGrbiUfFmoEQ9kXBAN2fSMCgkO/OBGuDNlXtqVMQ4HEVthI9BMspziQvG3bNt1DyXqCAZM1fxhV2aW/piEQ2RLe53QpOXz4sPTZAez9XQFOW+M0uKv9PoTi+++/NyQUOLqHs6ewO8CkrAdcDqioRBaMt/Vo/axsO4+3VXaOGVqvS+SlxdBnifF54/79+9vwOWNlu/xm29hXYuM4CUPtsAnbxj4VoQ1le4r0HU6rAUYCI3XNSHOJxw4XtqjNq+/Qx3nNUoL/rZF6JVXTLRxfMNYgkMYIIN4ApmAYjvAVGSwzeh8c02sLRidYOdXrM2jhYVV9wR9au5qJFbisDuj9CBAF9BfjLkb96TELb0YjGIjgin68NH3/1mrhfCXGhBgCrPXqGAdvjpeXBCmABYeGlBDKB3GbDG0pFX07WtgG6gmGsqqV9oADujpGiWqlqNfyV/+OUchoFYol/Aa+BHznWi0UQXzSHEf2KzarwlF5kGULfM0BXcFApziyj4eBOEkl4BtWvoQPPvhA03zHaV0J7mkLso4D8JVAAZNfwzT+4Kryn5ogYvzE1nh5RPiCHEy93vZZoAOY1REUqwT8k5X8RSPkMd1Kvqm8bKV9xAEIBGzXmQ948NEADTQLSclrgM4icYMDWEq+doPejKRXvDQonJmYwjiDsZOX2vTrZqJ59DcYzbindzUm2DCqZJL7I7k+Zk3E7yv7S+H8fMZ/NOB4Fjw+SsaYPX2Gx6v15Rt/jCWZFHtitUAo7xvX9jCKGjgX5HSQlU6+UXh3KjMWYXS4W+Fr2Q14cFcZ8Y2j2x4MBveMWWIMY6jB+pOZzvfnJgwOxiLzPgfKcZPwQejOEoF5AmwhQYHKGUOZxgvjDiDYpBYjLLBR7lS0aLOOAzCZv8kIK6nyYUvpAPbJDO7RxnZl+xzbjf0LbEN6ttXQcL1ejEbgMSbCnhv6j70dmLg3M9433XLCguznAJTTnxntD0n4jY0pZtuycKzNdnipjCl7P7OFBmtmjhUubgWHbrDkQAiEPhR5XBvEaHqAgctfAFNzNCMegFI3cjb+enxxAqMmOiY3/+O5Pp2b05QR/6KQYNEHFxoSRJEF89Op88JOONhBR1BMn2Wcy+g4mPN+ZYzjQ8bfGNfeLzLnX38QDBivBjK+xejo4bjF3WJRBeiH2SOpegXImRZ+2XdELRQgOqqlpIpctpCxps41R0UQjs8Z3dVZHLXnk3Iz7z5ww2GMxxjHM2ZaKFiXoOfaN6DTmz5xKBQ7Dx6nBt3HcncaWMolEX8jPgE4g/Ewo65QRBePpLVxo2jHsnFUqUxxJhMAyijaMC0YnZKz6wYuc8deMddHhOel/SsnUUxJPBN92MVCUavTaP2LbpTOG9+HejzTWK5xLOEcxbbSqBavMMFsmchkCTPPGM8wr7zyVvXu1Iwu/jLLqVBs23+U6nbDauU5tG9Sk5J3z6cXnm4kNFL4AUw0GoDOZFowq46BmQxvkzCjNew+gcrV6iAcSr6XfpfmDaoqMLhL6zo0tv//SWUh/IH4B4tFCnUEYs5s2X2YWveexHGlzv8VlbqePV+iSCFJZ6laHp/N0sLE2di9amCzpsREBWYVDFgkhdmiRstXqVaHEZoHnH43TcPOiHxhVKG0Zl3X0KHgp12HqU2fSZRyQ/y+li6xqjBvaDB9PLoH/atdfT6Dq7WaJyalUof+k+knFjwVwA+zV1VmqqxZBUMzruKxj2uEwhucvJacSp+OfdHtphAV3+ixSlQsqqBu3cVrfqaB739GlxKT9a7/R6/QTGWaB2CSwQlLiDSmXL5Rh9o3edSrt5zOJ+/6vRtHc77cSEjrwM9cNlSn3FRFZhUMUzHJ6GD2H06gzoP/n47yLkQHICVzGXvrXDNdkSUYXngkaay0zluxkXq/PcdRa9h2w0i3yBGB2cotwcjkEzl3KZGeH/EJ/bjtoKOWsPvoxnjWEYEZy80qGJrF+eb1i6bi3717Ntq445C0o7l9B74xDSBWZBLj25orflBgVsHA3hFGBfgVJNj6xViKrd2RwgoW5XyGbgo7hhrw6QVM776CO2l3adiHC2nmknWOurjEFx5nPOGIwCr3nAMzuSpmDpsfIcaLY3KyQHPaAi9zAPtTTMf+IhiYury79/UyQ3NScw34ZsBwswsHAoYL5CTG+8O9YK2+wGhG4YDQDmM0q67GQ/MMMrQ4z+pnVS34ThBYCyGBp8oMazi0Wzj6fmW0wOKAxQGLAxYHLA5YHLA4YHHA4oDFAYsDFgcsDlgcsDhgccDigMUBiwMWBywOWBywOGBxwOKAxYGczoH/Ad4Fid76t0W1AAAAAElFTkSuQmCC"

/***/ }),

/***/ "./src/Icon/mosque-1.png":
/*!*******************************!*\
  !*** ./src/Icon/mosque-1.png ***!
  \*******************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAC8AAAAvCAYAAABzJ5OsAAAAAXNSR0IArs4c6QAABcdJREFUaAXNmluIVVUYgB3H+y3TymympNTIwspKzcIIUSLDHnooi6IoChJDKLAybHqJHiR7iNSysCisCELooQfFLkQU0VUawQuOYSpm49hoKl6m7zvNOuyzzz57zt7nOOMPn+v+r3+vs9a/LmNDvxqlq6trICrGwHkwDibDpd3xiwjHQie8DZ81NDR0EdZFGvJqweiLaTsXZsNUmAgXQH8I0k5kNazD6J0hs89CjB4BLbAPTsAZiIt5X8JEaAzGEh8Jl4V0r4V0Og4WwU6oJBrdBs/DyGAc8SZYCYdgXcg/6yGdNcAc+BqOQ5p8T6HTqCikHe13Io16x3g6bIT7wBFLE0d8F7hQS4S8G+EvUPbCwyUVzkaCTkbDS+C8ThMN/xaak+wgfwFYR3nBOoQDYRbMhekQXeRJaqrPQ9kAeAWOQU9ygAq3VdJO2Tw41a3kDcJBMA12wUH4FOpjPIo0fDGchJ7EEX25kuHmUz4ZWrsVqXMbhEE5TXxJWvuqy1Dk4nwIelqYVCnID/w7Kq0Dyh2MJyBMHaIFMb0eil4pTU9SWfznmkKlZTA4qXIs7zTpF9l8/onlx5MDyJgQz+xOTyLMbXyJTkZhDcRHiKxE+Y5cd9kyIb8/XAH3wmYIc55omfxJzjK4AYaWKasmg4a3w1GoRvzA16C4e4Y+zINn4FfoyVNRpSDO/d3wJiQOSNBfFtJA1/UbVCuuiYVxReTpSVZVq6RCvR/JT11HJf1SWV8cPEAFnSXZR0h5GCsR8lzsh0tq5kt8RLMRJcoTEs7NQeTPh2oWaVBxikhbSETCJ4nXYwHOQ8+tEb2JUb2Nx9gZkOV43ImX8YxeFAZhPImZkEVPsX0scj7pGehM1aXxbuu6yCxyOKHyTeTFXW9CtaqyNNpzUplDiLa2s1mQ1UUlGak/r6fYR+rI2+H0HD2O5SdtZOq4UQXZTGR2SNQh3Iv+k6l6MOInyCp6lGz+ONWKfIWOfFOOprabBPujbfmgC0nrvWqVMyhoZ+RPpCnSiNFpFSqUaeA0+CZW/h5p11CtcggFj8EXaYo0PmnxpbWxTC+gKxvK6ByLVNbH5xmMiIpC1JHXtlSxgp1n3Vj0Ao6wU24HBNlEZE9I1BAeoW3JlEzUxejtgDziYWppotLeysSAjXks727jGccXsT4Rp83PMDdn78Np9zof8Dhz3+P0MNK+DlyfU19odpTIWnRuDRmJIR3eDdVeQKhaJo7+/UE58anQWlYrW8bfVPdwliiUFQ6Uehq/7kBireoyHf1XUegDaz9GawvBHHDH9fSprz6YQgdlXVCV0I/ebBFc6bRR8S9wB+SV8TTchGJ982Y+YD/xB4k/Cnqkt6CSeC9YA065IH6M7rIo6NPDOSjL4XdY7dOELwbLIe2eSXFV8ge15qO4IMT9eVMvFZTPhE5QnL5b4REofgxxbVwKTidf79zJ/xcS10EH1EO8IrZAM6SeCu2dOlfBu+Al/GZwNhSEuB/vA5UPU4oD/Gwoj1ZaWyiuzz8+Lvno+hR42ckktHGkp8AK2A7uKYr37AllysgcBr6511v8mVfCJBgOg8GHKEc1YHoIjILZ8DHE79SO+nNRw0t+VgpdxSugON+ilWuMuwC3wzbYA97GToJ3Z699jujVcAnoBeOiU7lTZxAvKKQxfgx8DueaeH+4NtHoaCaVroG955D1/2LL4qiNIV728/Cz6EMfgLZQqY/DdfT/fiYb+Np7wMXWl/IhnbsmsgsNF8JOqOXsQ/PM4lRZBfkMD5+KAjeOr6C3PsDddgl4hqldUKT//QD0vWfrI/TjW8DHq/oLihfABohvIGTlFo1uhRZorr/VEY104F8Jb4FPwHNMLbKLxk/D5VCyYUa6rBjN3CCqyU5J+05/F7gzeoL06dCF5guD+t1Z3UmPgzckd1Zvb+thI67ZslxSk/GhRz7Cdxx/8ibwf3r4x4EhoH4vJL4GtMM+2I3BHYQ1y39j/whKb5zflgAAAABJRU5ErkJggg=="

/***/ }),

/***/ "./src/Icon/mosque.png":
/*!*****************************!*\
  !*** ./src/Icon/mosque.png ***!
  \*****************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAHgAAAB4CAYAAAA5ZDbSAAAAAXNSR0IArs4c6QAAGCxJREFUeAHtXQl0XcV5/udub9VuybJkI29ggoONzRqDsQjbAQKHsLgNgYLjACEpaSCtS5qG6JwQbEiTniRtEwhgSGiaAw2nIa1rg40djFls2YCJV7mWZVs21mLJ2t97997pN0/r09OT7r3Se+8+SXMsv7vM8v//d+eff2b+mWHk4sA/WTLHZOpdxFiTZOj/zha80+RicseMtKp1KwrDnumriEfmZfM/fXfGte994jRzxWnCZKXjnCTat2SGybX7Dc5WcuLNCjO+PFHAFXLVPWWPcwqs8PLtAcbr53OiuYwIP/aDZD9J8lLwygtV2ld+iWF6nuUmewwlFUmM/QfpvDp5pbovZ5NJc0GVT6YmYmTOPvzK7GynVLoGYAGu7g1eYZj0IzBzHTiT8VtvRoz3J1LtjQJpgnv8w3/RIKlm72XPE+s/rgA4qpZ9WfPB0iowdsUA8vcoMv90wP3kpU0JuAJgOliebzL2VwD3ukH0HydFbhn0bPLWhgTSDjDfXK7oEeNCzvndoDuWHk7tnaFQxAY/k1EHSSBWoINepuS2QM9iXL6bOCuMK49x1Ser6acxjrDMeZB24YU4L4K4rh1KZOgXFIa5FBjq3fh/JnpFjnpGMaJJK8AwrmRFkufDXBQgxwWJ2FyNmflxLybAA4l3kMSF+TE6kNMKMO2dLzPG5sO4GrIbgHZ5tiHRuby63DsBMO1jUaIIybwRBkl73zOnF+kFuK5QQi0uTkg8Yz7YXTdTRC9JGGecvah8BoM9TFZlasBHb0T7wrnZoRynbKYX4Jkgm9Hww6WcrjHCyucxEOJ3ymQmpQuec/1fmMw3R6F6iEaPkt6lex53ykN6AdaPc6jo1hGID+BTfoy8/kX8lejo1gjRM/d11VuPzNfNrBWM69Ml3gS2zSgznNGXTmwo+7ITztIL8JkcwcGR4QlnDF/BLJ2UZyPzli6ESh+yvR4+D/e/3f/OLVk65T9MTFki8XqZUXgAo2iqmPTk0XUz5tjlJL0AX7hTN3SqBNHDm4qYYQKq50myvMHYW367VSb5sc/59E+uvI3vu6TAapp0xdO7Ft5jkHYLkexVuFDP4VhSOC9VFfnnlZWkxr4Y/i7ttYHvKS/WTbYBdvSC4UmNebtB5uwn5G1/n7xShLrqTQp7OGkhRmdyZMr1BI0u7fNo4B9mzKyUNPMJdvbW+pgc3HPD9mz8uytIKngSDFxBUFEBYz1pdBggd6voAaS2MDJ+uLX56I+XL4cFZiEMb+BYyGDUUfTTrSTl/Y64JPrDYgbJSrjeYPxaCvmP4UP/gNOMGol4O66D3MvOoS6+GODC8ubtkNcaOi671lFg7/pvFXOWs4JzeYnQyYzaIISWocCFXHgWlNmDS7LLdnBeswWVYnjNhxRpB5gt3N0e2rX0D4qH3QUwEvaJh0BdNC9lYLFMqKHYphlPOBn4/3VJjuxmV73bbY4OkUk6H737yiM+UvNuQs29BV2jaHPZrZ5DCciCSUo0Q5LosZNvTTtMdLImQcS+x+ltg3vI0Io7jnBu/BS3jX2UjeqCwy6jAybxl+gz7x4fVVZJSrwZkyzZ+b7LTC4/CMOqx0bA0B5vQO3tSlgqqqxogy/muvaNxnX5IzoCuAJgVrKzQ2byf6MGvwjiTyfkztIL5ELsKDH+M6W96wMAbamtspT1GEYqpsumk+S/lyTvhX3ZcgPquREADzKw+iL0XrA8xqRbQ2rWF/jm4bWwKwAWZLP5Wz5VTPPfMDz5G9yego4dsX3pZbfvF2BCVVfBOPmppBmvscs+cOVc8v53VmXBIwe9Ae0OcA6t2x0ktL0STAkLTasQz2zOpa8cN6af15t+qF/XACyIYwvfrg5HIk/D8n0a6O4cVlfFcoPovBVMb2Tc+J4UYWtdbDUThbUS05S/DhMoZqZM5mess8wY+sp0qcLV64YbAEq7kRWLE1Fg8bYTpysvfD7Ll/0x5yY8PNgyuPLA+OIQRv/XHk0n+hGcmgHsTibRW5JMG8lo3M0W7RlJxw0uNqX3EVPxSEwqjS9UjD3bUlxBeJ0WHApG1fSQTZHrABZM51+08wx+NvGqpbv1EL0OYOe0tkkLKg8Y98PLMhtMEWzO41cskn+BGnsI11UkG4fYudta44U2sZ+4EuBeSHrUbD1U0Psv7/3c9h37pC9hSCtqOeI7P1Z+kfFzmr+tDarK1mffm7+bf02McURQJzUgNEhv2SLb1QD3csIwarN0afiMX/PAobTbJoEJFhmvNbYTA3Ibd/npaJ1KN3+ujc4qct6NzwiAe4GeCL+nmmRa/342fXTISx0hiWobVPriFa108bzEfePh5DIJ8HDSSeE7dA/p4FGT3twWpMO1Ifg4CE3F6GSjQi9tyKVjde2oza2k2kTMZvQUcjzBijpS20jrtp6g6uMRdAq6we0WAaP2LkZvVAaihsbtS+3Zka7qB08wTGPYDYd1CuGve3in284YGEHHyHpLh324MqEGM3r4Bq2p01/YxiW5/byzSQ2FKPvIcV/hbRXB+noM3FZUOLdCBkpxHF67F+A775Tz8iioerJmqoq0vLnEv0IpKSpm6DfosKQ7phUtxnjfpsBZxi+N55/awLPUpuPLH+0chxiNiiX3AVxerhSfPysPi5/nkyndicGq26X8nCKleApjSj+5UGJi7vgSieRFmFj4M7VFXiz71Q/e7Agbx+q/USH8Tcdd39gJ0v0Sc5J6LNOgxk4p9RRpIWk+1/XbMQh5M/PKJai1TC7IwXRpAl8ARljbwhaBlM9yRdsTkIyXAmt/uKWlNXL49DcrhEUyoYFOP8AVJE1p+MtixVAWSV10Izf0m9A7OEvKDjKltJDwC38zS8aFilp9AZPk8zCU+WFOUP7PnOeeeruV0b6GlX9vz/Qcyw93QF4woNiB1yumsTArNTCry3tccsQ0vooPOODTKNuvgP34IMmcMNCDF7Hew4x8AR8tL6ze7G+YdVVFXGd5qLzic0/Sk+LvrCikFv1iMowbmEG3kGmeJQaZ5YJcUqYVEgvCmdDxOB3HhAOrNDn/AzPZFi537j6yIl4ASWJtyGwPvFKBBe78S2hczgKiN/AetaSqHmrtMqj6WB21tbcNOeEA31IqnUL0mbJYPy3G8nYwadY7jEs7jU5pQ8nNDzYMLDwtAGdXrMz3N4SvZrpxHTfp88wwZ4EofMYKyYV5AHcK5sHxtToGt59F1JoQstkpphINbq6vORb+AFZ3rJT6oyf1at8rFW8ZhnHV4EL8/izyB0Z0zhicLOYeAyVH8eDeadc+tGXgi9Sq6L+9J1DSxa/ln3beBO20jAxTbC7S/ZEBXKW4gOSpBd3gDqRyFNcAV+i1JfDhWYQpuvKZM3xv8edWv17z1e98OIpsHSWFMXCuo4QWEkHTTYMk0bGIDakBGAbUtBL/5bxdX27qZjmZ/Fyg2m81KXJUJcsAWNJsuf3GcjPMHcrDOid2JeaNF8GDc9msF9asI1N/tfqr/1gzTLKxfWUOWuA+trkzw4jfyyPpAJf8zVfm8XDobt4ZuZ6Z5nyoTB+EjX89AW2uUlIYrb0sSeD2FtXzm4WRwGXCiYBL6rJZz695tVmnPzQ9+JiYgx53IWkAi3Y22KB/0ezovAv2AWoNFyvkxAqF/gD9KYypqEFldxS9PxfbV6BBmOWFKP56qO7P5mp0Te7ap39VvW7Hu/Tqq0N6RtguxCUJxh7gigqptPnIAvPT0CpTh0HBeQF4FV2YuCBPzSeltIhYCsEdQIQgSbQHZfibCleZxTNvvOg3/JoLfl3z4HdPDog3dpcStcLHc+rYZRiTU5dEUtyQraUOZkw2w9zkfuu+3OL66ofMjsjvMN91G+Mcght6LY2UEyS1rCRd4PZxAfpQkckHA+gzWB3xDxgKfW762tXl9MwzY24MwN3oSUmSWyUJncK+0O2FJZzLTCwXhdGPN9b/ONexCwYWM3H6LfY62deXbc+F+IrHIrDCR1bOUTpDq0nXb0I760HGiT8ej0aeBefAoIICEeJ1TxDuXthBkdeDqp+FDe3Z4199dJR+2v3MVT7zgEp5eX5vUPss07ybOIm+IAIKg01AKjb085nbYX029yca6Ypp/8wi5auLfYtbqfw+dAnRIA4Io5cuLOSS0uybeWfXL0k3LKkf7fy5JOfEWfQDyHLFJcDm67t45JETK793EBTFCG40FH6ysWKBxLTtnPUA3JOZah4iv/meTYD5mvbIkYqzb4xW+ziyEteyuKjxD6bec0+geKr/Sd7W8Xur4MoziknKinEHjs/YHU9QqdgNXqa9OfOFNbfMXFuRkfuEOAMYhtS0b6wsY0H6H9YZwra33FI+DKOmKvq6zocf04A8oxlQc78j5v/m7GfWiJ5ARgVLwMRw9MAD6tRTRy4hves1Fo4si3k33I3oEmHyIOpU5K52dziqo+/wQXqxW8waU6XVM56vKBkxgYsi2AO4olyZ6tGXMT3yAteNC+zwIWUH7MwM2ck6VXEZrOCHVOb5p2nPPVGWqkJHW44tgIsayi6VQqGnyDTOgdqynhaR5Sm5ae8SjVZY3emlOzyS+v2Za59OvP3T2BQ0JrlYBqn4a/fNlML6KoCLbX8HjCNbIUPTSAr6rc7rWskxfXGEgwGjO9B7vT8TDC+rADOSzHswlnw5OgvdfTcbIhbgZmLbm4hF9JeCeHcP554lieK45bklgKc8ev/ZGMC4CiMmeU4IZ15tfNTeHuahwfCPSrCCcznmlsd+uNeJkBOksQSwEg5fjim+2eDKUvzBZUW3n4idZhgcJRPv/VDTi0pLtNluJt4SYLCYz0fPZsgdYS0xJ8aAxN84Cj21uFBT2UI3szUywI/c6ZMMo4ib3PlIDvxyxh3C4AggB3E6zJyMBniKrzQXu7EHe75YR7xAA2DbxXFWhaOS4FipT47sEkeCdJBoxBosURvW5EaroIPsu5NwHLsADeA4vXsTCsHEzt64jdYRAa578rk6uLiI+SvHCPEwztUQS9bHWYBAOjE7V+tmtkYEGMRzLBlphrNNnFO1VcYwlUhYrYA5CcffiNWiUhoPzVY7fNmPpbRQm4VZARjGBD8MbJps5t0fHeqZd+D7GEe1GJ+q+FyxFyY2XXNxsAQwFpsfAA+N+HNcBY3mNtTiAZ4qLhaKJdKEOwCnxjOs47Cl+GmKZAlgNTtnF6pxNdB13JCarW3YNmb8qGloNSx74tuaV1TY8K9JPcqWAK5d/a+NpGpbMS8qarGzgNXrRjPWgI0DNd2tntkJnXHs4eXuYAlgwYKuyn/kEtsPizrONdMqi0Z9E/HoNgWONb3VopIdD7tm8LePrdu1K9kFjTZ/ywA3/PSFA5Iq/x6OSsLYcoQQb+8k4zQWEAjzJEMDKMf6EF5tkPRsJjjJWwY4iofX91uuyJtwjaWZzoJ+Ei68wqLO3CAs51/UfGWV62uvELEtgE/8+NkGpmg/xhreveJLdoRRV5gitXXEcRJ0JgaYzq9ypr2cKbTbnss8+S8vVH7xift/K5vqPBhdlib/xQbXMq+DId6zVb3UQh6lI6x4PRl2dKzZtrer4Cfb7nvK1ZbzwI/PNsBV6x6e06IWLWvoUFTUYgzmjBw0vp+85jGoi37VLGtHFQlh5NTuiSGj+S3Ma7uzYx0dSuRo7h5quymxBfCe9V+fG1GLn/Iyz9WlWZJIOyLAOMGL/MYRLFBqRuQY4yqjwO0BDhqLPRSQyxS+ruYHLMFqAjeBbFnIe9/4JnYgK32aM88NONpFzA2PCK44XFEzD2K36lODwXWTDGzSwrF5iHR/rXzW93mVff80m4WNOrolgAW4XJ76NDHNBrjYsN6sJQ0Lqvra3lGT64YMhNc+L5SYvPJkddnj1ZvJuSNECtgZEeBD6x/BSrGpa+BMeSP2WRdGlYWai0g44FjDJ45jUq0lSAGzY1dEN8ioyQ94IjMfP/bKdGwP4c4wLMBVmx6eE1LzV5vkuRXMaGDBErhiHETlaHd5DRLEtLvulIIjqgAypyn4+5qco363evNMV9bkIQEW1vHBjY/NjrCpP8Ta6Nts+bxilEoYVhqWQspjcIK1I9mnMhE2qoK6+ppX539dt7lQ+Eu7KgwJ8J83PDpdp+C3OffeyS1uM9fPlU6qeRRW8/H+R+P+ihWgB/XtSMh/74k/lsRuRZdm3uMArvzjA35ZyfmCybSVtsFF7RVHs4l+r0sPHEuauLGZSzHWxT7ANeXyigp7I4RJIwoZxwEcDBTNAEx34Dw9S6NUscSZUMtnUHvrYx9PkDu0yudjMubKlZdNz3ULy3EAQy0XYCHSRXYJFO4NXfCebMNEQgZPFkWnq8+0Y8wtDBvKtn0orGt2vswU54sE7Ap+hPhxI1kRhklBrtgyFkxM4tc1ttGeqlpikVO0ZJ5Cxfk6ZdZAZNThJHqUza4qL+VnGbRobhflZmF7Oot9h6isGZuCr8Q17XAcwCrGnQwcbTnCh9H3Wses0MHqU/Teh4fp4OE6UmSdTtZl0dWL22luSZhSs3ldHzmOL8TBFx//n5f+9LGfDp/ELkCqSTWnVCq/oIPKiiKUaLvqwQXiW1DhBN6/TePgCCm+jwPYTvniEIntHx+hHbtrqPZU9wSLbki0+7CXWnGAxLKFHbRwThcFxS5ULg1CDTe3SfTeXh9t/cRPp5oUqGa4rRgyvbvHT40tMpWDj0Vnd+HjdSkTw5DlGOCmMx205YODtHt/LbW09c8SibLECSGHarXoKSH1Z2Raen4HVF7foWXDkJPaV8I9TJxLtHFXgD5C7W1ui0VQ7O25t8YTfX6qWaZroZWwxVdGBUcAn6xrpnVb9lBVTT1FxAF7QwQ4hKM2qPTWhxLVQzi3Xt5GhblDxx0iedIfCXBr6hR67e1sqsLHGIokapUYnWhU6Y0dwejH8OVrWsjvca9GGiy4RFwNjhdzf/REE1UdSQzuwMitHTJ9eMhHzbBMxyKItnIsnEFEHsdxNuDeo55hwO2nuLVTpu37/dQ6Rnz055zcK0dSD6PW2lmGEong7JT4rYxtcyba9bX/m0u/35o1au9b0fZG0JTYoUs0PXqGeRo5UtG2kRmDBG2djF56I4c+hgEnHUEDAIBuv7I147piYyAKW1lkBMBCLf8a4O6GIRRd/QJLfdOHgWjX5VaczGmrn2pLPJkf2ZGKTiXbHThL9+WNOThu1RdVqb0zlsIoEgc2vo7TOu2POKWSg/SW5doaLEBr65To1T9l0a6D3h5wY4UlQN64CwfY40yh6y9uJ3Ewmp1Bp9jcxuedKwEW4La0M3r93SDtOOCjsJ5Y0bRjn/M3dwbFcUt01aIO9FNFB20y9ErAdQALcMXI0vodAXpvnx+D/onB7WWiBV2xN3diL0xEvXJBB/kyqJ/ay0OyfkeWXrJKHiJfAa6YyRFgbftzgDpxxLnV0NSGESmk277fh6PRJ+twr9wc1eCAX6NpRTk4kW5wp1CoxxBcdrAWeEAQbaRHuMkPE3rBfXu3n7biT6heu6GhRaENqPliV/uLzukivzdxmWJiL+gzaUahvcUVmGqzS1Za4zsCePaMQsrJ8g0x2AFnO6xg8PGdMUyJ+lSUm3jVabTNxSCGGOzfhHHhtq7YMeGYzEa4OXkaw4qVwWjXabEAOYG6Fm322aVhuutqe8cl5QQGf9QjEJTm144Azs32kfiLCxyO7vw0BU3riw8FuGIQQwC75SMMBWJIcLShtkGldR8Eo1tzXTyva8g2WbTXeZgAycuyTuto6UpHevt6MAlUnmmXxwzcXvJETa6EBS7a9Ikc0s899Ldoysei5g4GUvSjxfjxRA7pB3giSz8FvE8CnAIhp7OISYDTKf0UlD0JcAqEnM4iJgFOp/RTUPYkwCkQcjqLmAQ4ndJPQdmTAKdAyOksYhLgdEo/BWVPApwCIaeziEmA0yn9FJTtaDYpBXSN2yI246Q0jy4X6RIG4Fn3FGp00SnmsKWos5GYpbMxR216/EqoHIle7NlGMFZ0kwDHyiOpd/te+06BGTHv01vMq7CIU+kFWNU85MGiJ8ankswuBczWAeaUs8wTmLbq5PpL/+t0y6mP5i+viJn/nAQ4qZDGZs50791k6t9D3YU7jHjXvVZLlmTSPDj2nuHP5gFzqP04eY2fg2n1WUX5Jd9HBgdFzr1hsg3ulUQKfk2T3wsgcsa+KObD6dXXRMzIzMF5TwI8WCLJvGd8WrKyx6x3jiRHN6qLKWIS4BhxJPkGW/EkqwRoBgYnyLj8JwFOlsRdku8kwC4BIllkTAKcLMm6JN//B+0kyR+YO4qsAAAAAElFTkSuQmCC"

/***/ }),

/***/ "./src/Icon/store-1.png":
/*!******************************!*\
  !*** ./src/Icon/store-1.png ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAC8AAAAvCAYAAABzJ5OsAAAAAXNSR0IArs4c6QAABelJREFUaAXtmWtsVEUUx3ehLc8KpWCMShN8oBKFCFRMa6SJsSpRYlLwgRpJU/ERYkT44iNRIoJiUEM0Bp9EK9VA0A+2EXyAUcMHKqBEbYgmNjFEQFAoUt7r77/ulPHuzO7cRrdu4kl+nTNnzsycOzN3Zu42kXBIKpUqg7fgKPSVnKTjPTDNEWLa1M9TMBD7DVDqKS+EOUknI6HB15kv+G4qtMBRX8UC2FP0sQtW+vrS0zmF6SpxFhTemEomkydc3TqDJ/BJOM93VegD2w76XMYDdOXtWyMOn4NL9BIJW1w2leey2/WN7vPXSzvVFbhrzetFrXU4H8e2Gn6OlO3L2O2p1Xr9NmM/afnrXVoD20E+RuSjtreAbVd5JTRKySk84XBoNUMRSdvInw7rI/bl5M+BI5b9BPoiGAWHLPtu9GqYCl1g5BjKeGiA343RSg+iT8wX/DU47bMqGXU/ygSohZ3GSKopHQ0PWDapCv4xGAp/gBEtjWUwBFaB/Iy0oIyAjcYQST8hP8D7ABSuAXVgi/JvQim8BKZcHT8ClfAl2OILXj4aiMlQD7+BEY3uFTANNBNR0Qxe7wyegingqqSZUINaGvYodpAfkymzlwwm78irTLIBBsNqMKJBWQGDoNUYrVTlmq0hf3sADJreL8AlWuOazveswuPoT8Mw2GTZjZpr5OWj9T4DzgW9B0Y6UbT2rwTX2pfv5SZ4s9tci2GyMVqpdpCnYBJcbdkPojfDDKi27KGqRm827IYXwOxIZ6PPgW2wEaI7z0hs83iA9PnUD6Ucwywog6io8iC4CwZbhbJXwXzob9mNqsbPgzpw3Y9UXg83whEwosFsAm3V5oFMmVLVa4CadIbg61DWQoUMDlEjqiSMKHgRtZtypcbHzK5dZnQTYNTH1aepo7QNbi7hz32g0euCYpGLCbQ6mVk29qgWywN0K/g3iiXaSJzNCt6sr0iZM6t1LAmZqTi+ajOOv3zv14uyXzUDZS9+xwJ9dQmTf6jI91Cgs7bwwwp+U2AFua0H7c0h8gNO8g+VdTh2BDprc+lU8CtAo2SmDTVLVKar72L4CnL5UpzQyHwE8j8AufxVpkNvCXwAunrnEvnvgPYEa15H/OugI98nOpZvUYukugP96HPM2DeTXpjxX4B+IIe/LlxPZnx1Xfgsh6+KOqFO/mkhUwEvguti9gv2mdBzkqKPBd0kdVmyRXeaD+Esq+2B5HWku9qWbSEMtfzPIL8W1JYt6msb6EaavWFgrIF2kKjhpXCmadhOsZ8GjdANW2El6GNC14kswX4+KFAF9j4sgfFZjhiwl8F02AuSwzAXRrj80zYKdWd/ByQKqsbr/Fcn5fjoft6Yy88uw7c/6GTPKfjoavwTSHQLHRatkLeRaAVPXi9okPh+xgiqHHHSblMwYfRGwWxITz/pALgVxvQmiIIGT4AL4TkYR8D6SVHb4/PQ84GBHiyxgqfDKtgAWotTQnvBVx/cr+F/GzwM7Zl0Dukq0JU8tsQKntYvAgU9Gq6DvELQlTg9A7eDRv5d0APoA0ez8BDvgf1BgilM4gb/Hc1uBx3PrWFdpL96dMDdDa/Ao/AgvAo6nCbygP4tEAefxA1+Jw11goL/2tdoxP4x+atAy2Me3Al6CI26dA3CdIgt/9RW6e2YJaHz4nscNMp3wHJ4FprgcWiB0FnE9ZTEHXn5JzP0XBdONZetEbhOXO0qGnUtlaUwCxbB2zzcPbAHPbbEDf4CetCRXgH1gb1dhp+WxROggMfCYngZFkCvpSRmTV24qqAMLoVPIZ9swUEv7FZGWDfXDvSZ0E4+9OMD92yJG7zu8tqv9QDN2c1lWwhQL/dmU0Je22LIQ5sq3jRW8HT8Ky3NNa0xiuVG74s07prvixi9ff4fvHdo/uWCoh55/edvAgNUC6Wgg0f7sET6TZRXp3PuP7rWatvUfzmGu116bVU8ZkOQfi996FcOyS5oS2L4BmUcaBZ0ev7XRT99aPttKtZlowdIaOQvIa0BTU2xiJbNuj8BkYYE/Yc2ZmkAAAAASUVORK5CYII="

/***/ }),

/***/ "./src/Icon/thumb-up-sign.png":
/*!************************************!*\
  !*** ./src/Icon/thumb-up-sign.png ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFAAAABOCAYAAAC3zZFGAAAAAXNSR0IArs4c6QAAEsdJREFUeAHVnAl0lcUVx8lGwhZBEBOIJHjUCiiCIka2QqtHOWFxbWmpwkGLRpFFNm2tjRaFCCaALEot7krFhX0RUVqUzaogiKhYQhIhEoGEsCQkIf39P973fGu+t3wvsXPOZPY7d/5z586dme8lqkE9ugceeCAxNjY2vaampg0+CVaS5aOiooy4I68B6SLyD8qTZ8Sjo6ONdFVV1ZZnnnnmGGX14qLqutdx48ZdBAgD6HcgvjP+M7wBjAnKmTNniqhzEHAEUoP4+HgDUPLdQKZOW8DtSriDtsupujI3N3ev2tSViziAt99+e0zbtm17M6CBDDSD8DiDXkN8zYEDBzYvXry4OpzBOuinQ6M/NG8kbIpfIUALCws/Cpe+FW8RAzArKyv26NGjw2DgfvxX+NVI0FqWW7EVU+GUT5gwoTWSe4PAZKI6QWtm8+bNX4WfqnDo+mtrO4AwGg1wQ2B+DJ2+CvPzI8W8v0GZ+Y5JvJf0nQCaM3v27H8SrzHL7QhtBXDs2LG3wOgjgLeuYcOGU5566qkyO5gMl8bkyZPPKS8v/zN0pEqyZs2atTZcmmZ7WwAEuL4QfBKfh+6ZnJOTU2B28HMK4TONCZ4KT2lM8sMzZ87cEC5/YQEoBd6mTZs5MDMMxn7DzK4Il6G6aI8lMBh9/Dp9LWQjGxvORhMygLLhYmJi3oaJiwEwA/Phy7oYvF19jB49+jLt1Ez816dOnbplwYIFJ0OhHRKAjqWwGuAO0OmtLIWSUDoPts2IESOatWrVKoVBn4sERWGEl1VXVx+cMWPGj9A6Eyy9zMzMFtiY70KvKWAORgi+D5ZG0AAieV3pbB0dvdyiRYsJ7HRBMx4sk6o/cuTIc3CDid7BxF1FGEv4DUCuBsR3jh8/vgspqlTdYJx26pKSkhza3AKtwezUnwbTPigAHbrjBTrIRN/JJKgTx6TFs6vfhsrQQFv76PQ/gJmL+bQ4FBBFj1U1lGAO/i5W1DvKC8RFB1JJdcaMGdOZGfo7jA6sS/DUNxLfHH8PURO808RPs/RMm64bvD3FiriV/KCEgvqGA7TXGNsgSD4LmF3MfKswIADHjx/fCkIr8Zl09LEVUbvL0XUpDK676DLACoIlhLLrPsQbyp/ytuQ9PnHixF7kheTQgRtpmIlfhtSfFwgRSwBZto3QMcsg9hySp123zh3Sp1uaeHUMUMeRtg9KS0tnAdg4smQ6CVS59viH7rvvPp2HQ3IaI3SfR128q7FbEbEEEGZfgeA+CE+xIlYX5fBSDYiV0nXsvjuJP06/X+PPUBZD2K1x48b9wuGFsT4Orf2M/SUrOrUCiC7IgsE22EnDrQhFuFw6z3DwIx1n8l2TnZ29mxUylwFXqAwnfXmzo3rIgcYMrRRhUBsRkxGvOmwa15A5tKKiIiPUnc2LaIgZSEK5S9Noh6SZWTUMVqeKI8qgLI6gA8uvrdKhOo1ZY6f9MPShrst8Or8AUnsWM3vH/Pnzj/psWbeZrrooGslw43vevHnSi2vFkkSQoAU6rEO4LGrs0B2KROf6o+XGiFkJsZX47ufubouZV58hYDRz6f8MvHldwlLHaQAjhY2pk+LSJuQohvUmaBVy9Bvii4gXgJMmTWoGA0/A0CRfDeojD34ucen3BFLh9QZCnR/MOgw4jjrnmGkbwonQnMJSTvSk5QUg61721WtPP/30fs/K9ZjWPZ7hAEq6Tg9Lbo58w8xxZNYwYC8pdWsQRALbNw96S1nKf/Js5gYgG0c7Kgxj9n4WJouYRZ2kwbw2NDmduwuYZK+HI1bML4waZ/9oRz7skg47yn7wGDTv5FCR6krMDUAqzMZPqc9nQlfmFI+Li7uLoIXiSJmW7o6ioqKDSru4KCa9h0u6DEC9QHYpDzoqTMAmm/eWbNfGTgCx3pNgsOP333//rGuF+oyjjzvAtACU07Lci1/peQHK8e1C8q8wKmFoExYcOnRoj9J2Ol755kCv+6hRo9qYdJ0Act68icz5nsyZFes61DUTfT6MT3b0LVNlHaeM7Z68oJuuI884vgFkOfFtCxcutP09RthAfw79DTJ5cAJIgQBcZRbUd3jixInh8GAwysqoxu/GPwewAsjNkT+QDBnQWuYn8O+7VbA3oYtkYWU4A0CZLmReyhlQZ8p6dw8++GA6/IwGCMNsIC5jfjqWQb4nc9S9gHqX4XUOlo1Y1KRJE6dN6Fk/3DQ7st64L9FLn2gZAPLklwEDy8Mlbkd7LIHzUSdPwk9HwNCpQlL11vTp099W1LMP6vahWqKjbiXLa6UvKfVsF06avtZwfOwvGgaAZNyEr/fly7V9Y3bd8fB1NfxIogRYHjufbFN/Tkc20wasqqys3Oivol35TOgqYSZ60cxWQ8Lr+IJgvV0dhEJHd2/wcDeMDaG9eZ9XDLN3ctEp49mf05FNG44ktZqNJuJqSJsZfPYTdtE8qHSk760knFdGYqYunRjB/QoA7oKxCxx9l8HoVJaulUTpdsZY6mqHtEZ8HNiEFXS1XdhpCcvS36XO68MBXmxZWVk6YI2l/8sdPFQAyhvHjh1bYMUTvP+Ir1I9whieKa+2amNT+XbopEXDuAzoQzYRDYqMvmzgObIjp4YxNPwlAETBi0wWLZGZ3MmdtCII/59Q55SjXjybSCZfaF0rfWrVNszyYmEXS4dJEPouTGKhNI9KSUnpCngPAdgAAIsj1KaxkfgTGMzfBkKUXfh9lu0X1O1Nu1hI9CXM5Q15I+bZZs6wuw8fPpz34osvetmPgdD3V4c+isGuvZRvGkgGdO/HcovmW5JmLVu21MZjOLbzWEBwppUJ01HsprpSMozbszWdfxtRP5GBXkWOzKd0QqM9TH0OU7Mxoj/nyy5jWTpb+YlMnTq1GJtsGn2lUUXqSBPRnVDqIIO+Clu3bp2HVO4jvY96+ZR9g02prxlCdoxRAPYzACQS0BI+efJkEjvleBr3MHtG+UuPyjsVOUyrOJrQMJOUcHEqFGCtGIguCcx2exlcLpK3dtq0aVLSAbtGjRqtR4+OZBy50OyEF00tYZk48pVknSAsQ2KPE5Yind8C9HLMnnUYxyXkBeWgpw9F0yTySTAeEIB0loBg6dpIUhOWOzvGsyTgQYOZzgQt4aXNUu95dszKqOrbt++H3bp1G0LZMGgPJUx2qRdHXnPS8qbrQl4/BGAHlxHZAez2ZjsjpK1MrCS9LyRxv/aDW2ndJSRp65iYYU2bNn1Bbxuhdr1hw4YqwN/FqeoRBnYlvj+CkUW4FF+I91QJCeTpMf5G+nyOe76bgumbyT4k7AwDNDExUUuqMhgCYdTVjcZ21MAq/DKM5M+gZdsHSg4bTTfWaxxerEaxZJPZbLqxhK8n/Tt8S/jQUpdK6YC6Gcm5eh8fh+4gbemgY6gnLeEiJDCJFpHeiaWDptLX81xa1LXE17ApHaB/fWGxjCU/EQkaTnwSPhUvMHqjnnphWu0K5EpPP71Awou0hItAXwBG1DFRq/D/qAfwvMYFgOVsVgvg5z4KTf3fFEAuT01NbenVwEcGdZOEnY5B+lFLpAGU/nkvLy+vtjOtDzYjlwWIUhvb2bnXmr0IB0BpZqYtwmRhZ0ggjSIN4FH62M/SiPg51WLQbsWnTxtfyDk3F3jUUvZlerm1U0KY4YsMHUjkfK8a9mb8gMiHvMPay8pZao5L5F8jRb1d6B8GC+lqS+eQ1jx9Jqsl7DSMLVuGUAH6p/DOmQ6BhK1NeCDXd9YPQbQ3oT6SF/2ThHswqXT7HYhLRih2SlzzaBjRJQz9U+iaujKTLAefkJBwKzzdQ8XOhOZJaCtmzr8wq8yLiVrp0CyJMRVFY4vJDuuGNwnV2jCUQjo7TWfVobSNUJtG0DVsYAf9r8BhHtIYkA1IG90adeUU82m0HoxJfMHHM9dGiNkGiLpxwRAp+sHSxWxbxJh1CjLd12woAUsf7zY9abhDP2UzdhyILUFKbjGpRSCMAcSISXiw/HJZsZ/xFpjtGH8My9q4ATHzLEL9JnCJ6hgAsryWEo8YgNAXcwGZB2Iq0g4bUPwYBjNA6A6yHB4D0n3iTcKGvvwJQK5z9K3JIc6CV6iC3Q4eG3N29HU3aHdXlvQ4qjXkGPdbKuoqTWCU4/P4VLjUsjEV+NjpSoI8Lj6ML8RcFekSltnNFO4IhFCQdc6Dtu7n6s0NHz48gYvVVCazJ346oOkVT9/b5CN97wfBmH7RtMis77qs9JsxARgJdz60W0HYtb9I9OOXZlJSUi/A0tdn8/DnOsDTvx9478iRIx/5behRIIzM5asi54D0WQfE9vE++0uPNnYkE5jlrhiw5nuvHTSDoRGFmTIUyelHI+MRnrGWE3+TvFmBPF6pM3bfPgQ7zeWrPCeASkBMl5HP6LVMaZvdjex0F6LA3fq0uY/ayOmY5jTmGefHnDqmcRMd0DWeMKH9HHT5ZNdO3AbDB9W7ILyJ/7Ix0rWSTXE98jzCg9F1gKgL3Lp0DKtmOx2WmZ0CxhecOvaaaasQTO4VNp6fPrtuIgYNLhUfZWY+Ybm9JiPbinCg5TCsXVivcBfzkrePd4hC0ofxe8hbz3W8eS8XKMmg6qFCmtPAOV76VN8BOV08cBE8iRWqE5ubc5NAlWggDPYFrPW/utW0J5EAbf3qcxChPuMYh89hcIt4mhwiE8Oebtyp8KTZHsBuINf4JE2l6MQP3Gv5T3FK+Rul8xEovcS5OeeMuOZCPAcAd7OhzA9GzF1p1BYHNJ1KEhxeF5itGCCXwalN+KLgjUCVem19SGoYxw2MQxuHJKcTfUiPyfb7N19EfFpbe7MMDC6i3UAmvaOZ5xr63Cy2bdtW0b17dz0vTu/Ro8dLW7ZsMa6ievbsqe/w9AmG/tfVEUI9Tuv6JxCvlzHd/JRQX5LgOnlaCS3wfXlriKbvHfSpXTIkJ4XPb4cHINmvQEBn/Lb4hvStiSuFjwykyfJhHcltAnDraJPt75fsIujTabfkF+BvURgHM4NJ2/ZyBs0oTj36DXAvJGQU6avVD15Ov7pchPQ8VlBQ8F0gDzxnm/30V6ogLS3t9+QsdICmwiriP0A3EzW1/KfavmMaP19frYWXk4z/Zn/j9ymBIsk7aw3StwwCmSjQK5GIlb67Ci138+bNx/C70tPT10NBRnYaPp5B6pnhcqTnBr5vOdKnT5/j1ImDlwabNm3SlZjOrrW63bt3V7NaNEE635uSvgID+I/5+fkfU25Jo0uXLs/SNpXfJWdw1HWaP54d+5VAs6J+rc6sbSY9H0I5Zr6doX68AmD3MuA/QFeXu8agmTy9IX9O+Blevw3+Bl/CstLt8Sl5JLiCQVYUFxef7tSp0xnikuRzsSaG0CabOjGElfg7ebx/058kuY4Hg3ki6XsA/Jq5c+fWultbAijCUqQwvYnoCE4sK5Rnt9NH2wzyJg2UQadDX5eenvxJEo5QR5cfOszLYighLSNZulW6Wr/1u5R0X+JpeLmd8H8PS1eCUKvjXvRmJnMBQpOOnrQ0sj0Z9EscuzCd2dYyHoAkWjLil1AtBVL+7dq168IAbgMA/Va3E97L1KqFhFcRYOoXRjPgfR5fctUqTUheLwispP/rsT62eRHzkeFXB3rWZWcuZHf8lvxl6KS9W7du/dKzTrhp6Sb04kH0np4Z9iA1+xlMKXFJlpZ1PD7gSaeuLIW3WYoLOLIVkvbrAE9XXNo0hyIgAduIwTBjdK47Qwa2lMS7/CxsQii7pEEogD/65xH85iMZEFoiSYk00SVoW3w78pIJjVselRNvRJ2mhKWkfyS+h/hSeF3H8auAuE8rQlLPMW0G9ftTZ7AuVQgDdkEDKMr3339/S5T0m0SlsAcxY9I/EXcabPv27Rsz2CZ4ASYfzwE/Hp2lJ1p97HkaiasAuFJ28QNsGn4/l+NyVMc7WRql1B8aytE1JACFlGPmpikK4/93/3yMjVEnkxX4l5G6LMZhadpQx8uFDKBJSboDAPUkOCxSO7TZl10hkqdHoXnQuztcnsMGUIPSj6IJdPnQHDAfZQfbSfxn5+CzC8A9Co+6ZcpC9eSFy6QtAJpM6H/ywVwOXv+W7i/olN1mWX2GrJLO8KQJTkTXjdO9p1382AqgyRT65TpmOgcvUySrvoDUhGIGCbg0AHyYlfG+yaNdYUQAFHPsfjqMy7aaApD6vG2FPIPQNVJIClt0LVwUk3cV/Q2gngxxnbEfYam+ThiRPiMGIAwbTrs1P6jpoUE5BnYuQK5CMlfwbcl7HK9OmHVDCWUrYsZcD00BNghfjNeJaQW/afk4knYqfQRl1at+2E4XB+jHQYCZwaD7EOp4lU98P/F8M046H5vuv/hYboTbUSafSrmMaNe47hE3aEKwTVd7vllQFlEXcQmsjXtJJ2ffFAZvAERdZ6i4w4uEAaxCAasQMAsU5zRUEGkpEwP+3P8AMtnAD9IhU3UAAAAASUVORK5CYII="

/***/ }),

/***/ "./src/Icon/user-1.png":
/*!*****************************!*\
  !*** ./src/Icon/user-1.png ***!
  \*****************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAHgAAAB4CAYAAAA5ZDbSAAAAAXNSR0IArs4c6QAAFztJREFUeAHtXQt0VEWarqp7u5OmEwIkqEBIOonoyowKCOqsCjGoiCs6Lzk7c3ZRWB46O66OOw4G9di76BiZPaOyq7MZRB2P7pwDZxQZnhowigcURdQ5KohJdzoJ70ceJJ1+3Fv7VYfEDt2d9OP27Xux65DTl3vrVv31f/X/VX/VX/+l5BxIxb//lY3kF02QJXYJJXQCodTBCRnHCB+NX/xRO+XcRgmxckop4dyHXy/+34W/45zzY8hzkDLSwFW1kVD1K78/8PXBJc5us7MH7TNJWnOH5Oj6nqXLTmTLcWlcjt0yjai0EmBNRQsmALdhWraEE95NOGlAd9ijqvRDogQ/OC3JDXnMG3DbJwbI3LmKlvWlqyxjA+x0Wh2lZBhV5AJVslwBiayknMyEhF4ChuhOOyTdg7q3EqK+Dc3wkeol7Z4iXxeZ6/SnC6BUy9WdSUMS7HSykWNy8+0SH20hZDJh7AZC+U3AczyIlYZ8X4cMnBMVNB0BTW9B0rcEmPqJRfUfdW/6opOsXWsoyTYOwGvWQAW7R4M7E2RIKiF8DlTkpVC9uTpglkIV3A8690OyX1dU8q4iB75u6fEcJUv+GEihUM1ezTzAkNiyYnk8oezykLQSeita5wBhmactATZDZYt/zYTTtyhXtwSCwU+bDylNxOkMJlCM5lkzysSxLz4+3kIt0zCu3orJzGwQc4HmLcxAgVDbRyhh21RONhCq7HLfVd2E7ooOoH/KCMAlzz85ktnoLEbpbZi4VMK0GaN/0/WoMWR+1WPM3ugPeDfC7DquR63hdegLMEydku6plbLK78CYdQMIqQgn5ly9huh60JHrMDFb627y1emptnUDuPyVmhKi0J+hsT9GpZMBJibJ352Ediuw2T/nlK/nweCfmhY96tKj9ekH+A5I7ayplZJMFmOsrURDi1Ap06NxRqsDbRf/2iDNO1TCV3ny9m4mc9NrVqUV4OIXnKNkZvsnjLXzYDp+DxPjHFSY1jqNBurZ9AiEwQBhQu1XCXktwJQXWu9cduLsfFr9P23MHl+7vEK2Wu7DWPtT9NvzUJEhFim0Ylyq5QBoBTw5wbm6PqCSp1oWVn+TapnR3k8L04tXPXGZxSLXQFh/gkpHoiHfSZUcjeF99wRPAPIw6LNLJEYvyru9qrHjzW2tfc+1+kU9GiYsWpSW5F4NlbwC4+1VIF50IG3r0JBcgxQFsxkTMMr3YkfrYZf94+1ajsvaSbDTKZc7bJXolquxvDgJsAqpzYI7dC/CMoDgFR2H4fma4YGxDe23lLvIhj0YolNP2gC8Brs+ttwqECnALUudrO9mCUB6JJCens9GufNnXOrq2PpBysucqQMMcEtOD7uJUb4aBKIXZlNqHKDDsap5Pcu1Nw675TpX54Z6MeNOOgk1mnyCWnZ05VRJlK8CuOfocmPy7En2TWjB0fh71iLnzCYrV+YkW454L3kJxoSqosQ+A2VALZOs5KaCQpR3ITB5jJJr8q2+r9onX/cNqa/HpDvxlLQEl5bIV3Oq/gHgjk+82uwb8XGAjpMIe3Z8ce618eWPzJUUwONW11zOqGUFirsossjsHS05AFVdbmH06dLaJ6YkU27CAI+vXVFhZWR5yM5NpsbsO4lzgJIpzCrVFL+0YkKiLyc0Bou1ZYssV2P58cdQzWLwz9q5iXI8yfxg9HisMeTZb6za2blpmzfeYuIHGLtCoy4uWQSVsRiFi+XHLLjxclmDfFjlEtq2jFmZt8068gPy5ZdxTbriVtFlN02tYoTOQyWjs+BqgFiCRZzh+Qg48N9ZPuuKm+N9PS6Ax9Y6S4hMFqHLTERF8Ut9vFRk88XFAQyLWOanFapEf1H6yu/K4nlpaLCwmFE4ynY3bLKfoUCssmRVczyMTVceCBmggBZVFaVt8nXvwT4edM1aHoqQEodtBmbMP0TBhWYFF7S3o1sewFY7nN7wP84LMVGcgPaMGKr9RnsuMEALCjAkzyktzt3VRMjGwWhE/thJzJqtUu5vUeYC5DKXDxXnp3HAbDOAfZUx9kFHZ1e3RcaJBKRAkDBc51rk3GkQh59j1WgO8hXE5oQhnwQA9BrFq97r+UX1qVgUDirBAPdmzN5uQi8wD7iciB2Yt1Si/qd7096PBzlKchr5NsPDcatjrOUyIP4YQL4VAj4oT2IxMgP3LcBlupwriQnXn2PVH1OCx736VHGOn6xAo8XYa/gkFC901wmVqs/1cP7c4QXLjiVCdMhXO4fcRxm7B++ZxzGQkz8rMnu4ad6DrmjtjT6Lhs1r9ROsNZPro71ktHsAF6qXH1aY+ozf71uRKLiiPULN+YO+FfCRqoEUt/aWabSWRtLDGQ4OBNXpBD7nkU9j+EqVzbqqGK6d/wDxNvxREiG5aNgp0PtyD5OfSeXQtnjX7/HVor/UYiKGQ+GhsqPxzTD3KKdjgNPs4q5p5dGIipRgmEWcBSdRRuM2pqMVrOO9HswTtnp9vmeOzHuwK9V6Dzqd3b4AQKZkPaU85fJSpSee92EfX28l6jTyjjNi/hABcOkYCzabuZhYmUF6xVnc/YQHVx65x3k0HmbEk0ecIVKCvJar9G+QYlGH0dN5GKNmjmmQI/blBwKMTXzVarkQmnu20Vsk6EMnRJgF/hf3gmUfak2vZ1H1Hmjo1/HXpnXZ6SgPpt5MK5UnEsyfwssfALCjgAyXCK9CBkd4JiNeQ7KETXvAr9CX00VfwBeE+UHFqr4ZpLgEw+qMsT+6amQ4PwYAHBwhjcaN2yAZ+GfsBJMogIg4G1sXLW1JF6Ut9zzSijNEW+AEZ/ixWGAGntxs9anF4fz4FuDaWouFWS/HWVacITJ+grd4TyAYWJtuSmE2rUMdnWaYUYPOvyOqMsnxkjO3jy/9ADush+wwCmad2cjve27cX0r3t5CWfekm0BNwH8BJ/a9RT2iZM931pVK+wA4LNVU4EdO/xt4PMOkhI6hKZqVSgZ7vQoJ36hLoJBRMhe+GCkzZCV0P/giTSZECRX119QKMVRBuzbkCK1dj+x4Y/Rfrkh/rSONeLKSYYaIFo4KMkRV5Mll5r3Cp6j31JyLIMcKq0EsHTLF1ZGDCVbEgO5DwS0m+oHCGiHfUFACHMORkuiO/0CaaG5Lg06rNgl3GmUm2PyOvBb0kbbPnyAZ5D+Ke4cfgfropma6otlBoxxDA1qAyDj304v4MJrjo4l6x3adL6uoip4XuM0uCFE8giipWtUKeesQiS9eYhfg+Ok+eJLrFhzwZbA/AecA8CANY2SpdSWoXyyEJZoz8oI9xZvktOa8nNMboQW9J7nm5gBeCYaLElauKu+y9AMP+FSF5TZVU26h+UyDdhKu2YBHmKOYCmNBJvpwCiYWCaQudbbKE0MIOvUiWuIil2Tsh1atODeqpoFaLnZHCIgTTRjAQkyWZq9/Xi2SMYxM5p6YxIQVfsLs0zCbxCiYr7BK9GKVlPZzRv9eyvMHLotOgn+XB8xjvKaXSRfAapaZTz4KVGBKvwomLtI/Dog5ouKmYQpsPYELKRaymMuP1vaEpwtJhYY4sAr+kN1mstunYiBOB3Ew2yQJfVOIQZlKEm0d6WaZN6dA8Vs7Izwm2ObUpMUoptYvhe8x/AkblR3lq/FuMXQATmIw2PqWRFMJokUD71DJrW2XkU23uOKxl18D+vRJ1WbUpUedSOC9iID7t41g6mnVGZYqjrHePXuPM07qO0c858zDOz4d6HmdK9SwYQkkhw/Rfc+ZozexY5WFxyYKx+Jq8ztz5sfIkez/PNuwfsT5fiff7vSOSLStj7wFbnOnmoX3DjBGRQsV9UowDlQtKVz2p2YSrZHUNotrwBZiAmld6wVfwx4oAdaFYGymwObOvQspEG75PJfaIiP6TKjWlrzx1CQK7LcXkSti+plrciGg7XHgY2BNx33Q3KJFhq87IofTpMoQyTpb+8pdqLmVBceAOfuGUms7ujWg3TA3qeKnmNPS0PeKhOW9gSIYrDyXLA3ZrXcvcB+KLRoNwgWW2zulElh4HT6ah6edAr8cgw3k3LA3qMyeWUamGxg4B9Ce5y7+4aPVTQ9uvsKPL7N23U0n+P4B75bkCbi93aI9Q0KejssrUN6lwG7XRfO/Qm/SHDikKpSrMxeGmbnJ04rsEwMejPzPnXSAKj1p+FDOvA8fmOoc+keB0qoQph/BWQ+hdczY7OtWUnIAdzBM6CR+9JGPcBUDC89GNTlvrCzBxIG1oCUYmC7McAB9eQcdowhum8H+Oi+OcH8dmAxUeg6ZOQBGHDwg+TcO3w315uU9SVyZyZqlh3oNHg7znj9DTj+Ms0tsoRxz+No8XZWz0Dsuw9xrNOmkECOKDFiLCzPswCOoUyrd77Hv3JfNRi5aFzpMIg/ByWefU9xWqzsTsswqOlFjwQEwq2NqxeWjgJ5S7ZHCoAbNHA1MZnTSAexJk1xGubgtQuqvFP2IfWbIkED13nHfxFTIXWbsf3oiNpaziHfhwXA1kb0Q/ugGaTgRmMRWjOGcu2MErJsG/aW+cLMh8NhH/itCNnLJ1VFX3upq9DWn72CM+Wl3e5S5XuTIFK0KIpcVvRywf06zdBym9lp7/u1/bhxUVHYUUG9ovC5MgjIl0h8rVVQonH7a0+BoBrD7jJCIflJTkOCRKrgANCzEs3GB0tY3hpVsNBC8MqZyyF2s+B8GXZl48Y1DAyXEA+xwk9nVXa2AfgNXN6X0ARQhQUzrONgGq+4dYGv0VmGfkvfQvOtQTPwitt2IK8TF6pSEBxlj7ucL5Eyr31bUscooJFW5lKOFz7U2EfIUD1ocItX2BefZjGJWTCrWf9hZQ8mlu+wXB0OwQ4rwr7RUmUQGQ3KGq5H5PV+uboVluJsENo98939nm8o3YrHDll+hu74Q9Mswl+La7xW7vBRifwdxpGMr6CKEwffAtv6Zm77vk3/7beOvlmLE3Nfs/JFRdin63vY9sg/xCNrDQs3hJMKSiuxXqyWO8AbtKFUYgEKbbPkjGCtf8pTuMQE9MGjDJwyLCRyUvP/VbiYecAy6OmVffB40KV5sxfMAvESmPeQO4qNOXhpi1tQPc1S57z9aYOQz2wOMb8R6WOV+A2Ig5QsaTyvl71KeGtkpDALvtExF7mG/DWKyP2RGDBWAQLCD+Pj52/zqZm6GZcgzaBr0NdR0MBN5Ax6wTbRg0b5ofhjAEwJ7gwW5RVQhgMncuGEs+gkgfTnP9gxYPjXIY23YbPQuqofnMlZqXPNqAL8FtAtXNGab8aJDRPX3zll6AQREEuh0/WGjPTBILGVih+pIzaUtmKEi9Vr9Et8M2/jSTUoz1jO2SX+nfAu4H2FPk64J6BHN5ZhYRQsHG+J5Yga1TZ3/6Szg47yEPOuoeqGohLBlI3K+ofLvSZeuPr9kPsBjzAkz9BDp8fwYow2YQPYpARTCNTJ44342hrjUTrcAE6gAJBve2PPCtL9q3AIOinB52BLEo/gIVg3/6JVEfOtZxoiif6FdremrCEPM5Sj6cCR5i3WBDID/HE96yAQA3HurpxGj8Lvit60QBfs1B7NQcbKr7VLOYz+GN1PPavekjOAuoBzEW6zvU4TME2PV6/6D84QBTbQDAYndGUdnX2BLTd6JDCb6jQTyDfCFFT4xSq2vtWvjw0WZIcHwuu6nVFvY2304V5YuznR0GAozszcGGYyolW6Ezj4S9ndZLMEP09oyaaFo2EGGOhZ9bj5ZlDl4W/Oo42dbUqkSM/REAiwCfsFg+hYmsn8nEOfamjbEKNDgj43tKOROzWN1UNOZN73Km7o62jRoJMCjzeHweEWwbZpM+UkzFChrVscfHB1QKuURbdFnREpoWzNvi9rsbotGLuUD0VPbCf5USWVmBCdDc6Dm0u4uO1AOPEoTO5xEqRrtadCwJEV9R20SMxWn3kgHAawIqf7hlYfU30VoYE2DodApPj3/G0cz/QCZHtJe1unfGpFChpnFp/oQJo+CrOFQgftOXKGlBXctcdy19FTVF5V1ouzAqBXjB/z/qRqudTQe9d4FSKWo+DW6eYQS+B6JBYQYoIr2o9jYQwMKFm+wMqJaNscAVOaOOwX08av3lshMoaC0I/uyMlPU9yv5mkAMCC8jfPqD3WsvCB04ORsqgAIsX3Xl76lTK30RvOZUFeTBW6vMsBC4nHZg5b3S5vWL3atA0JMAhwxnndtBl3kOvSc2xfFBSsg/j4QC0aRCT0Y+C1F8Ls2jIc1RDA4xa3fOr3ZwoqzAW70MP0mX6H09jv2t5wHsx7rrhsfF8812PRjWLzuZJXACLl5o27YULjfoqLk9kVfXZbEz//8/wvAMTqtea8ivWx1tj3ACLdWI/4y+iB63DGC/WWVFnNunFgdCGDOcbevzSc8IDJ956EzJ9Otdt8w6/feaX+ELLRRgLLkRvir+DxEtRNl9UDuAzQjhkx5a2LnywJWqGGDcTAliU0f7mtlN5c25sRAzEywFycYxys7c15ABWq/6Gozu/bl7w0GeJFguMkkulLz5ZBT/qVViOK0+uhOxb8XAA46AbAN/tXvBQUm7ESavYJo+vHir6fvwlpDLiaVQ2Ty8HYA4dwmrnb9yenqR39hJW0f3Mr6/nbbeUu4ZLoxoxAZiBdWTTnJvtb4OBLyC5R+HL/KDSZnmzo7o66fWHpCU4xBvsHVPi3YRl0fvFtpWB+WUq0gAu3H7oUq+3Z124A10yjUh6DA6vrHjN721Sp+9mibFncX98+LPsdWIcEGpZEeB2e9849q/OlGOYJa+iw+juWLs1OPqGiS4u53+Fj2WIIJ6FYY+zl3FyQEyoFML+XT1lWXf4vurQ0ZM4X42ZTROAReknN+9WTk25tmF4PtvDKL0Mjs5jY9aafRDBAYD7OYa5+5o2f7yx4+mnNXP30URFn01t6QtPTmYSrUHhlRhL8N0DzLWzKYIDABXrF9g8oLwe8TScTYsf0fyctmYSHE59+/pth/N/dNMuqTewSxnc2nNhL2dBDmMSwIUrFekAxm8ELPw3zQseTngRI6y4mJdpAVjU1rHu7VP2ObN3Mqp44bsiVrwKIM3pd2OJ2VRjPAhJLbb80Ntd+FvlC/Qsb13waNrWEtIGsGBnx/qt3jbryA+GV4zFF7TFxIuKD4DkoGHfSWk+A24Hxtqd2PJ7Es4UqzrvfH7ogKkp9E3dGF26ankZka3zYXjfhkongmZLCnSb7lWAi1AofD9coDbhsPj/ivPEejQirRIc3oD2v77T1j752h0Fo2QXRQBfzLJHAOiC8Dzn7jXcgTndCpCfcZf1/KHjp48hcKo+STcJDm/OyNqaggIrmQ1pnoPP+lRCfZ+TJhUAPYpd83oMSBv8TNnUeucy3YDt43dGAO6rXKhtxizXIRTMLIA9E0Pz+X3PTP2LGNzChw3Ww5aASupb/qW6ASADb/1TRgEONXfNHVJxx5QyqyxNwzJdFdhwI+6XgLDM05YAHkAP5OPgN+P1eO1tnO/aHTpOgvX6BIrRPKtxmIg4kGPH5I61Snwileh1OMpyMzg2Eb/G/vIYJyJC0TdwbvkrXBx3YC71pSfP12KUKEHGAbiv7yKy67gy60hrkBXD93cSCLwewnE9JmXiK2S6TQr7yIn2C2kVixSI0MvrFZXWgba9PhJsPpx34clE/KWila31PeMBHNZCBP0U0jtCpbmFEqdToALxLV8yHURfFJZNr0sxhjaKIGMYW98ToYokWTmuHLO1pbqll84GGBrgAQ1feW+OI7/Qpqi2YYyr52NN7EqKD1PiiNck5KuAKtf0JB/Q7AZzXJgKfKao6h4oj10qUZtEBLlQkDEjxs8cwLDe/5gH4IHEU4Tdl4u77LIvp0CiVovdJvEK8c16NKgcWR2wO3GEkxYi9kchrvMg+TnoEDlQ+6LNPmiDHtzrgjSKsz3HYJkfxrVLhMFXVeVrqpxu7JKDp0VIXhG1lSxejDgi2BowWfp/SKQJHKKX5CgAAAAASUVORK5CYII="

/***/ }),

/***/ "./src/Icon/user-4.png":
/*!*****************************!*\
  !*** ./src/Icon/user-4.png ***!
  \*****************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAoCAYAAACM/rhtAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAACXBIWXMAAAsTAAALEwEAmpwYAAABWWlUWHRYTUw6Y29tLmFkb2JlLnhtcAAAAAAAPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iWE1QIENvcmUgNS40LjAiPgogICA8cmRmOlJERiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiPgogICAgICA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIgogICAgICAgICAgICB4bWxuczp0aWZmPSJodHRwOi8vbnMuYWRvYmUuY29tL3RpZmYvMS4wLyI+CiAgICAgICAgIDx0aWZmOk9yaWVudGF0aW9uPjE8L3RpZmY6T3JpZW50YXRpb24+CiAgICAgIDwvcmRmOkRlc2NyaXB0aW9uPgogICA8L3JkZjpSREY+CjwveDp4bXBtZXRhPgpMwidZAAAGpElEQVRYCb2YW4iVVRSAnRktU0u7mWXJZBYmlFBhGRlOCEU9CJE+Wd6gwAoUhOihILrQg/VgRVGRkKGm4ENRUBJOBoJRUYlpamhaamYXL1neZvq+ffY6/mc858wxZ1rwnX1be+21197//vf5m/o0IJ2dnS2odTY1NXWoTvkiklEwDm6Ba+AyOBea4ADshk3weWYL/feRt786zZRPWK4nKtaUroYoj0d5KkyCQbAXNsK2nP+LVBkIF8OVMBqGwSFYDctwbC2pjjrxDsqdlk9LsnOpD/kr4BXYD1vgWbgVzunOKDr9YTw8A9+DNl6D1uhLvm6gQq+c0qHZgh1hOnwHGp8HI8qKJZ1m6loy5ivKXXSH0z4XtLcJZkBfdUjTmEX9qvlQJB0EzvQgLIBLowP5vplujaKnw0m/0P8S6p6DA/AmnGcbqUteW1CIyA0hvwwOwazoQd6Ihk6kLuF1MBNeyJgfC/3tSxq6OlteTvIPgEv+LlxQ1I0xyykKqSOpkdO5rdAWCuSLhkPXvfkG/AZd5U8qbGvVBukp/XP9bbS5fVZCRLKsq05ZNAIuq5Frs4E0zT6UKIdzQ8mvhpATZIpE/RoyPsUVTuZyWlLaJ4ITWtR1nFSmIRSnk3dfzMoGajl3FjruHeU4dKRc5Y91timLoV+2WREd6mPsaeQNzIysV9qPBYUR5DfCAhWqSUHXGe8DpZpzpZaTbX9QMVGbpBWTLo5D29OwGUZm3RaV45B8jLxev5gbK2ZqXUFuJp82daGuXta95SFfVXAoxlqYFebntNPXjRGw8zR4lfIuO3Rzuvta06iTC+NkT5HQMRCXR2vBoVTlWHnMX6l4CVzuCdR3RLh9fdm4EpR6gzbSnox0+WnUpj7sgfvs77nki99363I8/pFyXz23sY78TFtEL7ZINfXQ0d5PoWDEIh+pY+axtb0C2igPM4KjwD3yITQqX6D4e6PK6Hm7WZf1u4ukau+D7/kxOuiV6Rf4CpR6V6CI7Br0Pk7a3EZIT4lIrgv9dsqfghJ1pVLlr+eoE/CK5jKP00Hvc5sI8WEaLdcUl0YDyDGUfOpXgU++Ro+DkwvIprZ20nn08Uy0b7XJqNsnt6UHl+JmuFGHvGxugyT1DKhgex5oJ8Xp8DbolLcSnQ3I9lkM99Nne3fOqdxFdlC+SqPDwYunUnd/xCAxCdLd1M2m3xJwq2hL2QXu01XoGO2Ijgc1VbWjqG4WT5VhOujN+HCurJqE0TBsGUUjZf/j1H9E3SfkzwblCHUuqdcsbzRp+e1fsBFLmTpU+dGnQQ6g1NwXDBCHuU5dDXfAvXA9DIG4Qrn54wGwzu2jXev2wzfUvUfqvvX/icdKd042GXKX6WE6mTcqZaGclpx0MDwBO+BMZScGnoLzHYjUiZSFcvKBdA7sNoI+zv7BUcqRpJFJpgfCKKXXT9IoRUTHk/O5rpFE2+Ir70m4ljEeZAyvWWks6oriC2Sv3vvXcGS0qCyUwwGPk2kQy2WfaCPbsNjHvi65tqbA43ms9PBQLkorha128IQfjeIAphF7qMU8dXfSlpafVKPqn6loQ1vKI3A3Y1n2j5eTcFyX2ePvy3DQG+8NoPgu9glUaTb4Z9yjoiecw0wSbWlzAMxkrH6OSd6xdfYm0Kd1Km6Bg3APKLF8Y8nfnmpKR0rO9liSHgasTYIITgRhMnV/wwYf831kPMOmMJMRlI+SV1phKDijcJpsj4k2fQMNhlbwMD+CD9413Z/tlPeEx8uo0BkbQuzYG46F/WLqbSrEe6C+LLciHbJ4upa8r6uHmMFwG5BYglKpd3/TWIytYz44S/HpM8pNRjCi+Dx5l3MuKOaVSEul3vmNMR7FvP4syMM0+9R4B/NY2U6qk34k+pbUTar8H8v8D2NOZaz5MAdftmaf3KOEqHT+mPpyfwv8UrAEPG4Uz6bekLDrp4898A7EclcGhoZUQep3maWg6GAYSRU9/KNtV1BZAfF+rnQuhbEUybQfUbwQlkNIGIlyT6RFmzqX7gOk8UyEW5UpChFibzCvw1FQjkHRaKr8Dz/a0Jai7UUQkWvs5KBDRNKLwwxYDyEuu4OcztLHUoZj2toA2k53UtL6kauM48kHx3o6j4KF4Le8ooSj1Zy1Lhwr9tHGy+BlIAn56nuO1poN9swdfR2mx51yG9V3wQTwRj0QGpHDKH0NvhA+wF47qfZd0rof0es6qBElG/JWma5jlL10jgQvFH7XGQPW+crS5gHwC8F68Dqncz/ALmzQPUWsPHHqa8q/zXBB5OtC2jMAAAAASUVORK5CYII="

/***/ }),

/***/ "./src/Icon/verification-of-delivery-list-clipboard-symbol.png":
/*!*********************************************************************!*\
  !*** ./src/Icon/verification-of-delivery-list-clipboard-symbol.png ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFAAAABOCAYAAAC3zZFGAAAAAXNSR0IArs4c6QAAFXZJREFUeAHVnAl0lUWWx7OThT2GLCAEZtRWUESHRWiwnUZsRWRxsLsPMw16PKigAsoio6NxxmlBJKwSjtMiMuoBscctLIoLoOwgaBBkaZKQQGICWUhIQrY3v//zvdfv5W3f24LUOZX6qr6qW7f+devWvVXfS3jYZQxPPPFE+6ioqEEmkymNmAIrqYrh4eHmZ0tZGPliyosUKTM/R0REmPONjY27ly1bdoF3lyWEt3avM2bM+EdAuJd+RxFvIn5LNANjBaW5ubmYOkWAI5DC2rRpYwaUcgeQqdMVcPuRfkfbT6i6YdGiRSfVprVCyAEcP358ZNeuXYcyoFEMdCRpNYPezPPms2fP7lq/fn1TIIO10B8Ejbuh+TvStsRsAVpYWPhNoPS98RYyADMyMqLKy8snwsBU4lHiJiToU5ZbqTemAnk/c+bMLkjuXQKTieoNrcUdO3Z8G34aA6Hrrm3QAYTRCID7A8xPo9O3YT4rVMy7G5S13DKJj5L/E4BmLl26dB3PJuv7YKRBBXD69OnjYPQ5wNsSExPz0iuvvFIVDCYDpTFnzpwOdXV1z0JHqiRjyZIlnwZK09o+KAAC3G8g+GdiHrpnTmZmZoG1g19SCp/pTPDL8JTOJM9dvHjx1kD5CwhAKfC0tLTlMDMRxh5gZrMDZag12mMJjEYfv0tfq9jIpgey0fgNoGy4yMjIv8LENQA4EvPhh9YYfLD6ePLJJ/top2bij9XW1o57/fXXa/yhHeFPIy0FOt9DW7W/+UoDT2NmQzl86dKlW5j82Li4uO1IZVeV+xp8lkAkrx/gbaGjNZ06dZrJTtfsa6f29WfPnt0OKRhB2QgGE0fcUVlZ+cHKlStL7OuF6lk7dUVFRSb0x7GsRwPsAV/6ivSlsnQH9f+POAV9l7l169aATYLBgwePBbTHibcAZHdo34jnUde3b9+j+/btq/eFP3/qMobm3bt3bxo0aNBP8LCW9AR52a2GguElPG3atJuYof+hk1GAJ3sq4IA0t4GefOFoiC0gTqGPfPJ3tW3btmPAHfhAgB35HXi5j75XoqJuNtrU0BJ++umnr8K6PwjR6YCnjcPvoJ07JSUlqqqqKrxz587XsRG9CONJML64vr7+WHR09AuoiN70N4my7zDKTenp6Y2tZYwjKPfDz6KmpqZbjXhNXgFk2cYxkC+IGwHvJb+Ro+HcuXMTAekOALoVJhMp6gXdvjxHkh4nraZM7lcH8vvJHyetIz1AuikhIaEkUJ0Lba8BEJ+nT+nkO9kgaz018AogxN6HwCXAm+CGUDiDikMRd0F3taOOk1pgNi+wJM9UV1c/AFNTiD0BJJY0lvoxFrqNlJko03IWXw1kZVo0U6ZTmb9A523AjyIK/CiiNTRTt76hoaG6Q4cO5+GnzvrC35Rxv0PbaMb9gCcaHgFEF2TQeERNTc3t2EkNrgjhvCdQ3p9BaYO5noG0aVkPAHaxVBcxwFk8P8L7dqQe+7anAc0G6H9M2Qs8DyCVrrLXkfWQO0/5Scq/QFL3AaJfdp2138mTJ0fHx8dvI/8Z+jHDWt4ytZ9Fh3fMwEAKJmArDXAHHu8jkIp0DkU1S6NhPpmBOO3slNcTEwBBkqGJ0O5tGEBoNrG5XCTGQ+NG8kOJkkJzgLaOxOpIyyi/lgm/RH73z2/9+6sxP/bYYyNZVd+y2W1GH7qk57Tc7LpbAjj/lpWVVW5X5vAI4WgGdB1M3wrzLsFTA94nsynoIHUv2a9ID5MeMxKp+yP1dtDPJuh0IF5DXkvfFijTpCWQdiW9gzb9kUC3wmFr6OVBY2fSJtD3IndVXXbC0p1Eg3x3qFuJAXA0SzOFvAbmJHnWeqRy3nVS878wtIznNNrF82xECpsYQB51a0h1kq1T7Dg72vaP4fShDSgVfStVEvAZIIb1TvAoxPX7A89r7TvTsxOA8gxYtv/NshzcsnLLPBuDGI4GEE+SHEad9tT5F9pr4JKkIvShoZNoQDMhvR1pM44oPZtGdAc8VcLFkzYajzxBw5cg3f05K24jQuVw/+IEIOA9C+V3Fi5cmO9LD57qalC8TyKZRPwjA6xigqSnvAbqqvlVVLTuzl7bBBc7jrQXL85DCj+C7r/T+TP2DDgAyMYhV2oiy+U6+0pBfpb30QZggkw2tORQVy+idn7EqciyFy4HMWdgS4kvtRTT0LJ2ZVAXJmAzH3Uy355jmwROmTIlBam4gQPG++0r+Pisk5lKYiHRkI7zkb6r6hICLXHpxpAGbvmWc4A87fHHH09bvnz5WXVmAxCdNIZ8ViCns0yAbLyDxIXoCwdlS1lIAlKh3XwsxB8NSQd2RIUNunA5Y7uP4pV6ZQMQRgTgNBX6G6AhqTuHvtibl5dX7opO7969o3kXRmxMSkoKWBFy59wOY1dHYa66C0WZrIhFEP47gDJdcPJ/xW4j4zbQYEIiml1JMrPXkVOYUQCnk5ivgnH5hGkhP7jV0AOjo2y21+qmb/78+ZVmCeTKbySofhIoci3b6+gKsOJWrFhxkVOdWHYxnXBMJO4mbpMfjT2YyBWojF5DASVugs4FQrkHF9MQLX8rwftm7lHupv1aM4AUjCG+5S9BN+3Cu3Xr1pPzvWmzZs3aTh0dCz1MP3L6tyGkJbwbB3j3kI8nGgroahmG32PEf4S7lnP+vMi1bkDgN8LDv9Lr2iiYiOHQcjhfEPwp2GwwWBnQ8l2H0WENHeu7mDU4+3swC2pRHf/EOx1gGpZAC4+J0P4WPXq0XTudoLVuYPK3sAreEHa6ULmB7veQqQ8yG3LBCpGyhQA0EdoppKuJOwGvytKXZlLPhiWQutJ5x0mPXrx4sQEAreeJFpKhT+D/Evr8kLDTEpajfzgU3eo0l4nZxi1bMTOWCJg5L7/8cgV9mZV+SUnJ16mpqYfQwYZB4AqymV2+5tSpU9XaqNhEQsG6EZqHqJQehThKMkqMtPCnjkWyc6xtJfZ6Jm1YvXp1HY+KV2IoFXbYhBEhBdAemUmTJsVixvRFB96EGdDe/t2V9ozQlQo78xIGyd2hGABSFsG5XFx+fn4dBnQ4psctdDoRHVaA+VLG+1p20Vh0paezRAfWMJpNtGlg2de5sjUdKocwgxoRgHeYAeQhJEsYaevMTI3o2bPnAR1wsnMKvGsp283lT5kkMTY29jbG2cHoWGnbhN6Uwa9JLzbaLtj1JIHQTI9iQClIYCgADAewrtCfTUdFTJJcuxvI/4XNZCPSV4kZo0PShyg3vIlQFxImgVfHBvIF6WUJAhA+UqJ4SOEQ9acQcGHCvjyBpM2nj3l0Fk9cSl/rXn311XP0R9ZsF+o0pZMP/TdCjzlvroFWM0vah6bBq8rqKeHWzqwDw9q3by8J0ElKUAOuVg3u3Hu9evU6zIB7oeu2YUPpuMtsxixYsODPPCv6HZBCXRO0emB1aeLDIpCCYmYyJVQcSNFju/2A67UBR7yCpauLeHPnoeqzNegi+To/LdYSlpErAP8Wqo4BTAetzUhLG7yH69GHJspyia1yZhiKcbGiZP4VaxPRThYSCZSkccIdy1KuZSlHs+Pqq4JH6TMfK2QV38q0ESMsbY+bCAcOVcz4KegFfE0ZRDBTbRIYIgDDkbYubCL3sttaXcUH6asToG1g9rSLjeJ5DCtAP45xG7C5cjmv/E8qaLIdzv54J99YJoU+RpJfrU86juGnhvRKgb4kdD9LIJlkMqEI+gZmJAMcCfGLRP0Obh06d3tiYmIVO1kP8v0o9wgg79sBdAISKP3pAGBpaWk9Bwp7ALIcSS5DPdQwIUVYAIauTaHtV2BMWsJ5Zh1IZrBfVDw3MuFtlLL8dAQ+hj6SGFw2A90CeMUA0cwV4V4Gq896PQJI2wJAL5s3b54DeOqeTaoB3XoSOmfYrKJ1MAE9WRRSC3VEpzaUBSOkMqk58kTy6HBcMCi2pMGuW8lJ9PuAWAAIkr6t+/fvL+CzWrMuwyX7mo8ttfQ87sp4MQ2AXg19BzCYhBi8HdHVmePVDEgnPm3pqxHvpwLVcRapzMXkOBnsH/3Q58+bCBLxLbOng00dfjowSD7QYOJIq4wN5DP5wgxYesnWh7+nMdCJQCcmowL6w/sdADYEuv9AKmNd45CVbv6uEPAO8byVw4vttDnu7YNJtTUQ9PlIPza2A5F79+69NHDgwDuJp/bs2VNgoLGtypAhQ+QG9GM2tLsmErV0jiMJG/hQ2/Zl55EjR0z6mBsgY+65557etEsdOnRo7Y4dO3zWU6KBB/ArBvB74pP0fS+xG1EGtVSSNepTEHk411P2a+qmoUKqhg0bVvrNN9/43C90bIFLpV+TuY5LsVVawpqtD+lEy3in8kEM4einmOLi4kZJIBJzM309xkAKAXk1kqSNJYoDV7PUuOqXXfwS9WRHhumjR27zetP2YbL3wbMu0z0uf7Uj6Nxf45PJtHLq1Kmfvvbaa4FcpuhLsw9F2AwgA/qIgh3kZ6owWAE7rzMbyeDu3bufQFeFsZx076KltgcwK1AfN1LWB73l5I4Bjq5HmwD3U9qcFU/stmm00WfCusOWGWEEPLPTTRtNUn9iLRJcwWRslatJ3ucAqXGMa5AamgFE2RchliVPPfVUX8TyO58pum4ghpPp7CkmKBfQ9JWqPihfT+cbkIAylPwMyqZSz9XNkG7fGgH3jwx2g/x16PwzdUcTuxANg0e7KvqR16PP5ATiOOidJj1CNEs3qaHAXcgtVMzjqtZ8lGbPxIfM+FhDVIxVMmHMFlFVon4zjN9FuoUl9FeYP8OzAMon3c8AnSIDPsC7vdQ5h02nDUH/V0GS151onnhSr4F2JYzrDdq+SFxLXlI3mD4HPvPMM4bPIe060i+a1lrz9ox8QAfv8SLD+jLQVJ/IPvTQQ6v4SdgRGE9E8nbyXGjVaQxCk7aPZem0hNU3bRpY6jm00UTry9S+8BhPuV4bCTqDXEL9t3Jzc3/invp7VIY2lT6UD6Vv6XzVMRzofyxC8FtrAxuAfM5/DPHMxW67na1+m7VCoOmqVauqAEwHnxGkDr4sN3RywRQ9Bpa6fk8nnaNPiY2id5GqWbR7CxuwiN07Ans0WRMAHRnu1/B8NTydaMmXO2ZQc8N4l2NdvqpnA1AZZuQ5pOJdOusXzPsGGJSe8UnXiB9rwKeO4acLvcjLu/AWZGfqNyZrkPg3+CTtJ/KmHj16XM/4HgTUa8hLorV8UzjsEE2HiSXvFMAkkrbLsTtH2b+014Hmn4DS8U6+eJpsX+lyP6MzxWcivFkvn2p5/oyyR0jnkFo3PjN45Dcz0DcOHjx4WoLA5ng1ZbIwbqd+AqkM4RhiR9RDNHmvAUx0irTT/utUNXKQQBWw6z3PzO3DfnuH02PtXL4EDfQq9MwALH9f2zr0g140sRrKWH7HLC9sS5eB5BM34fd+QF9NROnYR6in348cRNJe4+gsRy4jaikVMOdQfi/vZVib6ZCXeWMh7TmRCsEXnw1deWwOwQlA7itK6PRNOn2Bmk871G6RwUc1sbQk/ublCUP6Yl+eie5AvC6LFuQcstCQ2yfJmsx1aDP96Ec0KtPmIg+oLSBFci1wjq+8viKvzeBL+i1kye+kvF5f3aL3ZCpp97aBx7OQ06+bLjBRXvnEBfwv6mchUE762glAKoZBNBMAj7ChZLGhnFSZq8AO2cjAZA9VwmQTDGmJdbZEV00Ml0FLk9IZ/Rmle2CeT9GHBq1NQI783UhEKdKxgR22CE9nH8b6MQGCkq/GiE/i+UHqjqeujuvs1ZXOEC8g4Tr2qued2wAG+oGQzi1vcFXJqlMc3sk/HjBggOylBfwg+i38WpezNGbMmGZEOwxGNCB5Bvplp5Stbbk5EPY9E8/sr2WQ5UyUJGgIpPVFgxS/fG+dJybg3pXoe0Ecgip4reMnWp1QReN5/zCxJzzJP7bxRL6acn2juJkfz8hWdRn0/SLAbeHlfHe/ZHcpgaIGU6/D9wgIvIcUjLbspA4dqQwvIQ8lvxYQJYUymDVQ+9l2aONrBnrd0tPTc1EXOTwfpX0SUVKoeCOxM/12Yamu43zxCCckkYD+W8omUV+migN41JdRXsy49sGz2ZugzCkwtggcgQ9pn8tGs8apgqXALYACB/GdQEdfQmg59ae4IiJ/km9eDiYnJ5+BoUTq657DNtuu2vhSxmB1+dSEVBXgC39O22sZlCRPkh7N89X0ez8bSRL9fsUGKMD0EbgMbx2u2rqjrryfixR8T929qIYK28sWD4w5i6L2+OL6hb5bE+zv1FsQsGb1a3V0yS7yWSyRTGt5a6eSCKRQ/6pkOn1rR9USNku6BRh9wGkGBJCkh1t6N6omVXSI98uQ0k8Yj0sAMZhnUe8RPI6B3k5tXOpAGtvCrl27am677bZNdP4m/5DhKGeGx20vW/EBk8TUp0+fMk5SyuFFd7JdACIWFkjMYiZp02/ypCNd2Xbawf9GfJM62dhz53h2CvyocCyTNA8J/Q2/BZHP7jF4BVCtUcxlbCpfQ3g9IH5NvtAj1RC9PHDgQPPw4cNPs0xzAUEGcVeiQPSmcyWdudRbiFStA7wSnp2CDkqhu55x/o5le9ipgosCQwCqHTtzISCe4PFjQDyJJP7ggl7IiySJnGSf6d+/v05w5Kbp5k+6XLadVJKidFYdUbahpOhjJOpZLIbPAUY7sFMAvN9T+D5xAkv7S6cKbgq86sCW7XRmCDMfUf7BmTNnZgbTZ27Zl5E8A09m5x1I3QFIWTqpfFwtV+2w3yFx24kn9V0zeacgHxc37VXa3s3L0TpUcarkocBnAEWLI3Hdfr3Ho3TNfe6User+kgMeV0f4+xjwKhGKCX64rj/7hf4M0jJz82grS/+K++djmGi9AS6buAapy2AcUgE+B78k0L4X6Q4AXAEjE2Ek2/7dL/UZydOl0Ar4ezhQngMGUCDBUDrJC0TdOTyP/5zD8y8uwKduBZ+HR50UZaB68gJlMigAWpnQ/+SDuUxiFcb3f6BTjljfXc6UVXITPGmC26PrZuDXGjJRjPAcVACtHaJfhjPTmcQfYTjjcgFp+SeLAi4dAOeyMuQKBjWEBEBxKNcLf1K21UsAWc4AshUZhG7b/FLYouslhDN5+h82cvVGEq8iPsdSfZc0JH2GDEAYNgft1tyGDdagLAPTTx82IpnZnKB8xgGunHu/A4embTlIuBOaAkyHCDr03EDM5r5jR6jt1JADyEAcAocTPdCP+t9X+o3yMNLzVDjNs47pT1ufyZ/mUPcUMQrHvzvvFHvwvjvv7J91fLZVE4JtuqnlnQXvQhpaHUD70Ug6+eyjG4M3A8Q7W6pnS1QTM7BKBaxSwCzQM95QQailTAy4C/8PAWvBUltMtRIAAAAASUVORK5CYII="

/***/ }),

/***/ "./src/Images/anafulus.jpg":
/*!*********************************!*\
  !*** ./src/Images/anafulus.jpg ***!
  \*********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "static/media/anafulus.7dbc6c6b.jpg";

/***/ }),

/***/ "./src/Images/gatsu.jpg":
/*!******************************!*\
  !*** ./src/Images/gatsu.jpg ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "static/media/gatsu.3ab23fe4.jpg";

/***/ }),

/***/ "./src/Images/image-2.jpg":
/*!********************************!*\
  !*** ./src/Images/image-2.jpg ***!
  \********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "static/media/image-2.b62a189b.jpg";

/***/ }),

/***/ "./src/Images/image-3.jpg":
/*!********************************!*\
  !*** ./src/Images/image-3.jpg ***!
  \********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "static/media/image-3.9c3e66e5.jpg";

/***/ }),

/***/ "./src/Images/image-4.jpg":
/*!********************************!*\
  !*** ./src/Images/image-4.jpg ***!
  \********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "static/media/image-4.feec1553.jpg";

/***/ }),

/***/ "./src/Images/logo-tagline.png":
/*!*************************************!*\
  !*** ./src/Images/logo-tagline.png ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "static/media/logo-tagline.1454853c.png";

/***/ }),

/***/ "./src/Images/promo-anafulus.jpg":
/*!***************************************!*\
  !*** ./src/Images/promo-anafulus.jpg ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "static/media/promo-anafulus.fcda0166.jpg";

/***/ }),

/***/ "./src/PinjamanBadanUsaha.jsx":
/*!************************************!*\
  !*** ./src/PinjamanBadanUsaha.jsx ***!
  \************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _Component_Header__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Component/Header */ "./src/Component/Header.jsx");
/* harmony import */ var _Component_ContentLandingTop__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Component/ContentLandingTop */ "./src/Component/ContentLandingTop.jsx");
/* harmony import */ var _Component_ContentLandingBottom__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./Component/ContentLandingBottom */ "./src/Component/ContentLandingBottom.jsx");
/* harmony import */ var _Component_Footer__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./Component/Footer */ "./src/Component/Footer.jsx");
/* harmony import */ var _Images_image_4_jpg__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./Images/image-4.jpg */ "./src/Images/image-4.jpg");
/* harmony import */ var _Images_image_4_jpg__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_Images_image_4_jpg__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _css_PinjamanBadanUsaha_css__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./css/PinjamanBadanUsaha.css */ "./src/css/PinjamanBadanUsaha.css");
/* harmony import */ var _css_PinjamanBadanUsaha_css__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_css_PinjamanBadanUsaha_css__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var reactstrap__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! reactstrap */ "./node_modules/reactstrap/es/index.js");
var _jsxFileName = "/Users/mac/Desktop/anafulus-web/src/PinjamanBadanUsaha.jsx";









const PinjamanBadanUsaha = () => {
  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0__["Fragment"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 12
    },
    __self: undefined
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Component_Header__WEBPACK_IMPORTED_MODULE_1__["default"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 13
    },
    __self: undefined
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_7__["Container"], {
    fluid: true,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 14
    },
    __self: undefined
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_7__["Row"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 15
    },
    __self: undefined
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
    src: _Images_image_4_jpg__WEBPACK_IMPORTED_MODULE_5___default.a,
    alt: "banner-loan-corporate",
    className: "img-responsive",
    width: "100%",
    height: "70%",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 16
    },
    __self: undefined
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_7__["Col"], {
    className: "banner-text",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 17
    },
    __self: undefined
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_7__["Col"], {
    className: "text-banner",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 18
    },
    __self: undefined
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_7__["Label"], {
    className: "text-banner1",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 19
    },
    __self: undefined
  }, "Penuhi berbagai kebutuhan usaha anda."), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_7__["Label"], {
    className: "text-banner2",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 20
    },
    __self: undefined
  }, "Anafulus menyediakan peminjaman untuk badan usaha dengan cicilan yang ringan."), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_7__["Button"], {
    id: "btn-ajukan-badan-usaha",
    href: "/",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 21
    },
    __self: undefined
  }, " Ajukan Pinjaman Badan Usaha"))))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Component_ContentLandingTop__WEBPACK_IMPORTED_MODULE_2__["default"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 31
    },
    __self: undefined
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Component_ContentLandingBottom__WEBPACK_IMPORTED_MODULE_3__["default"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 32
    },
    __self: undefined
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Component_Footer__WEBPACK_IMPORTED_MODULE_4__["default"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 33
    },
    __self: undefined
  }));
};

/* harmony default export */ __webpack_exports__["default"] = (PinjamanBadanUsaha);

/***/ }),

/***/ "./src/PinjamanMurabahah.jsx":
/*!***********************************!*\
  !*** ./src/PinjamanMurabahah.jsx ***!
  \***********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _Component_Header__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Component/Header */ "./src/Component/Header.jsx");
/* harmony import */ var _Component_ContentLandingTop__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Component/ContentLandingTop */ "./src/Component/ContentLandingTop.jsx");
/* harmony import */ var _Component_ContentLandingBottom__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./Component/ContentLandingBottom */ "./src/Component/ContentLandingBottom.jsx");
/* harmony import */ var _Component_Footer__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./Component/Footer */ "./src/Component/Footer.jsx");
/* harmony import */ var reactstrap__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! reactstrap */ "./node_modules/reactstrap/es/index.js");
/* harmony import */ var _css_PinjamanMurabahah_css__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./css/PinjamanMurabahah.css */ "./src/css/PinjamanMurabahah.css");
/* harmony import */ var _css_PinjamanMurabahah_css__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_css_PinjamanMurabahah_css__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _Images_image_3_jpg__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./Images/image-3.jpg */ "./src/Images/image-3.jpg");
/* harmony import */ var _Images_image_3_jpg__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(_Images_image_3_jpg__WEBPACK_IMPORTED_MODULE_7__);
var _jsxFileName = "/Users/mac/Desktop/anafulus-web/src/PinjamanMurabahah.jsx";









class PinjamanMurabahah extends react__WEBPACK_IMPORTED_MODULE_0__["Component"] {
  render() {
    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0__["Fragment"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 15
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Component_Header__WEBPACK_IMPORTED_MODULE_1__["default"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 16
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["Container"], {
      fluid: true,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 18
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["Row"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 19
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: _Images_image_3_jpg__WEBPACK_IMPORTED_MODULE_7___default.a,
      className: "img-responsive",
      alt: "banner-loan-muarabahah",
      width: "100%",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 20
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["Col"], {
      md: "6",
      className: "banner-text",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 21
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["Label"], {
      className: "text-banner1",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 22
      },
      __self: this
    }, "Penuhi berbagai kebutuhan anda dengan keberkahan"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["Label"], {
      className: "text-banner2",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 24
      },
      __self: this
    }, "Lakukan peminjaman Murabahah di Anafulus untuk berbagai keperluan anda. ")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["Col"], {
      md: "1",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 27
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["Col"], {
      md: "4",
      className: "form-apply-pinjaman-murabahah",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 29
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["Form"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 30
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h5", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 31
      },
      __self: this
    }, "Simulasi Pinjaman Murabahah"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["FormGroup"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 32
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["Label"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 33
      },
      __self: this
    }, "Nominal Pinjaman"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["Input"], {
      type: "text",
      placeholder: "Rp 500.000 - Rp 50.000.000",
      className: "tenorPinjaman",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 34
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
      id: "note-nominal-pinjaman",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 35
      },
      __self: this
    }, "*Jumlah pinjaman minimal Rp 500.000 dan maksimal Rp 50.000.000"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["FormGroup"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 37
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["Label"], {
      for: "tenorPinjman",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 38
      },
      __self: this
    }, "Tenor Pinjaman"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["Input"], {
      type: "select",
      name: "select",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 39
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("option", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 40
      },
      __self: this
    }, "24 Minggu"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("option", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 41
      },
      __self: this
    }, "8 Minggu"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("option", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 42
      },
      __self: this
    }, "4 Minggu"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("option", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 43
      },
      __self: this
    }, "2 Minggu"))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["FormGroup"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 46
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["Label"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 47
      },
      __self: this
    }, "Jenis cicilan"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["Input"], {
      type: "select",
      name: "select",
      className: "tenorPinjaman",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 48
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("option", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 49
      },
      __self: this
    }, "Mingguan"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("option", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 50
      },
      __self: this
    }, "Bulanan "))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["Button"], {
      className: "",
      href: "/UserAccount",
      id: "btn-hitung",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 54
      },
      __self: this
    }, " Hitung")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["FormGroup"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 62
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["Label"], {
      id: "Title-Cicilan",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 63
      },
      __self: this
    }, "Cicilan:"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 64
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["Label"], {
      id: "Cicilan",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 65
      },
      __self: this
    }, "Rp 0 / Minggu"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 66
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["Label"], {
      for: "AlasanPeminjaman",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 68
      },
      __self: this
    }, "Alasan Peminjaman*"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["Input"], {
      type: "number",
      min: "10",
      max: "12",
      placeholder: "Masukkan alasan peminjaman anda",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 69
      },
      __self: this
    })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["Button"], {
      id: "btn-ajukan",
      className: "",
      href: "/UserAccount",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 71
      },
      __self: this
    }, " Ajukan Peminjaman"))))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Component_ContentLandingTop__WEBPACK_IMPORTED_MODULE_2__["default"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 83
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Component_ContentLandingBottom__WEBPACK_IMPORTED_MODULE_3__["default"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 84
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Component_Footer__WEBPACK_IMPORTED_MODULE_4__["default"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 85
      },
      __self: this
    }));
  }

}

/* harmony default export */ __webpack_exports__["default"] = (PinjamanMurabahah);

/***/ }),

/***/ "./src/PinjamanReguler.jsx":
/*!*********************************!*\
  !*** ./src/PinjamanReguler.jsx ***!
  \*********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _Component_Header__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Component/Header */ "./src/Component/Header.jsx");
/* harmony import */ var _Component_ContentLandingTop__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Component/ContentLandingTop */ "./src/Component/ContentLandingTop.jsx");
/* harmony import */ var _Component_ContentLandingBottom__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./Component/ContentLandingBottom */ "./src/Component/ContentLandingBottom.jsx");
/* harmony import */ var _Component_Footer__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./Component/Footer */ "./src/Component/Footer.jsx");
/* harmony import */ var _Images_image_2_jpg__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./Images/image-2.jpg */ "./src/Images/image-2.jpg");
/* harmony import */ var _Images_image_2_jpg__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_Images_image_2_jpg__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _css_PinjamanReguler_css__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./css/PinjamanReguler.css */ "./src/css/PinjamanReguler.css");
/* harmony import */ var _css_PinjamanReguler_css__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_css_PinjamanReguler_css__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var reactstrap__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! reactstrap */ "./node_modules/reactstrap/es/index.js");
var _jsxFileName = "/Users/mac/Desktop/anafulus-web/src/PinjamanReguler.jsx";









class PinjamanReguler extends react__WEBPACK_IMPORTED_MODULE_0__["Component"] {
  render() {
    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0__["Fragment"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 13
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Component_Header__WEBPACK_IMPORTED_MODULE_1__["default"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 14
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_7__["Container"], {
      fluid: true,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 15
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_7__["Row"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 16
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: _Images_image_2_jpg__WEBPACK_IMPORTED_MODULE_5___default.a,
      className: "img-responsive",
      alt: "banner-loan-muarabahah",
      width: "100%",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 17
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_7__["Col"], {
      md: "6",
      className: "banner-text",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 18
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_7__["Label"], {
      className: "text-banner1",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 19
      },
      __self: this
    }, "Penuhi berbagai kebutuhan anda dengan keberkahan"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_7__["Label"], {
      className: "text-banner2",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 21
      },
      __self: this
    }, "Lakukan peminjaman di Anafulus sebagai solusi berbagai kebutuhan anda. ")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_7__["Col"], {
      md: "1",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 24
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_7__["Col"], {
      md: "4",
      className: "form-apply-pinjaman-murabahah",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 26
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_7__["Form"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 27
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h5", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 28
      },
      __self: this
    }, "Simulasi Pinjaman Murabahah"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_7__["FormGroup"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 29
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_7__["Label"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 30
      },
      __self: this
    }, "Nominal Pinjaman"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_7__["Input"], {
      type: "text",
      placeholder: "Rp 500.000 - Rp 50.000.000",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 31
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
      id: "note-nominal-pinjaman",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 32
      },
      __self: this
    }, "*Jumlah pinjaman minimal Rp 500.000 dan maksimal Rp 50.000.000"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_7__["FormGroup"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 34
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_7__["Label"], {
      for: "tenorPinjman",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 35
      },
      __self: this
    }, "Tenor Pinjaman"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_7__["Input"], {
      type: "select",
      name: "select",
      id: "tenorPinjaman",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 36
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("option", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 37
      },
      __self: this
    }, "24 Minggu"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("option", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 38
      },
      __self: this
    }, "8 Minggu"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("option", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 39
      },
      __self: this
    }, "4 Minggu"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("option", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 40
      },
      __self: this
    }, "2 Minggu"))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_7__["FormGroup"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 43
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_7__["Label"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 44
      },
      __self: this
    }, "Jenis cicilan"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_7__["Input"], {
      type: "select",
      name: "select",
      id: "tenorPinjaman",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 45
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("option", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 46
      },
      __self: this
    }, "Mingguan"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("option", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 47
      },
      __self: this
    }, "Bulanan "))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_7__["Button"], {
      className: "",
      href: "/UserAccount",
      id: "btn-hitung",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 51
      },
      __self: this
    }, " Hitung")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_7__["FormGroup"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 59
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_7__["Label"], {
      id: "Title-Cicilan",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 60
      },
      __self: this
    }, "Cicilan:"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 61
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_7__["Label"], {
      id: "Cicilan",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 62
      },
      __self: this
    }, "Rp 0 / Minggu"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 63
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_7__["Label"], {
      for: "AlasanPeminjaman",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 65
      },
      __self: this
    }, "Alasan Peminjaman*"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_7__["Input"], {
      type: "number",
      min: "10",
      max: "12",
      placeholder: "Masukkan alasan peminjaman anda",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 66
      },
      __self: this
    })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_7__["Button"], {
      id: "btn-ajukan",
      className: "",
      href: "/UserAccount",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 68
      },
      __self: this
    }, " Ajukan Peminjaman"))))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Component_ContentLandingTop__WEBPACK_IMPORTED_MODULE_2__["default"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 78
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Component_ContentLandingBottom__WEBPACK_IMPORTED_MODULE_3__["default"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 79
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Component_Footer__WEBPACK_IMPORTED_MODULE_4__["default"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 80
      },
      __self: this
    }));
  }

}

/* harmony default export */ __webpack_exports__["default"] = (PinjamanReguler);

/***/ }),

/***/ "./src/UserAccount.jsx":
/*!*****************************!*\
  !*** ./src/UserAccount.jsx ***!
  \*****************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _Component_HeaderAccount__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Component/HeaderAccount */ "./src/Component/HeaderAccount.jsx");
/* harmony import */ var _Component_Carousel__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Component/Carousel */ "./src/Component/Carousel.jsx");
/* harmony import */ var _Component_ContentLandingTop__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./Component/ContentLandingTop */ "./src/Component/ContentLandingTop.jsx");
/* harmony import */ var _Component_ContentLandingBottom__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./Component/ContentLandingBottom */ "./src/Component/ContentLandingBottom.jsx");
/* harmony import */ var _Component_Footer__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./Component/Footer */ "./src/Component/Footer.jsx");
var _jsxFileName = "/Users/mac/Desktop/anafulus-web/src/UserAccount.jsx";







const Beranda = () => {
  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0__["Fragment"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 10
    },
    __self: undefined
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Component_HeaderAccount__WEBPACK_IMPORTED_MODULE_1__["default"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 11
    },
    __self: undefined
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Component_Carousel__WEBPACK_IMPORTED_MODULE_2__["default"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 12
    },
    __self: undefined
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Component_ContentLandingTop__WEBPACK_IMPORTED_MODULE_3__["default"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 13
    },
    __self: undefined
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Component_ContentLandingBottom__WEBPACK_IMPORTED_MODULE_4__["default"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 14
    },
    __self: undefined
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Component_Footer__WEBPACK_IMPORTED_MODULE_5__["default"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 15
    },
    __self: undefined
  }));
};

/* harmony default export */ __webpack_exports__["default"] = (Beranda);

/***/ }),

/***/ "./src/css/ContactUs.css":
/*!*******************************!*\
  !*** ./src/css/ContactUs.css ***!
  \*******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../node_modules/css-loader/dist/cjs.js??ref--6-oneOf-3-1!../../node_modules/postcss-loader/src??postcss!./ContactUs.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/css/ContactUs.css");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(true) {
	module.hot.accept(/*! !../../node_modules/css-loader/dist/cjs.js??ref--6-oneOf-3-1!../../node_modules/postcss-loader/src??postcss!./ContactUs.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/css/ContactUs.css", function() {
		var newContent = __webpack_require__(/*! !../../node_modules/css-loader/dist/cjs.js??ref--6-oneOf-3-1!../../node_modules/postcss-loader/src??postcss!./ContactUs.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/css/ContactUs.css");

		if(typeof newContent === 'string') newContent = [[module.i, newContent, '']];

		var locals = (function(a, b) {
			var key, idx = 0;

			for(key in a) {
				if(!b || a[key] !== b[key]) return false;
				idx++;
			}

			for(key in b) idx--;

			return idx === 0;
		}(content.locals, newContent.locals));

		if(!locals) throw new Error('Aborting CSS HMR due to changed css-modules locals.');

		update(newContent);
	});

	module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ "./src/css/ContentLandingBottom.css":
/*!******************************************!*\
  !*** ./src/css/ContentLandingBottom.css ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../node_modules/css-loader/dist/cjs.js??ref--6-oneOf-3-1!../../node_modules/postcss-loader/src??postcss!./ContentLandingBottom.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/css/ContentLandingBottom.css");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(true) {
	module.hot.accept(/*! !../../node_modules/css-loader/dist/cjs.js??ref--6-oneOf-3-1!../../node_modules/postcss-loader/src??postcss!./ContentLandingBottom.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/css/ContentLandingBottom.css", function() {
		var newContent = __webpack_require__(/*! !../../node_modules/css-loader/dist/cjs.js??ref--6-oneOf-3-1!../../node_modules/postcss-loader/src??postcss!./ContentLandingBottom.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/css/ContentLandingBottom.css");

		if(typeof newContent === 'string') newContent = [[module.i, newContent, '']];

		var locals = (function(a, b) {
			var key, idx = 0;

			for(key in a) {
				if(!b || a[key] !== b[key]) return false;
				idx++;
			}

			for(key in b) idx--;

			return idx === 0;
		}(content.locals, newContent.locals));

		if(!locals) throw new Error('Aborting CSS HMR due to changed css-modules locals.');

		update(newContent);
	});

	module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ "./src/css/ContentLandingTop.css":
/*!***************************************!*\
  !*** ./src/css/ContentLandingTop.css ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../node_modules/css-loader/dist/cjs.js??ref--6-oneOf-3-1!../../node_modules/postcss-loader/src??postcss!./ContentLandingTop.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/css/ContentLandingTop.css");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(true) {
	module.hot.accept(/*! !../../node_modules/css-loader/dist/cjs.js??ref--6-oneOf-3-1!../../node_modules/postcss-loader/src??postcss!./ContentLandingTop.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/css/ContentLandingTop.css", function() {
		var newContent = __webpack_require__(/*! !../../node_modules/css-loader/dist/cjs.js??ref--6-oneOf-3-1!../../node_modules/postcss-loader/src??postcss!./ContentLandingTop.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/css/ContentLandingTop.css");

		if(typeof newContent === 'string') newContent = [[module.i, newContent, '']];

		var locals = (function(a, b) {
			var key, idx = 0;

			for(key in a) {
				if(!b || a[key] !== b[key]) return false;
				idx++;
			}

			for(key in b) idx--;

			return idx === 0;
		}(content.locals, newContent.locals));

		if(!locals) throw new Error('Aborting CSS HMR due to changed css-modules locals.');

		update(newContent);
	});

	module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ "./src/css/Faq.css":
/*!*************************!*\
  !*** ./src/css/Faq.css ***!
  \*************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../node_modules/css-loader/dist/cjs.js??ref--6-oneOf-3-1!../../node_modules/postcss-loader/src??postcss!./Faq.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/css/Faq.css");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(true) {
	module.hot.accept(/*! !../../node_modules/css-loader/dist/cjs.js??ref--6-oneOf-3-1!../../node_modules/postcss-loader/src??postcss!./Faq.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/css/Faq.css", function() {
		var newContent = __webpack_require__(/*! !../../node_modules/css-loader/dist/cjs.js??ref--6-oneOf-3-1!../../node_modules/postcss-loader/src??postcss!./Faq.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/css/Faq.css");

		if(typeof newContent === 'string') newContent = [[module.i, newContent, '']];

		var locals = (function(a, b) {
			var key, idx = 0;

			for(key in a) {
				if(!b || a[key] !== b[key]) return false;
				idx++;
			}

			for(key in b) idx--;

			return idx === 0;
		}(content.locals, newContent.locals));

		if(!locals) throw new Error('Aborting CSS HMR due to changed css-modules locals.');

		update(newContent);
	});

	module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ "./src/css/Footer.css":
/*!****************************!*\
  !*** ./src/css/Footer.css ***!
  \****************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../node_modules/css-loader/dist/cjs.js??ref--6-oneOf-3-1!../../node_modules/postcss-loader/src??postcss!./Footer.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/css/Footer.css");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(true) {
	module.hot.accept(/*! !../../node_modules/css-loader/dist/cjs.js??ref--6-oneOf-3-1!../../node_modules/postcss-loader/src??postcss!./Footer.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/css/Footer.css", function() {
		var newContent = __webpack_require__(/*! !../../node_modules/css-loader/dist/cjs.js??ref--6-oneOf-3-1!../../node_modules/postcss-loader/src??postcss!./Footer.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/css/Footer.css");

		if(typeof newContent === 'string') newContent = [[module.i, newContent, '']];

		var locals = (function(a, b) {
			var key, idx = 0;

			for(key in a) {
				if(!b || a[key] !== b[key]) return false;
				idx++;
			}

			for(key in b) idx--;

			return idx === 0;
		}(content.locals, newContent.locals));

		if(!locals) throw new Error('Aborting CSS HMR due to changed css-modules locals.');

		update(newContent);
	});

	module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ "./src/css/Header.css":
/*!****************************!*\
  !*** ./src/css/Header.css ***!
  \****************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../node_modules/css-loader/dist/cjs.js??ref--6-oneOf-3-1!../../node_modules/postcss-loader/src??postcss!./Header.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/css/Header.css");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(true) {
	module.hot.accept(/*! !../../node_modules/css-loader/dist/cjs.js??ref--6-oneOf-3-1!../../node_modules/postcss-loader/src??postcss!./Header.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/css/Header.css", function() {
		var newContent = __webpack_require__(/*! !../../node_modules/css-loader/dist/cjs.js??ref--6-oneOf-3-1!../../node_modules/postcss-loader/src??postcss!./Header.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/css/Header.css");

		if(typeof newContent === 'string') newContent = [[module.i, newContent, '']];

		var locals = (function(a, b) {
			var key, idx = 0;

			for(key in a) {
				if(!b || a[key] !== b[key]) return false;
				idx++;
			}

			for(key in b) idx--;

			return idx === 0;
		}(content.locals, newContent.locals));

		if(!locals) throw new Error('Aborting CSS HMR due to changed css-modules locals.');

		update(newContent);
	});

	module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ "./src/css/Login.css":
/*!***************************!*\
  !*** ./src/css/Login.css ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../node_modules/css-loader/dist/cjs.js??ref--6-oneOf-3-1!../../node_modules/postcss-loader/src??postcss!./Login.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/css/Login.css");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(true) {
	module.hot.accept(/*! !../../node_modules/css-loader/dist/cjs.js??ref--6-oneOf-3-1!../../node_modules/postcss-loader/src??postcss!./Login.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/css/Login.css", function() {
		var newContent = __webpack_require__(/*! !../../node_modules/css-loader/dist/cjs.js??ref--6-oneOf-3-1!../../node_modules/postcss-loader/src??postcss!./Login.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/css/Login.css");

		if(typeof newContent === 'string') newContent = [[module.i, newContent, '']];

		var locals = (function(a, b) {
			var key, idx = 0;

			for(key in a) {
				if(!b || a[key] !== b[key]) return false;
				idx++;
			}

			for(key in b) idx--;

			return idx === 0;
		}(content.locals, newContent.locals));

		if(!locals) throw new Error('Aborting CSS HMR due to changed css-modules locals.');

		update(newContent);
	});

	module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ "./src/css/PinjamanBadanUsaha.css":
/*!****************************************!*\
  !*** ./src/css/PinjamanBadanUsaha.css ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../node_modules/css-loader/dist/cjs.js??ref--6-oneOf-3-1!../../node_modules/postcss-loader/src??postcss!./PinjamanBadanUsaha.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/css/PinjamanBadanUsaha.css");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(true) {
	module.hot.accept(/*! !../../node_modules/css-loader/dist/cjs.js??ref--6-oneOf-3-1!../../node_modules/postcss-loader/src??postcss!./PinjamanBadanUsaha.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/css/PinjamanBadanUsaha.css", function() {
		var newContent = __webpack_require__(/*! !../../node_modules/css-loader/dist/cjs.js??ref--6-oneOf-3-1!../../node_modules/postcss-loader/src??postcss!./PinjamanBadanUsaha.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/css/PinjamanBadanUsaha.css");

		if(typeof newContent === 'string') newContent = [[module.i, newContent, '']];

		var locals = (function(a, b) {
			var key, idx = 0;

			for(key in a) {
				if(!b || a[key] !== b[key]) return false;
				idx++;
			}

			for(key in b) idx--;

			return idx === 0;
		}(content.locals, newContent.locals));

		if(!locals) throw new Error('Aborting CSS HMR due to changed css-modules locals.');

		update(newContent);
	});

	module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ "./src/css/PinjamanMurabahah.css":
/*!***************************************!*\
  !*** ./src/css/PinjamanMurabahah.css ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../node_modules/css-loader/dist/cjs.js??ref--6-oneOf-3-1!../../node_modules/postcss-loader/src??postcss!./PinjamanMurabahah.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/css/PinjamanMurabahah.css");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(true) {
	module.hot.accept(/*! !../../node_modules/css-loader/dist/cjs.js??ref--6-oneOf-3-1!../../node_modules/postcss-loader/src??postcss!./PinjamanMurabahah.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/css/PinjamanMurabahah.css", function() {
		var newContent = __webpack_require__(/*! !../../node_modules/css-loader/dist/cjs.js??ref--6-oneOf-3-1!../../node_modules/postcss-loader/src??postcss!./PinjamanMurabahah.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/css/PinjamanMurabahah.css");

		if(typeof newContent === 'string') newContent = [[module.i, newContent, '']];

		var locals = (function(a, b) {
			var key, idx = 0;

			for(key in a) {
				if(!b || a[key] !== b[key]) return false;
				idx++;
			}

			for(key in b) idx--;

			return idx === 0;
		}(content.locals, newContent.locals));

		if(!locals) throw new Error('Aborting CSS HMR due to changed css-modules locals.');

		update(newContent);
	});

	module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ "./src/css/PinjamanReguler.css":
/*!*************************************!*\
  !*** ./src/css/PinjamanReguler.css ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../node_modules/css-loader/dist/cjs.js??ref--6-oneOf-3-1!../../node_modules/postcss-loader/src??postcss!./PinjamanReguler.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/css/PinjamanReguler.css");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(true) {
	module.hot.accept(/*! !../../node_modules/css-loader/dist/cjs.js??ref--6-oneOf-3-1!../../node_modules/postcss-loader/src??postcss!./PinjamanReguler.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/css/PinjamanReguler.css", function() {
		var newContent = __webpack_require__(/*! !../../node_modules/css-loader/dist/cjs.js??ref--6-oneOf-3-1!../../node_modules/postcss-loader/src??postcss!./PinjamanReguler.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/css/PinjamanReguler.css");

		if(typeof newContent === 'string') newContent = [[module.i, newContent, '']];

		var locals = (function(a, b) {
			var key, idx = 0;

			for(key in a) {
				if(!b || a[key] !== b[key]) return false;
				idx++;
			}

			for(key in b) idx--;

			return idx === 0;
		}(content.locals, newContent.locals));

		if(!locals) throw new Error('Aborting CSS HMR due to changed css-modules locals.');

		update(newContent);
	});

	module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ "./src/css/Register.css":
/*!******************************!*\
  !*** ./src/css/Register.css ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../node_modules/css-loader/dist/cjs.js??ref--6-oneOf-3-1!../../node_modules/postcss-loader/src??postcss!./Register.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/css/Register.css");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(true) {
	module.hot.accept(/*! !../../node_modules/css-loader/dist/cjs.js??ref--6-oneOf-3-1!../../node_modules/postcss-loader/src??postcss!./Register.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/css/Register.css", function() {
		var newContent = __webpack_require__(/*! !../../node_modules/css-loader/dist/cjs.js??ref--6-oneOf-3-1!../../node_modules/postcss-loader/src??postcss!./Register.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/css/Register.css");

		if(typeof newContent === 'string') newContent = [[module.i, newContent, '']];

		var locals = (function(a, b) {
			var key, idx = 0;

			for(key in a) {
				if(!b || a[key] !== b[key]) return false;
				idx++;
			}

			for(key in b) idx--;

			return idx === 0;
		}(content.locals, newContent.locals));

		if(!locals) throw new Error('Aborting CSS HMR due to changed css-modules locals.');

		update(newContent);
	});

	module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ "./src/css/index.css":
/*!***************************!*\
  !*** ./src/css/index.css ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../node_modules/css-loader/dist/cjs.js??ref--6-oneOf-3-1!../../node_modules/postcss-loader/src??postcss!./index.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/css/index.css");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(true) {
	module.hot.accept(/*! !../../node_modules/css-loader/dist/cjs.js??ref--6-oneOf-3-1!../../node_modules/postcss-loader/src??postcss!./index.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/css/index.css", function() {
		var newContent = __webpack_require__(/*! !../../node_modules/css-loader/dist/cjs.js??ref--6-oneOf-3-1!../../node_modules/postcss-loader/src??postcss!./index.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/css/index.css");

		if(typeof newContent === 'string') newContent = [[module.i, newContent, '']];

		var locals = (function(a, b) {
			var key, idx = 0;

			for(key in a) {
				if(!b || a[key] !== b[key]) return false;
				idx++;
			}

			for(key in b) idx--;

			return idx === 0;
		}(content.locals, newContent.locals));

		if(!locals) throw new Error('Aborting CSS HMR due to changed css-modules locals.');

		update(newContent);
	});

	module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ "./src/index.js":
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-dom */ "./node_modules/react-dom/index.js");
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_dom__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _css_index_css__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./css/index.css */ "./src/css/index.css");
/* harmony import */ var _css_index_css__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_css_index_css__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _App__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./App */ "./src/App.js");
/* harmony import */ var _serviceWorker__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./serviceWorker */ "./src/serviceWorker.js");
/* harmony import */ var bootstrap_dist_css_bootstrap_min_css__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! bootstrap/dist/css/bootstrap.min.css */ "./node_modules/bootstrap/dist/css/bootstrap.min.css");
/* harmony import */ var bootstrap_dist_css_bootstrap_min_css__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(bootstrap_dist_css_bootstrap_min_css__WEBPACK_IMPORTED_MODULE_5__);
var _jsxFileName = "/Users/mac/Desktop/anafulus-web/src/index.js";






react_dom__WEBPACK_IMPORTED_MODULE_1___default.a.render(react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_App__WEBPACK_IMPORTED_MODULE_3__["default"], {
  __source: {
    fileName: _jsxFileName,
    lineNumber: 8
  },
  __self: undefined
}), document.getElementById('root')); // If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA

_serviceWorker__WEBPACK_IMPORTED_MODULE_4__["unregister"]();

/***/ }),

/***/ "./src/serviceWorker.js":
/*!******************************!*\
  !*** ./src/serviceWorker.js ***!
  \******************************/
/*! exports provided: register, unregister */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "register", function() { return register; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "unregister", function() { return unregister; });
// This optional code is used to register a service worker.
// register() is not called by default.
// This lets the app load faster on subsequent visits in production, and gives
// it offline capabilities. However, it also means that developers (and users)
// will only see deployed updates on subsequent visits to a page, after all the
// existing tabs open on the page have been closed, since previously cached
// resources are updated in the background.
// To learn more about the benefits of this model and instructions on how to
// opt-in, read https://bit.ly/CRA-PWA
const isLocalhost = Boolean(window.location.hostname === 'localhost' || // [::1] is the IPv6 localhost address.
window.location.hostname === '[::1]' || // 127.0.0.1/8 is considered localhost for IPv4.
window.location.hostname.match(/^127(?:\.(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)){3}$/));
function register(config) {
  if (false) {}
}

function registerValidSW(swUrl, config) {
  navigator.serviceWorker.register(swUrl).then(registration => {
    registration.onupdatefound = () => {
      const installingWorker = registration.installing;

      if (installingWorker == null) {
        return;
      }

      installingWorker.onstatechange = () => {
        if (installingWorker.state === 'installed') {
          if (navigator.serviceWorker.controller) {
            // At this point, the updated precached content has been fetched,
            // but the previous service worker will still serve the older
            // content until all client tabs are closed.
            console.log('New content is available and will be used when all ' + 'tabs for this page are closed. See https://bit.ly/CRA-PWA.'); // Execute callback

            if (config && config.onUpdate) {
              config.onUpdate(registration);
            }
          } else {
            // At this point, everything has been precached.
            // It's the perfect time to display a
            // "Content is cached for offline use." message.
            console.log('Content is cached for offline use.'); // Execute callback

            if (config && config.onSuccess) {
              config.onSuccess(registration);
            }
          }
        }
      };
    };
  }).catch(error => {
    console.error('Error during service worker registration:', error);
  });
}

function checkValidServiceWorker(swUrl, config) {
  // Check if the service worker can be found. If it can't reload the page.
  fetch(swUrl).then(response => {
    // Ensure service worker exists, and that we really are getting a JS file.
    const contentType = response.headers.get('content-type');

    if (response.status === 404 || contentType != null && contentType.indexOf('javascript') === -1) {
      // No service worker found. Probably a different app. Reload the page.
      navigator.serviceWorker.ready.then(registration => {
        registration.unregister().then(() => {
          window.location.reload();
        });
      });
    } else {
      // Service worker found. Proceed as normal.
      registerValidSW(swUrl, config);
    }
  }).catch(() => {
    console.log('No internet connection found. App is running in offline mode.');
  });
}

function unregister() {
  if ('serviceWorker' in navigator) {
    navigator.serviceWorker.ready.then(registration => {
      registration.unregister();
    });
  }
}

/***/ }),

/***/ 0:
/*!**********************************************************************************!*\
  !*** multi ./node_modules/react-dev-utils/webpackHotDevClient.js ./src/index.js ***!
  \**********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(/*! /Users/mac/Desktop/anafulus-web/node_modules/react-dev-utils/webpackHotDevClient.js */"./node_modules/react-dev-utils/webpackHotDevClient.js");
module.exports = __webpack_require__(/*! /Users/mac/Desktop/anafulus-web/src/index.js */"./src/index.js");


/***/ })

},[[0,"runtime~main",1]]]);
//# sourceMappingURL=main.chunk.js.map