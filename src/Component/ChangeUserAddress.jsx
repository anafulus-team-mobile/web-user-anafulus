// Component, Page imported //
import React, { Fragment }                    from 'react';
import HeaderAccount                          from '../Component/HeaderAccount';
import SideBarProfile                         from '../Component/SidebarProfile';
import ContentLandingBottom                   from '../Component/ContentLandingBottom';
import Footer                                 from '../Component/Footer';

import { Link }                               from 'react-router-dom';

// Styling imported //
import { Container, Row, Col, Button, 
         Label, Input, Form, FormGroup }      from 'reactstrap';
import '../Css/Reuse.css';
import '../Css/UserDashboard.css';


class ChangeUserAddress extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
          modal: false
        };

    
        this.toggle = this.toggle.bind(this);
      }
    
      toggle() {
        this.setState(prevState => ({
          modal: !prevState.modal
        }));
      }

    render() {
        return (
            <Fragment>
                <HeaderAccount/>
                    <Container className="container">
                        <Row>
                            {/* Component sidebar profile*/}
                            <Col sm="4">
                                <SideBarProfile/>
                            </Col>
                            {/* end */}
                                <Col sm="8" className="col-dashboard-user">
                                    <h3>Ubah Alamat</h3><hr/>
                                        <Container className="container-wrap2"> 
                                                <Col xs={{offset:1}}>
                                                    <Container>
                                                        <Row>
                                                            <Col sm="8">
                                                                <Label className="title-dashboard">Formulir Data Diri</Label>
                                                                <Label>*Harus diisi sesuai dengan Alamat E-KTP anda</Label>
                                                            </Col>
                                                        </Row>
                                                    </Container>
                                                    <Container>
                                                        <Row >
                                                            <Col xs={{size:11 }}>
                                                                <Form>
                                                                    <FormGroup>
                                                                        <Label for="provinsiAddress">Provinsi*</Label>
                                                                        <Input type="select" name="select-provinsi" id="provinsiAddress">
                                                                            <option>Jawa Barat</option>
                                                                            <option>DKI Jakarta</option>
                                                                            <option>Jawa Barat</option>
                                                                            <option>Jawa Timur</option>
                                                                            <option>Banten</option>
                                                                        </Input>
                                                                        </FormGroup>
                                                                        <FormGroup>
                                                                        <Label for="kotaAddress">Kota*</Label>
                                                                        <Input type="select" name="select-kota" id="kotaAddress">
                                                                            <option>Pilih Kota</option>
                                                                            <option>Kota Bandung</option>
                                                                            <option>Kota Karawang</option>
                                                                            <option>Kota Purwakarta</option>
                                                                            <option>Kota Bekasi</option>
                                                                            <option>Kota Bogor</option>
                                                                        </Input>
                                                                        </FormGroup>
                                                                        <FormGroup>
                                                                        <Label for="KecamatanAddress">Kabupaten*</Label>
                                                                        <Input type="select" name="select-kabupaten" id="kabupatenAddress">
                                                                            <option>Pilih Kabupaten</option>
                                                                            <option>Kab.Bandung Barat</option>
                                                                            <option>Kab.Bandung Utara</option>
                                                                            <option>Kab.Bandung Selatan</option>
                                                                            <option>Kab.Bekasi</option>
                                                                            <option>Kab.Bogor</option>
                                                                        </Input>
                                                                        </FormGroup>
                                                                        <FormGroup>
                                                                        <Label for="provinsiAddress">Kecamatan*</Label>
                                                                        <Input type="select" name="select-kecamatan" id="kecamatanAddress">
                                                                            <option>Pilih Kecamatan</option>
                                                                            <option>Kec.Batujajar</option>
                                                                            <option>Kec.Cihampelas</option>
                                                                            <option>Kec.Cikalong Wetan</option>
                                                                            <option>Kec.Tambun Selatan</option>
                                                                            <option>Kec.Ciawi</option>
                                                                        </Input>
                                                                        </FormGroup>
                                                                        <FormGroup>
                                                                        <Label for="address">Alamat*</Label>
                                                                        <Input type="text" name="input-address" id="address"></Input>
                                                                        </FormGroup>
                                                                        <FormGroup>
                                                                        <Label for="kodePos">Kode Pos</Label>
                                                                        <Input type="text" name="input-kode-pos" id="kodePos"></Input>
                                                                        </FormGroup>
                                                                        <Col xs="10" className="mx-auto">
                                                                            <FormGroup check>
                                                                                <Input type="checkbox" name="checkbox-addressDefault"/>{' '}
                                                                                <Label check>
                                                                                Alamat KTP sama pengan alamat domisili.
                                                                                <br/>
                                                                                <br/>
                                                                                Jika alamat KTP anda sama pengan alamat domisili maka
                                                                                tandai bahwa alamat sama tapi jika alamat berbeda maka
                                                                                isi kolom alamat domisili .
                                                                                </Label>
                                                                            </FormGroup>
                                                                        </Col>
                                                                        <br/>   
                                                                        <FormGroup>
                                                                        <Label for="addressDomisili">Alamat Domisili</Label>
                                                                        <Input type="text" name="input-address-domisili" id="addressDomisili"></Input>
                                                                        </FormGroup>
                                                                    <center><Link to="/AccountUser"><Button className="btn-green2">Simpan Perubahan</Button></Link></center>
                                                                </Form>                                               
                                                            </Col>
                                                        </Row>
                                                    </Container>
                                                </Col>
                                        </Container>
                                    </Col>
                                </Row>
                            </Container>
                <ContentLandingBottom/>
            <Footer/>
            </Fragment>
        );
    }
}

export default ChangeUserAddress;