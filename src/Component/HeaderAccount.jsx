import React                                      from 'react';

import icoUser                                    from '../Icon/user-4.png';

import { Link }                                   from 'react-router-dom';

import { Collapse, Navbar, NavbarToggler,
         NavbarBrand, Nav, NavLink, NavItem, 
         UncontrolledDropdown, DropdownToggle, 
         DropdownMenu, DropdownItem, Row }        from 'reactstrap';
import '../Css/Header.css';


// Class Header
class HeaderAccount extends React.Component {
  constructor(props) {
    super(props);

// Responsive navbar with toggle
    this.toggle = this.toggle.bind(this);
    this.state = {
      isOpen: false
    };
  }
  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }
  // Call view
  render() {
    return (
      <div>
        <Navbar dark expand="md">
          <NavbarBrand id="logo" href="/">anafulus</NavbarBrand>
            <NavbarToggler onClick={this.toggle}/>
              <Collapse navbar isOpen={this.state.isOpen}>
                <Nav className="ml-auto">
                  <NavItem>
                    <NavLink href="/">Beranda</NavLink>
                  </NavItem>
                  <NavItem>
                   <UncontrolledDropdown nav inNavbar>
                      <DropdownToggle nav>Pinjaman</DropdownToggle>
                      <DropdownMenu right >
                        <DropdownItem >
                          <Link to="/Loan-Murabahah" className="down-list">Murabahah</Link>
                        </DropdownItem>
                        <DropdownItem>
                          <Link to="/Loan-Reguler" className="down-list">Reguler</Link>  
                        </DropdownItem>
                        <DropdownItem>
                          <Link to="/Loan-Badan-Usaha" className="down-list">Badan Usaha</Link>  
                        </DropdownItem>
                      </DropdownMenu>
                    </UncontrolledDropdown>
                  </NavItem>
                  <NavItem>
                    <NavLink href="/">Pendanaan</NavLink>
                  </NavItem>
                    <UncontrolledDropdown nav inNavbar>
                      <DropdownToggle nav>Tentang Anafulus</DropdownToggle>
                    <DropdownMenu right >
                      <DropdownItem className="down-list">
                        <Link to="/About" className="down-list">Tentang Anafulus</Link>
                      </DropdownItem>
                      <DropdownItem>
                        <Link to="/ContactUs" className="down-list">Hubungi Kami</Link>
                      </DropdownItem>
                      <DropdownItem>
                        <Link to="/Faq" className="down-list">Bantuan (FAQ)</Link>
                      </DropdownItem>
                      </DropdownMenu>
                    </UncontrolledDropdown>
                </Nav>
                <Nav className="mx-auto">
                  <Row>
                    <img src={icoUser} alt="icon-user" hspace="10px"/>{' '}
                    <Link to="/Dashboard" id="account-login" style={{marginTop:'5px'}}>Halo, John Doe</Link>
                  </Row>
                </Nav>
          </Collapse>
        </Navbar>
      </div>
    );
  }
};

export default HeaderAccount;  