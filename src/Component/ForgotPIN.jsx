import React, { Fragment }                              from 'react';
import Header                                           from './Header';
import ContentLandingBottom                             from './ContentLandingBottom';
import Footer                                           from './Footer';

import { Link }                                         from 'react-router-dom';

import { Card, Button, CardHeader, CardBody,
         CardTitle, Container, Col, Row, 
         Input }                                        from 'reactstrap';

import '../Css/Login.css';  
  

class ForgotPIN extends React.Component {
    
    render(){
      return(
        <Fragment>
            <Header/>
                    <Container className="card-login">
                        <Col sm="12" md={{ size: 6, offset: 3 }}>
                            <Card>
                                <CardHeader className="card-header">
                                    <h5>Reset PIN</h5>
                                </CardHeader>
                                <CardBody>
                                    <p>Silahkan masukkan nomor ponsel Anda. Kami akan mengirimkan 
                                    kode verifikasi melalui SMS untuk Reset PIN Anda.
                                    </p>
                                <Row>
                                    <Col>
                                        <CardTitle className="title-forgotPIN">Nomor Ponsel</CardTitle>
                                        <Input type="number" id="input-box"/>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col>
                                        <CardTitle className="title-forgotPIN">Tanggal Lahir</CardTitle>
                                        <Input type="text" id="input-box"/>
                                    </Col>
                                </Row>
                                    <Col className="btn-card">
                                        <Link to="/Verify"><Button className="btn-green2">Lanjutkan</Button></Link>
                                    </Col>
                                </CardBody> 
                            </Card>
                        </Col>
                    </Container>
                <ContentLandingBottom/>
            <Footer/>
        </Fragment>
      );
      
    }
    
  };
  
  export default ForgotPIN;

  
      