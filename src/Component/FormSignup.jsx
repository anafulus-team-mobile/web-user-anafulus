import React, { Fragment }                                  from 'react';
import Header                                               from './Header';
import ModalRegister                                        from './ModalRegister';
import ContentLandingBottom                                 from './ContentLandingBottom';
import Footer                                               from './Footer';

import { Link }                                             from 'react-router-dom';

import { Card, Button, CardHeader, CardBody,
         Container, Col, Input, Form, FormText, 
         FormGroup, Label }                                 from 'reactstrap';
import '../Css/Register.css';  
  

class FormSignup extends React.Component {

render(){
    return(
        <Fragment>
            <Header/>
                <div>
                    <Container>
                        <Col sm="12" md={{ size: 6, offset: 3 }}>
                            <Card>
                                <CardHeader>
                                    <h5 className="card-header-form">Daftar</h5>
                                    <Label className="card-header-form">Silahkan isi data di bawah ini</Label>
                                </CardHeader>
                                <CardBody>
                                    <Form onSubmit={this.handleSubmit}>
                                        <FormGroup>
                                            <Label for="emailUser">Email</Label>
                                            <Input type="text" id="emailUser" name="emailUser" placeholder="jhondoe@gmail.com"/>
                                        </FormGroup>
                                        <FormGroup>
                                            <Label for="nameUser">Nama Lengkap sesuai E-KTP*</Label>
                                            <Input type="text" id="nameUser" name="nameUser" placeholder="John Doe"/>
                                        </FormGroup>
                                        <FormGroup>
                                            <Label for="phoneUser">Nomor Ponsel*</Label>
                                            <Input type="number" id="phoneUser" name="phoneUser" placeholder="08123456789"/>
                                        </FormGroup>
                                        <FormGroup>
                                            <Label for="userPIN">PIN*</Label>
                                            <Input type="number" id="userPIN" name="userPIN" placeholder="654321"/>
                                            <FormText color="muted">Masukkan PIN anda dengan 6 digit angka</FormText>
                                        </FormGroup>
                                        <FormGroup>
                                            <Label for="confirmPIN">Konfirmasi PIN*</Label>
                                            <Input type="number" id="confirmPIN" name="ConfirmPIN"/>
                                        </FormGroup>
                                        <Col>
                                        <FormGroup check style={{textAlign:'justify'}}>
                                            <Label check>
                                            <Input type="checkbox" name="signUpAgree"/>{' '} Saya telah membaca dan setuju dengan <Link to="/Kebijakan-Privasi">Kebijakan Privasi</Link>, <Link to="/RegOJK">Regulasi OJK </Link>
                                            dan <Link to="/TermCon">Syarat & Ketentuan</Link>
                                            </Label>
                                        </FormGroup>
                                        </Col>
                                        <Col sm="10" className="mx-auto">
                                            <Button type="submit" className="btn-green1 btn-block">Daftar</Button>
                                        </Col>
                                    </Form>
                                </CardBody> 
                            </Card>
                        </Col>
                    </Container>
                </div>
            <ContentLandingBottom/>
            <Footer/>
        </Fragment>
      );
    }
  };
  
  export default FormSignup;
      