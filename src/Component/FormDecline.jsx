import React, { Fragment }                                  from 'react';
import Header                                               from './Header';
import ContentLandingBottom                                 from './ContentLandingBottom';
import Footer                                               from './Footer';

import { Link }                                             from 'react-router-dom';
import { Card, CardHeader, CardBody,
         Container, CardText, Col, Input, Button }          from 'reactstrap';
import '../Css/Register.css';  
  

class FormDecline extends React.Component {

    render(){
        return(
            <Fragment>
                <Header/>
                    <Container className="card-register">
                        <Col sm="12" md={{ size: 6, offset: 3 }}>
                            <Card>
                                <CardHeader>
                                    <h5 className="card-header-form">Pembatalan Pinjaman</h5>
                                </CardHeader>
                                <CardBody >
                                   <CardText>Alasan Pembatalan Pengajuan Pinjaman</CardText>
                                   <Input type="textarea" id="textarea-reason"/>
                                   <br/>
                                   <CardText style={{textAlign:'justify'}}>
                                   Perhatian : Untuk membatalkan pinjaman anda dikenakan denda sebesar 2% yaitu <span>Rp 100.000. </span> 
                                   Silahkan lakukan Pembayaran sebelum tanggal <span>1 Mei 2019</span>. Jika sampan tanggal tersebut anda tidak melakukan pembayaran pembatalan maka anda dianggap telah melakukan pinjaman.
                                   </CardText>
                                   <Link to="/Payment"><Button className="btn-yellow1">Bayar Denda</Button></Link>
                                </CardBody> 
                            </Card>
                        </Col>
                    </Container>
                <ContentLandingBottom/>
                <Footer/>
            </Fragment>
      );
    }
  };
  
  export default FormDecline;
      