// Component, Page imported //
import React, { Fragment }                    from 'react';
import HeaderAccount                          from '../Component/HeaderAccount';
import SideBarProfile                         from '../Component/SidebarProfile';
import ContentLandingBottom                   from '../Component/ContentLandingBottom';
import Footer                                 from '../Component/Footer';

import { Link }                               from 'react-router-dom';


// Styling imported //
import { Container, Row, Col, Button, 
         Label }                              from 'reactstrap';
import '../Css/DetailProcces.css';


class DetailDecline extends React.Component{
    constructor(props) {
        super(props);
    
        this.toggle = this.toggle.bind(this);
        this.state = {
          dropdownOpen: false
        };
      }
    
      toggle() {
        this.setState({
          dropdownOpen: !this.state.dropdownOpen
        });
      }

    render() {
        return (
            <Fragment>
                <HeaderAccount/>
                <div>
                    <Container>
                        <Row>
                            {/* Component sidebar profile*/}
                            <Col sm="4">
                                <SideBarProfile/>
                            </Col>
                            {/* end */}
                                <Col sm="8" className="col-dashboard-user">
                                    <h3>Riwayat</h3><hr/>
                                        <Container className="container-wrap2"> 
                                                <Row>
                                                    <Col xs="4" sm="3">
                                                    <Label style={{fontWeight:'bolder'}}>Id Pinjaman</Label>
                                                    <br/>
                                                    <Label>Jumlah Pinjaman</Label>
                                                    <br/>
                                                    <Label>Jumlah Cicilan</Label>
                                                    <br/>
                                                    <Label>Jumlah Tenor</Label>
                                                    <br/>
                                                    <Label>Jenis Cicilan</Label>
                                                    <br/>
                                                    <Label>Mulai Peminjaman</Label>
                                                    <br/>
                                                    <Label>Keterangan</Label>
                                                    <br/>
                                                    <Label>Jenis Pinjaman</Label>
                                                    <br/>
                                                    <Label style={{fontWeight:'bolder'}}>Status</Label>
                                                    
                                                    </Col>
                                                    <Col style={{color:'#229f89'}}>
                                                    {': '}<Label style={{fontWeight:'bolder'}}>1xxxxxxxx</Label> 
                                                    <br/>
                                                    {': '}<Label>Rp 5.000.000</Label> 
                                                    <br/>
                                                    {': '}<Label>Rp 250.000 / Minggu</Label> 
                                                    <br/>
                                                    {': '}<Label>24 Minggu</Label>
                                                    <br/>
                                                    {': '}<Label>Mingguan</Label> 
                                                    <br/>
                                                    {': '}<Label>1 Januari 2019</Label> 
                                                    <br/>
                                                    {': '}<Label>Perorangan</Label> 
                                                    <br/>
                                                    {': '}<Label>Reguler</Label> 
                                                    <br/>
                                                    {': '}<Label style={{fontWeight:'bolder'}}>Pinjaman Ditolak</Label> 
                                                    </Col>
                                                    <Col>
                                                    </Col>
                                                </Row>
                                                <hr/>
                                                <h5>Detail Proses Pinjaman</h5>
                                                <Row>
                                                        <Col xs="1">
                                                            <span className="eclipse1"></span>
                                                            <span className="lineVertical"></span>
                                                            <span className="eclipse1"></span>
                                                            <span className="lineVertical"></span>
                                                            <span className="eclipse3"></span>
                                                            <span className="lineVertical"></span>
                                                            <span className="eclipse2"></span>
                                                            <span className="lineVertical"></span>
                                                            <span className="eclipse4"></span>
                                                        </Col>
                                                        <Col>
                                                            <Label for="uploadBerkas">Upload Berkas</Label>
                                                            <Col>
                                                                <Label>1 Januari 2019</Label>{' - '}
                                                                <Label>08.00</Label>
                                                            </Col>
                                                            <br/>
                                                            <Label for="validData">Validasi Data</Label>
                                                            <Col>
                                                                <Label>3 Januari 2019</Label>{' - '}
                                                                <Label>10.00</Label>
                                                                </Col>
                                                            <br/>                                                            
                                                            <Label for="verifikasiData">Verifikasi Data</Label>
                                                            <Col>
                                                                <Label>5 Januari 2019</Label>{' - '}
                                                                <Label>10.00</Label>
                                                                </Col>
                                                            <br/>
                                                            <Label for="prosesApprove">Proses Disetujui</Label>
                                                            <Col>
                                                                <Label></Label>{' - '}
                                                                <Label></Label>
                                                            </Col>
                                                            <br/>
                                                            <Label for="withdrawDana">Pencairan Dana</Label>
                                                            <Col>
                                                                <Label></Label>{' - '}
                                                                <Label></Label>
                                                            </Col>
                                                        </Col>
                                                </Row>
                                                
                                                <Row style={{color:'#229f89'}}>
                                                    <Col sm={{size:2, offset:1}}>
                                                        <Label>Keterangan :</Label>
                                                    </Col>
                                                    <Col sm={{size:8}}>
                                                        <p> Proses pengajuan pinjaman anda ditolak karena tidak memenuhi syarat peminjaman di Anafulus.</p>
                                                    </Col>
                                                </Row>
                                                <Row style={{color:'#229f89'}}>
                                                    <Col sm={{size:2, offset:1}}>
                                                        <Label for="ketDetailPeminjaman">
                                                        Keterangan: {' '}
                                                        </Label>
                                                    </Col>
                                                    <Col sm="8">
                                                        <p className="ketDetailPinjaman">Proses pengajuan pinjaman anda disetujui dengan jumlah Rp 2.500.000. Jika anda Menyetujui dan ingin melakukan pinjaman silahkan klik tombol setuju dan lakukan pinjaman.</p>
                                                    </Col>
                                                </Row>
                                                
                                                <center>
                                                    <Link to="/Apply-Reguler"><Button className="btn-green2">Setuju dan Lakukan Pinjaman</Button></Link>
                                                    <br/>
                                                    <Link to="/Form-Reason-Decline"><Button className="btn-yellow2">Tolak Penawaran</Button></Link>
                                                </center>
                                        </Container>
                                    </Col>
                                </Row>
                            </Container>
                            </div>
                <ContentLandingBottom/>
            <Footer/>
            </Fragment>
        );
    }
}

export default DetailDecline;