import React, { Fragment }                           from 'react';

import ImagesContentBottom                          from '../Images/anafulus.jpg';

import { Container, Row}                            from 'reactstrap';
import '../Css/ContentLandingBottom.css';

class ContentLandingBottom extends React.Component {
    render() {
        return (
            <Fragment>
                <Container fluid>  
                    <Row>
                        <Col-full><img className="banner-anafulus" src={ImagesContentBottom} alt="banner"/></Col-full>
                    </Row>
                </Container>
                <Container className="disclaimer">
                        <h5>Disclaimer Resiko</h5>
                        <br />
                        <p >1. Layanan Pinjam Meminjam Berbasis Teknologi Informasi merupakan kesepakatan perdata antara Pemberi Pinjaman dengan Penerima Pinjaman,   sehingga segala risiko 
                        yang timbul dari kesepakatan tersebut ditanggung sepenuhnya oleh masing-masing pihak. 
                        <br/><br/>
                        2. Risiko kredit atau gagal bayar ditanggung sepenuhnya oleh Pemberi  Pinjaman. Tidak ada lembaga atau otoritas negara yang bertanggung jawab  atas risiko gagal bayar ini. 
                        <br/><br/>
                        3. Penyelenggara dengan persetujuan dari masing-masing Pengguna (Pemberi  Pinjaman dan/atau Penerima Pinjaman) mengakses, memperoleh, menyimpan, mengelola, 
                        dan/atau menggunakan data pribadi Pengguna ("Pemanfaatan Data") pada atau di dalam benda, perangkat elektronik (termasuk smartphone atau telepon seluler), 
                        perangkat keras (hardware)  maupun lunak (software), dokumen elektronik, aplikasi atau sistem  elektronik milik Pengguna atau yang dikuasai Pengguna, dengan
                        memberitahukan tujuan, batasan, dan mekanisme Pemanfaatan Data tersebut kepada Pengguna yang bersangkutan sebelum memperoleh persetujuan yang dimaksud.
                        <br/><br/>
                        4. Pemberi Pinjaman yang belum memiliki pengetahuan dan pengalaman  pinjam meminjam, disarankan untuk tidak menggunakan layanan ini.
                        <br/><br/>
                        5. Penerima Pinjaman harus mempertimbangkan tingkat bunga pinjaman dan biaya lainnya sesuai dengan kemampuan dalam melunasi pinjaman.
                        <br/><br/>
                        6. Setiap kecurangan tercatat secara digital di dunia maya dan dapat diketahui  masyarakat luas di media sosial.
                        <br/><br/>
                        7. Pengguna harus membaca dan memahami informasi ini sebelum membuat  keputusan menjadi Pemberi Pinjaman atau Penerima Pinjaman.
                        <br/><br/>
                        8. Pemerintah yaitu dalam hal ini Otoritas Jasa Keuangan, tidak bertanggung  jawab atas setiap pelanggaran atau ketidakpatuhan oleh Pengguna, baik Pemberi Pinjaman 
                        maupun Penerima Pinjaman (baik karena kesengajaan  atau kelalaian Pengguna) terhadap ketentuan peraturan perundang-undangan maupun kesepakatan atau perikatan a
                        antara Penyelenggara dengan Pemberi Pinjaman dan/atau Penerima Pinjaman.
                        <br/><br/>
                        9. Setiap transaksi dan kegiatan pinjam meminjam atau pelaksanaan  kesepakatan mengenai pinjam meminjam antara atau yang melibatkan Penyelenggara, Pemberi 
                        Pinjaman, dan/atau Penerima Pinjaman wajib dilakukan melalui escrow account dan virtual account sebagaimana yang diwajibkan berdasarkan Peraturan Otoritas Jasa 
                        Keuangan Nomor 77/POJK. 01/2016 tentang Layanan Pinjam Meminjam Uang Berbasis Teknologi Informasi dan pelanggaran atau ketidakpatuhan terhadap ketentuan 
                        tersebut merupakan bukti telah terjadinya pelanggaran hukum oleh Penyelenggara sehingga Penyelenggara wajib menanggung ganti rugi yang diderita oleh masing-masing 
                        Pengguna sebagai akibat langsung dari pelanggaran hukum tersebut di atas tanpa mengurangi hak Pengguna yang menderita kerugian menurut Kitab Undang-Undang 
                        Hukum Perdata.
                        <br/>
                        <br/>
                        PT Jasa Komunitas Digital (Anafulus) merupakan badan hukum yang didirikan berdasarkan Hukum Republik Indonesia. Berdiri sebagai perusahaan yang telah diatur oleh dan dalam pengawasan Otoritas Jasa Keuangan (OJK) di Indonesia, Perusahaan menyediakan layanan interfacing sebagai penghubung pihak yang memberikan pinjaman dan pihak yang membutuhkan pinjaman meliputi pendanaan dari individu, organisasi, maupun badan hukum kepada individu atau badan hukum tertentu. Perusahaan tidak menyediakan segala bentuk saran atau rekomendasi pendanaan terkait pilihan-pilihan dalam situs ini.
                        <br/>
                        <br/>
                        Isi dan materi yang tersedia pada situs Anafulus.id dimaksudkan untuk memberikan informasi dan tidak dianggap sebagai sebuah penawaran, permohonan, undangan, saran, maupun rekomendasi untuk menginvestasikan sekuritas, produk pasar modal, atau jasa keuangan lainya. Perusahaan dalam memberikan jasanya hanya terbatas pada fungsi administratif.
                        <br/>
                        <br/>
                        Pendanaan dan pinjaman yang ditempatkan di rekening Anafulus adalah tidak dan tidak akan dianggap sebagai simpanan yang diselenggarakan oleh Perusahaan seperti diatur dalam Peraturan Perundang-Undangan tentang Perbankan di Indonesia. Perusahaan atau setiap Direktur, Pegawai, Karyawan, Wakil, Afiliasi, atau Agen-Agennya tidak memiliki tanggung jawab terkait dengan setiap gangguan atau masalah yang terjadi atau yang dianggap terjadi yang disebabkan oleh minimnya persiapan atau publikasi dari materi yang tercantum pada situs Perusahaan.  
                        </p>
                </Container>
            </Fragment>
        );
    }
};

export default ContentLandingBottom;