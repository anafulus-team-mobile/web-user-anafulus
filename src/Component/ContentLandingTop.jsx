import React, { Fragment }                      from 'react';

import icoLeft                                  from '../Icon/mosque.png';
import icoCenter                                from '../Icon/group-2.png';
import icoRight                                 from '../Icon/loan.png';
import imageKospin                              from '../Images/gatsu.jpg';

import { Link }                                 from 'react-router-dom';

import { Container, Row, Col, Button }          from 'reactstrap';
import '../Css/ContentLandingTop.css';
import '../Css/Reuse.css';

class ContentLandingTop extends React.Component {
    render() {
        return (
            <Fragment>
                <div>
                <Container className="section">
                    <h4>Mengapa Pinjam di <span>Anafulus</span></h4>
                        <Row>
                            <Col sm="4">
                                <img className="img-resposive" src={icoLeft} alt="murabahah"/>
                                    <h4>Pinjaman Murabahah</h4>
                                        <p className="desc-section">Menyediakan peminjaman berbasis syariah untuk 
                                        pergi umrah atau naik haji ataupun usaha anda.
                                        </p>
                            </Col>
                            <Col sm="4">
                                <img className="img-resposive" src={icoCenter} alt="tanggung-renteng"/>
                                    <h4>Pinjaman Tanggung Renteng</h4>
                                        <p className="desc-section">Menyediakan peminjaman tanggung renteng atau
                                        kelompok. Sehingga memungkinkan anda meminjam.
                                        </p>
                            </Col>
                            <Col sm="4">
                                <img className="img-resposive" src={icoRight} alt="mudah-cepat"/>
                                    <h4>Pinjaman Mudah dan Cepat</h4>
                                        <p className="desc-section">Pengajuan peminjaman mudah dan cepat.</p>
                            </Col>
                        </Row>
                </Container>
                <Container className="aboutUs">
                        <Row>
                            <Col sm="2"> 
                                <img className="img-responsive" src={imageKospin} alt="gedung-kospin"/>
                            </Col>
                            <Col sm="12" md={{ size: 8, offset: 2 }}>
                                <p className="title-about"> Tentang Kami </p>
                                <p className="content-about"> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
                                incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud 
                                exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
                                <br/>
                                <br/>
                                Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla 
                                pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia 
                                deserunt mollit anim id est laborum.</p>

                                <Link to="/About"><Button className="btn-yellow1">Selengkapnya</Button></Link>
                            </Col>
                        </Row>
                </Container>
                </div>
            </Fragment>
        );
    }
};

export default ContentLandingTop;