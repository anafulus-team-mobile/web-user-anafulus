import React, { Fragment }                              from 'react';
import Header                                           from './Header';
import ContentLandingBottom                             from './ContentLandingBottom';
import Footer                                           from './Footer';

import { Link }                                         from 'react-router-dom';


import icoGoogle                                        from '../Icon/icon-google.png';
import icoFacebook                                      from '../Icon/facebook-login.png';  

import { Card, Button, CardHeader, CardBody,
         CardTitle, Container, Col, Row, 
         Input, Label }                                 from 'reactstrap';
import '../Css/Login.css';  
import '../Css/Reuse.css';


class Login extends React.Component {
    render(){
      return(
        <Fragment>
            <Header/>
                <div>
                    <Container className="card-login">
                        <Col sm="10" md="5" className="mx-auto">
                            <Card>
                                <CardHeader>
                                    <h5>Masuk</h5>
                                    <Label>Silahkan masukkan nomor ponsel anda</Label>
                                </CardHeader>
                                <CardBody>
                                    <form>
                                        <Col>
                                            <CardTitle id="num-phone">Nomor Ponsel</CardTitle>
                                            <Input type="number" id="input-box" placeholder="Contoh: 08123456789"/>
                                        </Col>
                                        <Col>
                                            <u><Link to="/ChangeNumPhone" id="change-num">Ganti nomor ponsel ?</Link></u>
                                        </Col>
                                        <br/>
                                        <br/>
                                        <Col>
                                            <Link to="/Form-OTP">
                                                <center><Button className="btn-green1 btn-block" type="submit">Masuk</Button></center>
                                            </Link>
                                        </Col>
                                    <center>
                                        <Col>
                                        <p>Atau masuk dengan</p>
                                        </Col>
                                        <Col>
                                        <Link to="/FormRegister">
                                        <Button
                                        className="btn-card btn-block"
                                        id="btn-login-facebook"
                                        color="light">
                                            <Row>
                                                <Col xs="1">
                                                <img src={icoFacebook} alt="icon-facebook-login"/>
                                                </Col>
                                                <Col> Masuk dengan Facebook </Col>
                                            </Row>
                                        </Button>  
                                        </Link>
                                    </Col>
                                    <Col>
                                       <Link to="/FormRegister">
                                        <Button
                                        className="btn-card btn-block"
                                        id="btn-login-google"
                                        color="light">
                                            <Row>
                                                <Col xs="1">
                                                <img src={icoGoogle} alt="icon-google-login"/>
                                                </Col>
                                                <Col> Masuk dengan Google </Col>
                                            </Row>
                                            </Button>
                                            </Link>
                                        </Col>
                                        </center>
                                    </form>
                                </CardBody> 
                            </Card>
                        </Col>
                    </Container>
                    </div>
                <ContentLandingBottom/>
            <Footer/>
        </Fragment>
      );
    }
  };
  
  export default Login;
      