// Component, Page imported //
import React, { Fragment }                    from 'react';
import HeaderAccount                          from '../Component/HeaderAccount';
import SideBarProfile                         from '../Component/SidebarProfile';
import ContentLandingBottom                   from '../Component/ContentLandingBottom';
import Footer                                 from '../Component/Footer';

import { Link }                               from 'react-router-dom';

// Styling imported //
import { Container, Row, Col, Button, 
         Label, Input, Form, FormGroup }      from 'reactstrap';
import '../Css/Reuse.css';
import '../Css/UserDashboard.css';


class ChangeUserData extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
          modal: false
        };

        this.toggle = this.toggle.bind(this);
      }
    
      toggle() {
        this.setState(prevState => ({
          modal: !prevState.modal
        }));
      }

    render() {
        return (
            <Fragment>
                <HeaderAccount/>
                    <div>
                    <Container className="container">
                        <Row>
                            {/* Component sidebar profile*/}
                            <Col sm="4">
                                <SideBarProfile/>
                            </Col>
                            {/* end */}
                                <Col sm="8" className="col-dashboard-user">
                                    <h3>Ubah Data Diri</h3><hr/>
                                        <Container className="container-wrap2"> 
                                                <Col xs={{offset:1}}>
                                                    <Container>
                                                        <Row>
                                                            <Col sm="5">
                                                                <Label className="title-dashboard">Formulir Data Diri</Label>
                                                                <Label>*Harus diisi</Label>
                                                            </Col>
                                                        </Row>
                                                    </Container>
                                                    <Container>
                                                        <Row >
                                                            <Col>
                                                                <Form>
                                                                    <FormGroup>
                                                                    <Label className="title-data">Nama Lengkap Sesuai KTP</Label>
                                                                        <Input type="text" id="input-box"></Input>
                                                                    </FormGroup>
                                                                    <FormGroup>
                                                                    <Label className="title-data">Nomor Ponsel*</Label>
                                                                        <Input type="number" id="input-box"></Input>
                                                                    </FormGroup>
                                                                    <FormGroup>
                                                                    <Label className="title-data">E-mail</Label>
                                                                        <Input type="text" id="input-box"></Input>
                                                                    </FormGroup>
                                                                    <FormGroup>
                                                                    <Label className="title-data" >Jenis Kelamin</Label>
                                                                        <Input type="select" id="input-box">
                                                                        <option>Laki-laki</option>
                                                                        <option>Perempuan</option>
                                                                        </Input>
                                                                    </FormGroup>
                                                                    <FormGroup>
                                                                    <Label className="title-data">Tanggal Lahir</Label>
                                                                        <Input type="date" id="input-box"></Input>
                                                                    </FormGroup>
                                                                    <FormGroup>
                                                                    <Label className="title-data">Pendidikan</Label>
                                                                        <Input type="text" id="input-box"></Input>
                                                                    </FormGroup>
                                                                    <FormGroup>
                                                                    <Label type="number" className="title-data">Nomor KTP</Label>
                                                                        <Input className="title-data" id="input-box"></Input>
                                                                    </FormGroup>
                                                                    <FormGroup>
                                                                    <Label className="title-data">Nomor Kartu Keluarga*</Label>
                                                                        <Input type="text" id="input-box"/>
                                                                    </FormGroup>
                                                                    <FormGroup>
                                                                    <Label className="title-data">Nomor NPWP</Label>
                                                                        <Input type="text" id="input-box"></Input>
                                                                    </FormGroup>
                                                                    <FormGroup>
                                                                    <Label className="title-data">Gaji Terakhir / Pendapatan per Bulan</Label>
                                                                        <Input type="text" id="input-box">Rp 5.000.000</Input>
                                                                    </FormGroup>
                                                                            <FormGroup check>
                                                                                <Label check>
                                                                                    <Input type="checkbox" name="pegawai-swasta"/>{' '}
                                                                                    Pegawai Swasta
                                                                                </Label>
                                                                            </FormGroup>
                                                                            <FormGroup check>
                                                                                <Label check>
                                                                                    <Input type="checkbox" name="pegawai-swasta"/>{' '}
                                                                                    Pegawai Negeri
                                                                                </Label>
                                                                            </FormGroup>
                                                                            <FormGroup check>
                                                                                <Label check>
                                                                                    <Input type="checkbox" name="Petani"/>{' '}
                                                                                    Guru/Dosen
                                                                                </Label>
                                                                            </FormGroup>
                                                                            <FormGroup check>
                                                                                <Label check>
                                                                                    <Input type="checkbox" name="Petani"/>{' '}
                                                                                    Wirausaha
                                                                                </Label>
                                                                            </FormGroup>
                                                                            <FormGroup check>
                                                                                <Label check>
                                                                                    <Input type="checkbox" name="Petani"/>{' '}
                                                                                    Petani
                                                                                </Label>
                                                                            </FormGroup>
                                                                            <div>
                                                                            <FormGroup check>
                                                                                <Row>
                                                                                <Col sm="2" xs="2"><Label check>
                                                                                    <Input type="checkbox" name="Lainnya"/>{' '}
                                                                                    Lainnya:
                                                                                </Label></Col>
                                                                                <Col sm="8" xs="8">
                                                                                <Input type="text" id="input-box" name="PekerjaanLainnya" placeholder="Masukkan pekerjaan anda disini"/>
                                                                                </Col>
                                                                                </Row>
                                                                            </FormGroup>
                                                                            </div>
                                                                        <Col sm="5" className="mx-auto"><Link to="/AccountUser"><Button className="btn-green1 btn-block">Simpan Perubahan</Button></Link></Col>                                                         
                                                                </Form>
                                                            </Col>
                                                        </Row>
                                                    </Container>
                                                </Col>
                                        </Container>
                                        
                                    </Col>
                                </Row>
                            </Container>
                            </div>
                <ContentLandingBottom/>
            <Footer/>
            </Fragment>
        );
    }
}

export default ChangeUserData;