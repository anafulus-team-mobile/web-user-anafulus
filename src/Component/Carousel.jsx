import React                                    from 'react';
import Carousel1                                from '../Images/promo-anafulus.jpg';

import { UncontrolledCarousel }                 from 'reactstrap';
import '../Css/Style.css';

const items = [
    {
        src: Carousel1,
        altText: 'Image1',
                    
    },
    {   
        src: Carousel1,
        altText: 'Image2',
    
    },
    {   
        src: Carousel1,
        altText: 'Image3',
    }
];
            
const Carousel = (className="carousel") => <UncontrolledCarousel items={items} />;

export default Carousel;