// Component, Page imported //
import React, { Fragment }                    from 'react';
import HeaderAccount                          from '../Component/HeaderAccount';
import SideBarProfile                         from '../Component/SidebarProfile';
import ContentLandingBottom                   from '../Component/ContentLandingBottom';
import Footer                                 from '../Component/Footer';

import { Link }                               from 'react-router-dom';

// Img, Icon,logo imported //
import IconAccountBank                        from '../Icon/icon-akun-bank.png';


// Styling imported //
import { Container, Row, Col, Button,
         Label }                              from 'reactstrap';
import '../Css/Reuse.css';
import '../Css/UserDashboard.css';


class AccountBank extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
          modal: false
        };

    
        this.toggle = this.toggle.bind(this);
      }
    
      toggle() {
        this.setState(prevState => ({
          modal: !prevState.modal
        }));
      }

    render() {
        return (
            <Fragment>
                <HeaderAccount/>
                    <div>
                    <Container>
                        <Row>
                            {/* Component sidebar profile*/}
                            <Col sm="4">
                                <SideBarProfile/>
                            </Col>
                            {/* end */}
                                <Col sm="8" className="col-dashboard-user">
                                    <h3>Rekening Bank</h3><hr/>
                                        <Container className="container-wrap2"> 
                                            <center>
                                                <Col xs="12"><img src={IconAccountBank} alt="IconBank"/></Col>
                                                <br/>
                                                <Label>Anda belum menambahkan rekening bank.</Label>
                                                <br/>
                                                <Link to="/DetailAccount"><Button className="btn-green1 btn-block">Tambah Rekening</Button></Link>                                                               
                                            </center> 
                                        </Container>
                                    </Col>
                                </Row>
                            </Container>
                            </div>
                <ContentLandingBottom/>
            <Footer/>
            </Fragment>
        );
    }
}

export default AccountBank;