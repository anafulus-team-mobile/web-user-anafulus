// Component, Page imported //
import React, { Fragment }                    from 'react';
import HeaderAccount                          from '../Component/HeaderAccount';
import ContentLandingBottom                   from '../Component/ContentLandingBottom';
import Footer                                 from '../Component/Footer';

import { Link }                               from 'react-router-dom';

// Styling imported //
import { Container, Row, Col, Button,
         Label, Form, FormGroup, Input }      from 'reactstrap';
import '../Css/PinjamanMurabahah.css';


class PinjamanMurabahah extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
          modal: false
        };

    
        this.toggle = this.toggle.bind(this);
      }
    
      toggle() {
        this.setState(prevState => ({
          modal: !prevState.modal
        }));
      }

    render() {
        return (
            <Fragment>
                <HeaderAccount/>
                    <Container className="container">
                        <h3>Pinjaman Melalui Agent</h3><hr/>
                            <Row>
                                <Col sm="7" xs="12" >
                                <Col className="refferal-code">
                                <h5>Masukkan Kode Agen</h5>
                                <Form>
                                    <FormGroup>
                                        <Label>Kode Agen</Label>
                                        <br/>
                                        <Input type="text" id="input-box"placeholder="Masukkan kode agen"/>
                                        <p>*Jika anda tidak memiliki kode agen, maka anda dapat
                                            melakukan pencarian agen yang terdekat dengan alamat anda.</p>
                                    </FormGroup>
                                    <Link to="/Agen-List"><Button className="btn-green1">Temukan Agen Terdekat</Button></Link>
                                </Form>
                                </Col>
                                </Col>
                                    <Col sm={{size:'4', offset:'1'}} xs="12" >
                                        <Col className="apply-pinjaman-murabahah">
                                            <Form>
                                                <h5>Detail Pinjaman</h5>
                                                    <FormGroup>
                                                        <Label for="">Nominal Pinjaman</Label>
                                                        <p name="nominalPinjaman">Rp 5.000.000</p>
                                                        
                                                        <FormGroup>
                                                            <Label for="tenorPinjman">Tenor Pinjaman</Label>
                                                            <p name="tenorPinjaman">16 Minggu</p>
                                                        </FormGroup>

                                                        <FormGroup>
                                                            <Label for="jenisCicilan">Jenis cicilan</Label>
                                                            <p name="jenisCicilan">Murabahah</p>
                                                        </FormGroup>
                                                    </FormGroup>  

                                                    <FormGroup>
                                                        <Label id="Title-Cicilan">Cicilan:</Label>
                                                        <p id="Total-Cicilan">Rp 200.000 / Minggu</p>
                                                    </FormGroup>
                                                <center><Link to="/Dashboard"><Button className="btn-green1">Lanjutkan</Button></Link></center>                             
                                            </Form>
                                        </Col> 
                                    </Col>
                            </Row>
                    </Container>
                <ContentLandingBottom/>
            <Footer/>
            </Fragment>
        );
    }
}

export default PinjamanMurabahah;