import React,   { Fragment }                                from 'react';
import Header                                               from './Header';
import ContentLandingBottom                                 from './ContentLandingBottom';
import Footer                                               from './Footer';

import { Link }                                             from 'react-router-dom';

import icoGoogle                                            from '../Icon/icon-google.png';
import icoFacebook                                          from '../Icon/facebook-login.png'; 

import { Card, Button, CardHeader, CardBody, Container, 
        Label, Form, FormGroup, Col, Input, Row }           from 'reactstrap';
import '../Css/Register.css';  
  

class Register extends React.Component {
   render(){
      return(
        <Fragment>
            <Header/>
                    <Container className="card-register">
                        <Col sm="10" md="5" className="mx-auto">                        
                            <Card>
                                <CardHeader >
                                    <h5>Daftar</h5>
                                    <p>Silahkan daftarkan email dan kata sandi anda</p>
                                </CardHeader>
                                <CardBody>
                                    <Form>
                                        <FormGroup>
                                            <Label for="Phone">Nomor Ponsel</Label>
                                            <Input type="number" id="input-box" name="Phone" placeholder="Contoh: 08123456789"/>
                                        </FormGroup>
                                        <br/>
                                        <Col>
                                            <Link to="/FormRegister">
                                            <Button
                                            className="btn-green1 btn-block" >
                                            Daftar  
                                            </Button></Link>
                                        </Col>
                                        <Col>
                                            <p>Atau masuk dengan</p>
                                        </Col>
                                        <Col>
                                            <Link to="/FormRegister">
                                            <Button
                                            className="btn-card btn-block"
                                            id="btn-login-facebook"
                                            color="light">
                                                <Row>
                                                    <Col xs="1">
                                                    <img src={icoFacebook} alt="icon-facebook-login"/>
                                                    </Col>
                                                    <Col>
                                                    Masuk dengan Facebook
                                                    </Col>
                                                </Row>
                                            </Button>  
                                            </Link>
                                        </Col>
                                        <Col>
                                        <Link to="/FormRegister">
                                            <Button
                                            className=" btn-card btn-block"
                                            id="btn-login-google"
                                            color="light">
                                                <Row>
                                                    <Col xs="1">
                                                    <img src={icoGoogle} alt="icon-google-login"/>
                                                    </Col>
                                                    <Col>
                                                    Masuk dengan Google
                                                    </Col>
                                                </Row>
                                            </Button>
                                            </Link>
                                        </Col>
                                    </Form>
                                </CardBody> 
                            </Card>
                        </Col>
                    </Container>
                <ContentLandingBottom/>
            <Footer/>
        </Fragment>
      );
    }
  };
  
  export default Register;
      