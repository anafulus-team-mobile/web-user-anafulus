import React                                         from 'react';

import { Link }                                      from 'react-router-dom';

import { Collapse, Navbar, NavbarToggler,
         NavbarBrand, Nav, NavLink, NavItem, 
         UncontrolledDropdown, DropdownToggle, 
         DropdownMenu, DropdownItem, Button }        from 'reactstrap';                   
import '../Css/Header.css';

// Class Header
class Header extends React.Component {
  constructor(props) {
    super(props);

// Responsive navbar with toggle
    this.toggle = this.toggle.bind(this);
    this.state = {
      isOpen: false
    };
  }
  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }
  // Call view
  render() {
    return (
      <div>
        <div align="right" style={{backgroundColor:'#229f89', opacity:'0.9'}}>
          <Button className="btn-TKB90">TKB90 = 100 %</Button>
        </div>
        <Navbar dark expand="sm">
          <NavbarBrand id="logo" href="/">anafulus</NavbarBrand>
            <NavbarToggler onClick={this.toggle} navbar />
              <Collapse navbar isOpen={this.state.isOpen}>
                <Nav className="mx-auto">
                  <NavItem>
                    <NavLink href="/">Beranda</NavLink>
                  </NavItem>
                  <NavItem>
                   <UncontrolledDropdown nav inNavbar>
                      <DropdownToggle nav>Pinjaman</DropdownToggle>
                      <DropdownMenu right >
                        <DropdownItem>
                          <Link to="/Loan-Murabahah" className="down-list">Murabahah</Link>
                        </DropdownItem>
                        <DropdownItem>
                          <Link to="/Loan-Reguler" className="down-list">Reguler</Link>  
                        </DropdownItem>
                        <DropdownItem>
                          <Link to="/Loan-Badan-Usaha" className="down-list">Badan Usaha</Link>  
                        </DropdownItem>
                      </DropdownMenu>
                    </UncontrolledDropdown>
                  </NavItem>
                  <NavItem>
                    <NavLink href="/">Pendanaan</NavLink>
                  </NavItem>
                  <NavItem>
                    <UncontrolledDropdown nav inNavbar>
                      <DropdownToggle nav>Tentang Anafulus</DropdownToggle>
                    <DropdownMenu right >
                      <DropdownItem className="down-list">
                        <Link to="/About" className="down-list">Tentang Anafulus</Link>
                      </DropdownItem>
                      <DropdownItem>
                        <Link to="/ContactUs" className="down-list">Hubungi Kami</Link>
                      </DropdownItem>
                      <DropdownItem>
                        <Link to="/Faq" className="down-list">Bantuan (FAQ)</Link>
                      </DropdownItem>
                      </DropdownMenu>
                    </UncontrolledDropdown>
                    </NavItem>
                </Nav>
                <NavItem>
                  <Link to="/Login"><Button id="login" title="login" color="light" size="sm">Masuk</Button></Link>{' '}
                  <Link to="/UserRegister"><Button id="signup" title="signup" color="light" size="sm">Daftar</Button></Link>{' '}
                </NavItem>
          </Collapse>
        </Navbar>
        </div>
    );
  }
}

export default Header;  