import React                                                    from 'react';
import { Button, Modal, ModalHeader, 
         ModalBody, ModalFooter, Container, Col }               from 'reactstrap';

class TKB90 extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
          modal: false
        };
    
        this.toggle = this.toggle.bind(this);
      }
    
      toggle() {
        this.setState(prevState => ({
          modal: !prevState.modal
        }));
      }
    
    render() {
        return (
            <div>
            <Container fluid>
                <Col>
                    <Button color="danger" onClick={this.toggle}>{this.props.buttonLabel}</Button>
                    <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
                    <ModalHeader toggle={this.toggle}>Modal title</ModalHeader>
                    <ModalBody>
                        <p textAlign="justify">
                            Tingkat Keberhasilan Penyelesaian Kewajiban Pinjam Meminjam oleh Anafulus
                            Dalam rangka mematuhi prinsip transparansi sesuai dengan ketentuan Pasal 29 huruf a Peraturan Otoritas Jasa Keuangan Nomor 77/POJK.01/2016 tentang Layanan Pinjam Meminjam Uang Berbasis Teknologi Informasi, Anafulus sebagai Penyelenggara Layanan Pinjam Meminjam Uang Berbasis Teknologi Informasi wajib mempublikasikan tingkat keberhasilan dalam memfasilitasi penyelesaian kewajiban pinjam meminjam antara Penerima Pinjaman kepada Pemberi Pinjaman dalam jangka waktu sampai dengan 90 hari terhitung sejak jatuh tempo (“Tingkat Keberhasilan 90 atau TKB90”) pada laman utama Ananfulus. Semakin tinggi persentase TKB90 yang tertera, semakin baik pula penyelenggaraan pinjam meminjam yang dilaksanakan oleh Anafulus.
                            Rumus perhitungan yang digunakan untuk menentukan TKB90 adalah sebagai berikut:

                        <center>
                            TKB90 = 100% - TWP90  
                            Outstanding wanprestasi di atas 90 hari
                                
                            TWP90 = 
                            x 100 %
                            Total Outstanding
                        </center>

                            TKB90 adalah ukuran tingkat keberhasilan penyelenggara fintech-peer-to-peer (P2P) lending dalam menfasilitasi penyelesaian kewajiban pinjam meminjam dalam jangka waktu sampai dengan 90 hari terhitung sejak jatuh tempo.
                            TWP90 adalah ukuran tingkat wanprestasi atau kelalaian penyelesaian kewajiban yang tertera dalam perjanjian di atas 90 hari sejak tanggal jatuh tempo.
                        </p>
                    </ModalBody>
                    <ModalFooter>
                        <Button color="primary" onClick={this.toggle}>Mengerti</Button>
                    </ModalFooter>
                    </Modal>
                </Col>
            </Container>
            
          </div>
                                
);
}
};

export default ContentLandingBottom;