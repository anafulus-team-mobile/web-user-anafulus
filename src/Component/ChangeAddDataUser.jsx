// Component, Page imported //
import React, { Fragment }                    from 'react';
import HeaderAccount                          from '../Component/HeaderAccount';
import SideBarProfile                         from '../Component/SidebarProfile';
import ContentLandingBottom                   from '../Component/ContentLandingBottom';
import Footer                                 from '../Component/Footer';

import { Link }                               from 'react-router-dom';

// Img, Icon,logo imported //
import IconUserWithID                         from '../Icon/card.png';
import IconIDCard                             from '../Icon/userID.png';
import IconUserHouse                          from '../Icon/house.png';
import IconUserSalary1                        from '../Icon/slip-gaji-anafulus.png';
import IconUserSalary2                        from '../Icon/cafe.png';


// Styling imported //
import { Container, Row, Col, Button,
         Label, Input, Form, FormGroup, 
         FormText }                          from 'reactstrap';
import '../Css/Reuse.css';
import '../Css/UserDashboard.css';


class ChangeAddDataUser extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
          modal: false
        };

    
        this.toggle = this.toggle.bind(this);
      }
    
      toggle() {
        this.setState(prevState => ({
          modal: !prevState.modal
        }));
      }

    render() {
        return (
            <Fragment>
                <HeaderAccount/>
                    <div>
                    <Container>
                        <Row>
                            {/* Component sidebar profile*/}
                            <Col sm="4">
                                <SideBarProfile/>
                            </Col>
                            {/* end */}
                                <Col sm="8" className="col-dashboard-user">
                                    <h3>Ubah Data Persyaratan</h3><hr/>
                                        <Container className="container-wrap2"> 
                                                <Col>
                                                    <h5 className="title-dashboard">Formulir Data Persyaratan Peminjam</h5>
                                                    <p>*Harus diisi</p>
                                                    <Container className="container-wrap2">
                                                        <Fragment>
                                                            <Row>
                                                                <Col>
                                                                    <Form>
                                                                        <FormGroup>
                                                                            <h6>Unggah Foto Diri dan KTP*</h6>
                                                                            <Label> Foto diri anda dengan KTP</Label>
                                                                                <Container>
                                                                                    <Row>
                                                                                        <Row>
                                                                                        <Col sm="7">
                                                                                            <Col style={{boxShadow:'0 3px 6px 0 rgba(0, 0, 0, 0.16)', width:'240px', height: '150px', padding:'20px 20px 20px 55px'}}>
                                                                                                <img src={IconUserWithID} alt="userWithID"/>
                                                                                            </Col>
                                                                                        </Col>
                                                                                        <Col sm="5">
                                                                                            <Button className="btn-file btn-yellow3">Unggah Foto
                                                                                                <input type="file"/>
                                                                                            </Button>
                                                                                            <FormText>maksimum size 1 MB dengan format jpg, png, gif.</FormText>                                                                                           
                                                                                        </Col>
                                                                                        </Row>
                                                                                    </Row>
                                                                                </Container>
                                                                        </FormGroup>
                                                                    </Form>
                                                                </Col>
                                                            </Row>
                                                        </Fragment>
                                                    </Container>
                                                    <Container className="container-wrap2">
                                                        <Fragment>
                                                            <Row>
                                                                <Col>
                                                                    <Form>
                                                                        <FormGroup>
                                                                            <h6>Unggah Foto KTP*</h6>
                                                                            <Label>Foto KTP anda</Label>
                                                                                <Container>
                                                                                    <Row>
                                                                                        <Row>
                                                                                        <Col sm="7">
                                                                                            <Col style={{boxShadow:'0 3px 6px 0 rgba(0, 0, 0, 0.16)', width:'240px', height: '150px', padding:'15px 20px 20px 25px'}}>
                                                                                                <img src={IconIDCard} alt="photoID"/>
                                                                                            </Col>
                                                                                        </Col>
                                                                                        <Col sm="5">
                                                                                            <Button className="btn-file btn-yellow3">Unggah Foto
                                                                                                <input type="file"/>
                                                                                            </Button>
                                                                                            <FormText>maksimum size 1 MB dengan format jpg, png, gif.</FormText>                                                                                          
                                                                                        </Col>
                                                                                        </Row>
                                                                                    </Row>
                                                                                </Container>
                                                                        </FormGroup>
                                                                    </Form>
                                                                </Col>
                                                            </Row>
                                                        </Fragment>
                                                    </Container>
                                                    <Container className="container-wrap2">
                                                        <Fragment>
                                                            <Row>
                                                                <Col>
                                                                    <Form>
                                                                        <FormGroup>
                                                                            <h6>Unggah Foto Rumah Anda</h6>
                                                                            <Label>Foto Tampak depan rumah anda</Label>
                                                                                <Container>
                                                                                    <Row>
                                                                                        <Row>
                                                                                        <Col sm="7">
                                                                                            <Col style={{boxShadow:'0 3px 6px 0 rgba(0, 0, 0, 0.16)', width:'240px', height: '150px', padding:'20px 20px 20px 60px'}}>
                                                                                                <img src={IconUserHouse} alt="house"/>
                                                                                            </Col>
                                                                                        </Col>
                                                                                        <Col sm="5">
                                                                                            <Button className="btn-file btn-yellow3">Unggah Foto
                                                                                                <input type="file"/>
                                                                                            </Button>
                                                                                            <FormText>maksimum size 1 MB dengan format jpg, png, gif.</FormText>                                                                                          
                                                                                        </Col>
                                                                                        </Row>
                                                                                    </Row>
                                                                                </Container>
                                                                        </FormGroup>
                                                                    </Form>
                                                                </Col>
                                                            </Row>
                                                        </Fragment>
                                                    </Container>
                                                    <Container className="container-wrap2">
                                                        <Fragment>
                                                            <Row>
                                                                <Col>
                                                                    <Form>
                                                                        <FormGroup>
                                                                            <h6>Unggah Foto Usaha atau Slip Gaji</h6>
                                                                            <Label>Foto tempat usaha atau slip gaji anda</Label>                                                                            
                                                                                <Container>
                                                                                    <Row>
                                                                                        <Row>
                                                                                        <Col sm="7">
                                                                                            <Col style={{boxShadow:'0 3px 6px 0 rgba(0, 0, 0, 0.16)', width:'240px', height: '150px', padding:'20px 20px 20px 30px'}}>
                                                                                                <Row>
                                                                                                    <img src={IconUserSalary1} alt="photoSlipGaji"/>
                                                                                                    <img src={IconUserSalary2} alt="photoUsaha" hspace="20px"/>
                                                                                                </Row>
                                                                                                
                                                                                            </Col>
                                                                                        </Col>
                                                                                        <Col sm="5">
                                                                                        <FormGroup>
                                                                                            <Button className="btn-file btn-yellow3">Unggah Foto
                                                                                                <input type="file"/>
                                                                                            </Button>
                                                                                            <FormText>maksimum size 1 MB dengan format jpg, png, gif.</FormText> 
                                                                                        </FormGroup>
                                                                                        </Col>
                                                                                        </Row>
                                                                                    </Row>
                                                                                </Container>
                                                                        </FormGroup>
                                                                        <h5>Data Keluarga atau Kerabat Tidak Serumah</h5>
                                                                        <br/>
                                                                        <FormGroup>
                                                                            <Label className="title-data">Nama Keluarga / Kerarabat tidak serumah</Label>
                                                                            <Input type="text" id="input-box"></Input>
                                                                        </FormGroup>
                                                                        <FormGroup>
                                                                            <Label className="title-data">Nomor Ponsel Keluarga / Kerabat tidak serumah</Label>
                                                                            <Input type="text" id="input-box"></Input>
                                                                        </FormGroup>
                                                                        <FormGroup>
                                                                            <Label className="title-data">Hubungan dengan anda</Label>
                                                                            <Input type="text" id="input-box"></Input>
                                                                        </FormGroup>
                                                                        <Col xs="12" sm="5" className="mx-auto"><Link to="/AccountUser"><Button className="btn-green1 btn-block">Simpan Perubahan</Button></Link></Col>                                                         
                                                                    </Form>
                                                                </Col>
                                                            </Row>
                                                        </Fragment>
                                                    </Container>
                                                </Col>
                                        </Container>
                                    </Col>
                                </Row>
                            </Container>
                            </div>
                <ContentLandingBottom/>
            <Footer/>
            </Fragment>
        );
    }
}

export default ChangeAddDataUser;