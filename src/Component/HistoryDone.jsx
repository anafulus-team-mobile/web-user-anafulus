// Component, Page imported //
import React, { Fragment }                    from 'react';
import HeaderAccount                          from '../Component/HeaderAccount';
import SideBarProfile                         from '../Component/SidebarProfile';
import ContentLandingBottom                   from '../Component/ContentLandingBottom';
import Footer                                 from '../Component/Footer';

import { Link }                               from 'react-router-dom';

// Img, Icon,logo imported //


// Styling imported //
import { Container, Row, Col, Button, 
         Label, Dropdown, DropdownToggle, 
         DropdownMenu, DropdownItem}          from 'reactstrap';
import '../Css/Reuse.css';
import '../Css/UserDashboard.css';


class HistoryDone extends React.Component{
    constructor(props) {
        super(props);
    
        this.toggle = this.toggle.bind(this);
        this.state = {
          dropdownOpen: false
        };
      }
    
      toggle() {
        this.setState({
          dropdownOpen: !this.state.dropdownOpen
        });
      }

    render() {
        return (
            <Fragment>
                <HeaderAccount/>
                <div>
                    <Container>
                        <Row>
                            {/* Component sidebar profile*/}
                            <Col sm="4">
                                <SideBarProfile/>
                            </Col>
                            {/* end */}
                                <Col sm="8" className="col-dashboard-user">
                                    <h3>Riwayat</h3><hr/>
                                        <Container>
                                            <Dropdown isOpen={this.state.dropdownOpen} toggle={this.toggle}>
                                                <DropdownToggle caret>
                                                Pinjaman Selesai
                                                </DropdownToggle>
                                                <DropdownMenu>
                                                <Link to="/History-Approve"><DropdownItem>Pinjaman Disetujui</DropdownItem></Link>
                                                <Link to="/History-Decline"><DropdownItem>Pinjaman Ditolak</DropdownItem></Link>
                                                <Link to="/History-Pending"><DropdownItem>Pinjaman Belum Disetujui</DropdownItem></Link>
                                                <Link to="/History-Canceled"><DropdownItem>Pinjaman Dibatalkan</DropdownItem></Link>
                                                <Link to="/History-Ongoing"><DropdownItem>Cicilan Sedang Berjalan</DropdownItem></Link>
                                                <Link to="/History-Overdue"><DropdownItem>Cicilan Terlambat</DropdownItem></Link>
                                                </DropdownMenu>
                                            </Dropdown>
                                        </Container>
                                        <Container className="container-wrap2"> 
                                                <Row>
                                                    <Col xs="4" sm="3">
                                                    <Label>Id Pinjaman</Label>
                                                    <br/>
                                                    <Label>Jumlah Pinjaman</Label>
                                                    <br/>
                                                    <Label>Jumlah Cicilan</Label>
                                                    <br/>
                                                    <Label>Jumlah Tenor</Label>
                                                    <br/>
                                                    <Label>Jenis Cicilan</Label>
                                                    <br/>
                                                    <Label>Status Pinjaman</Label>
                                                    </Col>
                                                    <Col>
                                                    {': '}<Label>1xxxxxxxx</Label> 
                                                    <br/>
                                                    {': '}<Label>Rp 5.000.000</Label> 
                                                    <br/>
                                                    {': '}<Label>Rp 250.000 / Minggu</Label> 
                                                    <br/>
                                                    {': '}<Label>24 Minggu</Label>
                                                    <br/>
                                                    {': '}<Label>Mingguan</Label> 
                                                    <br/>
                                                    {': '}<Label>Pinjaman Selesai</Label> 
                                                    </Col>
                                                    <Col>
                                                        <Link to="/Detail-History-Done" id="detailDone"><Button className="btn-green1">Lihat Detail</Button></Link>
                                                    </Col>
                                                </Row>
                                        </Container>
                                    </Col>
                                </Row>
                            </Container>
                        </div>
                <ContentLandingBottom/>
            <Footer/>
            </Fragment>
        );
    }
}

export default HistoryDone;