// Component, Page imported //
import React, { Component, Fragment }           from 'react';
import HeaderAccount                            from '../Component/HeaderAccount';
import ContentLandingBottom                     from '../Component/ContentLandingBottom';
import Footer                                   from '../Component/Footer';

import { Link }                                 from 'react-router-dom';

// Img, Icon, Logo Imported //
import IcoPhone                                 from '../Icon/smartphone-call-1.png';

// Styling imported //
import { Container, Row, Col, Label,
         Input, FormGroup, Button }             from 'reactstrap';

class AmountWithDraw extends Component{
    render() {
        return (
            <Fragment>
                <HeaderAccount/>
                <div>
                    <Container>
                        <Row>
                            <Col sm={{size:6, offset:3}} className="col-dashboard-user">
                                <h3>Penarikan Dana</h3>
                                    <hr/>
                                <Col className="container-wrap2 mx-auto"> 
                                <center><Label style={{fontWeight:'bold', fontSize:'18px'}}>Jumlah Penarikan Dana</Label></center>
                                    <Col style={{border:'solid 1px #bdbdbd', paddingTop:'20px'}}>
                                        <Col>
                                        <FormGroup>
                                            <Label>Saldo Anda:</Label>{' '}
                                            <Label>Rp 200.000.000</Label>
                                        </FormGroup>
                                        <FormGroup>
                                            <Label>Jumlah Penarikan Dana</Label>
                                            <Input type="text" id="input-box"></Input>
                                            <Label>*Masukkan jumlah dana di bawah atau sama dengan saldo yang anda miliki. </Label>
                                        </FormGroup>
                                        </Col>
                                    </Col>
                                <br/>
                                <center><Label style={{fontWeight:'bold', fontSize:'18px'}}>Pilih Rekening</Label></center>
                                    <Col style={{border:'solid 1px #bdbdbd', paddingTop:'20px'}}>
                                        <Col xs={{offset:1}}>
                                        <FormGroup>
                                            <Input type="radio" name="select-rek-ponsel" style={{marginTop:'20px'}}/>
                                            <img src={IcoPhone} alt="IconPhone"/>Rekening Ponsel
                                        </FormGroup>
                                        </Col>
                                    </Col>
                                    <hr/>
                                    <center><Link to="/BankList">+ Tambah Rekening</Link>
                                    <br/>
                                    <Link to="/Confirm-Withdraw"><Button className="btn-green1 btn-block">Lanjutkan</Button></Link>
                                    </center>
                                </Col>    
                            </Col>
                        </Row>
                    </Container>    
                    </div>
                <ContentLandingBottom/>
            <Footer/>
        </Fragment>
        );
    }
}

export default AmountWithDraw;