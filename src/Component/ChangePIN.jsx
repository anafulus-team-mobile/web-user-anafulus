// Component, Page imported //
import React, { Fragment }                    from 'react';
import HeaderAccount                          from '../Component/HeaderAccount';
import SideBarProfile                         from '../Component/SidebarProfile';
import ContentLandingBottom                   from '../Component/ContentLandingBottom';
import Footer                                 from '../Component/Footer';

import { Link }                               from 'react-router-dom';


// Styling imported //
import { Container, Row, Col, Button,
         Label, Form, FormGroup }             from 'reactstrap';
import '../Css/Reuse.css';
import '../Css/UserDashboard.css';


class ChangePIN extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
          modal: false
        };

        this.toggle = this.toggle.bind(this);
      }
    
      toggle() {
        this.setState(prevState => ({
          modal: !prevState.modal
        }));
      }

    render() {
        return (
            <Fragment>
                <HeaderAccount/>
                    <div>
                    <Container>
                        <Row>
                            {/* Component sidebar profile*/}
                            <Col sm="4">
                                    <SideBarProfile/>
                                </Col>
                                {/* end */}
                                    <Col sm="8" className="col-dashboard-user">
                                    <h3>Ubah PIN</h3><hr/>
                                        <Container className="container-wrap1"> 
                                            <Form>
                                            <FormGroup>
                                                    <h5>PIN Lama</h5>
                                                    <br/>
                                                    <Label>Silahkan masukkan PIN lama anda</Label>
                                                    <br/>  
                                                    <input type="text"/>
                                                </FormGroup>
                                                <FormGroup>
                                                    <h5>PIN Baru</h5>
                                                    <br/>
                                                    <Label>Silahkan masukkan PIN baru anda</Label>
                                                    <br/>  
                                                    <input type="text"/>
                                                </FormGroup>
                                                <FormGroup>
                                                    <h5>Konfirmasi PIN Baru</h5>
                                                    <br/>
                                                    <Label>Konfirmasi PIN baru anda</Label>
                                                    <br/>  
                                                    <input type="text"/>
                                                </FormGroup>
                                                <Link to=""><Button className="btn-green1">Simpan</Button></Link>     
                                            </Form>
                                        </Container>
                                    </Col>
                                </Row>
                            </Container>
                            </div>
                <ContentLandingBottom/>
            <Footer/>
            </Fragment>
        );
    }
}

export default ChangePIN;