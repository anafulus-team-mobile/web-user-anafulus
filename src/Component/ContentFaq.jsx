import React                                         from 'react';

import { Collapse, Button, CardBody, 
         Card }                                      from 'reactstrap';

class Faq extends React.Component {
    constructor(props) {
      super(props);
      this.toggle = this.toggle.bind(this);
      this.state = { collapse: false };
    }
  
    toggle() {
      this.setState(state => ({ collapse: !state.collapse }));
    }
  
    render() {
      return (
        <div>
          <Button color="primary" onClick={this.toggle} style={{ marginBottom: '1rem' }}>Toggle</Button>
            <Collapse isOpen={this.state.collapse}>
              <Card>
                <CardBody>
                  <div class="container">
                      <div class="row">
                          <div class="col align-self-start">
                          <p>
                          1. Question 1 ?
                          <br />
                          Answer : Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, 
                          quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
                          </p>
                          </div>
                          <div class="col align-self-center">
                          One of three columns
                          </div>
                      </div>
                  </div>
                </CardBody>
              </Card>
            </Collapse>
        </div>
      );
    }
  };
  
export default Faq;
  
