// Import Lib, Component React & Icon  
import React, { Fragment }                                  from 'react';  
                        // Import React
import { Link }                                             from 'react-router-dom';

import ojk                                                  from '../Icon/logo-ojk.png';              // Import logo ojk from folder Images
import icofb                                                from '../Icon/facebook.png';            // Import logo ojk from folder Icon
import icoig                                                from '../Icon/instagram.png';           // Import logo ojk from folder Icon 

// Styling
import { Container, Row, Col }                              from 'reactstrap';   
import '../Css/Footer.css';                         


class Footer extends React.Component {
    render() {                                     
        return (                                    
           <Fragment>                        
                <Container fluid className="footer">
                    <Container>
                        <Row>
                            <Col sm="4">
                                <h5 className="logo-anafulus">anafulus</h5>
                                    <p>Gedung Kospin Jasa Lantai 6
                                    Jl. Jendral Gatot Subroto No.1 RT .01
                                    RW.01 Menteng Dalam, Tebet, 
                                    Jakarta Selatan DKI Jakarta 12870 <br/>
                                    Telp : 021 22903175-77 <br/>
                                    Info@anafulus.co.id
                                    </p>
                            </Col>

                            <Col sm="1"></Col>

                            <Col sm="3">
                                <h5>Informasi</h5>              
                                    <div>
                                        <ul>
                                            <li><Link to="/Kebijakan-Perusahaan">Kebijakan Perusahaan</Link></li>
                                            <li><Link to="/Kebijakan-Pinjaman">Kebijakan Pinjaman</Link></li>
                                            <li><Link to="/Kebijakan-Privasi">Kebijakan Privasi</Link></li>
                                            <li><Link to="/TermCon">Syarat & Ketentuan</Link></li>
                                            <li><Link to="/RegOJK">Regulasi OJK</Link></li>
                                        </ul>
                                    </div>
                            </Col>
                            <Col sm="1"></Col>

                            <Col sm="3">
                                <h5>Terdaftar dan Diawasi Oleh</h5>
                                    <a href="https://ojk.go.id/">
                                        <img className="logo-ojk" src={ojk} alt="ojk" style={{border: 'solid 1px #fff', padding:'10px 28px 10px 28px', borderRadius:'8px'}}/>
                                    </a>
                        <Row>
                                <Col>
                                    <h5>Ikuti kami di sosial media</h5>
                                </Col>
                        </Row>
                                <a href="https://id-id.facebook.com/"><img className="ico-social" src={icofb} alt="ico-fb"/></a>
                                <a href="https://www.instagram.com/?hl=id"><img className="ico-social" src={icoig} alt="ico-ig"/></a>              
                            </Col>
                        </Row>
                    
                </Container>
                        <Row>
                            <Col className="copy">Copryright 2019 PT. Jasa Komunitas Digital. All Right Reserved.</Col>
                        </Row>
            </Container>
         </Fragment>
        );   
    }
};

export default Footer;      // Open access Footer.jsx for other file