// Component, Page imported //
import React, { Component, Fragment }           from 'react';
import HeaderAccount                            from '../Component/HeaderAccount';
import ContentLandingBottom                     from '../Component/ContentLandingBottom';
import Footer                                   from '../Component/Footer';

import { Link }                                 from 'react-router-dom';

// Img, Icon, Logo Imported //
import IcoCopy                                 from '../Icon/copy-content-1.png';

// Styling imported //
import { Container, Row, Col, Label, 
         FormGroup, UncontrolledCollapse }     from 'reactstrap';

class PaymentDirect extends Component{
    render() {
        return (
            <Fragment>
                <HeaderAccount/>
                    <div>
                    <Container>
                        <Row>   
                            <Col sm="2"></Col>
                                <Col sm="8" className="col-dashboard-user">
                                    <h3>Instruksi Pembayaran</h3>
                                        <hr/>
                                    <Col sm="12" className="container-wrap2"> 
                                    <center><Label style={{fontWeight:'bold', fontSize:'18px'}}>Informasi Pembayaran</Label></center>
                                         <Container style={{border:'solid 1px #bdbdbd'}}>
                                               <Row>
                                                   <Col>
                                                        <Col>
                                                            <Label>Jumlah Pembayaran</Label>{' :'}
                                                            <br/>
                                                            <Label>Rp 2.000.000</Label>
                                                        </Col>
                                                    </Col>
                                                    <Col>
                                                        <Col>
                                                            <Label>Nama Perusahaan</Label>{' :'}
                                                            <br/>
                                                            <Label>Ananfulus</Label>
                                                        </Col>
                                                    </Col>
                                                </Row>
                                                <Col><hr/></Col>
                                                <Row>
                                                    <Col>
                                                        <Col>
                                                            <FormGroup>
                                                                <Label>Transfer ke rekening</Label>{' :'}
                                                                <br/>
                                                                <Label>12345678900000</Label>
                                                                <img src={IcoCopy} alt="Icon-Copy" hspace="16px"/>
                                                            </FormGroup>
                                                        </Col>
                                                    </Col>
                                               </Row>
                                            </Container>
                                            <br/>
                                            <center><Label style={{fontWeight:'bold', fontSize:'18px'}}>Instruksi Pembayaran</Label></center>
                                            <Container>
                                                <Row>
                                                    <Col style={{border:'solid 1px #bdbdbd'}}>
                                                        <Col><Link className="dashboard-menu" id="togglerTransfer" >ATM BCA / Transfer</Link></Col>
                                                        <UncontrolledCollapse toggler="#togglerTransfer">
                                                            <Col styl={{backgroundColor:'#ffffff'}}>
                                                                <p>
                                                                1. Pilih Transfer Ke BCA Virtual Account
                                                                <br/>
                                                                2. Masukkan nomor virtual account dan klik correct
                                                                <br/>
                                                                3. Masukkan jumlah pembayaran lalu klik OK
                                                                <br/>
                                                                4. Cek kembali nama anda dan jumlah pembayaran
                                                                <br/>
                                                                5. Ikuti langkahnya sampai pembayaran selesai
                                                                </p>
                                                            </Col>
                                                        </UncontrolledCollapse>
                                                    </Col>
                                                </Row>
                                                <Row>    
                                                <Col style={{border:'solid 1px #bdbdbd'}}>
                                                        <Col><Link className="dashboard-menu" id="togglerVA" >Virtual Account BCA</Link></Col>
                                                        <UncontrolledCollapse toggler="#togglerVA">
                                                            <Col style={{backgroundColor:'#ffffff'}}>
                                                                <p>
                                                                1. Pilih Transfer Ke BCA Virtual Account
                                                                <br/>
                                                                2. Masukkan nomor virtual account dan klik correct
                                                                <br/>
                                                                3. Masukkan jumlah pembayaran lalu klik OK
                                                                <br/>
                                                                4. Cek kembali nama anda dan jumlah pembayaran
                                                                <br/>
                                                                5. Ikuti langkahnya sampai pembayaran selesai
                                                                </p>
                                                            </Col>
                                                        </UncontrolledCollapse>
                                                    </Col>
                                                </Row>
                                                <Row>
                                                <Col style={{border:'solid 1px #bdbdbd'}}>
                                                        <Col><Link className="dashboard-menu" id="togglerCIMB" >Bank CIMB</Link></Col>
                                                        <UncontrolledCollapse toggler="#togglerCIMB">
                                                            <Col style={{backgroundColor:'#ffffff'}}>
                                                                <p>
                                                                1. Pilih Transfer Ke BCA Virtual Account
                                                                <br/>
                                                                2. Masukkan nomor virtual account dan klik correct
                                                                <br/>
                                                                3. Masukkan jumlah pembayaran lalu klik OK
                                                                <br/>
                                                                4. Cek kembali nama anda dan jumlah pembayaran
                                                                <br/>
                                                                5. Ikuti langkahnya sampai pembayaran selesai
                                                                </p>
                                                            </Col>
                                                        </UncontrolledCollapse>
                                                    </Col>
                                                </Row>
                                                <Row>
                                                <Col style={{border:'solid 1px #bdbdbd'}}>
                                                        <Col><Link className="dashboard-menu" id="togglerKospin" >Kospin</Link></Col>
                                                        <UncontrolledCollapse toggler="#togglerKospin">
                                                            <Row style={{backgroundColor:'#ffffff', border: 'solid 1px #bdbdbd'}}>
                                                                <Col>
                                                                <p>
                                                                1. Pilih Transfer Ke BCA Virtual Account
                                                                <br/>
                                                                2. Masukkan nomor virtual account dan klik correct
                                                                <br/>
                                                                3. Masukkan jumlah pembayaran lalu klik OK
                                                                <br/>
                                                                4. Cek kembali nama anda dan jumlah pembayaran
                                                                <br/>
                                                                5. Ikuti langkahnya sampai pembayaran selesai
                                                                </p>
                                                                </Col>
                                                            </Row>
                                                        </UncontrolledCollapse>
                                                    </Col>
                                                </Row>
                                            </Container>
                                    </Col>    
                            </Col>
                        </Row>
                    </Container>   
                    </div> 
                <ContentLandingBottom/>
            <Footer/>
        </Fragment>
        );
    }
}

export default PaymentDirect;