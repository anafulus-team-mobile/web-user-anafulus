import React                                                from 'react';

import { Link }                                             from 'react-router-dom';


import { Modal,  ModalHeader, ModalBody, 
         ModalFooter, Button }                              from 'reactstrap';

class ModalRegister extends React.Component {
   
    constructor(props) {
        super(props);
            this.state = {
            modal: false
        };
    
        this.toggle = this.toggle.bind(this);
    }

        toggle() {
        this.setState(prevState => ({
          modal: !prevState.modal
        }));
    }

    render(){
        const externalCloseBtn = <button className="close" style={{ position: 'absolute', top: '15px', right: '15px' }} onClick={this.toggle}>&times;</button>
        return(
            <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className} external={externalCloseBtn}>
                <ModalHeader>Registrasi Berhasil</ModalHeader>
                    <ModalBody>
                        <br />
                        <p>Selamat anda telah berhasil mendaftarkan akun anda.
                        Anda dapat mengajukan pinjaman untuk berbagai kebutuhan anda dengan melengkapi semua data.
                        </p>
                    </ModalBody>
                <ModalFooter>
                    <Link to="/Login"><Button id="btn-access">Masuk sekarang</Button></Link>{' '}
                    <Link to="/"><Button id="btn-cancel">Tidak</Button></Link>
                </ModalFooter>
            </Modal>
        );
    }
};    

export default ModalRegister; 