// Component, Page imported //
import React, { Fragment }                    from 'react';
import HeaderAccount                          from '../Component/HeaderAccount';
import SideBarProfile                         from '../Component/SidebarProfile';
import ContentLandingBottom                   from '../Component/ContentLandingBottom';
import Footer                                 from '../Component/Footer';

import { Link }                               from 'react-router-dom';

// Img, Icon,logo imported //
import EmptyState                             from '../Icon/return-to-the-past.png'


// Styling imported //
import { Container, Row, Col,
         Dropdown, DropdownToggle, 
         DropdownMenu, DropdownItem}          from 'reactstrap';
import '../Css/Reuse.css';
import '../Css/UserDashboard.css';


class HistoryEmpty extends React.Component{
    constructor(props) {
        super(props);
    
        this.toggle = this.toggle.bind(this);
        this.state = {
          dropdownOpen: false
        };
      }
    
      toggle() {
        this.setState({
          dropdownOpen: !this.state.dropdownOpen
        });
      }
    
    render() {
        return (
            <Fragment>
                <HeaderAccount/>
                <div>
                    <Container>
                        <Row>
                            {/* Component sidebar profile*/}
                            <Col sm="4">
                                <SideBarProfile/>
                            </Col>
                                {/* end */}
                                <Col sm="8" className="col-dashboard-user">
                                    <h3>Riwayat</h3><hr/>
                                    <Col>
                                        <Dropdown isOpen={this.state.dropdownOpen} toggle={this.toggle} >
                                            <DropdownToggle caret>
                                            Pinjaman Selesai
                                            </DropdownToggle>
                                            <DropdownMenu>
                                                <Link to="/History-Approve" ><DropdownItem >Pinjaman Disetujui</DropdownItem></Link>
                                                <Link to="/History-Decline"><DropdownItem>Pinjaman Ditolak</DropdownItem></Link>
                                                <Link to="/History-Pending"><DropdownItem>Pinjaman Belum Disetujui</DropdownItem></Link>
                                                <Link to="/History-Canceled"><DropdownItem>Pinjaman Dibatalkan</DropdownItem></Link>
                                                <Link to="/History-Ongoing"><DropdownItem>Cicilan Sedang Berjalan</DropdownItem></Link>
                                                <Link to="/History-Overdue"><DropdownItem>Cicilan Terlambat</DropdownItem></Link>
                                            </DropdownMenu>
                                        </Dropdown>
                                    </Col>
                                    <Container>
                                        <Col>
                                            <center>
                                                <img src={EmptyState} alt="empty-state" vspace="32px"/>
                                                <p>Tidak ada Riwayat untuk <br/><span>Pinjaman Disetujui</span></p>
                                            </center>
                                        </Col>
                                    </Container>
                                </Col>
                        </Row>
                    </Container>
                    </div>
                <ContentLandingBottom/>
            <Footer/>
            </Fragment>
        );
    }
}

export default HistoryEmpty;