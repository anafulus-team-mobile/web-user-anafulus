import React, { Fragment }                                      from 'react';
import Header                                                   from './Header';
import ContentLandingBottom                                     from './ContentLandingBottom';
import Footer                                                   from './Footer';
import ReactCodeInput                                           from 'react-code-input';

import { Link }                                                 from 'react-router-dom';


import { Card, Button, CardHeader, CardBody,
         CardTitle, Container, Col }                            from 'reactstrap';
import '../Css/Login.css';  
import '../Css/Reuse.css';  

class FormOTP extends React.Component {
    render(){
      return(
        <Fragment>
            <Header/>
                    <Container className="card-register">
                        <Col sm="10" md="5" className="mx-auto">
                            <Card>
                                <CardHeader className="card-header">
                                    <h5>Verifikasi Akun</h5>
                                </CardHeader>
                                <CardBody>
                                    <p>Silahkan verifikasi akun kamu dengan memasukkan 
                                    kode yang telah dikirimkan melalui SMS ke nomor:</p>
                                    <Col>
                                        <CardTitle id="label-num-phone">08123456789</CardTitle>
                                        <br />
                                        <ReactCodeInput type="number" fields={6} />
                                    </Col>
                                    <Col>
                                        <Link to="/LoginSuccess">
                                            <Button className="btn-green1 btn-block">Verifikasi</Button>
                                        </Link>
                                    </Col>
                                    <Col>
                                    <p>OTP kamu akan berakhir dalam waktu <br/><b>9 menit 43 detik</b></p>
                                    </Col>
                                    <Link to="">Kirim ulang kode verifikasi</Link>
                                </CardBody> 
                            </Card>
                        </Col>
                    </Container>
                <ContentLandingBottom/>
            <Footer/>
        </Fragment>
      );
    }
  };
  
  export default FormOTP;
      