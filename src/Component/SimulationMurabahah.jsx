import React                                    from 'react';

import { Link }                                 from 'react-router-dom';

import { Col, Form, FormGroup, Input,
         Label, Button, Container }             from 'reactstrap';
import '../Css/PinjamanMurabahah.css';


class SimulationMurabahah extends React.Component{
    render()    {
        return(
            <div>
                <Container>
                    <Col xs={{ size:6, offset:6}} sm="8" className="form-apply-pinjaman">
                        <Form>
                            <h5>Simulasi Pinjaman Murabahah</h5>
                                <FormGroup>
                                    <Label>Nominal Pinjaman</Label>
                                    <Input type="text" placeholder="Rp 500.000 - Rp 50.000.000" className="tenorPinjaman" value="Rp 5.000.000"/>
                                    <p id="note-nominal">*Jumlah pinjaman minimal Rp 500.000 dan 
                                    maksimal Rp 50.000.000</p>
                                            
                                <FormGroup>
                                    <Label for="tenorPinjaman">Tenor Pinjaman</Label>
                                    <Input type="select" name="select">
                                    <option>1 Minggu</option>
                                    <option>2 Minggu</option>
                                    <option>3 Minggu</option>
                                    <option>4 Minggu</option>
                                    <option>5 Minggu</option>
                                    <option>6 Minggu</option>
                                    <option>7 Minggu</option>
                                    <option>8 Minggu</option>
                                    <option>9 Minggu</option>
                                    <option>10 Minggu</option>
                                    <option>11 Minggu</option>
                                    <option>12 Minggu</option>
                                    <option>13 Minggu</option>
                                    <option>14 Minggu</option>
                                    <option>15 Minggu</option>
                                    <option>16 Minggu</option>
                                    </Input>
                                <FormGroup>
                                    <Label>Jenis cicilan</Label>
                                    <Input type="select" name="select" className="tenorPinjaman">
                                    <option>Mingguan</option>
                                    </Input>
                                </FormGroup>
                                    <div><center><Link><Button className="btn-green1 btn-block">Hitung</Button></Link></center></div>
                                </FormGroup>  

                                <FormGroup>
                                    <Label id="Title-Cicilan">Cicilan:</Label>
                                    <p id="Total-Cicilan">Rp 200.000 / Minggu</p>
                                    <Label for="alasanPeminjaman">Alasan Peminjaman*</Label>
                                    <Input type="number" min="10" max="12" placeholder="Masukkan alasan peminjaman anda"/>
                                    <center>
                                    <div>
                                    <Link to="/Apply-Murabahah">
                                        <Button className="btn-yellow1 btn-block">Ajukan Peminjaman</Button>
                                    </Link>
                                    </div>
                                    </center>
                                </FormGroup>
                                </FormGroup>
                            </Form>
                        </Col>
                    </Container>
                </div>
        );
    };
}

export default SimulationMurabahah;