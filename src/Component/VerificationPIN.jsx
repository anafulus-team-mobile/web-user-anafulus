import React, { Fragment }                              from 'react';
import Header                                           from './Header';
import ContentLandingBottom                             from './ContentLandingBottom';
import Footer                                           from './Footer';

import { Link }                                         from 'react-router-dom';

import { Card, Button, CardHeader, CardBody,
         CardTitle, Container, Col, Row, 
         Input, Label }                                        from 'reactstrap';

import '../Css/Login.css';  
  

class VerificationPIN extends React.Component {
    
    render(){
      return(
        <Fragment>
            <Header/>
                    <Container className="card-login">
                        <Col sm="12" md={{ size: 6, offset: 3 }}>
                            <Card>
                                <CardHeader>
                                    <h5>Reset PIN</h5>
                                    <Label>Silahkan masukkan PIN baru anda dengan 6 digit angka</Label>
                                </CardHeader>
                                <CardBody>
                                <Row>
                                    <Col>
                                        <CardTitle className="title-forgotPIN">PIN Baru</CardTitle>
                                        <Input type="number" id="input-box"/>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col>
                                        <CardTitle className="title-forgotPIN">Konfirmasi PIN</CardTitle>
                                        <Input type="text" id="input-box"/>
                                    </Col>
                                </Row>
                                <br/>
                                    <Col className="btn-card">
                                        <Link to="/Login"><Button id="btn-access">Reset PIN</Button></Link>
                                    </Col>
                                </CardBody> 
                            </Card>
                        </Col>
                    </Container>
                <ContentLandingBottom/>
            <Footer/>
        </Fragment>
      );
      
    }
    
  };
  
  export default VerificationPIN;

  
      