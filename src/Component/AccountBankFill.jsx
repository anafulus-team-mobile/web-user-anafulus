// Component, Page imported //
import React, { Fragment }                    from 'react';
import HeaderAccount                          from '../Component/HeaderAccount';
import SideBarProfile                         from '../Component/SidebarProfile';
import ContentLandingBottom                   from '../Component/ContentLandingBottom';
import Footer                                 from '../Component/Footer';

import { Link }                               from 'react-router-dom';

// Img, Icon,logo imported //
import LogoBCA                                from '../Logo/Bank/image-1.png';
import btnMore                                from '../Icon/more-1.png';


// Styling imported //
import { Container, Row, Col, Button, Label,
         Card, CardBody, UncontrolledDropdown,
         DropdownToggle, DropdownMenu, 
         DropdownItem, Modal, ModalHeader, 
         ModalBody, ModalFooter }             from 'reactstrap';
import '../Css/Reuse.css';
import '../Css/UserDashboard.css';


class AccountBank extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
          modal: false
        };

    
        this.toggle = this.toggle.bind(this);
      }
    
      toggle() {
        this.setState(prevState => ({
          modal: !prevState.modal
        }));
      }

    render() {
        return (
            <Fragment>
                <HeaderAccount/>
                    <div>
                    <Container>
                        <Row>
                             {/* Component sidebar profile*/}
                             <Col sm="4">
                                <SideBarProfile/>
                            </Col>
                            {/* end */}
                                <Col sm="8" className="col-dashboard-user">
                                    <h3>Rekening Bank</h3><hr/>
                                        <Container className="container-wrap2">
                                            <Row>
                                                <Col><Link to="/DetailAccount"><Button className="btn-data-complete" style={{float:'right'}}>Tambah Rekening</Button></Link></Col>
                                            </Row>
                                            <Container>
                                                <Row>
                                                 <Col xs="12">
                                                        <Card>
                                                            <CardBody style={{boxShadow:'0 3px 6px 0 rgba(0, 0, 0, 0.16)'}}>
                                                                <Row>
                                                                    <Col xs="4">
                                                                        <img src={LogoBCA} alt="Logo-BCA"/>
                                                                    </Col> 
                                                                    <Col>
                                                                        <Label>Pengguna</Label>
                                                                        <br/>
                                                                        <Label>08932273</Label>
                                                                        <br/>
                                                                        <Label>PT. Bank Central Asia / BCA, TBK</Label>
                                                                    </Col>
                                                                    <Col xs={{offset:1}}>
                                                                        <UncontrolledDropdown nav inNavbar>
                                                                            <DropdownToggle nav>
                                                                            <img src={btnMore} alt="btn-more"/>
                                                                            </DropdownToggle>
                                                                            <DropdownMenu right>
                                                                            <DropdownItem>
                                                                            <Link to="/Edit-Bank">Ubah</Link>
                                                                            </DropdownItem>
                                                                            <DropdownItem divider />
                                                                            <DropdownItem>
                                                                            <Link onClick={this.toggle}>{this.props.buttonLabel} Hapus</Link>
                                                                            </DropdownItem>
                                                                            </DropdownMenu>
                                                                        </UncontrolledDropdown>
                                                                        <Modal isOpen={this.state.modal} modalTransition={{ timeout: 500 }} backdropTransition={{ timeout: 1300 }}
                                                                               toggle={this.toggle} className={this.props.className}>
                                                                            <ModalHeader toggle={this.toggle}>Konfirmasi</ModalHeader>
                                                                            <ModalBody>
                                                                                Apakah anda yakin ingin menghapus rekening bank ini?
                                                                            </ModalBody>
                                                                            <ModalFooter>
                                                                                <Link to="/AccountBank"><Button color="primary" onClick={this.toggle}>Ya, hapus</Button></Link>{' '}
                                                                            <Button color="secondary" onClick={this.toggle}>Batalkan</Button>
                                                                            </ModalFooter>
                                                                        </Modal>
                                                                    </Col>
                                                                </Row>
                                                            </CardBody>
                                                        </Card>
                                                    </Col> 
                                            </Row>     
                                            </Container>
                                        </Container>
                                    </Col>
                                </Row>
                            </Container>
                            </div>
                <ContentLandingBottom/>
            <Footer/>
            </Fragment>
        );
    }
}

export default AccountBank;