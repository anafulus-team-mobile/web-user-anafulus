import React, { Fragment }                      from 'react';
import Header                                   from './Header';
import ContentLandingBottom                     from './ContentLandingBottom';
import Footer                                   from './Footer';

import { Link }                                 from 'react-router-dom';

import { Card, Button, CardHeader, CardBody,
         CardTitle, Container, Col, Input,
         Label }                                from 'reactstrap';
import '../Css/Login.css';  
  

class ChangePhoneNum extends React.Component {
    render(){
      return(
        <Fragment>
            <Header/>
                    <Container className="card-login">
                        <Col sm="12" md={{ size: 6, offset: 3 }}>
                            <Card>
                                <CardHeader className="card-header">
                                    <h5>Ganti Nomor Ponsel</h5>
                                </CardHeader>
                                <CardBody>
                                    <Col>
                                        <CardTitle className="title-forgotPIN" id="old-num-phone">Nomor Ponsel Lama</CardTitle>
                                        <Input type="number" min="10" max="12" id="input-box"/>
                                    </Col>
                                    <Col>
                                        <CardTitle className="title-forgotPIN" id="new-num-phone">Nomor Ponsel Baru</CardTitle>
                                        <Input type="number" min="10" max="12" id="input-box"/>
                                        <Label style={{color:'#229f89', fontSize:'14px'}}>
                                            *Kami akan mengirimkan kode verifikasi melalui SMS
                                        </Label>
                                    </Col>
                                    <Col>
                                        <CardTitle className="title-forgotPIN" id="num-idcard">Nomor KTP</CardTitle>
                                        <Input type="number" min="10" max="12" id="input-box"/>
                                    </Col>
                                    <Col>
                                        <CardTitle className="title-forgotPIN" id="num-kk">Nomor Kartu Keluarga</CardTitle>
                                        <Input type="number" min="10" max="12" id="input-box"/>
                                    </Col>
                                    <br/>
                                    <Col className="btn-card">
                                        <Link to="/Login"><Button id="btn-access">Ganti Nomor Ponsel</Button></Link>
                                    </Col>
                                </CardBody> 
                            </Card>
                        </Col>
                    </Container>
                <ContentLandingBottom/>
            <Footer/>
        </Fragment>
      );
    }
  };
  
  export default ChangePhoneNum;
      