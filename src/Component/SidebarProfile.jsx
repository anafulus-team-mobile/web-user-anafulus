// Component, Page imported //
import React, { Fragment }                    from 'react';

import { Link }                               from 'react-router-dom';

// Img, Icon,logo imported //
import PhotoUser                              from '../Icon/user-1.png';

// Styling imported //
import { Row, Col, Button, Label, 
         UncontrolledCollapse, Container }    from 'reactstrap';
import '../Css/UserDashboard.css';


class SidebarProfile extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
          modal: false
        };

        this.toggle = this.toggle.bind(this);
      }
    
      toggle() {
        this.setState(prevState => ({
          modal: !prevState.modal
        }));
      }

render() {
    return (
        <Fragment>
            <div>
            <Container>
                <Row>
                    <Col sm="10" className="col-profile-user">
                        <Col className="profile-user-top">
                            <img src={PhotoUser} alt="user-profile"/>
                            <h5>Jhon Doe</h5>  
                            <Label>Penilaian Anda:</Label>
                            <br/>
                            <Label className="score-level">Baik</Label>
                            <hr/>
                            <Label className="font-weight-bold">Saldo</Label> 
                            <br/>
                            <Label className="user-amount">Rp 2.000.000</Label>
                            <br/>
                            <Link to="/Amount"><Button className="btn-green1 btn-block">Tarik Dana</Button></Link>
                        </Col>
                        <hr/>
                        <Row className="dashboard-choose">
                            <Col>
                                <Link to="/Dashboard" className="dashboard-menu">Dashboard</Link>
                            </Col>
                        </Row>
                        <Row className="dashboard-choose">
                            <Col> 
                                <Link className="dashboard-menu" id="toggler" >Akun Saya</Link>
                            </Col>
                        </Row>
                            <UncontrolledCollapse toggler="#toggler">
                            <Col>
                                <Row className="dashboard-choose">
                                    <Col><Link to="/ChangeUserData"className="dashboard-menu">Ubah Data Diri</Link></Col>
                                </Row>
                                <Row className="dashboard-choose">
                                    <Col><Link to="/ChangeAddress" className="dashboard-menu">Ubah Alamat</Link></Col>
                                </Row>
                                <Row className="dashboard-choose">
                                    <Col><Link to="/ChangeAddData" className="dashboard-menu">Ubah Data Persyaratan</Link></Col>
                                </Row>
                                <Row className="dashboard-choose">
                                <Col><Link to="/AccountBank" className="dashboard-menu">Rekening Bank</Link></Col>
                                </Row>
                                <Row className="dashboard-choose">
                                    <Col><Link to="/Reset" className="dashboard-menu">Ubah PIN</Link></Col>
                                </Row>
                            </Col>
                            </UncontrolledCollapse>
                        <Row className="dashboard-choose">
                            <Col>
                                <Link to="/History-Empty" className="dashboard-menu">Riwayat</Link>
                            </Col>
                        </Row>
                        <Row className="dashboard-choose">
                            <Col>
                                <Link to="/Notif" className="dashboard-menu">Notifikasi</Link>
                            </Col>
                        </Row>
                        <Row className="dashboard-choose">
                            <Col>
                                <Link to="/" style={{color:'#f1545a'}}>Keluar</Link>
                            </Col>
                        </Row> 
                    </Col>
                    </Row>
            </Container>
            </div>
        </Fragment>
        );
    }
}
    
export default SidebarProfile;