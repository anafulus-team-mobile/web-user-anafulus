// Component, Page imported //
import React, { Component, Fragment }           from 'react';
import HeaderAccount                            from '../Component/HeaderAccount';
import ContentLandingBottom                     from '../Component/ContentLandingBottom';
import Footer                                   from '../Component/Footer';

import { Link }                                 from 'react-router-dom';

// Img, Icon, Logo Imported //
import IcoPhone                                 from '../Icon/smartphone-call-1.png';

// Styling imported //
import { Container, Row, Col, Label,
         FormGroup, Button, CardText }          from 'reactstrap';

class WithDraw extends Component{
    render() {
        return (
            <Fragment>
                <HeaderAccount/>
                <div>
                    <Container>
                        <Row>   
                            <Col sm={{size:6, offset:3}} className="col-dashboard-user">
                                    <h3>Konfirmasi Penarikan Dana</h3>
                                        <hr/>
                                    <Col className="container-wrap2"> 
                                    <center><Label style={{fontWeight:'bold', fontSize:'18px'}}>Jumlah Penarikan Dana</Label></center>
                                    <Col style={{border:'solid 1px #bdbdbd', paddingTop:'10px'}}>
                                        <Col>
                                            <FormGroup>
                                                <Label>Jumlah Penarikan Dana :</Label>{' '}
                                                <p>Rp 200.000.000</p>
                                                <CardText>Dana diperkirakan akan sampan ke rekening Bank anda  maksimal 7 hari kerja.</CardText>
                                            </FormGroup>
                                        </Col>
                                    </Col>
                                    <br/>
                                    <center><Label style={{fontWeight:'bold', fontSize:'18px'}}>Dana dikirimkan ke:</Label></center>
                                        <Col style={{border:'solid 1px #bdbdbd', paddingTop:'10px'}}>
                                            <Row>
                                                <Col xs="1">
                                                    <img src={IcoPhone} alt="IconPhone"/>
                                                </Col>
                                                <Col xs="1"></Col>
                                                <Col>
                                                    <Label>Rekening Ponsel</Label>
                                                    <br/>
                                                    <p> Jhon Doe</p>{' - '}
                                                    <p>0811229393838</p>                                                   
                                                </Col>                                            
                                            </Row>
                                        </Col>
                                    <Link to="/ConfirmationWithdrawPIN"><Button className="btn-green1 btn-block">Lanjutkan</Button></Link>
                                </Col>    
                            </Col>
                        </Row>
                    </Container>
                    </div>
                <ContentLandingBottom/>
            <Footer/>
        </Fragment>
        );
    }
}

export default WithDraw;