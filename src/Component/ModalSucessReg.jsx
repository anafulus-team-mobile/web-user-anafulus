import React, { Fragment }                            from 'react';
import Header                                         from './Header';
import ContentLandingBottom                           from './ContentLandingBottom';
import Footer                                         from './Footer';

import { Button, Modal, ModalHeader, 
         ModalBody, ModalFooter }                     from 'reactstrap';


class ModalSucessReg extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modal: false
    };

    this.toggle = this.toggle.bind(this);
  }

  toggle() {
    this.setState(prevState => ({
      modal: !prevState.modal
    }));
  }

  render() {
      const externalCloseBtn = <button className="close" style={{ position: 'absolute', top: '15px', right: '15px' }} onClick={this.toggle}>&times;</button>;
          return (
            <Fragment>
              <div>
                <Header/>
                <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className} external={externalCloseBtn}>
                  <ModalHeader>Registrasi Berhasil</ModalHeader>
                  <ModalBody>
                    <b>Selamat anda telah berhasil mendaftarkan akun anda.
                    Anda dapat mengajukan pinjaman untuk berbagai kebutuhan anda dengan melengkapi semua data.
                    </b>
                  </ModalBody>
                  <ModalFooter>
                    <Button color="primary" onClick={this.toggle}>Masuk sekarang</Button>{' '}
                    <Button color="secondary" onClick={this.toggle}>Tidak</Button>
                  </ModalFooter>
                </Modal>
              </div>
            <ContentLandingBottom/>
          <Footer/>
        </Fragment>
    );
  }
};

export default ModalSucessReg;