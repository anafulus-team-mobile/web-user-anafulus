// Component, Page imported //
import React, { Fragment }                    from 'react';
import HeaderAccount                          from '../Component/HeaderAccount';
import ContentLandingBottom                   from '../Component/ContentLandingBottom';
import Footer                                 from '../Component/Footer';

import { Link }                               from 'react-router-dom';

import IconAgentEmpty                         from '../Icon/agent-anafulus.png';

// Styling imported //
import { Container, Row, Col, Button,
         Label, Form, FormGroup, Input }      from 'reactstrap';
import '../Css/PinjamanMurabahah.css';


class AgentList extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
          modal: false
        };

    
        this.toggle = this.toggle.bind(this);
      }
    
      toggle() {
        this.setState(prevState => ({
          modal: !prevState.modal
        }));
      }

    render() {
        return (
            <Fragment>
                <HeaderAccount/>
                    <Container className="container">
                        <h3>Pinjaman Melalui Agent</h3><hr/>
                            <Row>
                                <Col sm="7" xs="12" >
                                <Col className="refferal-code">
                                <h5>Daftar Agen Terdekat</h5>
                                <Container>
                                    <Row>
                                        <Col xs="3">
                                                <Label for="provinsi-agen">Provinsi</Label>
                                                <br/>
                                                <Label for="kota-agen">Kota</Label>
                                                <br/>
                                                <Label for="kecamatan-agen">Kecamatan</Label>
                                            </Col>
                                            <Col>
                                            <Label for="provinsi-agen">{':'} DKI Jakarta</Label>
                                                <br/>
                                                <Label for="kota-agen">{':'} Jakarta Timur</Label>
                                                <br/>
                                                <Label for="kecamatan-agen">{':'} Anggrek</Label>
                                            </Col>
                                    </Row>
                                    <Row>
                                        <Col xs={{size:'3', offset:'1'}}>
                                            <Input type="radio" name="agen"/>
                                                <Label for="provinsi-agen">Kode Agen</Label>
                                                <br/>
                                                <Label for="kota-agen">Nama Agen</Label>
                                                <br/>
                                                <Label for="kecamatan-agen">Nomor Ponsel</Label>
                                        </Col>
                                        <Col>
                                            <Label for="kode-agen">{':'} 00001</Label>
                                                <br/>
                                                <Label for="nama-agen">{':'} Anita</Label>
                                                <br/>
                                                <Label for="nomor-ponsel-agen">{':'} 08110000001</Label>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col xs={{size:'3', offset:'1'}}>
                                            <Input type="radio" name="agen"/>
                                                <Label for="provinsi-agen">Kode Agen</Label>
                                                <br/>
                                                <Label for="kota-agen">Nama Agen</Label>
                                                <br/>
                                                <Label for="kecamatan-agen">Nomor Ponsel</Label>
                                        </Col>
                                        <Col>
                                            <Label for="kode-agen">{':'} 00002</Label>
                                                <br/>
                                                <Label for="nama-agen">{':'} Rita</Label>
                                                <br/>
                                                <Label for="nomor-ponsel-agen">{':'} 08120000001</Label>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col xs={{size:'3', offset:'1'}}>
                                            <Input type="radio" name="agen"/>
                                                <Label for="provinsi-agen">Kode Agen</Label>
                                                <br/>
                                                <Label for="kota-agen">Nama Agen</Label>
                                                <br/>
                                                <Label for="kecamatan-agen">Nomor Ponsel</Label>
                                        </Col>
                                        <Col>
                                            <Label for="kode-agen">{':'} 00003</Label>
                                                <br/>
                                                <Label for="nama-agen">{':'} Jaka</Label>
                                                <br/>
                                                <Label for="nomor-ponsel-agen">{':'} 08130000001</Label>
                                        </Col>
                                    </Row>
                                </Container>
                                </Col>

                                <Col className="refferal-code">
                                <h5>Daftar Agen Terdekat</h5>
                                <Container>
                                    <Row>
                                            <Col xs="3">
                                                <Label for="provinsi-agen">Provinsi</Label>
                                                <br/>
                                                <Label for="kota-agen">Kota</Label>
                                                <br/>
                                                <Label for="kecamatan-agen">Kecamatan</Label>
                                            </Col>
                                            <Col>
                                            <Label for="provinsi-agen">{':'} DKI Jakarta</Label>
                                                <br/>
                                                <Label for="kota-agen">{':'} Jakarta Timur</Label>
                                                <br/>
                                                <Label for="kecamatan-agen">{':'} Anggrek</Label>
                                            </Col>
                                    </Row>
                                    <Row>
                                        <Col>
                                        <center><img src={IconAgentEmpty} alt="AgentEmpty"/></center>
                                        <p>Belum ada agen di wilayah anda, silahkan hubungi kami
                                            untuk info lebih lanjut.</p>
                                            <center>
                                            <Link to="/ContactUs"><Button className="btn-green1">Hubungi Kami</Button></Link>   
                                            <br/>                               
                                            <Link to="/Via-Agent"><Button className="btn-green1">Kembali</Button></Link>
                                            </center>                           
                                        </Col>
                                    </Row>
                                </Container>
                                </Col>
                                
                                </Col>
                                    <Col sm={{size:'4', offset:'1'}} xs="12" >
                                        <Col className="apply-pinjaman-murabahah">
                                            <Form>
                                                <h5>Detail Pinjaman</h5>
                                                    <FormGroup>
                                                        <Label for="">Nominal Pinjaman</Label>
                                                        <p name="nominalPinjaman">Rp 5.000.000</p>
                                                        
                                                        <FormGroup>
                                                            <Label for="tenorPinjman">Tenor Pinjaman</Label>
                                                            <p name="tenorPinjaman">16 Minggu</p>
                                                        </FormGroup>

                                                        <FormGroup>
                                                            <Label for="jenisCicilan">Jenis cicilan</Label>
                                                            <p name="jenisCicilan">Murabahah</p>
                                                        </FormGroup>
                                                    
                                                    </FormGroup>  

                                                    <FormGroup>
                                                        <Label id="Title-Cicilan">Cicilan:</Label>
                                                        <p id="Total-Cicilan">Rp 200.000 / Minggu</p>
                                                    </FormGroup>
                                                <center><Link to="/Dashboard"><Button className="btn-green1">Lanjutkan</Button></Link></center>                                
                                            </Form>
                                        </Col> 
                                    </Col>
                            </Row>
                    </Container>
                <ContentLandingBottom/>
            <Footer/>
            </Fragment>
        );
    }
}

export default AgentList;