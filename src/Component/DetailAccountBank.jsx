// Component, Page imported //
import React, { Fragment }                    from 'react';
import HeaderAccount                          from '../Component/HeaderAccount';
import SideBarProfile                         from '../Component/SidebarProfile';
import ContentLandingBottom                   from '../Component/ContentLandingBottom';
import Footer                                 from '../Component/Footer';

import { Link }                               from 'react-router-dom';

// Img, Icon,logo imported //


// Styling imported //
import { Container, Row, Col, Button, 
         Label, Form, FormGroup,Input }       from 'reactstrap';
import '../Css/Reuse.css';
import '../Css/UserDashboard.css';


class DetailAccountBank extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
          modal: false
        };

    
        this.toggle = this.toggle.bind(this);
      }
    
      toggle() {
        this.setState(prevState => ({
          modal: !prevState.modal
        }));
      }

    render() {
        return (
            <Fragment>
                <HeaderAccount/>
                    <div>
                    <Container>
                        <Row>
                            {/* Component sidebar profile*/}
                            <Col sm="4">
                                <SideBarProfile/>
                            </Col>
                                {/* end */}
                                <Col sm="8" className="col-dashboard-user">
                                    <h3>Rekening Bank</h3><hr/>
                                        <Container className="container-wrap2"> 
                                        <Row>
                                            <Col xs={{size:8, offset:2}}>
                                                <Form>
                                                    <FormGroup>
                                                        <Label className="title-data">Nama Akun</Label>
                                                        <Input type="text" id="input-box"></Input>
                                                    </FormGroup>
                                                    <FormGroup>
                                                        <Label className="title-data">Nomor Rekening</Label>
                                                        <Input type="number" id="input-box"></Input>
                                                    </FormGroup>
                                                    <FormGroup>
                                                        <Label className="title-data">Nama Bank</Label>
                                                        <Input type="text" id="input-box"></Input>
                                                    </FormGroup>
                                                    <FormGroup>
                                                        <Label className="title-data">Cabang</Label>
                                                        <Input type="text" id="input-box"></Input>
                                                    </FormGroup>
                                                    <p>* Isilah Cabang dengan nama kantor cabang beserta kota dimana bank tersebut tersedia. Contoh : Matraman-Jakarta Timur</p>
                                                    <center><Link to="BankList"><Button className="btn-data-complete">Simpan</Button></Link></center>
                                                </Form>
                                            </Col>
                                        </Row>
                                        </Container>
                                    </Col>
                                </Row>
                            </Container>
                            </div>
                <ContentLandingBottom/>
            <Footer/>
            </Fragment>
        );
    }
}

export default DetailAccountBank;